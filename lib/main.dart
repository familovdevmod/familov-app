// @dart=2.9
import 'package:bot_toast/bot_toast.dart';
import 'package:country_code_picker/country_localizations.dart';
import 'package:familov/screen/changePasswordScreen.dart';
import 'package:familov/screen/forgetPasswordScreen.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/screen/infoScreen.dart';
import 'package:familov/screen/languageScreen.dart';
import 'package:familov/screen/loginScreen.dart';
import 'package:familov/screen/PartnerScreen.dart';
import 'package:familov/screen/Transaction_Invoice/mobile_invoiceScreen.dart';
import 'package:familov/screen/Transaction_Invoice/shop_invoiceScreen.dart';
import 'package:familov/screen/beneficiaryAddScreen.dart';
import 'package:familov/screen/beneficiaryScreen.dart';
import 'package:familov/screen/beneficiaryUpdateScreen.dart';
import 'package:familov/screen/deliveryNote1.dart';
import 'package:familov/screen/forgetPassword2.dart';
import 'package:familov/screen/friendScreen.dart';
import 'package:familov/screen/goShoppingScreen.dart';
import 'package:familov/screen/hamburgerScreen.dart';
import 'package:familov/screen/myDetailsScreen.dart';
import 'package:familov/screen/otpScreen.dart';
import 'package:familov/screen/splash.dart';
import 'package:familov/screen/storePage.dart.dart';
import 'package:familov/screen/transactionScreen.dart';
import 'package:familov/widget/locale_provider.dart';
import 'package:familov/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

int isviewed;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Firebase.initializeApp();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  isviewed = prefs.getInt(
    'onBoard',
  );
  runApp(MyApp());
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0xffDA3C5F),
    statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.light,
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => LocaleProvider(),
        builder: (context, child) {
          final provider = Provider.of<LocaleProvider>(context);
          final provider1 = Provider.of<LocaleProvider>(context);
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            localizationsDelegates: const [
              CountryLocalizations.delegate,
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            builder: BotToastInit(), //1. call BotToastInit.
            navigatorObservers: [
              BotToastNavigatorObserver()
            ], //2. registered route observer
            supportedLocales: L10n.all,
            title: 'Familov',
            theme: ThemeData(
                fontFamily: 'Gilroy',
                primaryColor: Colors.white,
                colorScheme: Theme.of(context)
                    .colorScheme
                    .copyWith(secondary: Color(0xffDA3C5F))),
            locale: provider.locale,
            initialRoute: '/SpashScreen',
            routes: {
              '/SpashScreen': (BuildContext context) => SpashScreen(),
              '/loginScreen': (BuildContext context) => LoginScreen(
                    samePage: '',
                  ),
              '/InfoScreen': (BuildContext context) => InfoScreen(),
              '/LanguageScreen': (BuildContext context) => LanguageScreen(),
              '/ForgetScreen': (BuildContext context) => ForgetPassword(),
              '/HomeScreen': (BuildContext context) => HomeScreen(0, ''),
              '/ChangePasswordScreen': (BuildContext context) =>
                  ChangePasswordScreen(),
              '/DetailScreen': (BuildContext context) => MyDetailScreen(),
              '/Friend': (BuildContext context) => FriendScreen(),
              '/Partner': (BuildContext context) => PartnerScreen(),
              '/Hamburger': (BuildContext context) => Handburger(),
              '/TransactionScreen': (context) => TransactionScreen(),
              '/MobileInvoiceScreen': (context) => MobileInvoiceScreen(),
              '/ShopInvoiceScreen': (context) => ShopInvoiceScreen(),
              '/DeliveryNoteScreen': (context) => DeliveryNote(),
              '/BeneficiaryScreen': (context) => Beneficiary(),
              '/BeneficiaryAddScreen': (context) => BeneficiaryAddScreen(''),
              '/BeneficiaryUpdateScreen': (context) =>
                  BeneficiaryUpdateScreen(),
              '/GoShoppingScreen': (context) => GoShopping(),
              '/StorePage': (context) => Stores(),
              '/Otp': (BuildContext context) => Otp(),
              '/Forget2': ((context) => ForgetPassword2())
            },
          );
        });
  }
}
