import 'dart:convert';

class HomeModel {
  HomeModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  HomeModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    Shop_details? shopDetails,
    List<Sort_options>? sortOptions,
    Shop_categories? shopCategories,
  }) {
    _shopDetails = shopDetails;
    _sortOptions = sortOptions;
    _shopCategories = shopCategories;
  }

  Data.fromJson(dynamic json) {
    _shopDetails = json['shop_details'] != null
        ? Shop_details.fromJson(json['shop_details'])
        : null;
    if (json['sort_options'] != null) {
      _sortOptions = [];
      json['sort_options'].forEach((v) {
        _sortOptions?.add(Sort_options.fromJson(v));
      });
    }
    _shopCategories = json['shop_categories'] != null
        ? Shop_categories.fromJson(json['shop_categories'])
        : null;
  }
  Shop_details? _shopDetails;
  List<Sort_options>? _sortOptions;
  Shop_categories? _shopCategories;

  Shop_details? get shopDetails => _shopDetails;
  List<Sort_options>? get sortOptions => _sortOptions;
  Shop_categories? get shopCategories => _shopCategories;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_shopDetails != null) {
      map['shop_details'] = _shopDetails?.toJson();
    }
    if (_sortOptions != null) {
      map['sort_options'] = _sortOptions?.map((v) => v.toJson()).toList();
    }
    if (_shopCategories != null) {
      map['shop_categories'] = _shopCategories?.toJson();
    }
    return map;
  }
}

// ignore: camel_case_types
class Shop_categories {
  Shop_categories({
    int? count,
    List<The_Data_list>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Shop_categories.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(The_Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  List<The_Data_list>? _dataList;

  int? get count => _count;
  List<The_Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

// ignore: camel_case_types
class The_Data_list {
  The_Data_list({
    String? categoryId,
    String? mainCategoryId,
    String? categoryName,
    String? categoryNameEn,
    String? customerId,
    String? categoryImage,
    String? categoryStatus,
    String? productId,
    String? shopId,
    dynamic managerId,
    String? productName,
    String? productImage,
    String? productPriceCurrencyId,
    String? isProposedByManager,
    String? productPrices,
    dynamic productPriceCurrencyIdManager,
    dynamic productPricesManager,
    String? specialProductPrices,
    String? productStatus,
    String? productAvailability,
    String? isRead,
    dynamic createdAt,
    dynamic updatedAt,
    dynamic productUniqueId,
    String? isMarkedTop,
  }) {
    _categoryId = categoryId;
    _mainCategoryId = mainCategoryId;
    _categoryName = categoryName;
    _categoryNameEn = categoryNameEn;
    _customerId = customerId;
    _categoryImage = categoryImage;
    _categoryStatus = categoryStatus;
    _productId = productId;
    _shopId = shopId;
    _managerId = managerId;
    _productName = productName;
    _productImage = productImage;
    _productPriceCurrencyId = productPriceCurrencyId;
    _isProposedByManager = isProposedByManager;
    _productPrices = productPrices;
    _productPriceCurrencyIdManager = productPriceCurrencyIdManager;
    _productPricesManager = productPricesManager;
    _specialProductPrices = specialProductPrices;
    _productStatus = productStatus;
    _productAvailability = productAvailability;
    _isRead = isRead;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _productUniqueId = productUniqueId;
    _isMarkedTop = isMarkedTop;
  }

  The_Data_list.fromJson(dynamic json) {
    _categoryId = json['category_id'];
    _mainCategoryId = json['main_category_id'];
    _categoryName = json['category_name'];
    _categoryNameEn = json['category_name_en'];
    _customerId = json['customer_id'];
    _categoryImage = json['category_image'];
    _categoryStatus = json['category_status'];
    _productId = json['product_id'];
    _shopId = json['shop_id'];
    _managerId = json['manager_id'];
    _productName = json['product_name'];
    _productImage = json['product_image'];
    _productPriceCurrencyId = json['product_price_currency_id'];
    _isProposedByManager = json['is_proposed_by_manager'];
    _productPrices = json['product_prices'];
    _productPriceCurrencyIdManager = json['product_price_currency_id_manager'];
    _productPricesManager = json['product_prices_manager'];
    _specialProductPrices = json['special_product_prices'];
    _productStatus = json['product_status'];
    _productAvailability = json['product_availability'];
    _isRead = json['is_read'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _productUniqueId = json['product_unique_id'];
    _isMarkedTop = json['is_marked_top'];
  }
  String? _categoryId;
  String? _mainCategoryId;
  String? _categoryName;
  String? _categoryNameEn;
  String? _customerId;
  String? _categoryImage;
  String? _categoryStatus;
  String? _productId;
  String? _shopId;
  dynamic _managerId;
  String? _productName;
  String? _productImage;
  String? _productPriceCurrencyId;
  String? _isProposedByManager;
  String? _productPrices;
  dynamic _productPriceCurrencyIdManager;
  dynamic _productPricesManager;
  String? _specialProductPrices;
  String? _productStatus;
  String? _productAvailability;
  String? _isRead;
  dynamic _createdAt;
  dynamic _updatedAt;
  dynamic _productUniqueId;
  String? _isMarkedTop;

  String? get categoryId => _categoryId;
  String? get mainCategoryId => _mainCategoryId;
  String? get categoryName => _categoryName;
  String? get categoryNameEn => _categoryNameEn;
  String? get customerId => _customerId;
  String? get categoryImage => _categoryImage;
  String? get categoryStatus => _categoryStatus;
  String? get productId => _productId;
  String? get shopId => _shopId;
  dynamic get managerId => _managerId;
  String? get productName => _productName;
  String? get productImage => _productImage;
  String? get productPriceCurrencyId => _productPriceCurrencyId;
  String? get isProposedByManager => _isProposedByManager;
  String? get productPrices => _productPrices;
  dynamic get productPriceCurrencyIdManager => _productPriceCurrencyIdManager;
  dynamic get productPricesManager => _productPricesManager;
  String? get specialProductPrices => _specialProductPrices;
  String? get productStatus => _productStatus;
  String? get productAvailability => _productAvailability;
  String? get isRead => _isRead;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  dynamic get productUniqueId => _productUniqueId;
  String? get isMarkedTop => _isMarkedTop;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['category_id'] = _categoryId;
    map['main_category_id'] = _mainCategoryId;
    map['category_name'] = _categoryName;
    map['category_name_en'] = _categoryNameEn;
    map['customer_id'] = _customerId;
    map['category_image'] = _categoryImage;
    map['category_status'] = _categoryStatus;
    map['product_id'] = _productId;
    map['shop_id'] = _shopId;
    map['manager_id'] = _managerId;
    map['product_name'] = _productName;
    map['product_image'] = _productImage;
    map['product_price_currency_id'] = _productPriceCurrencyId;
    map['is_proposed_by_manager'] = _isProposedByManager;
    map['product_prices'] = _productPrices;
    map['product_price_currency_id_manager'] = _productPriceCurrencyIdManager;
    map['product_prices_manager'] = _productPricesManager;
    map['special_product_prices'] = _specialProductPrices;
    map['product_status'] = _productStatus;
    map['product_availability'] = _productAvailability;
    map['is_read'] = _isRead;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['product_unique_id'] = _productUniqueId;
    map['is_marked_top'] = _isMarkedTop;
    return map;
  }
}

// ignore: camel_case_types
class Sort_options {
  Sort_options({
    int? id,
    String? sortBy,
    String? sortOrder,
    String? label,
  }) {
    _id = id;
    _sortBy = sortBy;
    _sortOrder = sortOrder;
    _label = label;
  }

  Sort_options.fromJson(dynamic json) {
    _id = json['id'];
    _sortBy = json['sort_by'];
    _sortOrder = json['sort_order'];
    _label = json['label'];
  }
  int? _id;
  String? _sortBy;
  String? _sortOrder;
  String? _label;

  int? get id => _id;
  String? get sortBy => _sortBy;
  String? get sortOrder => _sortOrder;
  String? get label => _label;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['sort_by'] = _sortBy;
    map['sort_order'] = _sortOrder;
    map['label'] = _label;
    return map;
  }
}

// ignore: camel_case_types
class Shop_details {
  Shop_details({
    String? shopId,
    String? slug,
    String? shopName,
    String? shopAddress,
    String? shopLogo,
    String? shopBanner,
    String? cityId,
    String? countryId,
    dynamic minimumOrderAmount,
    String? minAmountPickup,
    String? pickupFromShopPrice,
    String? homeDeliveryPrice,
    String? homeDeliveryStatus,
    String? shopStatus,
    String? pickupFromShopStatus,
    String? parentShopId,
  }) {
    _shopId = shopId;
    _slug = slug;
    _shopName = shopName;
    _shopAddress = shopAddress;
    _shopLogo = shopLogo;
    _shopBanner = shopBanner;
    _cityId = cityId;
    _countryId = countryId;
    _minimumOrderAmount = minimumOrderAmount;
    _minAmountPickup = minAmountPickup;
    _pickupFromShopPrice = pickupFromShopPrice;
    _homeDeliveryPrice = homeDeliveryPrice;
    _homeDeliveryStatus = homeDeliveryStatus;
    _shopStatus = shopStatus;
    _pickupFromShopStatus = pickupFromShopStatus;
    _parentShopId = parentShopId;
  }

  Shop_details.fromJson(dynamic json) {
    _shopId = json['shop_id'];
    print('Id is : $_shopId');

    _slug = json['slug'];
    _shopName = json['shop_name'];
    _shopAddress = json['shop_address'];
    _shopLogo = json['shop_logo'];
    _shopBanner = json['shop_banner'];
    _cityId = json['city_id'];
    _countryId = json['country_id'];
    _minimumOrderAmount = json['minimum_order_amount'];
    _minAmountPickup = json['min_amount_pickup'];
    _pickupFromShopPrice = json['pickup_from_shop_price'];
    _homeDeliveryPrice = json['home_delivery_price'];
    _homeDeliveryStatus = json['home_delivery_status'];
    _shopStatus = json['shop_status'];
    _pickupFromShopStatus = json['pickup_from_shop_status'];
    _parentShopId = json['parent_shop_id'];
  }
  String? _shopId;
  String? _slug;
  String? _shopName;
  String? _shopAddress;
  String? _shopLogo;
  String? _shopBanner;
  String? _cityId;
  String? _countryId;
  dynamic _minimumOrderAmount;
  String? _minAmountPickup;
  String? _pickupFromShopPrice;
  String? _homeDeliveryPrice;
  String? _homeDeliveryStatus;
  String? _shopStatus;
  String? _pickupFromShopStatus;
  String? _parentShopId;

  String? get shopId => _shopId;
  String? get slug => _slug;
  String? get shopName => _shopName;
  String? get shopAddress => _shopAddress;
  String? get shopLogo => _shopLogo;
  String? get shopBanner => _shopBanner;
  String? get cityId => _cityId;
  String? get countryId => _countryId;
  dynamic get minimumOrderAmount => _minimumOrderAmount;
  String? get minAmountPickup => _minAmountPickup;
  String? get pickupFromShopPrice => _pickupFromShopPrice;
  String? get homeDeliveryPrice => _homeDeliveryPrice;
  String? get homeDeliveryStatus => _homeDeliveryStatus;
  String? get shopStatus => _shopStatus;
  String? get pickupFromShopStatus => _pickupFromShopStatus;
  String? get parentShopId => _parentShopId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shop_id'] = _shopId;
    map['slug'] = _slug;
    map['shop_name'] = _shopName;
    map['shop_address'] = _shopAddress;
    map['shop_logo'] = _shopLogo;
    map['shop_banner'] = _shopBanner;
    map['city_id'] = _cityId;
    map['country_id'] = _countryId;
    map['minimum_order_amount'] = _minimumOrderAmount;
    map['min_amount_pickup'] = _minAmountPickup;
    map['pickup_from_shop_price'] = _pickupFromShopPrice;
    map['home_delivery_price'] = _homeDeliveryPrice;
    map['home_delivery_status'] = _homeDeliveryStatus;
    map['shop_status'] = _shopStatus;
    map['pickup_from_shop_status'] = _pickupFromShopStatus;
    map['parent_shop_id'] = _parentShopId;
    return map;
  }
}
