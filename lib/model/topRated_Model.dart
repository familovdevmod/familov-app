import 'dart:convert';

class TopProduct {
  TopProduct({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  TopProduct.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<Data_lists>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_lists.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_lists>? _dataList;

  int? get count => _count;
  List<Data_lists>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data_lists {
  Data_lists(
      {String? productId,
      String? productName,
      String? productPrices,
      String? productStatus,
      String? productImage,
      String? productAvailability,
      String? shopName,
      String? categoryName,
      String? specialproductprices}) {
    //special_product_prices
    _productId = productId;
    _productName = productName;
    _productPrices = productPrices;
    _productStatus = productStatus;
    _productImage = productImage;
    _productAvailability = productAvailability;
    _shopName = shopName;
    _categoryName = categoryName;
    _specialproductprices = specialproductprices;
  }

  Data_lists.fromJson(dynamic json) {
    _productId = json['product_id'];
    _productName = json['product_name'];
    _productPrices = json['product_prices'];
    _productStatus = json['product_status'];
    _productImage = json['product_image'];
    _productAvailability = json['product_availability'];
    _shopName = json['shop_name'];
    _categoryName = json['category_name'];
    _specialproductprices = json['special_product_prices'];
  }
  String? _productId;
  String? _productName;
  String? _productPrices;
  String? _productStatus;
  String? _productImage;
  String? _productAvailability;
  String? _shopName;
  String? _categoryName;
  String? _specialproductprices;

  String? get productId => _productId;
  String? get productName => _productName;
  String? get productPrices => _productPrices;
  String? get productStatus => _productStatus;
  String? get productImage => _productImage;
  String? get productAvailability => _productAvailability;
  String? get shopName => _shopName;
  String? get categoryName => _categoryName;
  String? get specialproductprices => _specialproductprices;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['product_id'] = _productId;
    map['product_name'] = _productName;
    map['product_prices'] = _productPrices;
    map['product_status'] = _productStatus;
    map['product_image'] = _productImage;
    map['product_availability'] = _productAvailability;
    map['shop_name'] = _shopName;
    map['category_name'] = _categoryName;
    map['special_product_prices'] = _specialproductprices;

    return map;
  }
}
