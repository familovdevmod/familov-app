import 'dart:convert';

class Stripecheckout {
  Stripecheckout({
    bool? status,
    String? message,
    Data66? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Stripecheckout.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data66.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data66? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;
  Stripecheckout copyWith({
    bool? status,
    String? message,
    Data66? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) =>
      Stripecheckout(
        status: status ?? _status,
        message: message ?? _message,
        data: data ?? _data,
        errorCode: errorCode ?? _errorCode,
        errorMsg: errorMsg ?? _errorMsg,
        meta: meta ?? _meta,
      );
  bool? get status => _status;
  String? get message => _message;
  Data66? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data66 {
  Data66({
    String? checkoutUrl,
    Next? next,
    String? pgTransactionId,
    String? pmSlug,
    String? totalAmount,
    String? promoCode,
    String? deliveryMode,
    Summary? summary,
    OrderData? orderData,
  }) {
    _checkoutUrl = checkoutUrl;
    _next = next;
    _pgTransactionId = pgTransactionId;
    _pmSlug = pmSlug;
    _totalAmount = totalAmount;
    _promoCode = promoCode;
    _deliveryMode = deliveryMode;
    _summary = summary;
    _orderData = orderData;
  }

  Data66.fromJson(dynamic json) {
    _checkoutUrl = json['checkout_url'];
    _next = json['next'] != null ? Next.fromJson(json['next']) : null;
    _pgTransactionId = json['pg_transaction_id'];
    _pmSlug = json['pm_slug'];
    _totalAmount = json['total_amount'];
    _promoCode = json['promo_code'];
    _deliveryMode = json['delivery_mode'];
    _summary =
        json['summary'] != null ? Summary.fromJson(json['summary']) : null;
    _orderData = json['order_data'] != null
        ? OrderData.fromJson(json['order_data'])
        : null;
  }
  String? _checkoutUrl;
  Next? _next;
  String? _pgTransactionId;
  String? _pmSlug;
  String? _totalAmount;
  String? _promoCode;
  String? _deliveryMode;
  Summary? _summary;
  OrderData? _orderData;
  Data66 copyWith({
    String? checkoutUrl,
    Next? next,
    String? pgTransactionId,
    String? pmSlug,
    String? totalAmount,
    String? promoCode,
    String? deliveryMode,
    Summary? summary,
    OrderData? orderData,
  }) =>
      Data66(
        checkoutUrl: checkoutUrl ?? _checkoutUrl,
        next: next ?? _next,
        pgTransactionId: pgTransactionId ?? _pgTransactionId,
        pmSlug: pmSlug ?? _pmSlug,
        totalAmount: totalAmount ?? _totalAmount,
        promoCode: promoCode ?? _promoCode,
        deliveryMode: deliveryMode ?? _deliveryMode,
        summary: summary ?? _summary,
        orderData: orderData ?? _orderData,
      );
  String? get checkoutUrl => _checkoutUrl;
  Next? get next => _next;
  String? get pgTransactionId => _pgTransactionId;
  String? get pmSlug => _pmSlug;
  String? get totalAmount => _totalAmount;
  String? get promoCode => _promoCode;
  String? get deliveryMode => _deliveryMode;
  Summary? get summary => _summary;
  OrderData? get orderData => _orderData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['checkout_url'] = _checkoutUrl;
    if (_next != null) {
      map['next'] = _next?.toJson();
    }
    map['pg_transaction_id'] = _pgTransactionId;
    map['pm_slug'] = _pmSlug;
    map['total_amount'] = _totalAmount;
    map['promo_code'] = _promoCode;
    map['delivery_mode'] = _deliveryMode;
    if (_summary != null) {
      map['summary'] = _summary?.toJson();
    }
    if (_orderData != null) {
      map['order_data'] = _orderData?.toJson();
    }
    return map;
  }
}

class OrderData {
  OrderData({
    int? orderId,
    String? genCode,
  }) {
    _orderId = orderId;
    _genCode = genCode;
  }

  OrderData.fromJson(dynamic json) {
    _orderId = json['order_id'];
    _genCode = json['gen_code'];
  }
  int? _orderId;
  String? _genCode;
  OrderData copyWith({
    int? orderId,
    String? genCode,
  }) =>
      OrderData(
        orderId: orderId ?? _orderId,
        genCode: genCode ?? _genCode,
      );
  int? get orderId => _orderId;
  String? get genCode => _genCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['order_id'] = _orderId;
    map['gen_code'] = _genCode;
    return map;
  }
}

class Summary {
  Summary({
    String? totalAmount,
    String? subTotal,
    String? serviceCharge,
    String? deliveryCharge,
    String? promoDiscount,
  }) {
    _totalAmount = totalAmount;
    _subTotal = subTotal;
    _serviceCharge = serviceCharge;
    _deliveryCharge = deliveryCharge;
    _promoDiscount = promoDiscount;
  }

  Summary.fromJson(dynamic json) {
    _totalAmount = json['total_amount'];
    _subTotal = json['sub_total'];
    _serviceCharge = json['service_charge'];
    _deliveryCharge = json['delivery_charge'];
    _promoDiscount = json['promo_discount'];
  }
  String? _totalAmount;
  String? _subTotal;
  String? _serviceCharge;
  String? _deliveryCharge;
  String? _promoDiscount;
  Summary copyWith({
    String? totalAmount,
    String? subTotal,
    String? serviceCharge,
    String? deliveryCharge,
    String? promoDiscount,
  }) =>
      Summary(
        totalAmount: totalAmount ?? _totalAmount,
        subTotal: subTotal ?? _subTotal,
        serviceCharge: serviceCharge ?? _serviceCharge,
        deliveryCharge: deliveryCharge ?? _deliveryCharge,
        promoDiscount: promoDiscount ?? _promoDiscount,
      );
  String? get totalAmount => _totalAmount;
  String? get subTotal => _subTotal;
  String? get serviceCharge => _serviceCharge;
  String? get deliveryCharge => _deliveryCharge;
  String? get promoDiscount => _promoDiscount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total_amount'] = _totalAmount;
    map['sub_total'] = _subTotal;
    map['service_charge'] = _serviceCharge;
    map['delivery_charge'] = _deliveryCharge;
    map['promo_discount'] = _promoDiscount;
    return map;
  }
}

class Next {
  Next({
    String? action,
    String? url,
  }) {
    _action = action;
    _url = url;
  }

  Next.fromJson(dynamic json) {
    _action = json['action'];
    _url = json['url'];
  }
  String? _action;
  String? _url;
  Next copyWith({
    String? action,
    String? url,
  }) =>
      Next(
        action: action ?? _action,
        url: url ?? _url,
      );
  String? get action => _action;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['action'] = _action;
    map['url'] = _url;
    return map;
  }
}
