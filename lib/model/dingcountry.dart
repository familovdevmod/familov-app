import 'dart:convert';

class Dingcountry {
  Dingcountry({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Dingcountry.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<DataList>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(DataList.fromJson(v));
      });
    }
  }
  int? _count;
  List<DataList>? _dataList;

  int? get count => _count;
  List<DataList>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class DataList {
  DataList({
    String? countryIso,
    String? countryName,
    String? prefix,
    String? minimumLength,
    String? maximumLength,
    String? regionCodes,
  }) {
    _countryIso = countryIso;
    _countryName = countryName;
    _prefix = prefix;
    _minimumLength = minimumLength;
    _maximumLength = maximumLength;
    _regionCodes = regionCodes;
  }

  DataList.fromJson(dynamic json) {
    _countryIso = json['CountryIso'];
    _countryName = json['CountryName'];
    _prefix = json['Prefix'];
    _minimumLength = json['MinimumLength'];
    _maximumLength = json['MaximumLength'];
    _regionCodes = json['RegionCodes'];
  }
  String? _countryIso;
  String? _countryName;
  String? _prefix;
  String? _minimumLength;
  String? _maximumLength;
  String? _regionCodes;

  String? get countryIso => _countryIso;
  String? get countryName => _countryName;
  String? get prefix => _prefix;
  String? get minimumLength => _minimumLength;
  String? get maximumLength => _maximumLength;
  String? get regionCodes => _regionCodes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['CountryIso'] = _countryIso;
    map['CountryName'] = _countryName;
    map['Prefix'] = _prefix;
    map['MinimumLength'] = _minimumLength;
    map['MaximumLength'] = _maximumLength;
    map['RegionCodes'] = _regionCodes;
    return map;
  }
}
