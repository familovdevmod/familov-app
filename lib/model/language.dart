import 'dart:convert';

class Language {
  Language({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Language.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    DataList? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
  //  _dataList = json['data_list'];
  _dataList = json['data_list'] != null ? DataList.fromJson(json['data_list']) : null;
  }
  int? _count;
  DataList? _dataList;

  int? get count => _count;
  DataList? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['data_list'] = _dataList;
    return map;
  }
}

class DataList {
  DataList({
    String? lblmenu01,
    String? lblmenu02,
    String? lblmenu021,
    String? lblmenu03,
    String? lblmenu04,
    String? lblmenu05,
    String? lblmenu06,
    String? lblmenu07,
    String? lblmenu08,
    String? lblmenu09,
    String? lblmenu10,
    String? lblmenu11,
    String? lblmenu12,
    String? lblmenu13,
    String? lblmenu14,
    String? lblmenu15,
    String? lblmenu16,
    String? lblmenu17,
    String? lblmenu18,
    String? lblmenu19,
    String? lblmenu20,
    String? lblmenu21,
    String? lblmenu22,
    String? lblmenu23,
    String? lblmenu24,
    String? lblmenu25,
    String? lblsocialfacebook,
    String? lblsocialtwitter,
    String? lblfooter01,
    String? lblfooter02,
    String? welcomeMessage,
    String? lblherotwo,
    String? lblherothree,
    String? lblreceivercountry,
    String? lblreceivercity,
    String? lblchooseshop,
    String? lblreceivergo,
    String? lblsearchvalidationcountry,
    String? lblsearchvalidationcity,
    String? lblsearchvalidationshop,
    String? lblsearchcitylist,
    String? lblsearchshoplist,
    String? lblreceiverstarted,
    String? lblhome01,
    String? lblhome02,
    String? lblhome03,
    String? lblhome04,
    String? lblhome05,
    String? lblhome06,
    String? lblhome07,
    String? lblhome08,
    String? lblhome09,
    String? lblhome10,
    String? lblhome11,
    String? lblhome12,
    String? lblhome13,
    String? lblhome14,
    String? lblhome15,
    String? lblhome16,
    String? lblhowitworks01,
    String? lblhowitworks02,
    String? lblhowitworks03,
    String? lblhowitworks04,
    String? lblhowitworks05,
    String? lblhowitworks06,
    String? lblhowitworks07,
    String? lblhowitworks08,
    String? lblhowitworks09,
    String? lblhowitworks10,
    String? lblhowitworks11,
    String? lbltestimonial01,
    String? lbltestimonial02,
    String? lbltestimonialperson101,
    String? lbltestimonialperson102,
    String? lbltestimonialperson103,
    String? lbltestimonialperson201,
    String? lbltestimonialperson202,
    String? lbltestimonialperson203,
    String? lbltestimonialperson301,
    String? lbltestimonialperson302,
    String? lbltestimonialperson303,
    String? pagetitlefaq,
    String? lblfaq01,
    String? lblfaq02,
    String? lblfaq03,
    String? pagetitlehiw,
    String? lblhiw01,
    String? lblhiw02,
    String? lblhiw03,
    String? lblhiw04,
    String? lblhiw05,
    String? lblhiw06,
    String? lblhiw07,
    String? lblhiw08,
    String? lblhiw09,
    String? lblhiw10,
    String? lblhiw11,
    String? lblhiw12,
    String? lblhiw13,
    String? lblhiw14,
    String? lblhiw15,
    String? lblhiw16,
    String? lblhiw17,
    String? lblhiw18,
    String? lblhiw19,
    String? lblhiw20,
    String? lblhiw21,
    String? lblhiw22,
    String? lblhiw23,
    String? lblhiw24,
    String? lblhiw25,
    String? lblhiw26,
    String? lblhiw27,
    String? lblhiw28,
    String? pagetitleterms,
    String? lblterms01,
    String? pagetitleprivacy,
    String? lblprivacypolicy01,
    String? pagetitlecookie,
    String? lblcookiepolicy01,
    String? pagetitleaboutus,
    String? lblaboutus01,
    String? pagetitlecontactus,
    String? lblcontactus01,
    String? lblrecharge01,
    String? pagetitlepress,
    String? lblpress01,
    String? lblpress02,
    String? lblpress03,
    String? pagetitlejobs,
    String? lbljobs01,
    String? lbljobs02,
    String? pagetitlesignup,
    String? lblsignup01,
    String? lblsignup02,
    String? lblsignup03,
    String? lblsignup04,
    String? lblsignup05,
    String? lblsignup06,
    String? lblsignup07,
    String? lblsignup08,
    String? lblsignup09,
    String? lblsignup10,
    String? lblsignup11,
    String? lblsignup12,
    String? lblsignup13,
    String? lblsignupvalidation01,
    String? lblsignupvalidation02,
    String? lblsignupvalidation03,
    String? lblsignupvalidation04,
    String? lblsignupvalidation05,
    String? lblsignupvalidation06,
    String? lblsignupvalidation07,
    String? lblsignupvalidation08,
    String? lblsignupvalidation09,
    String? lblsignupvalidation10,
    String? msgsignupemailexist,
    String? msgsignupuserexist,
    String? msgsignupsuccess,
    String? msgsignupfailed,
    String? msgdatanotfound,
    String? msgfree2lov,
    String? pagetitlelogin,
    String? lbllogin01,
    String? lbllogin02,
    String? lbllogin03,
    String? lbllogin04,
    String? lbllogin05,
    String? lbllogin06,
    String? lbllogin07,
    String? lbllogin08,
    String? lbllogin09,
    String? lbllogin10,
    String? lbllogin11,
    String? lbllogin12,
    String? msglogininvalid,
    String? msgloginstatusinactive,
    String? msgforgotpasswordinvalid,
    String? msgforgotpasswordsuccess,
    String? msgforgotpasswordinactive,
    String? msgresetpasswordmissing,
    String? msgresetpasswordinvalid,
    String? msgresetpasswordsuccess,
    String? msgresetpasswordinactive,
    String? msgemailinvalid,
    String? msgloginsuccess,
    String? msglogoutsuccess,
    String? msgmailheadertitle,
    String? msgmailcopyrighttext,
    String? msgmailfooterteam,
    String? msgproducttitle,
    String? msgproduct01,
    String? msgproduct02,
    String? msgproduct03,
    String? msgproduct02notavailable,
    String? msgproduct04,
    String? msgproduct05,
    String? msgproduct06,
    String? msgproduct07,
    String? msgproduct08,
    String? msgproduct09,
    String? msgproductcartaddsuccess,
    String? msgproductcartaddfailed,
    String? msgproductwishlistaddfailed,
    String? msgproduct10,
    String? msgproduct11,
    String? msgproduct12,
    String? msgproduct13,
    String? msgproductdetailtitle,
    String? msgproductdetail01,
    String? msgproductdetail02,
    String? msgproductdetail03,
    String? msgproductdetail04,
    String? msgproductdetail05,
    String? msgproductcarttitle,
    String? msgproductcart01,
    String? msgproductcart02,
    String? msgproductcart03,
    String? msgproductcart04,
    String? msgproductcart05,
    String? msgproductcart06,
    String? msgproductcart07,
    String? msgproductcart08,
    String? msgproductcart09,
    String? msgproductcart10,
    String? msgproductcart11,
    String? msgproductcart12,
    String? msgproductcart13,
    String? msgproductcart14,
    String? msgproductcart15,
    String? msgproductcart16,
    String? msgproductcart17,
    String? msgproductcart18,
    String? msgproductcart19,
    String? msgproductcart20,
    String? msgproductcart21,
    String? msgproductcart22,
    String? msgproductcart23,
    String? msgproductcart24,
    String? msgproductcart25,
    String? msgproductcart26,
    String? msgproductcheckouttitle,
    String? msgproductcheckout01,
    String? msgproductcheckout02,
    String? msgproductcheckout03,
    String? msgproductcheckout04,
    String? msgproductcheckout05,
    String? msgproductcheckout06,
    String? msgproductcheckout07,
    String? msgproductcheckout08,
    String? msgproductcheckout09,
    String? msgproductcheckout10,
    String? msgproductcheckout11,
    String? msgproductcheckout12,
    String? msgproductcheckout13,
    String? msgproductcheckout14,
    String? msgproductcheckout15,
    String? msgproductcheckout16,
    String? msgproductcheckout17,
    String? msgproductcheckout18,
    String? msgproductcheckout19,
    String? msgproductcheckout20,
    String? msgproductcheckout21,
    String? msgproductcheckout22,
    String? msgproductcheckout23,
    String? msgproductcheckout24,
    String? msgproductcheckout25,
    String? msgproductcheckout26,
    String? msgproductcheckout27,
    String? msgproductcheckout28,
    String? msgproductcheckout29,
    String? msgproductcheckout30,
    String? msgproductcheckout31,
    String? msgproductcheckout32,
    String? msgproductcheckout33,
    String? msgproductcheckout34,
    String? msgproductcheckout35,
    String? msgproductcheckout36,
    String? msgproductcheckout37,
    String? msgproductcheckout38,
    String? msgproductcheckout39,
    String? msgproductcheckout40,
    String? msgproductcheckout41,
    String? msgproductcheckout42,
    String? msgproductcheckout43,
    String? msgproductcheckout44,
    String? msgproductcheckout45,
    String? msgproductcheckout46,
    String? msgproductcheckout47,
    String? msgproductcheckout48,
    String? msgproductcheckout49,
    String? msgproductcheckout50,
    String? msgproductcheckout51,
    String? msgproductcheckout52,
    String? msgproductcheckout53,
    String? msgproductcheckout54,
    String? msgproductcheckout55,
    String? msgproductcheckout56,
    String? msgproductcheckout57,
    String? msgproductcheckout58,
    String? msgproductcheckout59,
    String? msgproductcheckout60,
    String? msgproductcheckout61,
    String? msgproductcheckout62,
    String? msgproductcheckout63,
    String? msgproductcheckout64,
    String? msgproductcheckout65,
    String? msgproductcheckout66,
    String? msgproductcheckout67,
    String? msgproductcheckout68,
    String? msgoctitle,
    String? msgoc01,
    String? msgoc02,
    String? msgoc03,
    String? msgoc04,
    String? msgoc05,
    String? msgoc06,
    String? msgoc07,
    String? msgoc08,
    String? msgoc09,
    String? msgoc10,
    String? msgoc11,
    String? msgoc12,
    String? msgoc13,
    String? msgoc14,
    String? msgoc15,
    String? msgvotitle,
    String? msgvo01,
    String? msgvo02,
    String? msgvo03,
    String? msgvo04,
    String? msgvo05,
    String? msgvo06,
    String? msgvo07,
    String? msgvo08,
    String? msgvo09,
    String? msgvo10,
    String? msgvo11,
    String? msgvo12,
    String? msgvo13,
    String? msgvo14,
    String? msgvo15,
    String? msgvo16,
    String? msgvo17,
    String? msgvo18,
    String? msgvo19,
    String? msgvo20,
    String? msgcptitle,
    String? msgcp01,
    String? msgcp02,
    String? msgcp03,
    String? msgcp04,
    String? msgcp05,
    String? msgcp06,
    String? msgcp07,
    String? msgcp08,
    String? lblcpvalidation01,
    String? lblcpvalidation02,
    String? lblcpvalidation03,
    String? lblcpvalidation04,
    String? lblcpvalidation05,
    String? msgcpinvalid,
    String? msgcpsuccess,
    String? msgcpfailed,
    String? msgmatitle,
    String? msgma01,
    String? msgma02,
    String? msgma03,
    String? msgma04,
    String? msgma05,
    String? msgma06,
    String? msgma07,
    String? msgma08,
    String? msgma09,
    String? msgma10,
    String? msgma11,
    String? msgma12,
    String? msgma13,
    String? msgma14,
    String? msgma15,
    String? msgma16,
    String? msgma17,
    String? msgma18,
    String? msgma19,
    String? msgma20,
    String? msgma21,
    String? msgma22,
    String? msgma23,
    String? lblmavalidation01,
    String? lblmavalidation02,
    String? lblmavalidation03,
    String? lblmavalidation04,
    String? lblmavalidation05,
    String? lblmavalidation06,
    String? lblmavalidation07,
    String? msgaddressdetailssuccess,
    String? msgaddressdetailserror,
    String? msgwait,
    String? msgcopy,
    String? msgsettingsuccess,
    String? msgsettingerror,
    String? msgiftitle,
    String? msgif01,
    String? msgif02,
    String? msgif03,
    String? msgif04,
    String? msgif05,
    String? msgif06,
    String? msgif07,
    String? msgif08,
    String? msgif09,
    String? msgfptitle,
    String? msgfp01,
    String? msgfp02,
    String? msgrptitle,
    String? msgrp01,
    String? msgrp02,
    String? msgbadrequest,
    String? shopchange01,
    String? shopchange02,
    String? shopchange03,
    String? shopchange04,
    String? shopchange05,
    String? shopchange06,
    String? msgusernotfound,
    String? msgordernotfound,
    String? msgorderdetailnotfound,
    String? rechargecheckoutbtnlbl,
    String? lblalert,
    String? enternumber,
    String? operatordetected,
    String? change,
    String? selectamount,
    String? havepromocode,
    String? pleaseselect,
    String? errornumber,
    String? characterlong,
    String? packageerror,
    String? willreceived,
    String? priceerrormax,
    String? priceerrormin,
    String? rechargestatus,
    String? becauseof,
    String? somewentwrong,
    String? checkmobileno,
    String? needproduct,
    String? purposeproduct,
    String? needproducthead,
    String? countrylabel,
    String? productplaceholder,
    String? productdescplaceholder,
    String? send,
    String? productsent,
    String? productname,
    String? productdesc,
    String? productqty,
    String? anywhere,
    String? informavailability,
    String? proposecountrylabel,
    String? proposecitylabel,
    String? proposechooseshop,
    String? proposeemail,
    String? proposetelephone,
    String? footermenustories,
    String? footermenupress,
    String? footermenucareer,
    String? footermenuhelp,
    String? footermenufaq,
    String? checkouttransactiondetails,
    String? checkoutbenificierydetails,
    String? rechargerecipientdetails,
    String? checkoutpaymentmode,
    String? pleaseselectdeliverymethod,
    String? doyouhaveapromocode,
    String? step2text,
    String? shippingandhabdling,
    String? transactionaredeliveredonluunderpresentationofvalidcni,
    String? abetterwaytosendwhatreallymatter,
    String? wedeliveredproductlovedonce,
    String? selectarea,
    String? buysafelyonline,
    String? getdelivery,
    String? selectareaandshop,
    String? paysafely,
    String? benificieryrecieves,
    String? jointhousandsofhappyfamilies,
    String? worldfamilyconnected,
    String? satisfiedusers,
    String? whychoosefamilov,
    String? discoveredwhyouruserlovedus,
    String? secure,
    String? weusesslcertificate,
    String? yourmoneyiswellused,
    String? sendmore,
    String? easytouse,
    String? positiveimpact,
    String? socialaspect,
    String? giranteemoney,
    String? savetraditionalmoney,
    String? familovisveryeasytouse,
    String? weimprovepeoplelives,
    String? wehelpyoutohelppeople,
    String? ourservices,
    String? morewaystosendwhatreallymatter,
    String? fooddrinkgrocery,
    String? fooddrinkgrocerydesc,
    String? mobiletopup,
    String? mobiletopupdesc,
    String? healthpaharmacy,
    String? healthpaharmacydesc,
    String? educationformation,
    String? educationformationdesc,
    String? construction,
    String? constructiondesc,
    String? mobilityenergy,
    String? mobilityenergydesc,
    String? signupforfree,
    String? becomepartner,
    String? howtogiveback,
    String? webelivethatgoinggood,
    String? learnmore,
    String? subscribetofamilov,
    String? amazingoffersupdatesinterestingnews,
    String? enterhereanyplaceindications,
    String? bravoyourpackageisready,
    String? pleaseselectthecountry,
    String? usefamilovandstayconnectedwithlovedonce,
    String? selectthecountry,
    String? bemoreefficientthantraditional,
    String? iliketogiveahelpinghandinthecountry,
    String? helpustohelp,
    String? go,
    String? loveourapp,
    String? legal,
    String? termsandcondition,
    String? forpatners,
    String? mobileinputplaceholder,
    String? submitproduct,
    String? rechargemaintitle,
    String? rechargetitledesctiption,
    String? rechargestep1title,
    String? rechargestep1,
    String? rechargestep2title,
    String? rechargestep2,
    String? rechargestep3title,
    String? rechargestep3,
    String? rechargebannertitle,
    String? rechargebannerdescription,
    String? rechargesearchplaceholder,
    String? newtofamilov,
    String? takelessthanminutes,
    String? signinwithfacebook,
    String? signinwithgoogle,
    String? or,
    String? signupwithfacebook,
    String? signupwithgoogle,
    String? signuptermsandcondition,
    String? signupcondition,
    String? noproductfound,
    String? subscribetogetbestofferes,
    String? viewmycart,
    String? dlbill,
    String? dlorddets,
    String? selectbenificerydetails,
    String? addnewreciepent,
    String? easyisnotit,
    String? selectpaymentmethod,
    String? personalmessage,
    String? doyouwanttoleave,
    String? accept,
    String? securecheckout,
    String? satisfactionguaranteed,
    String? privacyprotected,
    String? selectreciepent,
    String? addreciepent,
    String? myreciepent,
    String? mywishlist,
    String? reciepentname,
    String? reciepentmainno,
    String? reciepentanotherno,
    String? pleaseenterfullnameindicatedonhiscni,
    String? edit,
    String? lblmyacct01,
    String? lblmyacct02,
    String? lblmyacct03,
    String? lblmyacct04,
    String? lblmyacct05,
    String? lblmyacct06,
    String? lblmyacct07,
    String? lblmyacct08,
    String? lblmyacct09,
    String? lblmyacct10,
    String? lblmyacct11,
    String? lblmyacct12,
    String? lblmyacct13,
    String? lblmyacct14,
    String? lblmyacct15,
    String? checkoutcontinue,
    String? checkoutback,
    String? orderconfirmation1,
    String? lblproductsortmostsold,
    String? lblproductsortleastsold,
    String? lblproductsortpricelow,
    String? lblproductsortpricehigh,
    String? lblproductsortrecentsold,
  }) {
    _lblmenu01 = lblmenu01;
    _lblmenu02 = lblmenu02;
    _lblmenu021 = lblmenu021;
    _lblmenu03 = lblmenu03;
    _lblmenu04 = lblmenu04;
    _lblmenu05 = lblmenu05;
    _lblmenu06 = lblmenu06;
    _lblmenu07 = lblmenu07;
    _lblmenu08 = lblmenu08;
    _lblmenu09 = lblmenu09;
    _lblmenu10 = lblmenu10;
    _lblmenu11 = lblmenu11;
    _lblmenu12 = lblmenu12;
    _lblmenu13 = lblmenu13;
    _lblmenu14 = lblmenu14;
    _lblmenu15 = lblmenu15;
    _lblmenu16 = lblmenu16;
    _lblmenu17 = lblmenu17;
    _lblmenu18 = lblmenu18;
    _lblmenu19 = lblmenu19;
    _lblmenu20 = lblmenu20;
    _lblmenu21 = lblmenu21;
    _lblmenu22 = lblmenu22;
    _lblmenu23 = lblmenu23;
    _lblmenu24 = lblmenu24;
    _lblmenu25 = lblmenu25;
    _lblsocialfacebook = lblsocialfacebook;
    _lblsocialtwitter = lblsocialtwitter;
    _lblfooter01 = lblfooter01;
    _lblfooter02 = lblfooter02;
    _welcomeMessage = welcomeMessage;
    _lblherotwo = lblherotwo;
    _lblherothree = lblherothree;
    _lblreceivercountry = lblreceivercountry;
    _lblreceivercity = lblreceivercity;
    _lblchooseshop = lblchooseshop;
    _lblreceivergo = lblreceivergo;
    _lblsearchvalidationcountry = lblsearchvalidationcountry;
    _lblsearchvalidationcity = lblsearchvalidationcity;
    _lblsearchvalidationshop = lblsearchvalidationshop;
    _lblsearchcitylist = lblsearchcitylist;
    _lblsearchshoplist = lblsearchshoplist;
    _lblreceiverstarted = lblreceiverstarted;
    _lblhome01 = lblhome01;
    _lblhome02 = lblhome02;
    _lblhome03 = lblhome03;
    _lblhome04 = lblhome04;
    _lblhome05 = lblhome05;
    _lblhome06 = lblhome06;
    _lblhome07 = lblhome07;
    _lblhome08 = lblhome08;
    _lblhome09 = lblhome09;
    _lblhome10 = lblhome10;
    _lblhome11 = lblhome11;
    _lblhome12 = lblhome12;
    _lblhome13 = lblhome13;
    _lblhome14 = lblhome14;
    _lblhome15 = lblhome15;
    _lblhome16 = lblhome16;
    _lblhowitworks01 = lblhowitworks01;
    _lblhowitworks02 = lblhowitworks02;
    _lblhowitworks03 = lblhowitworks03;
    _lblhowitworks04 = lblhowitworks04;
    _lblhowitworks05 = lblhowitworks05;
    _lblhowitworks06 = lblhowitworks06;
    _lblhowitworks07 = lblhowitworks07;
    _lblhowitworks08 = lblhowitworks08;
    _lblhowitworks09 = lblhowitworks09;
    _lblhowitworks10 = lblhowitworks10;
    _lblhowitworks11 = lblhowitworks11;
    _lbltestimonial01 = lbltestimonial01;
    _lbltestimonial02 = lbltestimonial02;
    _lbltestimonialperson101 = lbltestimonialperson101;
    _lbltestimonialperson102 = lbltestimonialperson102;
    _lbltestimonialperson103 = lbltestimonialperson103;
    _lbltestimonialperson201 = lbltestimonialperson201;
    _lbltestimonialperson202 = lbltestimonialperson202;
    _lbltestimonialperson203 = lbltestimonialperson203;
    _lbltestimonialperson301 = lbltestimonialperson301;
    _lbltestimonialperson302 = lbltestimonialperson302;
    _lbltestimonialperson303 = lbltestimonialperson303;
    _pagetitlefaq = pagetitlefaq;
    _lblfaq01 = lblfaq01;
    _lblfaq02 = lblfaq02;
    _lblfaq03 = lblfaq03;
    _pagetitlehiw = pagetitlehiw;
    _lblhiw01 = lblhiw01;
    _lblhiw02 = lblhiw02;
    _lblhiw03 = lblhiw03;
    _lblhiw04 = lblhiw04;
    _lblhiw05 = lblhiw05;
    _lblhiw06 = lblhiw06;
    _lblhiw07 = lblhiw07;
    _lblhiw08 = lblhiw08;
    _lblhiw09 = lblhiw09;
    _lblhiw10 = lblhiw10;
    _lblhiw11 = lblhiw11;
    _lblhiw12 = lblhiw12;
    _lblhiw13 = lblhiw13;
    _lblhiw14 = lblhiw14;
    _lblhiw15 = lblhiw15;
    _lblhiw16 = lblhiw16;
    _lblhiw17 = lblhiw17;
    _lblhiw18 = lblhiw18;
    _lblhiw19 = lblhiw19;
    _lblhiw20 = lblhiw20;
    _lblhiw21 = lblhiw21;
    _lblhiw22 = lblhiw22;
    _lblhiw23 = lblhiw23;
    _lblhiw24 = lblhiw24;
    _lblhiw25 = lblhiw25;
    _lblhiw26 = lblhiw26;
    _lblhiw27 = lblhiw27;
    _lblhiw28 = lblhiw28;
    _pagetitleterms = pagetitleterms;
    _lblterms01 = lblterms01;
    _pagetitleprivacy = pagetitleprivacy;
    _lblprivacypolicy01 = lblprivacypolicy01;
    _pagetitlecookie = pagetitlecookie;
    _lblcookiepolicy01 = lblcookiepolicy01;
    _pagetitleaboutus = pagetitleaboutus;
    _lblaboutus01 = lblaboutus01;
    _pagetitlecontactus = pagetitlecontactus;
    _lblcontactus01 = lblcontactus01;
    _lblrecharge01 = lblrecharge01;
    _pagetitlepress = pagetitlepress;
    _lblpress01 = lblpress01;
    _lblpress02 = lblpress02;
    _lblpress03 = lblpress03;
    _pagetitlejobs = pagetitlejobs;
    _lbljobs01 = lbljobs01;
    _lbljobs02 = lbljobs02;
    _pagetitlesignup = pagetitlesignup;
    _lblsignup01 = lblsignup01;
    _lblsignup02 = lblsignup02;
    _lblsignup03 = lblsignup03;
    _lblsignup04 = lblsignup04;
    _lblsignup05 = lblsignup05;
    _lblsignup06 = lblsignup06;
    _lblsignup07 = lblsignup07;
    _lblsignup08 = lblsignup08;
    _lblsignup09 = lblsignup09;
    _lblsignup10 = lblsignup10;
    _lblsignup11 = lblsignup11;
    _lblsignup12 = lblsignup12;
    _lblsignup13 = lblsignup13;
    _lblsignupvalidation01 = lblsignupvalidation01;
    _lblsignupvalidation02 = lblsignupvalidation02;
    _lblsignupvalidation03 = lblsignupvalidation03;
    _lblsignupvalidation04 = lblsignupvalidation04;
    _lblsignupvalidation05 = lblsignupvalidation05;
    _lblsignupvalidation06 = lblsignupvalidation06;
    _lblsignupvalidation07 = lblsignupvalidation07;
    _lblsignupvalidation08 = lblsignupvalidation08;
    _lblsignupvalidation09 = lblsignupvalidation09;
    _lblsignupvalidation10 = lblsignupvalidation10;
    _msgsignupemailexist = msgsignupemailexist;
    _msgsignupuserexist = msgsignupuserexist;
    _msgsignupsuccess = msgsignupsuccess;
    _msgsignupfailed = msgsignupfailed;
    _msgdatanotfound = msgdatanotfound;
    _msgfree2lov = msgfree2lov;
    _pagetitlelogin = pagetitlelogin;
    _lbllogin01 = lbllogin01;
    _lbllogin02 = lbllogin02;
    _lbllogin03 = lbllogin03;
    _lbllogin04 = lbllogin04;
    _lbllogin05 = lbllogin05;
    _lbllogin06 = lbllogin06;
    _lbllogin07 = lbllogin07;
    _lbllogin08 = lbllogin08;
    _lbllogin09 = lbllogin09;
    _lbllogin10 = lbllogin10;
    _lbllogin11 = lbllogin11;
    _lbllogin12 = lbllogin12;
    _msglogininvalid = msglogininvalid;
    _msgloginstatusinactive = msgloginstatusinactive;
    _msgforgotpasswordinvalid = msgforgotpasswordinvalid;
    _msgforgotpasswordsuccess = msgforgotpasswordsuccess;
    _msgforgotpasswordinactive = msgforgotpasswordinactive;
    _msgresetpasswordmissing = msgresetpasswordmissing;
    _msgresetpasswordinvalid = msgresetpasswordinvalid;
    _msgresetpasswordsuccess = msgresetpasswordsuccess;
    _msgresetpasswordinactive = msgresetpasswordinactive;
    _msgemailinvalid = msgemailinvalid;
    _msgloginsuccess = msgloginsuccess;
    _msglogoutsuccess = msglogoutsuccess;
    _msgmailheadertitle = msgmailheadertitle;
    _msgmailcopyrighttext = msgmailcopyrighttext;
    _msgmailfooterteam = msgmailfooterteam;
    _msgproducttitle = msgproducttitle;
    _msgproduct01 = msgproduct01;
    _msgproduct02 = msgproduct02;
    _msgproduct03 = msgproduct03;
    _msgproduct02notavailable = msgproduct02notavailable;
    _msgproduct04 = msgproduct04;
    _msgproduct05 = msgproduct05;
    _msgproduct06 = msgproduct06;
    _msgproduct07 = msgproduct07;
    _msgproduct08 = msgproduct08;
    _msgproduct09 = msgproduct09;
    _msgproductcartaddsuccess = msgproductcartaddsuccess;
    _msgproductcartaddfailed = msgproductcartaddfailed;
    _msgproductwishlistaddfailed = msgproductwishlistaddfailed;
    _msgproduct10 = msgproduct10;
    _msgproduct11 = msgproduct11;
    _msgproduct12 = msgproduct12;
    _msgproduct13 = msgproduct13;
    _msgproductdetailtitle = msgproductdetailtitle;
    _msgproductdetail01 = msgproductdetail01;
    _msgproductdetail02 = msgproductdetail02;
    _msgproductdetail03 = msgproductdetail03;
    _msgproductdetail04 = msgproductdetail04;
    _msgproductdetail05 = msgproductdetail05;
    _msgproductcarttitle = msgproductcarttitle;
    _msgproductcart01 = msgproductcart01;
    _msgproductcart02 = msgproductcart02;
    _msgproductcart03 = msgproductcart03;
    _msgproductcart04 = msgproductcart04;
    _msgproductcart05 = msgproductcart05;
    _msgproductcart06 = msgproductcart06;
    _msgproductcart07 = msgproductcart07;
    _msgproductcart08 = msgproductcart08;
    _msgproductcart09 = msgproductcart09;
    _msgproductcart10 = msgproductcart10;
    _msgproductcart11 = msgproductcart11;
    _msgproductcart12 = msgproductcart12;
    _msgproductcart13 = msgproductcart13;
    _msgproductcart14 = msgproductcart14;
    _msgproductcart15 = msgproductcart15;
    _msgproductcart16 = msgproductcart16;
    _msgproductcart17 = msgproductcart17;
    _msgproductcart18 = msgproductcart18;
    _msgproductcart19 = msgproductcart19;
    _msgproductcart20 = msgproductcart20;
    _msgproductcart21 = msgproductcart21;
    _msgproductcart22 = msgproductcart22;
    _msgproductcart23 = msgproductcart23;
    _msgproductcart24 = msgproductcart24;
    _msgproductcart25 = msgproductcart25;
    _msgproductcart26 = msgproductcart26;
    _msgproductcheckouttitle = msgproductcheckouttitle;
    _msgproductcheckout01 = msgproductcheckout01;
    _msgproductcheckout02 = msgproductcheckout02;
    _msgproductcheckout03 = msgproductcheckout03;
    _msgproductcheckout04 = msgproductcheckout04;
    _msgproductcheckout05 = msgproductcheckout05;
    _msgproductcheckout06 = msgproductcheckout06;
    _msgproductcheckout07 = msgproductcheckout07;
    _msgproductcheckout08 = msgproductcheckout08;
    _msgproductcheckout09 = msgproductcheckout09;
    _msgproductcheckout10 = msgproductcheckout10;
    _msgproductcheckout11 = msgproductcheckout11;
    _msgproductcheckout12 = msgproductcheckout12;
    _msgproductcheckout13 = msgproductcheckout13;
    _msgproductcheckout14 = msgproductcheckout14;
    _msgproductcheckout15 = msgproductcheckout15;
    _msgproductcheckout16 = msgproductcheckout16;
    _msgproductcheckout17 = msgproductcheckout17;
    _msgproductcheckout18 = msgproductcheckout18;
    _msgproductcheckout19 = msgproductcheckout19;
    _msgproductcheckout20 = msgproductcheckout20;
    _msgproductcheckout21 = msgproductcheckout21;
    _msgproductcheckout22 = msgproductcheckout22;
    _msgproductcheckout23 = msgproductcheckout23;
    _msgproductcheckout24 = msgproductcheckout24;
    _msgproductcheckout25 = msgproductcheckout25;
    _msgproductcheckout26 = msgproductcheckout26;
    _msgproductcheckout27 = msgproductcheckout27;
    _msgproductcheckout28 = msgproductcheckout28;
    _msgproductcheckout29 = msgproductcheckout29;
    _msgproductcheckout30 = msgproductcheckout30;
    _msgproductcheckout31 = msgproductcheckout31;
    _msgproductcheckout32 = msgproductcheckout32;
    _msgproductcheckout33 = msgproductcheckout33;
    _msgproductcheckout34 = msgproductcheckout34;
    _msgproductcheckout35 = msgproductcheckout35;
    _msgproductcheckout36 = msgproductcheckout36;
    _msgproductcheckout37 = msgproductcheckout37;
    _msgproductcheckout38 = msgproductcheckout38;
    _msgproductcheckout39 = msgproductcheckout39;
    _msgproductcheckout40 = msgproductcheckout40;
    _msgproductcheckout41 = msgproductcheckout41;
    _msgproductcheckout42 = msgproductcheckout42;
    _msgproductcheckout43 = msgproductcheckout43;
    _msgproductcheckout44 = msgproductcheckout44;
    _msgproductcheckout45 = msgproductcheckout45;
    _msgproductcheckout46 = msgproductcheckout46;
    _msgproductcheckout47 = msgproductcheckout47;
    _msgproductcheckout48 = msgproductcheckout48;
    _msgproductcheckout49 = msgproductcheckout49;
    _msgproductcheckout50 = msgproductcheckout50;
    _msgproductcheckout51 = msgproductcheckout51;
    _msgproductcheckout52 = msgproductcheckout52;
    _msgproductcheckout53 = msgproductcheckout53;
    _msgproductcheckout54 = msgproductcheckout54;
    _msgproductcheckout55 = msgproductcheckout55;
    _msgproductcheckout56 = msgproductcheckout56;
    _msgproductcheckout57 = msgproductcheckout57;
    _msgproductcheckout58 = msgproductcheckout58;
    _msgproductcheckout59 = msgproductcheckout59;
    _msgproductcheckout60 = msgproductcheckout60;
    _msgproductcheckout61 = msgproductcheckout61;
    _msgproductcheckout62 = msgproductcheckout62;
    _msgproductcheckout63 = msgproductcheckout63;
    _msgproductcheckout64 = msgproductcheckout64;
    _msgproductcheckout65 = msgproductcheckout65;
    _msgproductcheckout66 = msgproductcheckout66;
    _msgproductcheckout67 = msgproductcheckout67;
    _msgproductcheckout68 = msgproductcheckout68;
    _msgoctitle = msgoctitle;
    _msgoc01 = msgoc01;
    _msgoc02 = msgoc02;
    _msgoc03 = msgoc03;
    _msgoc04 = msgoc04;
    _msgoc05 = msgoc05;
    _msgoc06 = msgoc06;
    _msgoc07 = msgoc07;
    _msgoc08 = msgoc08;
    _msgoc09 = msgoc09;
    _msgoc10 = msgoc10;
    _msgoc11 = msgoc11;
    _msgoc12 = msgoc12;
    _msgoc13 = msgoc13;
    _msgoc14 = msgoc14;
    _msgoc15 = msgoc15;
    _msgvotitle = msgvotitle;
    _msgvo01 = msgvo01;
    _msgvo02 = msgvo02;
    _msgvo03 = msgvo03;
    _msgvo04 = msgvo04;
    _msgvo05 = msgvo05;
    _msgvo06 = msgvo06;
    _msgvo07 = msgvo07;
    _msgvo08 = msgvo08;
    _msgvo09 = msgvo09;
    _msgvo10 = msgvo10;
    _msgvo11 = msgvo11;
    _msgvo12 = msgvo12;
    _msgvo13 = msgvo13;
    _msgvo14 = msgvo14;
    _msgvo15 = msgvo15;
    _msgvo16 = msgvo16;
    _msgvo17 = msgvo17;
    _msgvo18 = msgvo18;
    _msgvo19 = msgvo19;
    _msgvo20 = msgvo20;
    _msgcptitle = msgcptitle;
    _msgcp01 = msgcp01;
    _msgcp02 = msgcp02;
    _msgcp03 = msgcp03;
    _msgcp04 = msgcp04;
    _msgcp05 = msgcp05;
    _msgcp06 = msgcp06;
    _msgcp07 = msgcp07;
    _msgcp08 = msgcp08;
    _lblcpvalidation01 = lblcpvalidation01;
    _lblcpvalidation02 = lblcpvalidation02;
    _lblcpvalidation03 = lblcpvalidation03;
    _lblcpvalidation04 = lblcpvalidation04;
    _lblcpvalidation05 = lblcpvalidation05;
    _msgcpinvalid = msgcpinvalid;
    _msgcpsuccess = msgcpsuccess;
    _msgcpfailed = msgcpfailed;
    _msgmatitle = msgmatitle;
    _msgma01 = msgma01;
    _msgma02 = msgma02;
    _msgma03 = msgma03;
    _msgma04 = msgma04;
    _msgma05 = msgma05;
    _msgma06 = msgma06;
    _msgma07 = msgma07;
    _msgma08 = msgma08;
    _msgma09 = msgma09;
    _msgma10 = msgma10;
    _msgma11 = msgma11;
    _msgma12 = msgma12;
    _msgma13 = msgma13;
    _msgma14 = msgma14;
    _msgma15 = msgma15;
    _msgma16 = msgma16;
    _msgma17 = msgma17;
    _msgma18 = msgma18;
    _msgma19 = msgma19;
    _msgma20 = msgma20;
    _msgma21 = msgma21;
    _msgma22 = msgma22;
    _msgma23 = msgma23;
    _lblmavalidation01 = lblmavalidation01;
    _lblmavalidation02 = lblmavalidation02;
    _lblmavalidation03 = lblmavalidation03;
    _lblmavalidation04 = lblmavalidation04;
    _lblmavalidation05 = lblmavalidation05;
    _lblmavalidation06 = lblmavalidation06;
    _lblmavalidation07 = lblmavalidation07;
    _msgaddressdetailssuccess = msgaddressdetailssuccess;
    _msgaddressdetailserror = msgaddressdetailserror;
    _msgwait = msgwait;
    _msgcopy = msgcopy;
    _msgsettingsuccess = msgsettingsuccess;
    _msgsettingerror = msgsettingerror;
    _msgiftitle = msgiftitle;
    _msgif01 = msgif01;
    _msgif02 = msgif02;
    _msgif03 = msgif03;
    _msgif04 = msgif04;
    _msgif05 = msgif05;
    _msgif06 = msgif06;
    _msgif07 = msgif07;
    _msgif08 = msgif08;
    _msgif09 = msgif09;
    _msgfptitle = msgfptitle;
    _msgfp01 = msgfp01;
    _msgfp02 = msgfp02;
    _msgrptitle = msgrptitle;
    _msgrp01 = msgrp01;
    _msgrp02 = msgrp02;
    _msgbadrequest = msgbadrequest;
    _shopchange01 = shopchange01;
    _shopchange02 = shopchange02;
    _shopchange03 = shopchange03;
    _shopchange04 = shopchange04;
    _shopchange05 = shopchange05;
    _shopchange06 = shopchange06;
    _msgusernotfound = msgusernotfound;
    _msgordernotfound = msgordernotfound;
    _msgorderdetailnotfound = msgorderdetailnotfound;
    _rechargecheckoutbtnlbl = rechargecheckoutbtnlbl;
    _lblalert = lblalert;
    _enternumber = enternumber;
    _operatordetected = operatordetected;
    _change = change;
    _selectamount = selectamount;
    _havepromocode = havepromocode;
    _pleaseselect = pleaseselect;
    _errornumber = errornumber;
    _characterlong = characterlong;
    _packageerror = packageerror;
    _willreceived = willreceived;
    _priceerrormax = priceerrormax;
    _priceerrormin = priceerrormin;
    _rechargestatus = rechargestatus;
    _becauseof = becauseof;
    _somewentwrong = somewentwrong;
    _checkmobileno = checkmobileno;
    _needproduct = needproduct;
    _purposeproduct = purposeproduct;
    _needproducthead = needproducthead;
    _countrylabel = countrylabel;
    _productplaceholder = productplaceholder;
    _productdescplaceholder = productdescplaceholder;
    _send = send;
    _productsent = productsent;
    _productname = productname;
    _productdesc = productdesc;
    _productqty = productqty;
    _anywhere = anywhere;
    _informavailability = informavailability;
    _proposecountrylabel = proposecountrylabel;
    _proposecitylabel = proposecitylabel;
    _proposechooseshop = proposechooseshop;
    _proposeemail = proposeemail;
    _proposetelephone = proposetelephone;
    _footermenustories = footermenustories;
    _footermenupress = footermenupress;
    _footermenucareer = footermenucareer;
    _footermenuhelp = footermenuhelp;
    _footermenufaq = footermenufaq;
    _checkouttransactiondetails = checkouttransactiondetails;
    _checkoutbenificierydetails = checkoutbenificierydetails;
    _rechargerecipientdetails = rechargerecipientdetails;
    _checkoutpaymentmode = checkoutpaymentmode;
    _pleaseselectdeliverymethod = pleaseselectdeliverymethod;
    _doyouhaveapromocode = doyouhaveapromocode;
    _step2text = step2text;
    _shippingandhabdling = shippingandhabdling;
    _transactionaredeliveredonluunderpresentationofvalidcni =
        transactionaredeliveredonluunderpresentationofvalidcni;
    _abetterwaytosendwhatreallymatter = abetterwaytosendwhatreallymatter;
    _wedeliveredproductlovedonce = wedeliveredproductlovedonce;
    _selectarea = selectarea;
    _buysafelyonline = buysafelyonline;
    _getdelivery = getdelivery;
    _selectareaandshop = selectareaandshop;
    _paysafely = paysafely;
    _benificieryrecieves = benificieryrecieves;
    _jointhousandsofhappyfamilies = jointhousandsofhappyfamilies;
    _worldfamilyconnected = worldfamilyconnected;
    _satisfiedusers = satisfiedusers;
    _whychoosefamilov = whychoosefamilov;
    _discoveredwhyouruserlovedus = discoveredwhyouruserlovedus;
    _secure = secure;
    _weusesslcertificate = weusesslcertificate;
    _yourmoneyiswellused = yourmoneyiswellused;
    _sendmore = sendmore;
    _easytouse = easytouse;
    _positiveimpact = positiveimpact;
    _socialaspect = socialaspect;
    _giranteemoney = giranteemoney;
    _savetraditionalmoney = savetraditionalmoney;
    _familovisveryeasytouse = familovisveryeasytouse;
    _weimprovepeoplelives = weimprovepeoplelives;
    _wehelpyoutohelppeople = wehelpyoutohelppeople;
    _ourservices = ourservices;
    _morewaystosendwhatreallymatter = morewaystosendwhatreallymatter;
    _fooddrinkgrocery = fooddrinkgrocery;
    _fooddrinkgrocerydesc = fooddrinkgrocerydesc;
    _mobiletopup = mobiletopup;
    _mobiletopupdesc = mobiletopupdesc;
    _healthpaharmacy = healthpaharmacy;
    _healthpaharmacydesc = healthpaharmacydesc;
    _educationformation = educationformation;
    _educationformationdesc = educationformationdesc;
    _construction = construction;
    _constructiondesc = constructiondesc;
    _mobilityenergy = mobilityenergy;
    _mobilityenergydesc = mobilityenergydesc;
    _signupforfree = signupforfree;
    _becomepartner = becomepartner;
    _howtogiveback = howtogiveback;
    _webelivethatgoinggood = webelivethatgoinggood;
    _learnmore = learnmore;
    _subscribetofamilov = subscribetofamilov;
    _amazingoffersupdatesinterestingnews = amazingoffersupdatesinterestingnews;
    _enterhereanyplaceindications = enterhereanyplaceindications;
    _bravoyourpackageisready = bravoyourpackageisready;
    _pleaseselectthecountry = pleaseselectthecountry;
    _usefamilovandstayconnectedwithlovedonce =
        usefamilovandstayconnectedwithlovedonce;
    _selectthecountry = selectthecountry;
    _bemoreefficientthantraditional = bemoreefficientthantraditional;
    _iliketogiveahelpinghandinthecountry = iliketogiveahelpinghandinthecountry;
    _helpustohelp = helpustohelp;
    _go = go;
    _loveourapp = loveourapp;
    _legal = legal;
    _termsandcondition = termsandcondition;
    _forpatners = forpatners;
    _mobileinputplaceholder = mobileinputplaceholder;
    _submitproduct = submitproduct;
    _rechargemaintitle = rechargemaintitle;
    _rechargetitledesctiption = rechargetitledesctiption;
    _rechargestep1title = rechargestep1title;
    _rechargestep1 = rechargestep1;
    _rechargestep2title = rechargestep2title;
    _rechargestep2 = rechargestep2;
    _rechargestep3title = rechargestep3title;
    _rechargestep3 = rechargestep3;
    _rechargebannertitle = rechargebannertitle;
    _rechargebannerdescription = rechargebannerdescription;
    _rechargesearchplaceholder = rechargesearchplaceholder;
    _newtofamilov = newtofamilov;
    _takelessthanminutes = takelessthanminutes;
    _signinwithfacebook = signinwithfacebook;
    _signinwithgoogle = signinwithgoogle;
    _or = or;
    _signupwithfacebook = signupwithfacebook;
    _signupwithgoogle = signupwithgoogle;
    _signuptermsandcondition = signuptermsandcondition;
    _signupcondition = signupcondition;
    _noproductfound = noproductfound;
    _subscribetogetbestofferes = subscribetogetbestofferes;
    _viewmycart = viewmycart;
    _dlbill = dlbill;
    _dlorddets = dlorddets;
    _selectbenificerydetails = selectbenificerydetails;
    _addnewreciepent = addnewreciepent;
    _easyisnotit = easyisnotit;
    _selectpaymentmethod = selectpaymentmethod;
    _personalmessage = personalmessage;
    _doyouwanttoleave = doyouwanttoleave;
    _accept = accept;
    _securecheckout = securecheckout;
    _satisfactionguaranteed = satisfactionguaranteed;
    _privacyprotected = privacyprotected;
    _selectreciepent = selectreciepent;
    _addreciepent = addreciepent;
    _myreciepent = myreciepent;
    _mywishlist = mywishlist;
    _reciepentname = reciepentname;
    _reciepentmainno = reciepentmainno;
    _reciepentanotherno = reciepentanotherno;
    _pleaseenterfullnameindicatedonhiscni =
        pleaseenterfullnameindicatedonhiscni;
    _edit = edit;
    _lblmyacct01 = lblmyacct01;
    _lblmyacct02 = lblmyacct02;
    _lblmyacct03 = lblmyacct03;
    _lblmyacct04 = lblmyacct04;
    _lblmyacct05 = lblmyacct05;
    _lblmyacct06 = lblmyacct06;
    _lblmyacct07 = lblmyacct07;
    _lblmyacct08 = lblmyacct08;
    _lblmyacct09 = lblmyacct09;
    _lblmyacct10 = lblmyacct10;
    _lblmyacct11 = lblmyacct11;
    _lblmyacct12 = lblmyacct12;
    _lblmyacct13 = lblmyacct13;
    _lblmyacct14 = lblmyacct14;
    _lblmyacct15 = lblmyacct15;
    _checkoutcontinue = checkoutcontinue;
    _checkoutback = checkoutback;
    _orderconfirmation1 = orderconfirmation1;
    _lblproductsortmostsold = lblproductsortmostsold;
    _lblproductsortleastsold = lblproductsortleastsold;
    _lblproductsortpricelow = lblproductsortpricelow;
    _lblproductsortpricehigh = lblproductsortpricehigh;
    _lblproductsortrecentsold = lblproductsortrecentsold;
  }

  DataList.fromJson(dynamic json) {
    _lblmenu01 = json['LBL_MENU_01'];
    _lblmenu02 = json['LBL_MENU_02'];
    _lblmenu021 = json['LBL_MENU_02_1'];
    _lblmenu03 = json['LBL_MENU_03'];
    _lblmenu04 = json['LBL_MENU_04'];
    _lblmenu05 = json['LBL_MENU_05'];
    _lblmenu06 = json['LBL_MENU_06'];
    _lblmenu07 = json['LBL_MENU_07'];
    _lblmenu08 = json['LBL_MENU_08'];
    _lblmenu09 = json['LBL_MENU_09'];
    _lblmenu10 = json['LBL_MENU_10'];
    _lblmenu11 = json['LBL_MENU_11'];
    _lblmenu12 = json['LBL_MENU_12'];
    _lblmenu13 = json['LBL_MENU_13'];
    _lblmenu14 = json['LBL_MENU_14'];
    _lblmenu15 = json['LBL_MENU_15'];
    _lblmenu16 = json['LBL_MENU_16'];
    _lblmenu17 = json['LBL_MENU_17'];
    _lblmenu18 = json['LBL_MENU_18'];
    _lblmenu19 = json['LBL_MENU_19'];
    _lblmenu20 = json['LBL_MENU_20'];
    _lblmenu21 = json['LBL_MENU_21'];
    _lblmenu22 = json['LBL_MENU_22'];
    _lblmenu23 = json['LBL_MENU_23'];
    _lblmenu24 = json['LBL_MENU_24'];
    _lblmenu25 = json['LBL_MENU_25'];
    _lblsocialfacebook = json['LBL_SOCIAL_FACEBOOK'];
    _lblsocialtwitter = json['LBL_SOCIAL_TWITTER'];
    _lblfooter01 = json['LBL_FOOTER_01'];
    _lblfooter02 = json['LBL_FOOTER_02'];
    _welcomeMessage = json['welcome_message'];
    _lblherotwo = json['LBL_HERO_TWO'];
    _lblherothree = json['LBL_HERO_THREE'];
    _lblreceivercountry = json['LBL_RECEIVER_COUNTRY'];
    _lblreceivercity = json['LBL_RECEIVER_CITY'];
    _lblchooseshop = json['LBL_CHOOSE_SHOP'];
    _lblreceivergo = json['LBL_RECEIVER_GO'];
    _lblsearchvalidationcountry = json['LBL_SEARCH_VALIDATION_COUNTRY'];
    _lblsearchvalidationcity = json['LBL_SEARCH_VALIDATION_CITY'];
    _lblsearchvalidationshop = json['LBL_SEARCH_VALIDATION_SHOP'];
    _lblsearchcitylist = json['LBL_SEARCH_CITY_LIST'];
    _lblsearchshoplist = json['LBL_SEARCH_SHOP_LIST'];
    _lblreceiverstarted = json['LBL_RECEIVER_STARTED'];
    _lblhome01 = json['LBL_HOME_01'];
    _lblhome02 = json['LBL_HOME_02'];
    _lblhome03 = json['LBL_HOME_03'];
    _lblhome04 = json['LBL_HOME_04'];
    _lblhome05 = json['LBL_HOME_05'];
    _lblhome06 = json['LBL_HOME_06'];
    _lblhome07 = json['LBL_HOME_07'];
    _lblhome08 = json['LBL_HOME_08'];
    _lblhome09 = json['LBL_HOME_09'];
    _lblhome10 = json['LBL_HOME_10'];
    _lblhome11 = json['LBL_HOME_11'];
    _lblhome12 = json['LBL_HOME_12'];
    _lblhome13 = json['LBL_HOME_13'];
    _lblhome14 = json['LBL_HOME_14'];
    _lblhome15 = json['LBL_HOME_15'];
    _lblhome16 = json['LBL_HOME_16'];
    _lblhowitworks01 = json['LBL_HOW_IT_WORKS_01'];
    _lblhowitworks02 = json['LBL_HOW_IT_WORKS_02'];
    _lblhowitworks03 = json['LBL_HOW_IT_WORKS_03'];
    _lblhowitworks04 = json['LBL_HOW_IT_WORKS_04'];
    _lblhowitworks05 = json['LBL_HOW_IT_WORKS_05'];
    _lblhowitworks06 = json['LBL_HOW_IT_WORKS_06'];
    _lblhowitworks07 = json['LBL_HOW_IT_WORKS_07'];
    _lblhowitworks08 = json['LBL_HOW_IT_WORKS_08'];
    _lblhowitworks09 = json['LBL_HOW_IT_WORKS_09'];
    _lblhowitworks10 = json['LBL_HOW_IT_WORKS_10'];
    _lblhowitworks11 = json['LBL_HOW_IT_WORKS_11'];
    _lbltestimonial01 = json['LBL_TESTIMONIAL_01'];
    _lbltestimonial02 = json['LBL_TESTIMONIAL_02'];
    _lbltestimonialperson101 = json['LBL_TESTIMONIAL_PERSON_1_01'];
    _lbltestimonialperson102 = json['LBL_TESTIMONIAL_PERSON_1_02'];
    _lbltestimonialperson103 = json['LBL_TESTIMONIAL_PERSON_1_03'];
    _lbltestimonialperson201 = json['LBL_TESTIMONIAL_PERSON_2_01'];
    _lbltestimonialperson202 = json['LBL_TESTIMONIAL_PERSON_2_02'];
    _lbltestimonialperson203 = json['LBL_TESTIMONIAL_PERSON_2_03'];
    _lbltestimonialperson301 = json['LBL_TESTIMONIAL_PERSON_3_01'];
    _lbltestimonialperson302 = json['LBL_TESTIMONIAL_PERSON_3_02'];
    _lbltestimonialperson303 = json['LBL_TESTIMONIAL_PERSON_3_03'];
    _pagetitlefaq = json['PAGE_TITLE_FAQ'];
    _lblfaq01 = json['LBL_FAQ_01'];
    _lblfaq02 = json['LBL_FAQ_02'];
    _lblfaq03 = json['LBL_FAQ_03'];
    _pagetitlehiw = json['PAGE_TITLE_HIW'];
    _lblhiw01 = json['LBL_HIW_01'];
    _lblhiw02 = json['LBL_HIW_02'];
    _lblhiw03 = json['LBL_HIW_03'];
    _lblhiw04 = json['LBL_HIW_04'];
    _lblhiw05 = json['LBL_HIW_05'];
    _lblhiw06 = json['LBL_HIW_06'];
    _lblhiw07 = json['LBL_HIW_07'];
    _lblhiw08 = json['LBL_HIW_08'];
    _lblhiw09 = json['LBL_HIW_09'];
    _lblhiw10 = json['LBL_HIW_10'];
    _lblhiw11 = json['LBL_HIW_11'];
    _lblhiw12 = json['LBL_HIW_12'];
    _lblhiw13 = json['LBL_HIW_13'];
    _lblhiw14 = json['LBL_HIW_14'];
    _lblhiw15 = json['LBL_HIW_15'];
    _lblhiw16 = json['LBL_HIW_16'];
    _lblhiw17 = json['LBL_HIW_17'];
    _lblhiw18 = json['LBL_HIW_18'];
    _lblhiw19 = json['LBL_HIW_19'];
    _lblhiw20 = json['LBL_HIW_20'];
    _lblhiw21 = json['LBL_HIW_21'];
    _lblhiw22 = json['LBL_HIW_22'];
    _lblhiw23 = json['LBL_HIW_23'];
    _lblhiw24 = json['LBL_HIW_24'];
    _lblhiw25 = json['LBL_HIW_25'];
    _lblhiw26 = json['LBL_HIW_26'];
    _lblhiw27 = json['LBL_HIW_27'];
    _lblhiw28 = json['LBL_HIW_28'];
    _pagetitleterms = json['PAGE_TITLE_TERMS'];
    _lblterms01 = json['LBL_TERMS_01'];
    _pagetitleprivacy = json['PAGE_TITLE_PRIVACY'];
    _lblprivacypolicy01 = json['LBL_PRIVACY_POLICY_01'];
    _pagetitlecookie = json['PAGE_TITLE_COOKIE'];
    _lblcookiepolicy01 = json['LBL_COOKIE_POLICY_01'];
    _pagetitleaboutus = json['PAGE_TITLE_ABOUT_US'];
    _lblaboutus01 = json['LBL_ABOUT_US_01'];
    _pagetitlecontactus = json['PAGE_TITLE_CONTACT_US'];
    _lblcontactus01 = json['LBL_CONTACT_US_01'];
    _lblrecharge01 = json['LBL_RECHARGE_01'];
    _pagetitlepress = json['PAGE_TITLE_PRESS'];
    _lblpress01 = json['LBL_PRESS_01'];
    _lblpress02 = json['LBL_PRESS_02'];
    _lblpress03 = json['LBL_PRESS_03'];
    _pagetitlejobs = json['PAGE_TITLE_JOBS'];
    _lbljobs01 = json['LBL_JOBS_01'];
    _lbljobs02 = json['LBL_JOBS_02'];
    _pagetitlesignup = json['PAGE_TITLE_SIGN_UP'];
    _lblsignup01 = json['LBL_SIGN_UP_01'];
    _lblsignup02 = json['LBL_SIGN_UP_02'];
    _lblsignup03 = json['LBL_SIGN_UP_03'];
    _lblsignup04 = json['LBL_SIGN_UP_04'];
    _lblsignup05 = json['LBL_SIGN_UP_05'];
    _lblsignup06 = json['LBL_SIGN_UP_06'];
    _lblsignup07 = json['LBL_SIGN_UP_07'];
    _lblsignup08 = json['LBL_SIGN_UP_08'];
    _lblsignup09 = json['LBL_SIGN_UP_09'];
    _lblsignup10 = json['LBL_SIGN_UP_10'];
    _lblsignup11 = json['LBL_SIGN_UP_11'];
    _lblsignup12 = json['LBL_SIGN_UP_12'];
    _lblsignup13 = json['LBL_SIGN_UP_13'];
    _lblsignupvalidation01 = json['LBL_SIGN_UP_VALIDATION_01'];
    _lblsignupvalidation02 = json['LBL_SIGN_UP_VALIDATION_02'];
    _lblsignupvalidation03 = json['LBL_SIGN_UP_VALIDATION_03'];
    _lblsignupvalidation04 = json['LBL_SIGN_UP_VALIDATION_04'];
    _lblsignupvalidation05 = json['LBL_SIGN_UP_VALIDATION_05'];
    _lblsignupvalidation06 = json['LBL_SIGN_UP_VALIDATION_06'];
    _lblsignupvalidation07 = json['LBL_SIGN_UP_VALIDATION_07'];
    _lblsignupvalidation08 = json['LBL_SIGN_UP_VALIDATION_08'];
    _lblsignupvalidation09 = json['LBL_SIGN_UP_VALIDATION_09'];
    _lblsignupvalidation10 = json['LBL_SIGN_UP_VALIDATION_10'];
    _msgsignupemailexist = json['MSG_SIGN_UP_EMAIL_EXIST'];
    _msgsignupuserexist = json['MSG_SIGN_UP_USER_EXIST'];
    _msgsignupsuccess = json['MSG_SIGN_UP_SUCCESS'];
    _msgsignupfailed = json['MSG_SIGN_UP_FAILED'];
    _msgdatanotfound = json['MSG_DATA_NOT_FOUND'];
    _msgfree2lov = json['MSG_FREE2LOV'];
    _pagetitlelogin = json['PAGE_TITLE_LOG_IN'];
    _lbllogin01 = json['LBL_LOG_IN_01'];
    _lbllogin02 = json['LBL_LOG_IN_02'];
    _lbllogin03 = json['LBL_LOG_IN_03'];
    _lbllogin04 = json['LBL_LOG_IN_04'];
    _lbllogin05 = json['LBL_LOG_IN_05'];
    _lbllogin06 = json['LBL_LOG_IN_06'];
    _lbllogin07 = json['LBL_LOG_IN_07'];
    _lbllogin08 = json['LBL_LOG_IN_08'];
    _lbllogin09 = json['LBL_LOG_IN_09'];
    _lbllogin10 = json['LBL_LOG_IN_10'];
    _lbllogin11 = json['LBL_LOG_IN_11'];
    _lbllogin12 = json['LBL_LOG_IN_12'];
    _msglogininvalid = json['MSG_LOGIN_INVALID'];
    _msgloginstatusinactive = json['MSG_LOGIN_STATUS_INACTIVE'];
    _msgforgotpasswordinvalid = json['MSG_FORGOT_PASSWORD_INVALID'];
    _msgforgotpasswordsuccess = json['MSG_FORGOT_PASSWORD_SUCCESS'];
    _msgforgotpasswordinactive = json['MSG_FORGOT_PASSWORD_INACTIVE'];
    _msgresetpasswordmissing = json['MSG_RESET_PASSWORD_MISSING'];
    _msgresetpasswordinvalid = json['MSG_RESET_PASSWORD_INVALID'];
    _msgresetpasswordsuccess = json['MSG_RESET_PASSWORD_SUCCESS'];
    _msgresetpasswordinactive = json['MSG_RESET_PASSWORD_INACTIVE'];
    _msgemailinvalid = json['MSG_EMAIL_INVALID'];
    _msgloginsuccess = json['MSG_LOGIN_SUCCESS'];
    _msglogoutsuccess = json['MSG_LOGOUT_SUCCESS'];
    _msgmailheadertitle = json['MSG_MAIL_HEADER_TITLE'];
    _msgmailcopyrighttext = json['MSG_MAIL_COPYRIGHT_TEXT'];
    _msgmailfooterteam = json['MSG_MAIL_FOOTER_TEAM'];
    _msgproducttitle = json['MSG_PRODUCT_TITLE'];
    _msgproduct01 = json['MSG_PRODUCT_01'];
    _msgproduct02 = json['MSG_PRODUCT_02'];
    _msgproduct03 = json['MSG_PRODUCT_03'];
    _msgproduct02notavailable = json['MSG_PRODUCT_02_NOT_AVAILABLE'];
    _msgproduct04 = json['MSG_PRODUCT_04'];
    _msgproduct05 = json['MSG_PRODUCT_05'];
    _msgproduct06 = json['MSG_PRODUCT_06'];
    _msgproduct07 = json['MSG_PRODUCT_07'];
    _msgproduct08 = json['MSG_PRODUCT_08'];
    _msgproduct09 = json['MSG_PRODUCT_09'];
    _msgproductcartaddsuccess = json['MSG_PRODUCT_CART_ADD_SUCCESS'];
    _msgproductcartaddfailed = json['MSG_PRODUCT_CART_ADD_FAILED'];
    _msgproductwishlistaddfailed = json['MSG_PRODUCT_WISHLIST_ADD_FAILED'];
    _msgproduct10 = json['MSG_PRODUCT_10'];
    _msgproduct11 = json['MSG_PRODUCT_11'];
    _msgproduct12 = json['MSG_PRODUCT_12'];
    _msgproduct13 = json['MSG_PRODUCT_13'];
    _msgproductdetailtitle = json['MSG_PRODUCT_DETAIL_TITLE'];
    _msgproductdetail01 = json['MSG_PRODUCT_DETAIL_01'];
    _msgproductdetail02 = json['MSG_PRODUCT_DETAIL_02'];
    _msgproductdetail03 = json['MSG_PRODUCT_DETAIL_03'];
    _msgproductdetail04 = json['MSG_PRODUCT_DETAIL_04'];
    _msgproductdetail05 = json['MSG_PRODUCT_DETAIL_05'];
    _msgproductcarttitle = json['MSG_PRODUCT_CART_TITLE'];
    _msgproductcart01 = json['MSG_PRODUCT_CART_01'];
    _msgproductcart02 = json['MSG_PRODUCT_CART_02'];
    _msgproductcart03 = json['MSG_PRODUCT_CART_03'];
    _msgproductcart04 = json['MSG_PRODUCT_CART_04'];
    _msgproductcart05 = json['MSG_PRODUCT_CART_05'];
    _msgproductcart06 = json['MSG_PRODUCT_CART_06'];
    _msgproductcart07 = json['MSG_PRODUCT_CART_07'];
    _msgproductcart08 = json['MSG_PRODUCT_CART_08'];
    _msgproductcart09 = json['MSG_PRODUCT_CART_09'];
    _msgproductcart10 = json['MSG_PRODUCT_CART_10'];
    _msgproductcart11 = json['MSG_PRODUCT_CART_11'];
    _msgproductcart12 = json['MSG_PRODUCT_CART_12'];
    _msgproductcart13 = json['MSG_PRODUCT_CART_13'];
    _msgproductcart14 = json['MSG_PRODUCT_CART_14'];
    _msgproductcart15 = json['MSG_PRODUCT_CART_15'];
    _msgproductcart16 = json['MSG_PRODUCT_CART_16'];
    _msgproductcart17 = json['MSG_PRODUCT_CART_17'];
    _msgproductcart18 = json['MSG_PRODUCT_CART_18'];
    _msgproductcart19 = json['MSG_PRODUCT_CART_19'];
    _msgproductcart20 = json['MSG_PRODUCT_CART_20'];
    _msgproductcart21 = json['MSG_PRODUCT_CART_21'];
    _msgproductcart22 = json['MSG_PRODUCT_CART_22'];
    _msgproductcart23 = json['MSG_PRODUCT_CART_23'];
    _msgproductcart24 = json['MSG_PRODUCT_CART_24'];
    _msgproductcart25 = json['MSG_PRODUCT_CART_25'];
    _msgproductcart26 = json['MSG_PRODUCT_CART_26'];
    _msgproductcheckouttitle = json['MSG_PRODUCT_CHECKOUT_TITLE'];
    _msgproductcheckout01 = json['MSG_PRODUCT_CHECKOUT_01'];
    _msgproductcheckout02 = json['MSG_PRODUCT_CHECKOUT_02'];
    _msgproductcheckout03 = json['MSG_PRODUCT_CHECKOUT_03'];
    _msgproductcheckout04 = json['MSG_PRODUCT_CHECKOUT_04'];
    _msgproductcheckout05 = json['MSG_PRODUCT_CHECKOUT_05'];
    _msgproductcheckout06 = json['MSG_PRODUCT_CHECKOUT_06'];
    _msgproductcheckout07 = json['MSG_PRODUCT_CHECKOUT_07'];
    _msgproductcheckout08 = json['MSG_PRODUCT_CHECKOUT_08'];
    _msgproductcheckout09 = json['MSG_PRODUCT_CHECKOUT_09'];
    _msgproductcheckout10 = json['MSG_PRODUCT_CHECKOUT_10'];
    _msgproductcheckout11 = json['MSG_PRODUCT_CHECKOUT_11'];
    _msgproductcheckout12 = json['MSG_PRODUCT_CHECKOUT_12'];
    _msgproductcheckout13 = json['MSG_PRODUCT_CHECKOUT_13'];
    _msgproductcheckout14 = json['MSG_PRODUCT_CHECKOUT_14'];
    _msgproductcheckout15 = json['MSG_PRODUCT_CHECKOUT_15'];
    _msgproductcheckout16 = json['MSG_PRODUCT_CHECKOUT_16'];
    _msgproductcheckout17 = json['MSG_PRODUCT_CHECKOUT_17'];
    _msgproductcheckout18 = json['MSG_PRODUCT_CHECKOUT_18'];
    _msgproductcheckout19 = json['MSG_PRODUCT_CHECKOUT_19'];
    _msgproductcheckout20 = json['MSG_PRODUCT_CHECKOUT_20'];
    _msgproductcheckout21 = json['MSG_PRODUCT_CHECKOUT_21'];
    _msgproductcheckout22 = json['MSG_PRODUCT_CHECKOUT_22'];
    _msgproductcheckout23 = json['MSG_PRODUCT_CHECKOUT_23'];
    _msgproductcheckout24 = json['MSG_PRODUCT_CHECKOUT_24'];
    _msgproductcheckout25 = json['MSG_PRODUCT_CHECKOUT_25'];
    _msgproductcheckout26 = json['MSG_PRODUCT_CHECKOUT_26'];
    _msgproductcheckout27 = json['MSG_PRODUCT_CHECKOUT_27'];
    _msgproductcheckout28 = json['MSG_PRODUCT_CHECKOUT_28'];
    _msgproductcheckout29 = json['MSG_PRODUCT_CHECKOUT_29'];
    _msgproductcheckout30 = json['MSG_PRODUCT_CHECKOUT_30'];
    _msgproductcheckout31 = json['MSG_PRODUCT_CHECKOUT_31'];
    _msgproductcheckout32 = json['MSG_PRODUCT_CHECKOUT_32'];
    _msgproductcheckout33 = json['MSG_PRODUCT_CHECKOUT_33'];
    _msgproductcheckout34 = json['MSG_PRODUCT_CHECKOUT_34'];
    _msgproductcheckout35 = json['MSG_PRODUCT_CHECKOUT_35'];
    _msgproductcheckout36 = json['MSG_PRODUCT_CHECKOUT_36'];
    _msgproductcheckout37 = json['MSG_PRODUCT_CHECKOUT_37'];
    _msgproductcheckout38 = json['MSG_PRODUCT_CHECKOUT_38'];
    _msgproductcheckout39 = json['MSG_PRODUCT_CHECKOUT_39'];
    _msgproductcheckout40 = json['MSG_PRODUCT_CHECKOUT_40'];
    _msgproductcheckout41 = json['MSG_PRODUCT_CHECKOUT_41'];
    _msgproductcheckout42 = json['MSG_PRODUCT_CHECKOUT_42'];
    _msgproductcheckout43 = json['MSG_PRODUCT_CHECKOUT_43'];
    _msgproductcheckout44 = json['MSG_PRODUCT_CHECKOUT_44'];
    _msgproductcheckout45 = json['MSG_PRODUCT_CHECKOUT_45'];
    _msgproductcheckout46 = json['MSG_PRODUCT_CHECKOUT_46'];
    _msgproductcheckout47 = json['MSG_PRODUCT_CHECKOUT_47'];
    _msgproductcheckout48 = json['MSG_PRODUCT_CHECKOUT_48'];
    _msgproductcheckout49 = json['MSG_PRODUCT_CHECKOUT_49'];
    _msgproductcheckout50 = json['MSG_PRODUCT_CHECKOUT_50'];
    _msgproductcheckout51 = json['MSG_PRODUCT_CHECKOUT_51'];
    _msgproductcheckout52 = json['MSG_PRODUCT_CHECKOUT_52'];
    _msgproductcheckout53 = json['MSG_PRODUCT_CHECKOUT_53'];
    _msgproductcheckout54 = json['MSG_PRODUCT_CHECKOUT_54'];
    _msgproductcheckout55 = json['MSG_PRODUCT_CHECKOUT_55'];
    _msgproductcheckout56 = json['MSG_PRODUCT_CHECKOUT_56'];
    _msgproductcheckout57 = json['MSG_PRODUCT_CHECKOUT_57'];
    _msgproductcheckout58 = json['MSG_PRODUCT_CHECKOUT_58'];
    _msgproductcheckout59 = json['MSG_PRODUCT_CHECKOUT_59'];
    _msgproductcheckout60 = json['MSG_PRODUCT_CHECKOUT_60'];
    _msgproductcheckout61 = json['MSG_PRODUCT_CHECKOUT_61'];
    _msgproductcheckout62 = json['MSG_PRODUCT_CHECKOUT_62'];
    _msgproductcheckout63 = json['MSG_PRODUCT_CHECKOUT_63'];
    _msgproductcheckout64 = json['MSG_PRODUCT_CHECKOUT_64'];
    _msgproductcheckout65 = json['MSG_PRODUCT_CHECKOUT_65'];
    _msgproductcheckout66 = json['MSG_PRODUCT_CHECKOUT_66'];
    _msgproductcheckout67 = json['MSG_PRODUCT_CHECKOUT_67'];
    _msgproductcheckout68 = json['MSG_PRODUCT_CHECKOUT_68'];
    _msgoctitle = json['MSG_OC_TITLE'];
    _msgoc01 = json['MSG_OC_01'];
    _msgoc02 = json['MSG_OC_02'];
    _msgoc03 = json['MSG_OC_03'];
    _msgoc04 = json['MSG_OC_04'];
    _msgoc05 = json['MSG_OC_05'];
    _msgoc06 = json['MSG_OC_06'];
    _msgoc07 = json['MSG_OC_07'];
    _msgoc08 = json['MSG_OC_08'];
    _msgoc09 = json['MSG_OC_09'];
    _msgoc10 = json['MSG_OC_10'];
    _msgoc11 = json['MSG_OC_11'];
    _msgoc12 = json['MSG_OC_12'];
    _msgoc13 = json['MSG_OC_13'];
    _msgoc14 = json['MSG_OC_14'];
    _msgoc15 = json['MSG_OC_15'];
    _msgvotitle = json['MSG_VO_TITLE'];
    _msgvo01 = json['MSG_VO_01'];
    _msgvo02 = json['MSG_VO_02'];
    _msgvo03 = json['MSG_VO_03'];
    _msgvo04 = json['MSG_VO_04'];
    _msgvo05 = json['MSG_VO_05'];
    _msgvo06 = json['MSG_VO_06'];
    _msgvo07 = json['MSG_VO_07'];
    _msgvo08 = json['MSG_VO_08'];
    _msgvo09 = json['MSG_VO_09'];
    _msgvo10 = json['MSG_VO_10'];
    _msgvo11 = json['MSG_VO_11'];
    _msgvo12 = json['MSG_VO_12'];
    _msgvo13 = json['MSG_VO_13'];
    _msgvo14 = json['MSG_VO_14'];
    _msgvo15 = json['MSG_VO_15'];
    _msgvo16 = json['MSG_VO_16'];
    _msgvo17 = json['MSG_VO_17'];
    _msgvo18 = json['MSG_VO_18'];
    _msgvo19 = json['MSG_VO_19'];
    _msgvo20 = json['MSG_VO_20'];
    _msgcptitle = json['MSG_CP_TITLE'];
    _msgcp01 = json['MSG_CP_01'];
    _msgcp02 = json['MSG_CP_02'];
    _msgcp03 = json['MSG_CP_03'];
    _msgcp04 = json['MSG_CP_04'];
    _msgcp05 = json['MSG_CP_05'];
    _msgcp06 = json['MSG_CP_06'];
    _msgcp07 = json['MSG_CP_07'];
    _msgcp08 = json['MSG_CP_08'];
    _lblcpvalidation01 = json['LBL_CP_VALIDATION_01'];
    _lblcpvalidation02 = json['LBL_CP_VALIDATION_02'];
    _lblcpvalidation03 = json['LBL_CP_VALIDATION_03'];
    _lblcpvalidation04 = json['LBL_CP_VALIDATION_04'];
    _lblcpvalidation05 = json['LBL_CP_VALIDATION_05'];
    _msgcpinvalid = json['MSG_CP_INVALID'];
    _msgcpsuccess = json['MSG_CP_SUCCESS'];
    _msgcpfailed = json['MSG_CP_FAILED'];
    _msgmatitle = json['MSG_MA_TITLE'];
    _msgma01 = json['MSG_MA_01'];
    _msgma02 = json['MSG_MA_02'];
    _msgma03 = json['MSG_MA_03'];
    _msgma04 = json['MSG_MA_04'];
    _msgma05 = json['MSG_MA_05'];
    _msgma06 = json['MSG_MA_06'];
    _msgma07 = json['MSG_MA_07'];
    _msgma08 = json['MSG_MA_08'];
    _msgma09 = json['MSG_MA_09'];
    _msgma10 = json['MSG_MA_10'];
    _msgma11 = json['MSG_MA_11'];
    _msgma12 = json['MSG_MA_12'];
    _msgma13 = json['MSG_MA_13'];
    _msgma14 = json['MSG_MA_14'];
    _msgma15 = json['MSG_MA_15'];
    _msgma16 = json['MSG_MA_16'];
    _msgma17 = json['MSG_MA_17'];
    _msgma18 = json['MSG_MA_18'];
    _msgma19 = json['MSG_MA_19'];
    _msgma20 = json['MSG_MA_20'];
    _msgma21 = json['MSG_MA_21'];
    _msgma22 = json['MSG_MA_22'];
    _msgma23 = json['MSG_MA_23'];
    _lblmavalidation01 = json['LBL_MA_VALIDATION_01'];
    _lblmavalidation02 = json['LBL_MA_VALIDATION_02'];
    _lblmavalidation03 = json['LBL_MA_VALIDATION_03'];
    _lblmavalidation04 = json['LBL_MA_VALIDATION_04'];
    _lblmavalidation05 = json['LBL_MA_VALIDATION_05'];
    _lblmavalidation06 = json['LBL_MA_VALIDATION_06'];
    _lblmavalidation07 = json['LBL_MA_VALIDATION_07'];
    _msgaddressdetailssuccess = json['MSG_ADDRESS_DETAILS_SUCCESS'];
    _msgaddressdetailserror = json['MSG_ADDRESS_DETAILS_ERROR'];
    _msgwait = json['MSG_WAIT'];
    _msgcopy = json['MSG_COPY'];
    _msgsettingsuccess = json['MSG_SETTING_SUCCESS'];
    _msgsettingerror = json['MSG_SETTING_ERROR'];
    _msgiftitle = json['MSG_IF_TITLE'];
    _msgif01 = json['MSG_IF_01'];
    _msgif02 = json['MSG_IF_02'];
    _msgif03 = json['MSG_IF_03'];
    _msgif04 = json['MSG_IF_04'];
    _msgif05 = json['MSG_IF_05'];
    _msgif06 = json['MSG_IF_06'];
    _msgif07 = json['MSG_IF_07'];
    _msgif08 = json['MSG_IF_08'];
    _msgif09 = json['MSG_IF_09'];
    _msgfptitle = json['MSG_FP_TITLE'];
    _msgfp01 = json['MSG_FP_01'];
    _msgfp02 = json['MSG_FP_02'];
    _msgrptitle = json['MSG_RP_TITLE'];
    _msgrp01 = json['MSG_RP_01'];
    _msgrp02 = json['MSG_RP_02'];
    _msgbadrequest = json['MSG_BAD_REQUEST'];
    _shopchange01 = json['SHOP_CHANGE_01'];
    _shopchange02 = json['SHOP_CHANGE_02'];
    _shopchange03 = json['SHOP_CHANGE_03'];
    _shopchange04 = json['SHOP_CHANGE_04'];
    _shopchange05 = json['SHOP_CHANGE_05'];
    _shopchange06 = json['SHOP_CHANGE_06'];
    _msgusernotfound = json['MSG_USER_NOT_FOUND'];
    _msgordernotfound = json['MSG_ORDER_NOT_FOUND'];
    _msgorderdetailnotfound = json['MSG_ORDER_DETAIL_NOT_FOUND'];
    _rechargecheckoutbtnlbl = json['RECHARGE_CHECKOUT_BTN_LBL'];
    _lblalert = json['LBL_ALERT'];
    _enternumber = json['ENTER_NUMBER'];
    _operatordetected = json['OPERATOR_DETECTED'];
    _change = json['CHANGE'];
    _selectamount = json['SELECT_AMOUNT'];
    _havepromocode = json['HAVE_PROMO_CODE'];
    _pleaseselect = json['PLEASE_SELECT'];
    _errornumber = json['ERROR_NUMBER'];
    _characterlong = json['CHARACTER_LONG'];
    _packageerror = json['PACKAGE_ERROR'];
    _willreceived = json['WILL_RECEIVED'];
    _priceerrormax = json['PRICE_ERROR_MAX'];
    _priceerrormin = json['PRICE_ERROR_MIN'];
    _rechargestatus = json['RECHARGE_STATUS'];
    _becauseof = json['BECAUSE_OF'];
    _somewentwrong = json['SOME_WENT_WRONG'];
    _checkmobileno = json['CHECK_MOBILE_NO'];
    _needproduct = json['NEED_PRODUCT'];
    _purposeproduct = json['PURPOSE_PRODUCT'];
    _needproducthead = json['NEED_PRODUCT_HEAD'];
    _countrylabel = json['COUNTRY_LABEL'];
    _productplaceholder = json['PRODUCT_PLACEHOLDER'];
    _productdescplaceholder = json['PRODUCT_DESC_PLACEHOLDER'];
    _send = json['SEND'];
    _productsent = json['PRODUCT_SENT'];
    _productname = json['PRODUCT_NAME'];
    _productdesc = json['PRODUCT_DESC'];
    _productqty = json['PRODUCT_QTY'];
    _anywhere = json['ANY_WHERE'];
    _informavailability = json['INFORM_AVAILABILITY'];
    _proposecountrylabel = json['PROPOSE_COUNTRY_LABEL'];
    _proposecitylabel = json['PROPOSE_CITY_LABEL'];
    _proposechooseshop = json['PROPOSE_CHOOSE_SHOP'];
    _proposeemail = json['PROPOSE_EMAIL'];
    _proposetelephone = json['PROPOSE_TELEPHONE'];
    _footermenustories = json['FOOTER_MENU_STORIES'];
    _footermenupress = json['FOOTER_MENU_PRESS'];
    _footermenucareer = json['FOOTER_MENU_CAREER'];
    _footermenuhelp = json['FOOTER_MENU_HELP'];
    _footermenufaq = json['FOOTER_MENU_FAQ'];
    _checkouttransactiondetails = json['CHECKOUT_TRANSACTION_DETAILS'];
    _checkoutbenificierydetails = json['CHECKOUT_BENIFICIERY_DETAILS'];
    _rechargerecipientdetails = json['RECHARGE_RECIPIENT_DETAILS'];
    _checkoutpaymentmode = json['CHECKOUT_PAYMENT_MODE'];
    _pleaseselectdeliverymethod = json['PLEASE_SELECT_DELIVERY_METHOD'];
    _doyouhaveapromocode = json['DO_YOU_HAVE_A_PROMO_CODE'];
    _step2text = json['STEP_2_TEXT'];
    _shippingandhabdling = json['SHIPPING_AND_HABDLING'];
    _transactionaredeliveredonluunderpresentationofvalidcni =
        json['TRANSACTION_ARE_DELIVERED_ONLU_UNDER_PRESENTATION_OF_VALID_CNI'];
    _abetterwaytosendwhatreallymatter =
        json['A_BETTER_WAY_TO_SEND_WHAT_REALLY_MATTER'];
    _wedeliveredproductlovedonce = json['WE_DELIVERED_PRODUCT_LOVED_ONCE'];
    _selectarea = json['SELECT_AREA'];
    _buysafelyonline = json['BUY_SAFELY_ONLINE'];
    _getdelivery = json['GET_DELIVERY'];
    _selectareaandshop = json['SELECT_AREA_AND_SHOP'];
    _paysafely = json['PAY_SAFELY'];
    _benificieryrecieves = json['BENIFICIERY_RECIEVES'];
    _jointhousandsofhappyfamilies = json['JOIN_THOUSANDS_OF_HAPPY_FAMILIES'];
    _worldfamilyconnected = json['WORLD_FAMILY_CONNECTED'];
    _satisfiedusers = json['SATISFIED_USERS'];
    _whychoosefamilov = json['WHY_CHOOSE_FAMILOV'];
    _discoveredwhyouruserlovedus = json['DISCOVERED_WHY_OUR_USER_LOVED_US'];
    _secure = json['100%_SECURE'];
    _weusesslcertificate = json['WE_USE_SSL_CERTIFICATE'];
    _yourmoneyiswellused = json['YOUR_MONEY_IS_WELL_USED'];
    _sendmore = json['SEND_MORE'];
    _easytouse = json['EASY_TO_USE'];
    _positiveimpact = json['POSITIVE_IMPACT'];
    _socialaspect = json['SOCIAL_ASPECT'];
    _giranteemoney = json['GIRANTEE_MONEY'];
    _savetraditionalmoney = json['SAVE_TRADITIONAL_MONEY'];
    _familovisveryeasytouse = json['FAMILOV_IS_VERY_EASY_TO_USE'];
    _weimprovepeoplelives = json['WE_IMPROVE_PEOPLE_LIVES'];
    _wehelpyoutohelppeople = json['WE_HELP_YOU_TO_HELP_PEOPLE'];
    _ourservices = json['OUR_SERVICES'];
    _morewaystosendwhatreallymatter =
        json['MORE_WAYS_TO SEND_WHAT_REALLY_MATTER'];
    _fooddrinkgrocery = json['FOOD_DRINK_GROCERY'];
    _fooddrinkgrocerydesc = json['FOOD_DRINK_GROCERY_DESC'];
    _mobiletopup = json['MOBILE_TOP_UP'];
    _mobiletopupdesc = json['MOBILE_TOP_UP_DESC'];
    _healthpaharmacy = json['HEALTH_PAHARMACY'];
    _healthpaharmacydesc = json['HEALTH_PAHARMACY_DESC'];
    _educationformation = json['EDUCATION_FORMATION'];
    _educationformationdesc = json['EDUCATION_FORMATION_DESC'];
    _construction = json['CONSTRUCTION'];
    _constructiondesc = json['CONSTRUCTION_DESC'];
    _mobilityenergy = json['MOBILITY_ENERGY'];
    _mobilityenergydesc = json['MOBILITY_ENERGY_DESC'];
    _signupforfree = json['SIGN_UP_FOR_FREE'];
    _becomepartner = json['BECOME_PARTNER'];
    _howtogiveback = json['HOW_TO_GIVE_BACK'];
    _webelivethatgoinggood = json['WE_BELIVE_THAT_GOING_GOOD'];
    _learnmore = json['LEARN_MORE'];
    _subscribetofamilov = json['SUBSCRIBE_TO_FAMILOV'];
    _amazingoffersupdatesinterestingnews =
        json['AMAZING_OFFERS_UPDATES_INTERESTING_NEWS'];
    _enterhereanyplaceindications = json['ENTER_HERE_ANY_PLACE_INDICATIONS'];
    _bravoyourpackageisready = json['BRAVO_YOUR_PACKAGE_IS_READY'];
    _pleaseselectthecountry = json['PLEASE_SELECT_THE_COUNTRY'];
    _usefamilovandstayconnectedwithlovedonce =
        json['USE_FAMILOV_AND_STAY_CONNECTED_WITH_LOVED_ONCE'];
    _selectthecountry = json['SELECT_THE_COUNTRY'];
    _bemoreefficientthantraditional =
        json['BE_MORE_EFFICIENT_THAN_TRADITIONAL'];
    _iliketogiveahelpinghandinthecountry =
        json['I_LIKE_TO_GIVE_A_HELPING_HAND_IN_THE_COUNTRY'];
    _helpustohelp = json['HELP_US_TO_HELP'];
    _go = json['GO'];
    _loveourapp = json['LOVE_OUR_APP'];
    _legal = json['LEGAL'];
    _termsandcondition = json['TERMS_AND_CONDITION'];
    _forpatners = json['FOR_PATNERS'];
    _mobileinputplaceholder = json['MOBILE_INPUT_PLACEHOLDER'];
    _submitproduct = json['SUBMIT_PRODUCT'];
    _rechargemaintitle = json['RECHARGE_MAIN_TITLE'];
    _rechargetitledesctiption = json['RECHARGE_TITLE_DESCTIPTION'];
    _rechargestep1title = json['RECHARGE_STEP_1_TITLE'];
    _rechargestep1 = json['RECHARGE_STEP_1'];
    _rechargestep2title = json['RECHARGE_STEP_2_TITLE'];
    _rechargestep2 = json['RECHARGE_STEP_2'];
    _rechargestep3title = json['RECHARGE_STEP_3_TITLE'];
    _rechargestep3 = json['RECHARGE_STEP_3'];
    _rechargebannertitle = json['RECHARGE_BANNER_TITLE'];
    _rechargebannerdescription = json['RECHARGE_BANNER_DESCRIPTION'];
    _rechargesearchplaceholder = json['RECHARGE_SEARCH_PLACEHOLDER'];
    _newtofamilov = json['NEW_TO_FAMILOV'];
    _takelessthanminutes = json['TAKE_LESS_THAN_MINUTES'];
    _signinwithfacebook = json['SIGN_IN_WITH_FACEBOOK'];
    _signinwithgoogle = json['SIGN_IN_WITH_GOOGLE'];
    _or = json['OR'];
    _signupwithfacebook = json['SIGN_UP_WITH_FACEBOOK'];
    _signupwithgoogle = json['SIGN_UP_WITH_GOOGLE'];
    _signuptermsandcondition = json['SIGNUP_TERMS_AND_CONDITION'];
    _signupcondition = json['SIGNUP_CONDITION'];
    _noproductfound = json['NO_PRODUCT_FOUND'];
    _subscribetogetbestofferes = json['SUBSCRIBE_TO_GET_BEST_OFFERES'];
    _viewmycart = json['VIEW_MY_CART'];
    _dlbill = json['DL_BILL'];
    _dlorddets = json['DL_ORD_DETS'];
    _selectbenificerydetails = json['SELECT_BENIFICERY_DETAILS'];
    _addnewreciepent = json['ADD_NEW_RECIEPENT'];
    _easyisnotit = json['EASY_IS_NOT_IT'];
    _selectpaymentmethod = json['SELECT_PAYMENT_METHOD'];
    _personalmessage = json['PERSONAL_MESSAGE'];
    _doyouwanttoleave = json['DO_YOU_WANT_TO_LEAVE'];
    _accept = json['ACCEPT'];
    _securecheckout = json['SECURE_CHECKOUT'];
    _satisfactionguaranteed = json['SATISFACTION_GUARANTEED'];
    _privacyprotected = json['PRIVACY_PROTECTED'];
    _selectreciepent = json['SELECT_RECIEPENT'];
    _addreciepent = json['ADD_RECIEPENT'];
    _myreciepent = json['MY_RECIEPENT'];
    _mywishlist = json['MY_WISHLIST'];
    _reciepentname = json['RECIEPENT_NAME'];
    _reciepentmainno = json['RECIEPENT_MAIN_NO'];
    _reciepentanotherno = json['RECIEPENT_ANOTHER_NO'];
    _pleaseenterfullnameindicatedonhiscni =
        json['PLEASE_ENTER_FULL_NAME_INDICATED_ON_HIS_CNI'];
    _edit = json['EDIT'];
    _lblmyacct01 = json['LBL_MY_ACCT_01'];
    _lblmyacct02 = json['LBL_MY_ACCT_02'];
    _lblmyacct03 = json['LBL_MY_ACCT_03'];
    _lblmyacct04 = json['LBL_MY_ACCT_04'];
    _lblmyacct05 = json['LBL_MY_ACCT_05'];
    _lblmyacct06 = json['LBL_MY_ACCT_06'];
    _lblmyacct07 = json['LBL_MY_ACCT_07'];
    _lblmyacct08 = json['LBL_MY_ACCT_08'];
    _lblmyacct09 = json['LBL_MY_ACCT_09'];
    _lblmyacct10 = json['LBL_MY_ACCT_10'];
    _lblmyacct11 = json['LBL_MY_ACCT_11'];
    _lblmyacct12 = json['LBL_MY_ACCT_12'];
    _lblmyacct13 = json['LBL_MY_ACCT_13'];
    _lblmyacct14 = json['LBL_MY_ACCT_14'];
    _lblmyacct15 = json['LBL_MY_ACCT_15'];
    _checkoutcontinue = json['CHECKOUT_CONTINUE'];
    _checkoutback = json['CHECKOUT_BACK'];
    _orderconfirmation1 = json['ORDER_CONFIRMATION_1'];
    _lblproductsortmostsold = json['LBL_PRODUCT_SORT_MOST_SOLD'];
    _lblproductsortleastsold = json['LBL_PRODUCT_SORT_LEAST_SOLD'];
    _lblproductsortpricelow = json['LBL_PRODUCT_SORT_PRICE_LOW'];
    _lblproductsortpricehigh = json['LBL_PRODUCT_SORT_PRICE_HIGH'];
    _lblproductsortrecentsold = json['LBL_PRODUCT_SORT_RECENT_SOLD'];
  }
  String? _lblmenu01;
  String? _lblmenu02;
  String? _lblmenu021;
  String? _lblmenu03;
  String? _lblmenu04;
  String? _lblmenu05;
  String? _lblmenu06;
  String? _lblmenu07;
  String? _lblmenu08;
  String? _lblmenu09;
  String? _lblmenu10;
  String? _lblmenu11;
  String? _lblmenu12;
  String? _lblmenu13;
  String? _lblmenu14;
  String? _lblmenu15;
  String? _lblmenu16;
  String? _lblmenu17;
  String? _lblmenu18;
  String? _lblmenu19;
  String? _lblmenu20;
  String? _lblmenu21;
  String? _lblmenu22;
  String? _lblmenu23;
  String? _lblmenu24;
  String? _lblmenu25;
  String? _lblsocialfacebook;
  String? _lblsocialtwitter;
  String? _lblfooter01;
  String? _lblfooter02;
  String? _welcomeMessage;
  String? _lblherotwo;
  String? _lblherothree;
  String? _lblreceivercountry;
  String? _lblreceivercity;
  String? _lblchooseshop;
  String? _lblreceivergo;
  String? _lblsearchvalidationcountry;
  String? _lblsearchvalidationcity;
  String? _lblsearchvalidationshop;
  String? _lblsearchcitylist;
  String? _lblsearchshoplist;
  String? _lblreceiverstarted;
  String? _lblhome01;
  String? _lblhome02;
  String? _lblhome03;
  String? _lblhome04;
  String? _lblhome05;
  String? _lblhome06;
  String? _lblhome07;
  String? _lblhome08;
  String? _lblhome09;
  String? _lblhome10;
  String? _lblhome11;
  String? _lblhome12;
  String? _lblhome13;
  String? _lblhome14;
  String? _lblhome15;
  String? _lblhome16;
  String? _lblhowitworks01;
  String? _lblhowitworks02;
  String? _lblhowitworks03;
  String? _lblhowitworks04;
  String? _lblhowitworks05;
  String? _lblhowitworks06;
  String? _lblhowitworks07;
  String? _lblhowitworks08;
  String? _lblhowitworks09;
  String? _lblhowitworks10;
  String? _lblhowitworks11;
  String? _lbltestimonial01;
  String? _lbltestimonial02;
  String? _lbltestimonialperson101;
  String? _lbltestimonialperson102;
  String? _lbltestimonialperson103;
  String? _lbltestimonialperson201;
  String? _lbltestimonialperson202;
  String? _lbltestimonialperson203;
  String? _lbltestimonialperson301;
  String? _lbltestimonialperson302;
  String? _lbltestimonialperson303;
  String? _pagetitlefaq;
  String? _lblfaq01;
  String? _lblfaq02;
  String? _lblfaq03;
  String? _pagetitlehiw;
  String? _lblhiw01;
  String? _lblhiw02;
  String? _lblhiw03;
  String? _lblhiw04;
  String? _lblhiw05;
  String? _lblhiw06;
  String? _lblhiw07;
  String? _lblhiw08;
  String? _lblhiw09;
  String? _lblhiw10;
  String? _lblhiw11;
  String? _lblhiw12;
  String? _lblhiw13;
  String? _lblhiw14;
  String? _lblhiw15;
  String? _lblhiw16;
  String? _lblhiw17;
  String? _lblhiw18;
  String? _lblhiw19;
  String? _lblhiw20;
  String? _lblhiw21;
  String? _lblhiw22;
  String? _lblhiw23;
  String? _lblhiw24;
  String? _lblhiw25;
  String? _lblhiw26;
  String? _lblhiw27;
  String? _lblhiw28;
  String? _pagetitleterms;
  String? _lblterms01;
  String? _pagetitleprivacy;
  String? _lblprivacypolicy01;
  String? _pagetitlecookie;
  String? _lblcookiepolicy01;
  String? _pagetitleaboutus;
  String? _lblaboutus01;
  String? _pagetitlecontactus;
  String? _lblcontactus01;
  String? _lblrecharge01;
  String? _pagetitlepress;
  String? _lblpress01;
  String? _lblpress02;
  String? _lblpress03;
  String? _pagetitlejobs;
  String? _lbljobs01;
  String? _lbljobs02;
  String? _pagetitlesignup;
  String? _lblsignup01;
  String? _lblsignup02;
  String? _lblsignup03;
  String? _lblsignup04;
  String? _lblsignup05;
  String? _lblsignup06;
  String? _lblsignup07;
  String? _lblsignup08;
  String? _lblsignup09;
  String? _lblsignup10;
  String? _lblsignup11;
  String? _lblsignup12;
  String? _lblsignup13;
  String? _lblsignupvalidation01;
  String? _lblsignupvalidation02;
  String? _lblsignupvalidation03;
  String? _lblsignupvalidation04;
  String? _lblsignupvalidation05;
  String? _lblsignupvalidation06;
  String? _lblsignupvalidation07;
  String? _lblsignupvalidation08;
  String? _lblsignupvalidation09;
  String? _lblsignupvalidation10;
  String? _msgsignupemailexist;
  String? _msgsignupuserexist;
  String? _msgsignupsuccess;
  String? _msgsignupfailed;
  String? _msgdatanotfound;
  String? _msgfree2lov;
  String? _pagetitlelogin;
  String? _lbllogin01;
  String? _lbllogin02;
  String? _lbllogin03;
  String? _lbllogin04;
  String? _lbllogin05;
  String? _lbllogin06;
  String? _lbllogin07;
  String? _lbllogin08;
  String? _lbllogin09;
  String? _lbllogin10;
  String? _lbllogin11;
  String? _lbllogin12;
  String? _msglogininvalid;
  String? _msgloginstatusinactive;
  String? _msgforgotpasswordinvalid;
  String? _msgforgotpasswordsuccess;
  String? _msgforgotpasswordinactive;
  String? _msgresetpasswordmissing;
  String? _msgresetpasswordinvalid;
  String? _msgresetpasswordsuccess;
  String? _msgresetpasswordinactive;
  String? _msgemailinvalid;
  String? _msgloginsuccess;
  String? _msglogoutsuccess;
  String? _msgmailheadertitle;
  String? _msgmailcopyrighttext;
  String? _msgmailfooterteam;
  String? _msgproducttitle;
  String? _msgproduct01;
  String? _msgproduct02;
  String? _msgproduct03;
  String? _msgproduct02notavailable;
  String? _msgproduct04;
  String? _msgproduct05;
  String? _msgproduct06;
  String? _msgproduct07;
  String? _msgproduct08;
  String? _msgproduct09;
  String? _msgproductcartaddsuccess;
  String? _msgproductcartaddfailed;
  String? _msgproductwishlistaddfailed;
  String? _msgproduct10;
  String? _msgproduct11;
  String? _msgproduct12;
  String? _msgproduct13;
  String? _msgproductdetailtitle;
  String? _msgproductdetail01;
  String? _msgproductdetail02;
  String? _msgproductdetail03;
  String? _msgproductdetail04;
  String? _msgproductdetail05;
  String? _msgproductcarttitle;
  String? _msgproductcart01;
  String? _msgproductcart02;
  String? _msgproductcart03;
  String? _msgproductcart04;
  String? _msgproductcart05;
  String? _msgproductcart06;
  String? _msgproductcart07;
  String? _msgproductcart08;
  String? _msgproductcart09;
  String? _msgproductcart10;
  String? _msgproductcart11;
  String? _msgproductcart12;
  String? _msgproductcart13;
  String? _msgproductcart14;
  String? _msgproductcart15;
  String? _msgproductcart16;
  String? _msgproductcart17;
  String? _msgproductcart18;
  String? _msgproductcart19;
  String? _msgproductcart20;
  String? _msgproductcart21;
  String? _msgproductcart22;
  String? _msgproductcart23;
  String? _msgproductcart24;
  String? _msgproductcart25;
  String? _msgproductcart26;
  String? _msgproductcheckouttitle;
  String? _msgproductcheckout01;
  String? _msgproductcheckout02;
  String? _msgproductcheckout03;
  String? _msgproductcheckout04;
  String? _msgproductcheckout05;
  String? _msgproductcheckout06;
  String? _msgproductcheckout07;
  String? _msgproductcheckout08;
  String? _msgproductcheckout09;
  String? _msgproductcheckout10;
  String? _msgproductcheckout11;
  String? _msgproductcheckout12;
  String? _msgproductcheckout13;
  String? _msgproductcheckout14;
  String? _msgproductcheckout15;
  String? _msgproductcheckout16;
  String? _msgproductcheckout17;
  String? _msgproductcheckout18;
  String? _msgproductcheckout19;
  String? _msgproductcheckout20;
  String? _msgproductcheckout21;
  String? _msgproductcheckout22;
  String? _msgproductcheckout23;
  String? _msgproductcheckout24;
  String? _msgproductcheckout25;
  String? _msgproductcheckout26;
  String? _msgproductcheckout27;
  String? _msgproductcheckout28;
  String? _msgproductcheckout29;
  String? _msgproductcheckout30;
  String? _msgproductcheckout31;
  String? _msgproductcheckout32;
  String? _msgproductcheckout33;
  String? _msgproductcheckout34;
  String? _msgproductcheckout35;
  String? _msgproductcheckout36;
  String? _msgproductcheckout37;
  String? _msgproductcheckout38;
  String? _msgproductcheckout39;
  String? _msgproductcheckout40;
  String? _msgproductcheckout41;
  String? _msgproductcheckout42;
  String? _msgproductcheckout43;
  String? _msgproductcheckout44;
  String? _msgproductcheckout45;
  String? _msgproductcheckout46;
  String? _msgproductcheckout47;
  String? _msgproductcheckout48;
  String? _msgproductcheckout49;
  String? _msgproductcheckout50;
  String? _msgproductcheckout51;
  String? _msgproductcheckout52;
  String? _msgproductcheckout53;
  String? _msgproductcheckout54;
  String? _msgproductcheckout55;
  String? _msgproductcheckout56;
  String? _msgproductcheckout57;
  String? _msgproductcheckout58;
  String? _msgproductcheckout59;
  String? _msgproductcheckout60;
  String? _msgproductcheckout61;
  String? _msgproductcheckout62;
  String? _msgproductcheckout63;
  String? _msgproductcheckout64;
  String? _msgproductcheckout65;
  String? _msgproductcheckout66;
  String? _msgproductcheckout67;
  String? _msgproductcheckout68;
  String? _msgoctitle;
  String? _msgoc01;
  String? _msgoc02;
  String? _msgoc03;
  String? _msgoc04;
  String? _msgoc05;
  String? _msgoc06;
  String? _msgoc07;
  String? _msgoc08;
  String? _msgoc09;
  String? _msgoc10;
  String? _msgoc11;
  String? _msgoc12;
  String? _msgoc13;
  String? _msgoc14;
  String? _msgoc15;
  String? _msgvotitle;
  String? _msgvo01;
  String? _msgvo02;
  String? _msgvo03;
  String? _msgvo04;
  String? _msgvo05;
  String? _msgvo06;
  String? _msgvo07;
  String? _msgvo08;
  String? _msgvo09;
  String? _msgvo10;
  String? _msgvo11;
  String? _msgvo12;
  String? _msgvo13;
  String? _msgvo14;
  String? _msgvo15;
  String? _msgvo16;
  String? _msgvo17;
  String? _msgvo18;
  String? _msgvo19;
  String? _msgvo20;
  String? _msgcptitle;
  String? _msgcp01;
  String? _msgcp02;
  String? _msgcp03;
  String? _msgcp04;
  String? _msgcp05;
  String? _msgcp06;
  String? _msgcp07;
  String? _msgcp08;
  String? _lblcpvalidation01;
  String? _lblcpvalidation02;
  String? _lblcpvalidation03;
  String? _lblcpvalidation04;
  String? _lblcpvalidation05;
  String? _msgcpinvalid;
  String? _msgcpsuccess;
  String? _msgcpfailed;
  String? _msgmatitle;
  String? _msgma01;
  String? _msgma02;
  String? _msgma03;
  String? _msgma04;
  String? _msgma05;
  String? _msgma06;
  String? _msgma07;
  String? _msgma08;
  String? _msgma09;
  String? _msgma10;
  String? _msgma11;
  String? _msgma12;
  String? _msgma13;
  String? _msgma14;
  String? _msgma15;
  String? _msgma16;
  String? _msgma17;
  String? _msgma18;
  String? _msgma19;
  String? _msgma20;
  String? _msgma21;
  String? _msgma22;
  String? _msgma23;
  String? _lblmavalidation01;
  String? _lblmavalidation02;
  String? _lblmavalidation03;
  String? _lblmavalidation04;
  String? _lblmavalidation05;
  String? _lblmavalidation06;
  String? _lblmavalidation07;
  String? _msgaddressdetailssuccess;
  String? _msgaddressdetailserror;
  String? _msgwait;
  String? _msgcopy;
  String? _msgsettingsuccess;
  String? _msgsettingerror;
  String? _msgiftitle;
  String? _msgif01;
  String? _msgif02;
  String? _msgif03;
  String? _msgif04;
  String? _msgif05;
  String? _msgif06;
  String? _msgif07;
  String? _msgif08;
  String? _msgif09;
  String? _msgfptitle;
  String? _msgfp01;
  String? _msgfp02;
  String? _msgrptitle;
  String? _msgrp01;
  String? _msgrp02;
  String? _msgbadrequest;
  String? _shopchange01;
  String? _shopchange02;
  String? _shopchange03;
  String? _shopchange04;
  String? _shopchange05;
  String? _shopchange06;
  String? _msgusernotfound;
  String? _msgordernotfound;
  String? _msgorderdetailnotfound;
  String? _rechargecheckoutbtnlbl;
  String? _lblalert;
  String? _enternumber;
  String? _operatordetected;
  String? _change;
  String? _selectamount;
  String? _havepromocode;
  String? _pleaseselect;
  String? _errornumber;
  String? _characterlong;
  String? _packageerror;
  String? _willreceived;
  String? _priceerrormax;
  String? _priceerrormin;
  String? _rechargestatus;
  String? _becauseof;
  String? _somewentwrong;
  String? _checkmobileno;
  String? _needproduct;
  String? _purposeproduct;
  String? _needproducthead;
  String? _countrylabel;
  String? _productplaceholder;
  String? _productdescplaceholder;
  String? _send;
  String? _productsent;
  String? _productname;
  String? _productdesc;
  String? _productqty;
  String? _anywhere;
  String? _informavailability;
  String? _proposecountrylabel;
  String? _proposecitylabel;
  String? _proposechooseshop;
  String? _proposeemail;
  String? _proposetelephone;
  String? _footermenustories;
  String? _footermenupress;
  String? _footermenucareer;
  String? _footermenuhelp;
  String? _footermenufaq;
  String? _checkouttransactiondetails;
  String? _checkoutbenificierydetails;
  String? _rechargerecipientdetails;
  String? _checkoutpaymentmode;
  String? _pleaseselectdeliverymethod;
  String? _doyouhaveapromocode;
  String? _step2text;
  String? _shippingandhabdling;
  String? _transactionaredeliveredonluunderpresentationofvalidcni;
  String? _abetterwaytosendwhatreallymatter;
  String? _wedeliveredproductlovedonce;
  String? _selectarea;
  String? _buysafelyonline;
  String? _getdelivery;
  String? _selectareaandshop;
  String? _paysafely;
  String? _benificieryrecieves;
  String? _jointhousandsofhappyfamilies;
  String? _worldfamilyconnected;
  String? _satisfiedusers;
  String? _whychoosefamilov;
  String? _discoveredwhyouruserlovedus;
  String? _secure;
  String? _weusesslcertificate;
  String? _yourmoneyiswellused;
  String? _sendmore;
  String? _easytouse;
  String? _positiveimpact;
  String? _socialaspect;
  String? _giranteemoney;
  String? _savetraditionalmoney;
  String? _familovisveryeasytouse;
  String? _weimprovepeoplelives;
  String? _wehelpyoutohelppeople;
  String? _ourservices;
  String? _morewaystosendwhatreallymatter;
  String? _fooddrinkgrocery;
  String? _fooddrinkgrocerydesc;
  String? _mobiletopup;
  String? _mobiletopupdesc;
  String? _healthpaharmacy;
  String? _healthpaharmacydesc;
  String? _educationformation;
  String? _educationformationdesc;
  String? _construction;
  String? _constructiondesc;
  String? _mobilityenergy;
  String? _mobilityenergydesc;
  String? _signupforfree;
  String? _becomepartner;
  String? _howtogiveback;
  String? _webelivethatgoinggood;
  String? _learnmore;
  String? _subscribetofamilov;
  String? _amazingoffersupdatesinterestingnews;
  String? _enterhereanyplaceindications;
  String? _bravoyourpackageisready;
  String? _pleaseselectthecountry;
  String? _usefamilovandstayconnectedwithlovedonce;
  String? _selectthecountry;
  String? _bemoreefficientthantraditional;
  String? _iliketogiveahelpinghandinthecountry;
  String? _helpustohelp;
  String? _go;
  String? _loveourapp;
  String? _legal;
  String? _termsandcondition;
  String? _forpatners;
  String? _mobileinputplaceholder;
  String? _submitproduct;
  String? _rechargemaintitle;
  String? _rechargetitledesctiption;
  String? _rechargestep1title;
  String? _rechargestep1;
  String? _rechargestep2title;
  String? _rechargestep2;
  String? _rechargestep3title;
  String? _rechargestep3;
  String? _rechargebannertitle;
  String? _rechargebannerdescription;
  String? _rechargesearchplaceholder;
  String? _newtofamilov;
  String? _takelessthanminutes;
  String? _signinwithfacebook;
  String? _signinwithgoogle;
  String? _or;
  String? _signupwithfacebook;
  String? _signupwithgoogle;
  String? _signuptermsandcondition;
  String? _signupcondition;
  String? _noproductfound;
  String? _subscribetogetbestofferes;
  String? _viewmycart;
  String? _dlbill;
  String? _dlorddets;
  String? _selectbenificerydetails;
  String? _addnewreciepent;
  String? _easyisnotit;
  String? _selectpaymentmethod;
  String? _personalmessage;
  String? _doyouwanttoleave;
  String? _accept;
  String? _securecheckout;
  String? _satisfactionguaranteed;
  String? _privacyprotected;
  String? _selectreciepent;
  String? _addreciepent;
  String? _myreciepent;
  String? _mywishlist;
  String? _reciepentname;
  String? _reciepentmainno;
  String? _reciepentanotherno;
  String? _pleaseenterfullnameindicatedonhiscni;
  String? _edit;
  String? _lblmyacct01;
  String? _lblmyacct02;
  String? _lblmyacct03;
  String? _lblmyacct04;
  String? _lblmyacct05;
  String? _lblmyacct06;
  String? _lblmyacct07;
  String? _lblmyacct08;
  String? _lblmyacct09;
  String? _lblmyacct10;
  String? _lblmyacct11;
  String? _lblmyacct12;
  String? _lblmyacct13;
  String? _lblmyacct14;
  String? _lblmyacct15;
  String? _checkoutcontinue;
  String? _checkoutback;
  String? _orderconfirmation1;
  String? _lblproductsortmostsold;
  String? _lblproductsortleastsold;
  String? _lblproductsortpricelow;
  String? _lblproductsortpricehigh;
  String? _lblproductsortrecentsold;

  String? get lblmenu01 => _lblmenu01;
  String? get lblmenu02 => _lblmenu02;
  String? get lblmenu021 => _lblmenu021;
  String? get lblmenu03 => _lblmenu03;
  String? get lblmenu04 => _lblmenu04;
  String? get lblmenu05 => _lblmenu05;
  String? get lblmenu06 => _lblmenu06;
  String? get lblmenu07 => _lblmenu07;
  String? get lblmenu08 => _lblmenu08;
  String? get lblmenu09 => _lblmenu09;
  String? get lblmenu10 => _lblmenu10;
  String? get lblmenu11 => _lblmenu11;
  String? get lblmenu12 => _lblmenu12;
  String? get lblmenu13 => _lblmenu13;
  String? get lblmenu14 => _lblmenu14;
  String? get lblmenu15 => _lblmenu15;
  String? get lblmenu16 => _lblmenu16;
  String? get lblmenu17 => _lblmenu17;
  String? get lblmenu18 => _lblmenu18;
  String? get lblmenu19 => _lblmenu19;
  String? get lblmenu20 => _lblmenu20;
  String? get lblmenu21 => _lblmenu21;
  String? get lblmenu22 => _lblmenu22;
  String? get lblmenu23 => _lblmenu23;
  String? get lblmenu24 => _lblmenu24;
  String? get lblmenu25 => _lblmenu25;
  String? get lblsocialfacebook => _lblsocialfacebook;
  String? get lblsocialtwitter => _lblsocialtwitter;
  String? get lblfooter01 => _lblfooter01;
  String? get lblfooter02 => _lblfooter02;
  String? get welcomeMessage => _welcomeMessage;
  String? get lblherotwo => _lblherotwo;
  String? get lblherothree => _lblherothree;
  String? get lblreceivercountry => _lblreceivercountry;
  String? get lblreceivercity => _lblreceivercity;
  String? get lblchooseshop => _lblchooseshop;
  String? get lblreceivergo => _lblreceivergo;
  String? get lblsearchvalidationcountry => _lblsearchvalidationcountry;
  String? get lblsearchvalidationcity => _lblsearchvalidationcity;
  String? get lblsearchvalidationshop => _lblsearchvalidationshop;
  String? get lblsearchcitylist => _lblsearchcitylist;
  String? get lblsearchshoplist => _lblsearchshoplist;
  String? get lblreceiverstarted => _lblreceiverstarted;
  String? get lblhome01 => _lblhome01;
  String? get lblhome02 => _lblhome02;
  String? get lblhome03 => _lblhome03;
  String? get lblhome04 => _lblhome04;
  String? get lblhome05 => _lblhome05;
  String? get lblhome06 => _lblhome06;
  String? get lblhome07 => _lblhome07;
  String? get lblhome08 => _lblhome08;
  String? get lblhome09 => _lblhome09;
  String? get lblhome10 => _lblhome10;
  String? get lblhome11 => _lblhome11;
  String? get lblhome12 => _lblhome12;
  String? get lblhome13 => _lblhome13;
  String? get lblhome14 => _lblhome14;
  String? get lblhome15 => _lblhome15;
  String? get lblhome16 => _lblhome16;
  String? get lblhowitworks01 => _lblhowitworks01;
  String? get lblhowitworks02 => _lblhowitworks02;
  String? get lblhowitworks03 => _lblhowitworks03;
  String? get lblhowitworks04 => _lblhowitworks04;
  String? get lblhowitworks05 => _lblhowitworks05;
  String? get lblhowitworks06 => _lblhowitworks06;
  String? get lblhowitworks07 => _lblhowitworks07;
  String? get lblhowitworks08 => _lblhowitworks08;
  String? get lblhowitworks09 => _lblhowitworks09;
  String? get lblhowitworks10 => _lblhowitworks10;
  String? get lblhowitworks11 => _lblhowitworks11;
  String? get lbltestimonial01 => _lbltestimonial01;
  String? get lbltestimonial02 => _lbltestimonial02;
  String? get lbltestimonialperson101 => _lbltestimonialperson101;
  String? get lbltestimonialperson102 => _lbltestimonialperson102;
  String? get lbltestimonialperson103 => _lbltestimonialperson103;
  String? get lbltestimonialperson201 => _lbltestimonialperson201;
  String? get lbltestimonialperson202 => _lbltestimonialperson202;
  String? get lbltestimonialperson203 => _lbltestimonialperson203;
  String? get lbltestimonialperson301 => _lbltestimonialperson301;
  String? get lbltestimonialperson302 => _lbltestimonialperson302;
  String? get lbltestimonialperson303 => _lbltestimonialperson303;
  String? get pagetitlefaq => _pagetitlefaq;
  String? get lblfaq01 => _lblfaq01;
  String? get lblfaq02 => _lblfaq02;
  String? get lblfaq03 => _lblfaq03;
  String? get pagetitlehiw => _pagetitlehiw;
  String? get lblhiw01 => _lblhiw01;
  String? get lblhiw02 => _lblhiw02;
  String? get lblhiw03 => _lblhiw03;
  String? get lblhiw04 => _lblhiw04;
  String? get lblhiw05 => _lblhiw05;
  String? get lblhiw06 => _lblhiw06;
  String? get lblhiw07 => _lblhiw07;
  String? get lblhiw08 => _lblhiw08;
  String? get lblhiw09 => _lblhiw09;
  String? get lblhiw10 => _lblhiw10;
  String? get lblhiw11 => _lblhiw11;
  String? get lblhiw12 => _lblhiw12;
  String? get lblhiw13 => _lblhiw13;
  String? get lblhiw14 => _lblhiw14;
  String? get lblhiw15 => _lblhiw15;
  String? get lblhiw16 => _lblhiw16;
  String? get lblhiw17 => _lblhiw17;
  String? get lblhiw18 => _lblhiw18;
  String? get lblhiw19 => _lblhiw19;
  String? get lblhiw20 => _lblhiw20;
  String? get lblhiw21 => _lblhiw21;
  String? get lblhiw22 => _lblhiw22;
  String? get lblhiw23 => _lblhiw23;
  String? get lblhiw24 => _lblhiw24;
  String? get lblhiw25 => _lblhiw25;
  String? get lblhiw26 => _lblhiw26;
  String? get lblhiw27 => _lblhiw27;
  String? get lblhiw28 => _lblhiw28;
  String? get pagetitleterms => _pagetitleterms;
  String? get lblterms01 => _lblterms01;
  String? get pagetitleprivacy => _pagetitleprivacy;
  String? get lblprivacypolicy01 => _lblprivacypolicy01;
  String? get pagetitlecookie => _pagetitlecookie;
  String? get lblcookiepolicy01 => _lblcookiepolicy01;
  String? get pagetitleaboutus => _pagetitleaboutus;
  String? get lblaboutus01 => _lblaboutus01;
  String? get pagetitlecontactus => _pagetitlecontactus;
  String? get lblcontactus01 => _lblcontactus01;
  String? get lblrecharge01 => _lblrecharge01;
  String? get pagetitlepress => _pagetitlepress;
  String? get lblpress01 => _lblpress01;
  String? get lblpress02 => _lblpress02;
  String? get lblpress03 => _lblpress03;
  String? get pagetitlejobs => _pagetitlejobs;
  String? get lbljobs01 => _lbljobs01;
  String? get lbljobs02 => _lbljobs02;
  String? get pagetitlesignup => _pagetitlesignup;
  String? get lblsignup01 => _lblsignup01;
  String? get lblsignup02 => _lblsignup02;
  String? get lblsignup03 => _lblsignup03;
  String? get lblsignup04 => _lblsignup04;
  String? get lblsignup05 => _lblsignup05;
  String? get lblsignup06 => _lblsignup06;
  String? get lblsignup07 => _lblsignup07;
  String? get lblsignup08 => _lblsignup08;
  String? get lblsignup09 => _lblsignup09;
  String? get lblsignup10 => _lblsignup10;
  String? get lblsignup11 => _lblsignup11;
  String? get lblsignup12 => _lblsignup12;
  String? get lblsignup13 => _lblsignup13;
  String? get lblsignupvalidation01 => _lblsignupvalidation01;
  String? get lblsignupvalidation02 => _lblsignupvalidation02;
  String? get lblsignupvalidation03 => _lblsignupvalidation03;
  String? get lblsignupvalidation04 => _lblsignupvalidation04;
  String? get lblsignupvalidation05 => _lblsignupvalidation05;
  String? get lblsignupvalidation06 => _lblsignupvalidation06;
  String? get lblsignupvalidation07 => _lblsignupvalidation07;
  String? get lblsignupvalidation08 => _lblsignupvalidation08;
  String? get lblsignupvalidation09 => _lblsignupvalidation09;
  String? get lblsignupvalidation10 => _lblsignupvalidation10;
  String? get msgsignupemailexist => _msgsignupemailexist;
  String? get msgsignupuserexist => _msgsignupuserexist;
  String? get msgsignupsuccess => _msgsignupsuccess;
  String? get msgsignupfailed => _msgsignupfailed;
  String? get msgdatanotfound => _msgdatanotfound;
  String? get msgfree2lov => _msgfree2lov;
  String? get pagetitlelogin => _pagetitlelogin;
  String? get lbllogin01 => _lbllogin01;
  String? get lbllogin02 => _lbllogin02;
  String? get lbllogin03 => _lbllogin03;
  String? get lbllogin04 => _lbllogin04;
  String? get lbllogin05 => _lbllogin05;
  String? get lbllogin06 => _lbllogin06;
  String? get lbllogin07 => _lbllogin07;
  String? get lbllogin08 => _lbllogin08;
  String? get lbllogin09 => _lbllogin09;
  String? get lbllogin10 => _lbllogin10;
  String? get lbllogin11 => _lbllogin11;
  String? get lbllogin12 => _lbllogin12;
  String? get msglogininvalid => _msglogininvalid;
  String? get msgloginstatusinactive => _msgloginstatusinactive;
  String? get msgforgotpasswordinvalid => _msgforgotpasswordinvalid;
  String? get msgforgotpasswordsuccess => _msgforgotpasswordsuccess;
  String? get msgforgotpasswordinactive => _msgforgotpasswordinactive;
  String? get msgresetpasswordmissing => _msgresetpasswordmissing;
  String? get msgresetpasswordinvalid => _msgresetpasswordinvalid;
  String? get msgresetpasswordsuccess => _msgresetpasswordsuccess;
  String? get msgresetpasswordinactive => _msgresetpasswordinactive;
  String? get msgemailinvalid => _msgemailinvalid;
  String? get msgloginsuccess => _msgloginsuccess;
  String? get msglogoutsuccess => _msglogoutsuccess;
  String? get msgmailheadertitle => _msgmailheadertitle;
  String? get msgmailcopyrighttext => _msgmailcopyrighttext;
  String? get msgmailfooterteam => _msgmailfooterteam;
  String? get msgproducttitle => _msgproducttitle;
  String? get msgproduct01 => _msgproduct01;
  String? get msgproduct02 => _msgproduct02;
  String? get msgproduct03 => _msgproduct03;
  String? get msgproduct02notavailable => _msgproduct02notavailable;
  String? get msgproduct04 => _msgproduct04;
  String? get msgproduct05 => _msgproduct05;
  String? get msgproduct06 => _msgproduct06;
  String? get msgproduct07 => _msgproduct07;
  String? get msgproduct08 => _msgproduct08;
  String? get msgproduct09 => _msgproduct09;
  String? get msgproductcartaddsuccess => _msgproductcartaddsuccess;
  String? get msgproductcartaddfailed => _msgproductcartaddfailed;
  String? get msgproductwishlistaddfailed => _msgproductwishlistaddfailed;
  String? get msgproduct10 => _msgproduct10;
  String? get msgproduct11 => _msgproduct11;
  String? get msgproduct12 => _msgproduct12;
  String? get msgproduct13 => _msgproduct13;
  String? get msgproductdetailtitle => _msgproductdetailtitle;
  String? get msgproductdetail01 => _msgproductdetail01;
  String? get msgproductdetail02 => _msgproductdetail02;
  String? get msgproductdetail03 => _msgproductdetail03;
  String? get msgproductdetail04 => _msgproductdetail04;
  String? get msgproductdetail05 => _msgproductdetail05;
  String? get msgproductcarttitle => _msgproductcarttitle;
  String? get msgproductcart01 => _msgproductcart01;
  String? get msgproductcart02 => _msgproductcart02;
  String? get msgproductcart03 => _msgproductcart03;
  String? get msgproductcart04 => _msgproductcart04;
  String? get msgproductcart05 => _msgproductcart05;
  String? get msgproductcart06 => _msgproductcart06;
  String? get msgproductcart07 => _msgproductcart07;
  String? get msgproductcart08 => _msgproductcart08;
  String? get msgproductcart09 => _msgproductcart09;
  String? get msgproductcart10 => _msgproductcart10;
  String? get msgproductcart11 => _msgproductcart11;
  String? get msgproductcart12 => _msgproductcart12;
  String? get msgproductcart13 => _msgproductcart13;
  String? get msgproductcart14 => _msgproductcart14;
  String? get msgproductcart15 => _msgproductcart15;
  String? get msgproductcart16 => _msgproductcart16;
  String? get msgproductcart17 => _msgproductcart17;
  String? get msgproductcart18 => _msgproductcart18;
  String? get msgproductcart19 => _msgproductcart19;
  String? get msgproductcart20 => _msgproductcart20;
  String? get msgproductcart21 => _msgproductcart21;
  String? get msgproductcart22 => _msgproductcart22;
  String? get msgproductcart23 => _msgproductcart23;
  String? get msgproductcart24 => _msgproductcart24;
  String? get msgproductcart25 => _msgproductcart25;
  String? get msgproductcart26 => _msgproductcart26;
  String? get msgproductcheckouttitle => _msgproductcheckouttitle;
  String? get msgproductcheckout01 => _msgproductcheckout01;
  String? get msgproductcheckout02 => _msgproductcheckout02;
  String? get msgproductcheckout03 => _msgproductcheckout03;
  String? get msgproductcheckout04 => _msgproductcheckout04;
  String? get msgproductcheckout05 => _msgproductcheckout05;
  String? get msgproductcheckout06 => _msgproductcheckout06;
  String? get msgproductcheckout07 => _msgproductcheckout07;
  String? get msgproductcheckout08 => _msgproductcheckout08;
  String? get msgproductcheckout09 => _msgproductcheckout09;
  String? get msgproductcheckout10 => _msgproductcheckout10;
  String? get msgproductcheckout11 => _msgproductcheckout11;
  String? get msgproductcheckout12 => _msgproductcheckout12;
  String? get msgproductcheckout13 => _msgproductcheckout13;
  String? get msgproductcheckout14 => _msgproductcheckout14;
  String? get msgproductcheckout15 => _msgproductcheckout15;
  String? get msgproductcheckout16 => _msgproductcheckout16;
  String? get msgproductcheckout17 => _msgproductcheckout17;
  String? get msgproductcheckout18 => _msgproductcheckout18;
  String? get msgproductcheckout19 => _msgproductcheckout19;
  String? get msgproductcheckout20 => _msgproductcheckout20;
  String? get msgproductcheckout21 => _msgproductcheckout21;
  String? get msgproductcheckout22 => _msgproductcheckout22;
  String? get msgproductcheckout23 => _msgproductcheckout23;
  String? get msgproductcheckout24 => _msgproductcheckout24;
  String? get msgproductcheckout25 => _msgproductcheckout25;
  String? get msgproductcheckout26 => _msgproductcheckout26;
  String? get msgproductcheckout27 => _msgproductcheckout27;
  String? get msgproductcheckout28 => _msgproductcheckout28;
  String? get msgproductcheckout29 => _msgproductcheckout29;
  String? get msgproductcheckout30 => _msgproductcheckout30;
  String? get msgproductcheckout31 => _msgproductcheckout31;
  String? get msgproductcheckout32 => _msgproductcheckout32;
  String? get msgproductcheckout33 => _msgproductcheckout33;
  String? get msgproductcheckout34 => _msgproductcheckout34;
  String? get msgproductcheckout35 => _msgproductcheckout35;
  String? get msgproductcheckout36 => _msgproductcheckout36;
  String? get msgproductcheckout37 => _msgproductcheckout37;
  String? get msgproductcheckout38 => _msgproductcheckout38;
  String? get msgproductcheckout39 => _msgproductcheckout39;
  String? get msgproductcheckout40 => _msgproductcheckout40;
  String? get msgproductcheckout41 => _msgproductcheckout41;
  String? get msgproductcheckout42 => _msgproductcheckout42;
  String? get msgproductcheckout43 => _msgproductcheckout43;
  String? get msgproductcheckout44 => _msgproductcheckout44;
  String? get msgproductcheckout45 => _msgproductcheckout45;
  String? get msgproductcheckout46 => _msgproductcheckout46;
  String? get msgproductcheckout47 => _msgproductcheckout47;
  String? get msgproductcheckout48 => _msgproductcheckout48;
  String? get msgproductcheckout49 => _msgproductcheckout49;
  String? get msgproductcheckout50 => _msgproductcheckout50;
  String? get msgproductcheckout51 => _msgproductcheckout51;
  String? get msgproductcheckout52 => _msgproductcheckout52;
  String? get msgproductcheckout53 => _msgproductcheckout53;
  String? get msgproductcheckout54 => _msgproductcheckout54;
  String? get msgproductcheckout55 => _msgproductcheckout55;
  String? get msgproductcheckout56 => _msgproductcheckout56;
  String? get msgproductcheckout57 => _msgproductcheckout57;
  String? get msgproductcheckout58 => _msgproductcheckout58;
  String? get msgproductcheckout59 => _msgproductcheckout59;
  String? get msgproductcheckout60 => _msgproductcheckout60;
  String? get msgproductcheckout61 => _msgproductcheckout61;
  String? get msgproductcheckout62 => _msgproductcheckout62;
  String? get msgproductcheckout63 => _msgproductcheckout63;
  String? get msgproductcheckout64 => _msgproductcheckout64;
  String? get msgproductcheckout65 => _msgproductcheckout65;
  String? get msgproductcheckout66 => _msgproductcheckout66;
  String? get msgproductcheckout67 => _msgproductcheckout67;
  String? get msgproductcheckout68 => _msgproductcheckout68;
  String? get msgoctitle => _msgoctitle;
  String? get msgoc01 => _msgoc01;
  String? get msgoc02 => _msgoc02;
  String? get msgoc03 => _msgoc03;
  String? get msgoc04 => _msgoc04;
  String? get msgoc05 => _msgoc05;
  String? get msgoc06 => _msgoc06;
  String? get msgoc07 => _msgoc07;
  String? get msgoc08 => _msgoc08;
  String? get msgoc09 => _msgoc09;
  String? get msgoc10 => _msgoc10;
  String? get msgoc11 => _msgoc11;
  String? get msgoc12 => _msgoc12;
  String? get msgoc13 => _msgoc13;
  String? get msgoc14 => _msgoc14;
  String? get msgoc15 => _msgoc15;
  String? get msgvotitle => _msgvotitle;
  String? get msgvo01 => _msgvo01;
  String? get msgvo02 => _msgvo02;
  String? get msgvo03 => _msgvo03;
  String? get msgvo04 => _msgvo04;
  String? get msgvo05 => _msgvo05;
  String? get msgvo06 => _msgvo06;
  String? get msgvo07 => _msgvo07;
  String? get msgvo08 => _msgvo08;
  String? get msgvo09 => _msgvo09;
  String? get msgvo10 => _msgvo10;
  String? get msgvo11 => _msgvo11;
  String? get msgvo12 => _msgvo12;
  String? get msgvo13 => _msgvo13;
  String? get msgvo14 => _msgvo14;
  String? get msgvo15 => _msgvo15;
  String? get msgvo16 => _msgvo16;
  String? get msgvo17 => _msgvo17;
  String? get msgvo18 => _msgvo18;
  String? get msgvo19 => _msgvo19;
  String? get msgvo20 => _msgvo20;
  String? get msgcptitle => _msgcptitle;
  String? get msgcp01 => _msgcp01;
  String? get msgcp02 => _msgcp02;
  String? get msgcp03 => _msgcp03;
  String? get msgcp04 => _msgcp04;
  String? get msgcp05 => _msgcp05;
  String? get msgcp06 => _msgcp06;
  String? get msgcp07 => _msgcp07;
  String? get msgcp08 => _msgcp08;
  String? get lblcpvalidation01 => _lblcpvalidation01;
  String? get lblcpvalidation02 => _lblcpvalidation02;
  String? get lblcpvalidation03 => _lblcpvalidation03;
  String? get lblcpvalidation04 => _lblcpvalidation04;
  String? get lblcpvalidation05 => _lblcpvalidation05;
  String? get msgcpinvalid => _msgcpinvalid;
  String? get msgcpsuccess => _msgcpsuccess;
  String? get msgcpfailed => _msgcpfailed;
  String? get msgmatitle => _msgmatitle;
  String? get msgma01 => _msgma01;
  String? get msgma02 => _msgma02;
  String? get msgma03 => _msgma03;
  String? get msgma04 => _msgma04;
  String? get msgma05 => _msgma05;
  String? get msgma06 => _msgma06;
  String? get msgma07 => _msgma07;
  String? get msgma08 => _msgma08;
  String? get msgma09 => _msgma09;
  String? get msgma10 => _msgma10;
  String? get msgma11 => _msgma11;
  String? get msgma12 => _msgma12;
  String? get msgma13 => _msgma13;
  String? get msgma14 => _msgma14;
  String? get msgma15 => _msgma15;
  String? get msgma16 => _msgma16;
  String? get msgma17 => _msgma17;
  String? get msgma18 => _msgma18;
  String? get msgma19 => _msgma19;
  String? get msgma20 => _msgma20;
  String? get msgma21 => _msgma21;
  String? get msgma22 => _msgma22;
  String? get msgma23 => _msgma23;
  String? get lblmavalidation01 => _lblmavalidation01;
  String? get lblmavalidation02 => _lblmavalidation02;
  String? get lblmavalidation03 => _lblmavalidation03;
  String? get lblmavalidation04 => _lblmavalidation04;
  String? get lblmavalidation05 => _lblmavalidation05;
  String? get lblmavalidation06 => _lblmavalidation06;
  String? get lblmavalidation07 => _lblmavalidation07;
  String? get msgaddressdetailssuccess => _msgaddressdetailssuccess;
  String? get msgaddressdetailserror => _msgaddressdetailserror;
  String? get msgwait => _msgwait;
  String? get msgcopy => _msgcopy;
  String? get msgsettingsuccess => _msgsettingsuccess;
  String? get msgsettingerror => _msgsettingerror;
  String? get msgiftitle => _msgiftitle;
  String? get msgif01 => _msgif01;
  String? get msgif02 => _msgif02;
  String? get msgif03 => _msgif03;
  String? get msgif04 => _msgif04;
  String? get msgif05 => _msgif05;
  String? get msgif06 => _msgif06;
  String? get msgif07 => _msgif07;
  String? get msgif08 => _msgif08;
  String? get msgif09 => _msgif09;
  String? get msgfptitle => _msgfptitle;
  String? get msgfp01 => _msgfp01;
  String? get msgfp02 => _msgfp02;
  String? get msgrptitle => _msgrptitle;
  String? get msgrp01 => _msgrp01;
  String? get msgrp02 => _msgrp02;
  String? get msgbadrequest => _msgbadrequest;
  String? get shopchange01 => _shopchange01;
  String? get shopchange02 => _shopchange02;
  String? get shopchange03 => _shopchange03;
  String? get shopchange04 => _shopchange04;
  String? get shopchange05 => _shopchange05;
  String? get shopchange06 => _shopchange06;
  String? get msgusernotfound => _msgusernotfound;
  String? get msgordernotfound => _msgordernotfound;
  String? get msgorderdetailnotfound => _msgorderdetailnotfound;
  String? get rechargecheckoutbtnlbl => _rechargecheckoutbtnlbl;
  String? get lblalert => _lblalert;
  String? get enternumber => _enternumber;
  String? get operatordetected => _operatordetected;
  String? get change => _change;
  String? get selectamount => _selectamount;
  String? get havepromocode => _havepromocode;
  String? get pleaseselect => _pleaseselect;
  String? get errornumber => _errornumber;
  String? get characterlong => _characterlong;
  String? get packageerror => _packageerror;
  String? get willreceived => _willreceived;
  String? get priceerrormax => _priceerrormax;
  String? get priceerrormin => _priceerrormin;
  String? get rechargestatus => _rechargestatus;
  String? get becauseof => _becauseof;
  String? get somewentwrong => _somewentwrong;
  String? get checkmobileno => _checkmobileno;
  String? get needproduct => _needproduct;
  String? get purposeproduct => _purposeproduct;
  String? get needproducthead => _needproducthead;
  String? get countrylabel => _countrylabel;
  String? get productplaceholder => _productplaceholder;
  String? get productdescplaceholder => _productdescplaceholder;
  String? get send => _send;
  String? get productsent => _productsent;
  String? get productname => _productname;
  String? get productdesc => _productdesc;
  String? get productqty => _productqty;
  String? get anywhere => _anywhere;
  String? get informavailability => _informavailability;
  String? get proposecountrylabel => _proposecountrylabel;
  String? get proposecitylabel => _proposecitylabel;
  String? get proposechooseshop => _proposechooseshop;
  String? get proposeemail => _proposeemail;
  String? get proposetelephone => _proposetelephone;
  String? get footermenustories => _footermenustories;
  String? get footermenupress => _footermenupress;
  String? get footermenucareer => _footermenucareer;
  String? get footermenuhelp => _footermenuhelp;
  String? get footermenufaq => _footermenufaq;
  String? get checkouttransactiondetails => _checkouttransactiondetails;
  String? get checkoutbenificierydetails => _checkoutbenificierydetails;
  String? get rechargerecipientdetails => _rechargerecipientdetails;
  String? get checkoutpaymentmode => _checkoutpaymentmode;
  String? get pleaseselectdeliverymethod => _pleaseselectdeliverymethod;
  String? get doyouhaveapromocode => _doyouhaveapromocode;
  String? get step2text => _step2text;
  String? get shippingandhabdling => _shippingandhabdling;
  String? get transactionaredeliveredonluunderpresentationofvalidcni =>
      _transactionaredeliveredonluunderpresentationofvalidcni;
  String? get abetterwaytosendwhatreallymatter =>
      _abetterwaytosendwhatreallymatter;
  String? get wedeliveredproductlovedonce => _wedeliveredproductlovedonce;
  String? get selectarea => _selectarea;
  String? get buysafelyonline => _buysafelyonline;
  String? get getdelivery => _getdelivery;
  String? get selectareaandshop => _selectareaandshop;
  String? get paysafely => _paysafely;
  String? get benificieryrecieves => _benificieryrecieves;
  String? get jointhousandsofhappyfamilies => _jointhousandsofhappyfamilies;
  String? get worldfamilyconnected => _worldfamilyconnected;
  String? get satisfiedusers => _satisfiedusers;
  String? get whychoosefamilov => _whychoosefamilov;
  String? get discoveredwhyouruserlovedus => _discoveredwhyouruserlovedus;
  String? get secure => _secure;
  String? get weusesslcertificate => _weusesslcertificate;
  String? get yourmoneyiswellused => _yourmoneyiswellused;
  String? get sendmore => _sendmore;
  String? get easytouse => _easytouse;
  String? get positiveimpact => _positiveimpact;
  String? get socialaspect => _socialaspect;
  String? get giranteemoney => _giranteemoney;
  String? get savetraditionalmoney => _savetraditionalmoney;
  String? get familovisveryeasytouse => _familovisveryeasytouse;
  String? get weimprovepeoplelives => _weimprovepeoplelives;
  String? get wehelpyoutohelppeople => _wehelpyoutohelppeople;
  String? get ourservices => _ourservices;
  String? get morewaystosendwhatreallymatter => _morewaystosendwhatreallymatter;
  String? get fooddrinkgrocery => _fooddrinkgrocery;
  String? get fooddrinkgrocerydesc => _fooddrinkgrocerydesc;
  String? get mobiletopup => _mobiletopup;
  String? get mobiletopupdesc => _mobiletopupdesc;
  String? get healthpaharmacy => _healthpaharmacy;
  String? get healthpaharmacydesc => _healthpaharmacydesc;
  String? get educationformation => _educationformation;
  String? get educationformationdesc => _educationformationdesc;
  String? get construction => _construction;
  String? get constructiondesc => _constructiondesc;
  String? get mobilityenergy => _mobilityenergy;
  String? get mobilityenergydesc => _mobilityenergydesc;
  String? get signupforfree => _signupforfree;
  String? get becomepartner => _becomepartner;
  String? get howtogiveback => _howtogiveback;
  String? get webelivethatgoinggood => _webelivethatgoinggood;
  String? get learnmore => _learnmore;
  String? get subscribetofamilov => _subscribetofamilov;
  String? get amazingoffersupdatesinterestingnews =>
      _amazingoffersupdatesinterestingnews;
  String? get enterhereanyplaceindications => _enterhereanyplaceindications;
  String? get bravoyourpackageisready => _bravoyourpackageisready;
  String? get pleaseselectthecountry => _pleaseselectthecountry;
  String? get usefamilovandstayconnectedwithlovedonce =>
      _usefamilovandstayconnectedwithlovedonce;
  String? get selectthecountry => _selectthecountry;
  String? get bemoreefficientthantraditional => _bemoreefficientthantraditional;
  String? get iliketogiveahelpinghandinthecountry =>
      _iliketogiveahelpinghandinthecountry;
  String? get helpustohelp => _helpustohelp;
  String? get go => _go;
  String? get loveourapp => _loveourapp;
  String? get legal => _legal;
  String? get termsandcondition => _termsandcondition;
  String? get forpatners => _forpatners;
  String? get mobileinputplaceholder => _mobileinputplaceholder;
  String? get submitproduct => _submitproduct;
  String? get rechargemaintitle => _rechargemaintitle;
  String? get rechargetitledesctiption => _rechargetitledesctiption;
  String? get rechargestep1title => _rechargestep1title;
  String? get rechargestep1 => _rechargestep1;
  String? get rechargestep2title => _rechargestep2title;
  String? get rechargestep2 => _rechargestep2;
  String? get rechargestep3title => _rechargestep3title;
  String? get rechargestep3 => _rechargestep3;
  String? get rechargebannertitle => _rechargebannertitle;
  String? get rechargebannerdescription => _rechargebannerdescription;
  String? get rechargesearchplaceholder => _rechargesearchplaceholder;
  String? get newtofamilov => _newtofamilov;
  String? get takelessthanminutes => _takelessthanminutes;
  String? get signinwithfacebook => _signinwithfacebook;
  String? get signinwithgoogle => _signinwithgoogle;
  String? get or => _or;
  String? get signupwithfacebook => _signupwithfacebook;
  String? get signupwithgoogle => _signupwithgoogle;
  String? get signuptermsandcondition => _signuptermsandcondition;
  String? get signupcondition => _signupcondition;
  String? get noproductfound => _noproductfound;
  String? get subscribetogetbestofferes => _subscribetogetbestofferes;
  String? get viewmycart => _viewmycart;
  String? get dlbill => _dlbill;
  String? get dlorddets => _dlorddets;
  String? get selectbenificerydetails => _selectbenificerydetails;
  String? get addnewreciepent => _addnewreciepent;
  String? get easyisnotit => _easyisnotit;
  String? get selectpaymentmethod => _selectpaymentmethod;
  String? get personalmessage => _personalmessage;
  String? get doyouwanttoleave => _doyouwanttoleave;
  String? get accept => _accept;
  String? get securecheckout => _securecheckout;
  String? get satisfactionguaranteed => _satisfactionguaranteed;
  String? get privacyprotected => _privacyprotected;
  String? get selectreciepent => _selectreciepent;
  String? get addreciepent => _addreciepent;
  String? get myreciepent => _myreciepent;
  String? get mywishlist => _mywishlist;
  String? get reciepentname => _reciepentname;
  String? get reciepentmainno => _reciepentmainno;
  String? get reciepentanotherno => _reciepentanotherno;
  String? get pleaseenterfullnameindicatedonhiscni =>
      _pleaseenterfullnameindicatedonhiscni;
  String? get edit => _edit;
  String? get lblmyacct01 => _lblmyacct01;
  String? get lblmyacct02 => _lblmyacct02;
  String? get lblmyacct03 => _lblmyacct03;
  String? get lblmyacct04 => _lblmyacct04;
  String? get lblmyacct05 => _lblmyacct05;
  String? get lblmyacct06 => _lblmyacct06;
  String? get lblmyacct07 => _lblmyacct07;
  String? get lblmyacct08 => _lblmyacct08;
  String? get lblmyacct09 => _lblmyacct09;
  String? get lblmyacct10 => _lblmyacct10;
  String? get lblmyacct11 => _lblmyacct11;
  String? get lblmyacct12 => _lblmyacct12;
  String? get lblmyacct13 => _lblmyacct13;
  String? get lblmyacct14 => _lblmyacct14;
  String? get lblmyacct15 => _lblmyacct15;
  String? get checkoutcontinue => _checkoutcontinue;
  String? get checkoutback => _checkoutback;
  String? get orderconfirmation1 => _orderconfirmation1;
  String? get lblproductsortmostsold => _lblproductsortmostsold;
  String? get lblproductsortleastsold => _lblproductsortleastsold;
  String? get lblproductsortpricelow => _lblproductsortpricelow;
  String? get lblproductsortpricehigh => _lblproductsortpricehigh;
  String? get lblproductsortrecentsold => _lblproductsortrecentsold;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['LBL_MENU_01'] = _lblmenu01;
    map['LBL_MENU_02'] = _lblmenu02;
    map['LBL_MENU_02_1'] = _lblmenu021;
    map['LBL_MENU_03'] = _lblmenu03;
    map['LBL_MENU_04'] = _lblmenu04;
    map['LBL_MENU_05'] = _lblmenu05;
    map['LBL_MENU_06'] = _lblmenu06;
    map['LBL_MENU_07'] = _lblmenu07;
    map['LBL_MENU_08'] = _lblmenu08;
    map['LBL_MENU_09'] = _lblmenu09;
    map['LBL_MENU_10'] = _lblmenu10;
    map['LBL_MENU_11'] = _lblmenu11;
    map['LBL_MENU_12'] = _lblmenu12;
    map['LBL_MENU_13'] = _lblmenu13;
    map['LBL_MENU_14'] = _lblmenu14;
    map['LBL_MENU_15'] = _lblmenu15;
    map['LBL_MENU_16'] = _lblmenu16;
    map['LBL_MENU_17'] = _lblmenu17;
    map['LBL_MENU_18'] = _lblmenu18;
    map['LBL_MENU_19'] = _lblmenu19;
    map['LBL_MENU_20'] = _lblmenu20;
    map['LBL_MENU_21'] = _lblmenu21;
    map['LBL_MENU_22'] = _lblmenu22;
    map['LBL_MENU_23'] = _lblmenu23;
    map['LBL_MENU_24'] = _lblmenu24;
    map['LBL_MENU_25'] = _lblmenu25;
    map['LBL_SOCIAL_FACEBOOK'] = _lblsocialfacebook;
    map['LBL_SOCIAL_TWITTER'] = _lblsocialtwitter;
    map['LBL_FOOTER_01'] = _lblfooter01;
    map['LBL_FOOTER_02'] = _lblfooter02;
    map['welcome_message'] = _welcomeMessage;
    map['LBL_HERO_TWO'] = _lblherotwo;
    map['LBL_HERO_THREE'] = _lblherothree;
    map['LBL_RECEIVER_COUNTRY'] = _lblreceivercountry;
    map['LBL_RECEIVER_CITY'] = _lblreceivercity;
    map['LBL_CHOOSE_SHOP'] = _lblchooseshop;
    map['LBL_RECEIVER_GO'] = _lblreceivergo;
    map['LBL_SEARCH_VALIDATION_COUNTRY'] = _lblsearchvalidationcountry;
    map['LBL_SEARCH_VALIDATION_CITY'] = _lblsearchvalidationcity;
    map['LBL_SEARCH_VALIDATION_SHOP'] = _lblsearchvalidationshop;
    map['LBL_SEARCH_CITY_LIST'] = _lblsearchcitylist;
    map['LBL_SEARCH_SHOP_LIST'] = _lblsearchshoplist;
    map['LBL_RECEIVER_STARTED'] = _lblreceiverstarted;
    map['LBL_HOME_01'] = _lblhome01;
    map['LBL_HOME_02'] = _lblhome02;
    map['LBL_HOME_03'] = _lblhome03;
    map['LBL_HOME_04'] = _lblhome04;
    map['LBL_HOME_05'] = _lblhome05;
    map['LBL_HOME_06'] = _lblhome06;
    map['LBL_HOME_07'] = _lblhome07;
    map['LBL_HOME_08'] = _lblhome08;
    map['LBL_HOME_09'] = _lblhome09;
    map['LBL_HOME_10'] = _lblhome10;
    map['LBL_HOME_11'] = _lblhome11;
    map['LBL_HOME_12'] = _lblhome12;
    map['LBL_HOME_13'] = _lblhome13;
    map['LBL_HOME_14'] = _lblhome14;
    map['LBL_HOME_15'] = _lblhome15;
    map['LBL_HOME_16'] = _lblhome16;
    map['LBL_HOW_IT_WORKS_01'] = _lblhowitworks01;
    map['LBL_HOW_IT_WORKS_02'] = _lblhowitworks02;
    map['LBL_HOW_IT_WORKS_03'] = _lblhowitworks03;
    map['LBL_HOW_IT_WORKS_04'] = _lblhowitworks04;
    map['LBL_HOW_IT_WORKS_05'] = _lblhowitworks05;
    map['LBL_HOW_IT_WORKS_06'] = _lblhowitworks06;
    map['LBL_HOW_IT_WORKS_07'] = _lblhowitworks07;
    map['LBL_HOW_IT_WORKS_08'] = _lblhowitworks08;
    map['LBL_HOW_IT_WORKS_09'] = _lblhowitworks09;
    map['LBL_HOW_IT_WORKS_10'] = _lblhowitworks10;
    map['LBL_HOW_IT_WORKS_11'] = _lblhowitworks11;
    map['LBL_TESTIMONIAL_01'] = _lbltestimonial01;
    map['LBL_TESTIMONIAL_02'] = _lbltestimonial02;
    map['LBL_TESTIMONIAL_PERSON_1_01'] = _lbltestimonialperson101;
    map['LBL_TESTIMONIAL_PERSON_1_02'] = _lbltestimonialperson102;
    map['LBL_TESTIMONIAL_PERSON_1_03'] = _lbltestimonialperson103;
    map['LBL_TESTIMONIAL_PERSON_2_01'] = _lbltestimonialperson201;
    map['LBL_TESTIMONIAL_PERSON_2_02'] = _lbltestimonialperson202;
    map['LBL_TESTIMONIAL_PERSON_2_03'] = _lbltestimonialperson203;
    map['LBL_TESTIMONIAL_PERSON_3_01'] = _lbltestimonialperson301;
    map['LBL_TESTIMONIAL_PERSON_3_02'] = _lbltestimonialperson302;
    map['LBL_TESTIMONIAL_PERSON_3_03'] = _lbltestimonialperson303;
    map['PAGE_TITLE_FAQ'] = _pagetitlefaq;
    map['LBL_FAQ_01'] = _lblfaq01;
    map['LBL_FAQ_02'] = _lblfaq02;
    map['LBL_FAQ_03'] = _lblfaq03;
    map['PAGE_TITLE_HIW'] = _pagetitlehiw;
    map['LBL_HIW_01'] = _lblhiw01;
    map['LBL_HIW_02'] = _lblhiw02;
    map['LBL_HIW_03'] = _lblhiw03;
    map['LBL_HIW_04'] = _lblhiw04;
    map['LBL_HIW_05'] = _lblhiw05;
    map['LBL_HIW_06'] = _lblhiw06;
    map['LBL_HIW_07'] = _lblhiw07;
    map['LBL_HIW_08'] = _lblhiw08;
    map['LBL_HIW_09'] = _lblhiw09;
    map['LBL_HIW_10'] = _lblhiw10;
    map['LBL_HIW_11'] = _lblhiw11;
    map['LBL_HIW_12'] = _lblhiw12;
    map['LBL_HIW_13'] = _lblhiw13;
    map['LBL_HIW_14'] = _lblhiw14;
    map['LBL_HIW_15'] = _lblhiw15;
    map['LBL_HIW_16'] = _lblhiw16;
    map['LBL_HIW_17'] = _lblhiw17;
    map['LBL_HIW_18'] = _lblhiw18;
    map['LBL_HIW_19'] = _lblhiw19;
    map['LBL_HIW_20'] = _lblhiw20;
    map['LBL_HIW_21'] = _lblhiw21;
    map['LBL_HIW_22'] = _lblhiw22;
    map['LBL_HIW_23'] = _lblhiw23;
    map['LBL_HIW_24'] = _lblhiw24;
    map['LBL_HIW_25'] = _lblhiw25;
    map['LBL_HIW_26'] = _lblhiw26;
    map['LBL_HIW_27'] = _lblhiw27;
    map['LBL_HIW_28'] = _lblhiw28;
    map['PAGE_TITLE_TERMS'] = _pagetitleterms;
    map['LBL_TERMS_01'] = _lblterms01;
    map['PAGE_TITLE_PRIVACY'] = _pagetitleprivacy;
    map['LBL_PRIVACY_POLICY_01'] = _lblprivacypolicy01;
    map['PAGE_TITLE_COOKIE'] = _pagetitlecookie;
    map['LBL_COOKIE_POLICY_01'] = _lblcookiepolicy01;
    map['PAGE_TITLE_ABOUT_US'] = _pagetitleaboutus;
    map['LBL_ABOUT_US_01'] = _lblaboutus01;
    map['PAGE_TITLE_CONTACT_US'] = _pagetitlecontactus;
    map['LBL_CONTACT_US_01'] = _lblcontactus01;
    map['LBL_RECHARGE_01'] = _lblrecharge01;
    map['PAGE_TITLE_PRESS'] = _pagetitlepress;
    map['LBL_PRESS_01'] = _lblpress01;
    map['LBL_PRESS_02'] = _lblpress02;
    map['LBL_PRESS_03'] = _lblpress03;
    map['PAGE_TITLE_JOBS'] = _pagetitlejobs;
    map['LBL_JOBS_01'] = _lbljobs01;
    map['LBL_JOBS_02'] = _lbljobs02;
    map['PAGE_TITLE_SIGN_UP'] = _pagetitlesignup;
    map['LBL_SIGN_UP_01'] = _lblsignup01;
    map['LBL_SIGN_UP_02'] = _lblsignup02;
    map['LBL_SIGN_UP_03'] = _lblsignup03;
    map['LBL_SIGN_UP_04'] = _lblsignup04;
    map['LBL_SIGN_UP_05'] = _lblsignup05;
    map['LBL_SIGN_UP_06'] = _lblsignup06;
    map['LBL_SIGN_UP_07'] = _lblsignup07;
    map['LBL_SIGN_UP_08'] = _lblsignup08;
    map['LBL_SIGN_UP_09'] = _lblsignup09;
    map['LBL_SIGN_UP_10'] = _lblsignup10;
    map['LBL_SIGN_UP_11'] = _lblsignup11;
    map['LBL_SIGN_UP_12'] = _lblsignup12;
    map['LBL_SIGN_UP_13'] = _lblsignup13;
    map['LBL_SIGN_UP_VALIDATION_01'] = _lblsignupvalidation01;
    map['LBL_SIGN_UP_VALIDATION_02'] = _lblsignupvalidation02;
    map['LBL_SIGN_UP_VALIDATION_03'] = _lblsignupvalidation03;
    map['LBL_SIGN_UP_VALIDATION_04'] = _lblsignupvalidation04;
    map['LBL_SIGN_UP_VALIDATION_05'] = _lblsignupvalidation05;
    map['LBL_SIGN_UP_VALIDATION_06'] = _lblsignupvalidation06;
    map['LBL_SIGN_UP_VALIDATION_07'] = _lblsignupvalidation07;
    map['LBL_SIGN_UP_VALIDATION_08'] = _lblsignupvalidation08;
    map['LBL_SIGN_UP_VALIDATION_09'] = _lblsignupvalidation09;
    map['LBL_SIGN_UP_VALIDATION_10'] = _lblsignupvalidation10;
    map['MSG_SIGN_UP_EMAIL_EXIST'] = _msgsignupemailexist;
    map['MSG_SIGN_UP_USER_EXIST'] = _msgsignupuserexist;
    map['MSG_SIGN_UP_SUCCESS'] = _msgsignupsuccess;
    map['MSG_SIGN_UP_FAILED'] = _msgsignupfailed;
    map['MSG_DATA_NOT_FOUND'] = _msgdatanotfound;
    map['MSG_FREE2LOV'] = _msgfree2lov;
    map['PAGE_TITLE_LOG_IN'] = _pagetitlelogin;
    map['LBL_LOG_IN_01'] = _lbllogin01;
    map['LBL_LOG_IN_02'] = _lbllogin02;
    map['LBL_LOG_IN_03'] = _lbllogin03;
    map['LBL_LOG_IN_04'] = _lbllogin04;
    map['LBL_LOG_IN_05'] = _lbllogin05;
    map['LBL_LOG_IN_06'] = _lbllogin06;
    map['LBL_LOG_IN_07'] = _lbllogin07;
    map['LBL_LOG_IN_08'] = _lbllogin08;
    map['LBL_LOG_IN_09'] = _lbllogin09;
    map['LBL_LOG_IN_10'] = _lbllogin10;
    map['LBL_LOG_IN_11'] = _lbllogin11;
    map['LBL_LOG_IN_12'] = _lbllogin12;
    map['MSG_LOGIN_INVALID'] = _msglogininvalid;
    map['MSG_LOGIN_STATUS_INACTIVE'] = _msgloginstatusinactive;
    map['MSG_FORGOT_PASSWORD_INVALID'] = _msgforgotpasswordinvalid;
    map['MSG_FORGOT_PASSWORD_SUCCESS'] = _msgforgotpasswordsuccess;
    map['MSG_FORGOT_PASSWORD_INACTIVE'] = _msgforgotpasswordinactive;
    map['MSG_RESET_PASSWORD_MISSING'] = _msgresetpasswordmissing;
    map['MSG_RESET_PASSWORD_INVALID'] = _msgresetpasswordinvalid;
    map['MSG_RESET_PASSWORD_SUCCESS'] = _msgresetpasswordsuccess;
    map['MSG_RESET_PASSWORD_INACTIVE'] = _msgresetpasswordinactive;
    map['MSG_EMAIL_INVALID'] = _msgemailinvalid;
    map['MSG_LOGIN_SUCCESS'] = _msgloginsuccess;
    map['MSG_LOGOUT_SUCCESS'] = _msglogoutsuccess;
    map['MSG_MAIL_HEADER_TITLE'] = _msgmailheadertitle;
    map['MSG_MAIL_COPYRIGHT_TEXT'] = _msgmailcopyrighttext;
    map['MSG_MAIL_FOOTER_TEAM'] = _msgmailfooterteam;
    map['MSG_PRODUCT_TITLE'] = _msgproducttitle;
    map['MSG_PRODUCT_01'] = _msgproduct01;
    map['MSG_PRODUCT_02'] = _msgproduct02;
    map['MSG_PRODUCT_03'] = _msgproduct03;
    map['MSG_PRODUCT_02_NOT_AVAILABLE'] = _msgproduct02notavailable;
    map['MSG_PRODUCT_04'] = _msgproduct04;
    map['MSG_PRODUCT_05'] = _msgproduct05;
    map['MSG_PRODUCT_06'] = _msgproduct06;
    map['MSG_PRODUCT_07'] = _msgproduct07;
    map['MSG_PRODUCT_08'] = _msgproduct08;
    map['MSG_PRODUCT_09'] = _msgproduct09;
    map['MSG_PRODUCT_CART_ADD_SUCCESS'] = _msgproductcartaddsuccess;
    map['MSG_PRODUCT_CART_ADD_FAILED'] = _msgproductcartaddfailed;
    map['MSG_PRODUCT_WISHLIST_ADD_FAILED'] = _msgproductwishlistaddfailed;
    map['MSG_PRODUCT_10'] = _msgproduct10;
    map['MSG_PRODUCT_11'] = _msgproduct11;
    map['MSG_PRODUCT_12'] = _msgproduct12;
    map['MSG_PRODUCT_13'] = _msgproduct13;
    map['MSG_PRODUCT_DETAIL_TITLE'] = _msgproductdetailtitle;
    map['MSG_PRODUCT_DETAIL_01'] = _msgproductdetail01;
    map['MSG_PRODUCT_DETAIL_02'] = _msgproductdetail02;
    map['MSG_PRODUCT_DETAIL_03'] = _msgproductdetail03;
    map['MSG_PRODUCT_DETAIL_04'] = _msgproductdetail04;
    map['MSG_PRODUCT_DETAIL_05'] = _msgproductdetail05;
    map['MSG_PRODUCT_CART_TITLE'] = _msgproductcarttitle;
    map['MSG_PRODUCT_CART_01'] = _msgproductcart01;
    map['MSG_PRODUCT_CART_02'] = _msgproductcart02;
    map['MSG_PRODUCT_CART_03'] = _msgproductcart03;
    map['MSG_PRODUCT_CART_04'] = _msgproductcart04;
    map['MSG_PRODUCT_CART_05'] = _msgproductcart05;
    map['MSG_PRODUCT_CART_06'] = _msgproductcart06;
    map['MSG_PRODUCT_CART_07'] = _msgproductcart07;
    map['MSG_PRODUCT_CART_08'] = _msgproductcart08;
    map['MSG_PRODUCT_CART_09'] = _msgproductcart09;
    map['MSG_PRODUCT_CART_10'] = _msgproductcart10;
    map['MSG_PRODUCT_CART_11'] = _msgproductcart11;
    map['MSG_PRODUCT_CART_12'] = _msgproductcart12;
    map['MSG_PRODUCT_CART_13'] = _msgproductcart13;
    map['MSG_PRODUCT_CART_14'] = _msgproductcart14;
    map['MSG_PRODUCT_CART_15'] = _msgproductcart15;
    map['MSG_PRODUCT_CART_16'] = _msgproductcart16;
    map['MSG_PRODUCT_CART_17'] = _msgproductcart17;
    map['MSG_PRODUCT_CART_18'] = _msgproductcart18;
    map['MSG_PRODUCT_CART_19'] = _msgproductcart19;
    map['MSG_PRODUCT_CART_20'] = _msgproductcart20;
    map['MSG_PRODUCT_CART_21'] = _msgproductcart21;
    map['MSG_PRODUCT_CART_22'] = _msgproductcart22;
    map['MSG_PRODUCT_CART_23'] = _msgproductcart23;
    map['MSG_PRODUCT_CART_24'] = _msgproductcart24;
    map['MSG_PRODUCT_CART_25'] = _msgproductcart25;
    map['MSG_PRODUCT_CART_26'] = _msgproductcart26;
    map['MSG_PRODUCT_CHECKOUT_TITLE'] = _msgproductcheckouttitle;
    map['MSG_PRODUCT_CHECKOUT_01'] = _msgproductcheckout01;
    map['MSG_PRODUCT_CHECKOUT_02'] = _msgproductcheckout02;
    map['MSG_PRODUCT_CHECKOUT_03'] = _msgproductcheckout03;
    map['MSG_PRODUCT_CHECKOUT_04'] = _msgproductcheckout04;
    map['MSG_PRODUCT_CHECKOUT_05'] = _msgproductcheckout05;
    map['MSG_PRODUCT_CHECKOUT_06'] = _msgproductcheckout06;
    map['MSG_PRODUCT_CHECKOUT_07'] = _msgproductcheckout07;
    map['MSG_PRODUCT_CHECKOUT_08'] = _msgproductcheckout08;
    map['MSG_PRODUCT_CHECKOUT_09'] = _msgproductcheckout09;
    map['MSG_PRODUCT_CHECKOUT_10'] = _msgproductcheckout10;
    map['MSG_PRODUCT_CHECKOUT_11'] = _msgproductcheckout11;
    map['MSG_PRODUCT_CHECKOUT_12'] = _msgproductcheckout12;
    map['MSG_PRODUCT_CHECKOUT_13'] = _msgproductcheckout13;
    map['MSG_PRODUCT_CHECKOUT_14'] = _msgproductcheckout14;
    map['MSG_PRODUCT_CHECKOUT_15'] = _msgproductcheckout15;
    map['MSG_PRODUCT_CHECKOUT_16'] = _msgproductcheckout16;
    map['MSG_PRODUCT_CHECKOUT_17'] = _msgproductcheckout17;
    map['MSG_PRODUCT_CHECKOUT_18'] = _msgproductcheckout18;
    map['MSG_PRODUCT_CHECKOUT_19'] = _msgproductcheckout19;
    map['MSG_PRODUCT_CHECKOUT_20'] = _msgproductcheckout20;
    map['MSG_PRODUCT_CHECKOUT_21'] = _msgproductcheckout21;
    map['MSG_PRODUCT_CHECKOUT_22'] = _msgproductcheckout22;
    map['MSG_PRODUCT_CHECKOUT_23'] = _msgproductcheckout23;
    map['MSG_PRODUCT_CHECKOUT_24'] = _msgproductcheckout24;
    map['MSG_PRODUCT_CHECKOUT_25'] = _msgproductcheckout25;
    map['MSG_PRODUCT_CHECKOUT_26'] = _msgproductcheckout26;
    map['MSG_PRODUCT_CHECKOUT_27'] = _msgproductcheckout27;
    map['MSG_PRODUCT_CHECKOUT_28'] = _msgproductcheckout28;
    map['MSG_PRODUCT_CHECKOUT_29'] = _msgproductcheckout29;
    map['MSG_PRODUCT_CHECKOUT_30'] = _msgproductcheckout30;
    map['MSG_PRODUCT_CHECKOUT_31'] = _msgproductcheckout31;
    map['MSG_PRODUCT_CHECKOUT_32'] = _msgproductcheckout32;
    map['MSG_PRODUCT_CHECKOUT_33'] = _msgproductcheckout33;
    map['MSG_PRODUCT_CHECKOUT_34'] = _msgproductcheckout34;
    map['MSG_PRODUCT_CHECKOUT_35'] = _msgproductcheckout35;
    map['MSG_PRODUCT_CHECKOUT_36'] = _msgproductcheckout36;
    map['MSG_PRODUCT_CHECKOUT_37'] = _msgproductcheckout37;
    map['MSG_PRODUCT_CHECKOUT_38'] = _msgproductcheckout38;
    map['MSG_PRODUCT_CHECKOUT_39'] = _msgproductcheckout39;
    map['MSG_PRODUCT_CHECKOUT_40'] = _msgproductcheckout40;
    map['MSG_PRODUCT_CHECKOUT_41'] = _msgproductcheckout41;
    map['MSG_PRODUCT_CHECKOUT_42'] = _msgproductcheckout42;
    map['MSG_PRODUCT_CHECKOUT_43'] = _msgproductcheckout43;
    map['MSG_PRODUCT_CHECKOUT_44'] = _msgproductcheckout44;
    map['MSG_PRODUCT_CHECKOUT_45'] = _msgproductcheckout45;
    map['MSG_PRODUCT_CHECKOUT_46'] = _msgproductcheckout46;
    map['MSG_PRODUCT_CHECKOUT_47'] = _msgproductcheckout47;
    map['MSG_PRODUCT_CHECKOUT_48'] = _msgproductcheckout48;
    map['MSG_PRODUCT_CHECKOUT_49'] = _msgproductcheckout49;
    map['MSG_PRODUCT_CHECKOUT_50'] = _msgproductcheckout50;
    map['MSG_PRODUCT_CHECKOUT_51'] = _msgproductcheckout51;
    map['MSG_PRODUCT_CHECKOUT_52'] = _msgproductcheckout52;
    map['MSG_PRODUCT_CHECKOUT_53'] = _msgproductcheckout53;
    map['MSG_PRODUCT_CHECKOUT_54'] = _msgproductcheckout54;
    map['MSG_PRODUCT_CHECKOUT_55'] = _msgproductcheckout55;
    map['MSG_PRODUCT_CHECKOUT_56'] = _msgproductcheckout56;
    map['MSG_PRODUCT_CHECKOUT_57'] = _msgproductcheckout57;
    map['MSG_PRODUCT_CHECKOUT_58'] = _msgproductcheckout58;
    map['MSG_PRODUCT_CHECKOUT_59'] = _msgproductcheckout59;
    map['MSG_PRODUCT_CHECKOUT_60'] = _msgproductcheckout60;
    map['MSG_PRODUCT_CHECKOUT_61'] = _msgproductcheckout61;
    map['MSG_PRODUCT_CHECKOUT_62'] = _msgproductcheckout62;
    map['MSG_PRODUCT_CHECKOUT_63'] = _msgproductcheckout63;
    map['MSG_PRODUCT_CHECKOUT_64'] = _msgproductcheckout64;
    map['MSG_PRODUCT_CHECKOUT_65'] = _msgproductcheckout65;
    map['MSG_PRODUCT_CHECKOUT_66'] = _msgproductcheckout66;
    map['MSG_PRODUCT_CHECKOUT_67'] = _msgproductcheckout67;
    map['MSG_PRODUCT_CHECKOUT_68'] = _msgproductcheckout68;
    map['MSG_OC_TITLE'] = _msgoctitle;
    map['MSG_OC_01'] = _msgoc01;
    map['MSG_OC_02'] = _msgoc02;
    map['MSG_OC_03'] = _msgoc03;
    map['MSG_OC_04'] = _msgoc04;
    map['MSG_OC_05'] = _msgoc05;
    map['MSG_OC_06'] = _msgoc06;
    map['MSG_OC_07'] = _msgoc07;
    map['MSG_OC_08'] = _msgoc08;
    map['MSG_OC_09'] = _msgoc09;
    map['MSG_OC_10'] = _msgoc10;
    map['MSG_OC_11'] = _msgoc11;
    map['MSG_OC_12'] = _msgoc12;
    map['MSG_OC_13'] = _msgoc13;
    map['MSG_OC_14'] = _msgoc14;
    map['MSG_OC_15'] = _msgoc15;
    map['MSG_VO_TITLE'] = _msgvotitle;
    map['MSG_VO_01'] = _msgvo01;
    map['MSG_VO_02'] = _msgvo02;
    map['MSG_VO_03'] = _msgvo03;
    map['MSG_VO_04'] = _msgvo04;
    map['MSG_VO_05'] = _msgvo05;
    map['MSG_VO_06'] = _msgvo06;
    map['MSG_VO_07'] = _msgvo07;
    map['MSG_VO_08'] = _msgvo08;
    map['MSG_VO_09'] = _msgvo09;
    map['MSG_VO_10'] = _msgvo10;
    map['MSG_VO_11'] = _msgvo11;
    map['MSG_VO_12'] = _msgvo12;
    map['MSG_VO_13'] = _msgvo13;
    map['MSG_VO_14'] = _msgvo14;
    map['MSG_VO_15'] = _msgvo15;
    map['MSG_VO_16'] = _msgvo16;
    map['MSG_VO_17'] = _msgvo17;
    map['MSG_VO_18'] = _msgvo18;
    map['MSG_VO_19'] = _msgvo19;
    map['MSG_VO_20'] = _msgvo20;
    map['MSG_CP_TITLE'] = _msgcptitle;
    map['MSG_CP_01'] = _msgcp01;
    map['MSG_CP_02'] = _msgcp02;
    map['MSG_CP_03'] = _msgcp03;
    map['MSG_CP_04'] = _msgcp04;
    map['MSG_CP_05'] = _msgcp05;
    map['MSG_CP_06'] = _msgcp06;
    map['MSG_CP_07'] = _msgcp07;
    map['MSG_CP_08'] = _msgcp08;
    map['LBL_CP_VALIDATION_01'] = _lblcpvalidation01;
    map['LBL_CP_VALIDATION_02'] = _lblcpvalidation02;
    map['LBL_CP_VALIDATION_03'] = _lblcpvalidation03;
    map['LBL_CP_VALIDATION_04'] = _lblcpvalidation04;
    map['LBL_CP_VALIDATION_05'] = _lblcpvalidation05;
    map['MSG_CP_INVALID'] = _msgcpinvalid;
    map['MSG_CP_SUCCESS'] = _msgcpsuccess;
    map['MSG_CP_FAILED'] = _msgcpfailed;
    map['MSG_MA_TITLE'] = _msgmatitle;
    map['MSG_MA_01'] = _msgma01;
    map['MSG_MA_02'] = _msgma02;
    map['MSG_MA_03'] = _msgma03;
    map['MSG_MA_04'] = _msgma04;
    map['MSG_MA_05'] = _msgma05;
    map['MSG_MA_06'] = _msgma06;
    map['MSG_MA_07'] = _msgma07;
    map['MSG_MA_08'] = _msgma08;
    map['MSG_MA_09'] = _msgma09;
    map['MSG_MA_10'] = _msgma10;
    map['MSG_MA_11'] = _msgma11;
    map['MSG_MA_12'] = _msgma12;
    map['MSG_MA_13'] = _msgma13;
    map['MSG_MA_14'] = _msgma14;
    map['MSG_MA_15'] = _msgma15;
    map['MSG_MA_16'] = _msgma16;
    map['MSG_MA_17'] = _msgma17;
    map['MSG_MA_18'] = _msgma18;
    map['MSG_MA_19'] = _msgma19;
    map['MSG_MA_20'] = _msgma20;
    map['MSG_MA_21'] = _msgma21;
    map['MSG_MA_22'] = _msgma22;
    map['MSG_MA_23'] = _msgma23;
    map['LBL_MA_VALIDATION_01'] = _lblmavalidation01;
    map['LBL_MA_VALIDATION_02'] = _lblmavalidation02;
    map['LBL_MA_VALIDATION_03'] = _lblmavalidation03;
    map['LBL_MA_VALIDATION_04'] = _lblmavalidation04;
    map['LBL_MA_VALIDATION_05'] = _lblmavalidation05;
    map['LBL_MA_VALIDATION_06'] = _lblmavalidation06;
    map['LBL_MA_VALIDATION_07'] = _lblmavalidation07;
    map['MSG_ADDRESS_DETAILS_SUCCESS'] = _msgaddressdetailssuccess;
    map['MSG_ADDRESS_DETAILS_ERROR'] = _msgaddressdetailserror;
    map['MSG_WAIT'] = _msgwait;
    map['MSG_COPY'] = _msgcopy;
    map['MSG_SETTING_SUCCESS'] = _msgsettingsuccess;
    map['MSG_SETTING_ERROR'] = _msgsettingerror;
    map['MSG_IF_TITLE'] = _msgiftitle;
    map['MSG_IF_01'] = _msgif01;
    map['MSG_IF_02'] = _msgif02;
    map['MSG_IF_03'] = _msgif03;
    map['MSG_IF_04'] = _msgif04;
    map['MSG_IF_05'] = _msgif05;
    map['MSG_IF_06'] = _msgif06;
    map['MSG_IF_07'] = _msgif07;
    map['MSG_IF_08'] = _msgif08;
    map['MSG_IF_09'] = _msgif09;
    map['MSG_FP_TITLE'] = _msgfptitle;
    map['MSG_FP_01'] = _msgfp01;
    map['MSG_FP_02'] = _msgfp02;
    map['MSG_RP_TITLE'] = _msgrptitle;
    map['MSG_RP_01'] = _msgrp01;
    map['MSG_RP_02'] = _msgrp02;
    map['MSG_BAD_REQUEST'] = _msgbadrequest;
    map['SHOP_CHANGE_01'] = _shopchange01;
    map['SHOP_CHANGE_02'] = _shopchange02;
    map['SHOP_CHANGE_03'] = _shopchange03;
    map['SHOP_CHANGE_04'] = _shopchange04;
    map['SHOP_CHANGE_05'] = _shopchange05;
    map['SHOP_CHANGE_06'] = _shopchange06;
    map['MSG_USER_NOT_FOUND'] = _msgusernotfound;
    map['MSG_ORDER_NOT_FOUND'] = _msgordernotfound;
    map['MSG_ORDER_DETAIL_NOT_FOUND'] = _msgorderdetailnotfound;
    map['RECHARGE_CHECKOUT_BTN_LBL'] = _rechargecheckoutbtnlbl;
    map['LBL_ALERT'] = _lblalert;
    map['ENTER_NUMBER'] = _enternumber;
    map['OPERATOR_DETECTED'] = _operatordetected;
    map['CHANGE'] = _change;
    map['SELECT_AMOUNT'] = _selectamount;
    map['HAVE_PROMO_CODE'] = _havepromocode;
    map['PLEASE_SELECT'] = _pleaseselect;
    map['ERROR_NUMBER'] = _errornumber;
    map['CHARACTER_LONG'] = _characterlong;
    map['PACKAGE_ERROR'] = _packageerror;
    map['WILL_RECEIVED'] = _willreceived;
    map['PRICE_ERROR_MAX'] = _priceerrormax;
    map['PRICE_ERROR_MIN'] = _priceerrormin;
    map['RECHARGE_STATUS'] = _rechargestatus;
    map['BECAUSE_OF'] = _becauseof;
    map['SOME_WENT_WRONG'] = _somewentwrong;
    map['CHECK_MOBILE_NO'] = _checkmobileno;
    map['NEED_PRODUCT'] = _needproduct;
    map['PURPOSE_PRODUCT'] = _purposeproduct;
    map['NEED_PRODUCT_HEAD'] = _needproducthead;
    map['COUNTRY_LABEL'] = _countrylabel;
    map['PRODUCT_PLACEHOLDER'] = _productplaceholder;
    map['PRODUCT_DESC_PLACEHOLDER'] = _productdescplaceholder;
    map['SEND'] = _send;
    map['PRODUCT_SENT'] = _productsent;
    map['PRODUCT_NAME'] = _productname;
    map['PRODUCT_DESC'] = _productdesc;
    map['PRODUCT_QTY'] = _productqty;
    map['ANY_WHERE'] = _anywhere;
    map['INFORM_AVAILABILITY'] = _informavailability;
    map['PROPOSE_COUNTRY_LABEL'] = _proposecountrylabel;
    map['PROPOSE_CITY_LABEL'] = _proposecitylabel;
    map['PROPOSE_CHOOSE_SHOP'] = _proposechooseshop;
    map['PROPOSE_EMAIL'] = _proposeemail;
    map['PROPOSE_TELEPHONE'] = _proposetelephone;
    map['FOOTER_MENU_STORIES'] = _footermenustories;
    map['FOOTER_MENU_PRESS'] = _footermenupress;
    map['FOOTER_MENU_CAREER'] = _footermenucareer;
    map['FOOTER_MENU_HELP'] = _footermenuhelp;
    map['FOOTER_MENU_FAQ'] = _footermenufaq;
    map['CHECKOUT_TRANSACTION_DETAILS'] = _checkouttransactiondetails;
    map['CHECKOUT_BENIFICIERY_DETAILS'] = _checkoutbenificierydetails;
    map['RECHARGE_RECIPIENT_DETAILS'] = _rechargerecipientdetails;
    map['CHECKOUT_PAYMENT_MODE'] = _checkoutpaymentmode;
    map['PLEASE_SELECT_DELIVERY_METHOD'] = _pleaseselectdeliverymethod;
    map['DO_YOU_HAVE_A_PROMO_CODE'] = _doyouhaveapromocode;
    map['STEP_2_TEXT'] = _step2text;
    map['SHIPPING_AND_HABDLING'] = _shippingandhabdling;
    map['TRANSACTION_ARE_DELIVERED_ONLU_UNDER_PRESENTATION_OF_VALID_CNI'] =
        _transactionaredeliveredonluunderpresentationofvalidcni;
    map['A_BETTER_WAY_TO_SEND_WHAT_REALLY_MATTER'] =
        _abetterwaytosendwhatreallymatter;
    map['WE_DELIVERED_PRODUCT_LOVED_ONCE'] = _wedeliveredproductlovedonce;
    map['SELECT_AREA'] = _selectarea;
    map['BUY_SAFELY_ONLINE'] = _buysafelyonline;
    map['GET_DELIVERY'] = _getdelivery;
    map['SELECT_AREA_AND_SHOP'] = _selectareaandshop;
    map['PAY_SAFELY'] = _paysafely;
    map['BENIFICIERY_RECIEVES'] = _benificieryrecieves;
    map['JOIN_THOUSANDS_OF_HAPPY_FAMILIES'] = _jointhousandsofhappyfamilies;
    map['WORLD_FAMILY_CONNECTED'] = _worldfamilyconnected;
    map['SATISFIED_USERS'] = _satisfiedusers;
    map['WHY_CHOOSE_FAMILOV'] = _whychoosefamilov;
    map['DISCOVERED_WHY_OUR_USER_LOVED_US'] = _discoveredwhyouruserlovedus;
    map['100%_SECURE'] = _secure;
    map['WE_USE_SSL_CERTIFICATE'] = _weusesslcertificate;
    map['YOUR_MONEY_IS_WELL_USED'] = _yourmoneyiswellused;
    map['SEND_MORE'] = _sendmore;
    map['EASY_TO_USE'] = _easytouse;
    map['POSITIVE_IMPACT'] = _positiveimpact;
    map['SOCIAL_ASPECT'] = _socialaspect;
    map['GIRANTEE_MONEY'] = _giranteemoney;
    map['SAVE_TRADITIONAL_MONEY'] = _savetraditionalmoney;
    map['FAMILOV_IS_VERY_EASY_TO_USE'] = _familovisveryeasytouse;
    map['WE_IMPROVE_PEOPLE_LIVES'] = _weimprovepeoplelives;
    map['WE_HELP_YOU_TO_HELP_PEOPLE'] = _wehelpyoutohelppeople;
    map['OUR_SERVICES'] = _ourservices;
    map['MORE_WAYS_TO SEND_WHAT_REALLY_MATTER'] =
        _morewaystosendwhatreallymatter;
    map['FOOD_DRINK_GROCERY'] = _fooddrinkgrocery;
    map['FOOD_DRINK_GROCERY_DESC'] = _fooddrinkgrocerydesc;
    map['MOBILE_TOP_UP'] = _mobiletopup;
    map['MOBILE_TOP_UP_DESC'] = _mobiletopupdesc;
    map['HEALTH_PAHARMACY'] = _healthpaharmacy;
    map['HEALTH_PAHARMACY_DESC'] = _healthpaharmacydesc;
    map['EDUCATION_FORMATION'] = _educationformation;
    map['EDUCATION_FORMATION_DESC'] = _educationformationdesc;
    map['CONSTRUCTION'] = _construction;
    map['CONSTRUCTION_DESC'] = _constructiondesc;
    map['MOBILITY_ENERGY'] = _mobilityenergy;
    map['MOBILITY_ENERGY_DESC'] = _mobilityenergydesc;
    map['SIGN_UP_FOR_FREE'] = _signupforfree;
    map['BECOME_PARTNER'] = _becomepartner;
    map['HOW_TO_GIVE_BACK'] = _howtogiveback;
    map['WE_BELIVE_THAT_GOING_GOOD'] = _webelivethatgoinggood;
    map['LEARN_MORE'] = _learnmore;
    map['SUBSCRIBE_TO_FAMILOV'] = _subscribetofamilov;
    map['AMAZING_OFFERS_UPDATES_INTERESTING_NEWS'] =
        _amazingoffersupdatesinterestingnews;
    map['ENTER_HERE_ANY_PLACE_INDICATIONS'] = _enterhereanyplaceindications;
    map['BRAVO_YOUR_PACKAGE_IS_READY'] = _bravoyourpackageisready;
    map['PLEASE_SELECT_THE_COUNTRY'] = _pleaseselectthecountry;
    map['USE_FAMILOV_AND_STAY_CONNECTED_WITH_LOVED_ONCE'] =
        _usefamilovandstayconnectedwithlovedonce;
    map['SELECT_THE_COUNTRY'] = _selectthecountry;
    map['BE_MORE_EFFICIENT_THAN_TRADITIONAL'] = _bemoreefficientthantraditional;
    map['I_LIKE_TO_GIVE_A_HELPING_HAND_IN_THE_COUNTRY'] =
        _iliketogiveahelpinghandinthecountry;
    map['HELP_US_TO_HELP'] = _helpustohelp;
    map['GO'] = _go;
    map['LOVE_OUR_APP'] = _loveourapp;
    map['LEGAL'] = _legal;
    map['TERMS_AND_CONDITION'] = _termsandcondition;
    map['FOR_PATNERS'] = _forpatners;
    map['MOBILE_INPUT_PLACEHOLDER'] = _mobileinputplaceholder;
    map['SUBMIT_PRODUCT'] = _submitproduct;
    map['RECHARGE_MAIN_TITLE'] = _rechargemaintitle;
    map['RECHARGE_TITLE_DESCTIPTION'] = _rechargetitledesctiption;
    map['RECHARGE_STEP_1_TITLE'] = _rechargestep1title;
    map['RECHARGE_STEP_1'] = _rechargestep1;
    map['RECHARGE_STEP_2_TITLE'] = _rechargestep2title;
    map['RECHARGE_STEP_2'] = _rechargestep2;
    map['RECHARGE_STEP_3_TITLE'] = _rechargestep3title;
    map['RECHARGE_STEP_3'] = _rechargestep3;
    map['RECHARGE_BANNER_TITLE'] = _rechargebannertitle;
    map['RECHARGE_BANNER_DESCRIPTION'] = _rechargebannerdescription;
    map['RECHARGE_SEARCH_PLACEHOLDER'] = _rechargesearchplaceholder;
    map['NEW_TO_FAMILOV'] = _newtofamilov;
    map['TAKE_LESS_THAN_MINUTES'] = _takelessthanminutes;
    map['SIGN_IN_WITH_FACEBOOK'] = _signinwithfacebook;
    map['SIGN_IN_WITH_GOOGLE'] = _signinwithgoogle;
    map['OR'] = _or;
    map['SIGN_UP_WITH_FACEBOOK'] = _signupwithfacebook;
    map['SIGN_UP_WITH_GOOGLE'] = _signupwithgoogle;
    map['SIGNUP_TERMS_AND_CONDITION'] = _signuptermsandcondition;
    map['SIGNUP_CONDITION'] = _signupcondition;
    map['NO_PRODUCT_FOUND'] = _noproductfound;
    map['SUBSCRIBE_TO_GET_BEST_OFFERES'] = _subscribetogetbestofferes;
    map['VIEW_MY_CART'] = _viewmycart;
    map['DL_BILL'] = _dlbill;
    map['DL_ORD_DETS'] = _dlorddets;
    map['SELECT_BENIFICERY_DETAILS'] = _selectbenificerydetails;
    map['ADD_NEW_RECIEPENT'] = _addnewreciepent;
    map['EASY_IS_NOT_IT'] = _easyisnotit;
    map['SELECT_PAYMENT_METHOD'] = _selectpaymentmethod;
    map['PERSONAL_MESSAGE'] = _personalmessage;
    map['DO_YOU_WANT_TO_LEAVE'] = _doyouwanttoleave;
    map['ACCEPT'] = _accept;
    map['SECURE_CHECKOUT'] = _securecheckout;
    map['SATISFACTION_GUARANTEED'] = _satisfactionguaranteed;
    map['PRIVACY_PROTECTED'] = _privacyprotected;
    map['SELECT_RECIEPENT'] = _selectreciepent;
    map['ADD_RECIEPENT'] = _addreciepent;
    map['MY_RECIEPENT'] = _myreciepent;
    map['MY_WISHLIST'] = _mywishlist;
    map['RECIEPENT_NAME'] = _reciepentname;
    map['RECIEPENT_MAIN_NO'] = _reciepentmainno;
    map['RECIEPENT_ANOTHER_NO'] = _reciepentanotherno;
    map['PLEASE_ENTER_FULL_NAME_INDICATED_ON_HIS_CNI'] =
        _pleaseenterfullnameindicatedonhiscni;
    map['EDIT'] = _edit;
    map['LBL_MY_ACCT_01'] = _lblmyacct01;
    map['LBL_MY_ACCT_02'] = _lblmyacct02;
    map['LBL_MY_ACCT_03'] = _lblmyacct03;
    map['LBL_MY_ACCT_04'] = _lblmyacct04;
    map['LBL_MY_ACCT_05'] = _lblmyacct05;
    map['LBL_MY_ACCT_06'] = _lblmyacct06;
    map['LBL_MY_ACCT_07'] = _lblmyacct07;
    map['LBL_MY_ACCT_08'] = _lblmyacct08;
    map['LBL_MY_ACCT_09'] = _lblmyacct09;
    map['LBL_MY_ACCT_10'] = _lblmyacct10;
    map['LBL_MY_ACCT_11'] = _lblmyacct11;
    map['LBL_MY_ACCT_12'] = _lblmyacct12;
    map['LBL_MY_ACCT_13'] = _lblmyacct13;
    map['LBL_MY_ACCT_14'] = _lblmyacct14;
    map['LBL_MY_ACCT_15'] = _lblmyacct15;
    map['CHECKOUT_CONTINUE'] = _checkoutcontinue;
    map['CHECKOUT_BACK'] = _checkoutback;
    map['ORDER_CONFIRMATION_1'] = _orderconfirmation1;
    map['LBL_PRODUCT_SORT_MOST_SOLD'] = _lblproductsortmostsold;
    map['LBL_PRODUCT_SORT_LEAST_SOLD'] = _lblproductsortleastsold;
    map['LBL_PRODUCT_SORT_PRICE_LOW'] = _lblproductsortpricelow;
    map['LBL_PRODUCT_SORT_PRICE_HIGH'] = _lblproductsortpricehigh;
    map['LBL_PRODUCT_SORT_RECENT_SOLD'] = _lblproductsortrecentsold;
    return map;
  }
}
