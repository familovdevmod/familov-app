import 'dart:convert';

/// status : true
/// message : "Data retrieved successfully."
/// data : {"count":18,"data_list":[{"country_id":"22","country_name":"Cameroun","country_iso":"CM","imageloc":"FAMILOV_1616838674.svg","country_banner":"FAMILOV_1616840886.png","domain_name":"cameroun","sort_order":"1"},{"country_id":"32","country_name":"Cote d Ivoire","country_iso":"CI","imageloc":"FAMILOV_1604730714.svg","country_banner":"FAMILOV_1616839395.png","domain_name":"cote-d-ivoire","sort_order":"2"},{"country_id":"31","country_name":"Mali","country_iso":"ML","imageloc":"FAMILOV_1601741538.png","country_banner":"FAMILOV_1616841724.png","domain_name":"mali","sort_order":"3"},{"country_id":"46","country_name":"Gabon","country_iso":"GA","imageloc":"FAMILOV_1616839982.svg","country_banner":"FAMILOV_1616839983.png","domain_name":"gabon","sort_order":"4"},{"country_id":"33","country_name":"Senegal","country_iso":"SN","imageloc":"FAMILOV_1604729918.svg","country_banner":"FAMILOV_1616842732.png","domain_name":"senegal","sort_order":"5"},{"country_id":"47","country_name":"Togo","country_iso":"TG","imageloc":"FAMILOV_1634196991.png","country_banner":"FAMILOV_1604733791.png","domain_name":"togo","sort_order":"6"},{"country_id":"34","country_name":"Burkina Faso","country_iso":"BF","imageloc":"FAMILOV_1604730207.svg","country_banner":"FAMILOV_1616843022.png","domain_name":"bukrina","sort_order":"7"},{"country_id":"29","country_name":"Benin","country_iso":"BJ","imageloc":"FAMILOV_1604730085.svg","country_banner":"FAMILOV_1616840600.png","domain_name":"benin","sort_order":"8"},{"country_id":"35","country_name":"RD Congo","country_iso":"CG","imageloc":"FAMILOV_1604733028.svg","country_banner":"FAMILOV_1604733029.png","domain_name":"rd-congo","sort_order":"9"},{"country_id":"36","country_name":"Congo","country_iso":"CD","imageloc":"FAMILOV_1604733143.svg","country_banner":"FAMILOV_1604733144.png","domain_name":"congo","sort_order":"10"},{"country_id":"37","country_name":"Guinée","country_iso":"GN","imageloc":"FAMILOV_1604733242.svg","country_banner":"FAMILOV_1616842939.png","domain_name":"guinee","sort_order":"11"},{"country_id":"38","country_name":"Nigeria","country_iso":"NG","imageloc":"FAMILOV_1604733332.svg","country_banner":"FAMILOV_1604733333.png","domain_name":"nigeria","sort_order":"12"},{"country_id":"39","country_name":"Ghana","country_iso":"GH","imageloc":"FAMILOV_1604733391.svg","country_banner":"FAMILOV_1604733392.png","domain_name":"ghana","sort_order":"13"},{"country_id":"40","country_name":"Rwanda","country_iso":"RW","imageloc":"FAMILOV_1604733509.svg","country_banner":"FAMILOV_1604733510.png","domain_name":"rwanda","sort_order":"14"},{"country_id":"41","country_name":"Tunisia","country_iso":"TN","imageloc":"FAMILOV_1604733707.svg","country_banner":"FAMILOV_1604733708.png","domain_name":"tunisia","sort_order":"15"},{"country_id":"42","country_name":"Algeria","country_iso":"DZ","imageloc":"FAMILOV_1604733753.svg","country_banner":"FAMILOV_1604733754.png","domain_name":"algeria","sort_order":"16"},{"country_id":"43","country_name":"South Africa","country_iso":"CF","imageloc":"FAMILOV_1604733790.svg","country_banner":"FAMILOV_1604733791.png","domain_name":"south-africa","sort_order":"17"},{"country_id":"44","country_name":"Magadascar","country_iso":"MG","imageloc":"FAMILOV_1604733858.svg","country_banner":"FAMILOV_1604733859.png","domain_name":"magadascar","sort_order":"18"}]}
/// error_code : null
/// error_msg : null
/// meta : null

class CountryModel {
  CountryModel({
      bool? status, 
      String? message, 
      Data? data, 
      dynamic errorCode, 
      dynamic errorMsg, 
      dynamic meta,}){
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
}

  CountryModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }

}

/// count : 18
/// data_list : [{"country_id":"22","country_name":"Cameroun","country_iso":"CM","imageloc":"FAMILOV_1616838674.svg","country_banner":"FAMILOV_1616840886.png","domain_name":"cameroun","sort_order":"1"},{"country_id":"32","country_name":"Cote d Ivoire","country_iso":"CI","imageloc":"FAMILOV_1604730714.svg","country_banner":"FAMILOV_1616839395.png","domain_name":"cote-d-ivoire","sort_order":"2"},{"country_id":"31","country_name":"Mali","country_iso":"ML","imageloc":"FAMILOV_1601741538.png","country_banner":"FAMILOV_1616841724.png","domain_name":"mali","sort_order":"3"},{"country_id":"46","country_name":"Gabon","country_iso":"GA","imageloc":"FAMILOV_1616839982.svg","country_banner":"FAMILOV_1616839983.png","domain_name":"gabon","sort_order":"4"},{"country_id":"33","country_name":"Senegal","country_iso":"SN","imageloc":"FAMILOV_1604729918.svg","country_banner":"FAMILOV_1616842732.png","domain_name":"senegal","sort_order":"5"},{"country_id":"47","country_name":"Togo","country_iso":"TG","imageloc":"FAMILOV_1634196991.png","country_banner":"FAMILOV_1604733791.png","domain_name":"togo","sort_order":"6"},{"country_id":"34","country_name":"Burkina Faso","country_iso":"BF","imageloc":"FAMILOV_1604730207.svg","country_banner":"FAMILOV_1616843022.png","domain_name":"bukrina","sort_order":"7"},{"country_id":"29","country_name":"Benin","country_iso":"BJ","imageloc":"FAMILOV_1604730085.svg","country_banner":"FAMILOV_1616840600.png","domain_name":"benin","sort_order":"8"},{"country_id":"35","country_name":"RD Congo","country_iso":"CG","imageloc":"FAMILOV_1604733028.svg","country_banner":"FAMILOV_1604733029.png","domain_name":"rd-congo","sort_order":"9"},{"country_id":"36","country_name":"Congo","country_iso":"CD","imageloc":"FAMILOV_1604733143.svg","country_banner":"FAMILOV_1604733144.png","domain_name":"congo","sort_order":"10"},{"country_id":"37","country_name":"Guinée","country_iso":"GN","imageloc":"FAMILOV_1604733242.svg","country_banner":"FAMILOV_1616842939.png","domain_name":"guinee","sort_order":"11"},{"country_id":"38","country_name":"Nigeria","country_iso":"NG","imageloc":"FAMILOV_1604733332.svg","country_banner":"FAMILOV_1604733333.png","domain_name":"nigeria","sort_order":"12"},{"country_id":"39","country_name":"Ghana","country_iso":"GH","imageloc":"FAMILOV_1604733391.svg","country_banner":"FAMILOV_1604733392.png","domain_name":"ghana","sort_order":"13"},{"country_id":"40","country_name":"Rwanda","country_iso":"RW","imageloc":"FAMILOV_1604733509.svg","country_banner":"FAMILOV_1604733510.png","domain_name":"rwanda","sort_order":"14"},{"country_id":"41","country_name":"Tunisia","country_iso":"TN","imageloc":"FAMILOV_1604733707.svg","country_banner":"FAMILOV_1604733708.png","domain_name":"tunisia","sort_order":"15"},{"country_id":"42","country_name":"Algeria","country_iso":"DZ","imageloc":"FAMILOV_1604733753.svg","country_banner":"FAMILOV_1604733754.png","domain_name":"algeria","sort_order":"16"},{"country_id":"43","country_name":"South Africa","country_iso":"CF","imageloc":"FAMILOV_1604733790.svg","country_banner":"FAMILOV_1604733791.png","domain_name":"south-africa","sort_order":"17"},{"country_id":"44","country_name":"Magadascar","country_iso":"MG","imageloc":"FAMILOV_1604733858.svg","country_banner":"FAMILOV_1604733859.png","domain_name":"magadascar","sort_order":"18"}]

class Data {
  Data({
      int? count, 
      List<CountryData_list>? dataList,}){
    _count = count;
    _dataList = dataList;
}

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(CountryData_list.fromJson(v));
      });
    }
  }
  int? _count;
  List<CountryData_list>? _dataList;

  int? get count => _count;
  List<CountryData_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// country_id : "22"
/// country_name : "Cameroun"
/// country_iso : "CM"
/// imageloc : "FAMILOV_1616838674.svg"
/// country_banner : "FAMILOV_1616840886.png"
/// domain_name : "cameroun"
/// sort_order : "1"

// ignore: camel_case_types
class CountryData_list {
  CountryData_list({
      String? countryId, 
      String? countryName, 
      String? countryIso, 
      String? imageloc, 
      String? countryBanner, 
      String? domainName, 
      String? sortOrder,}){
    _countryId = countryId;
    _countryName = countryName;
    _countryIso = countryIso;
    _imageloc = imageloc;
    _countryBanner = countryBanner;
    _domainName = domainName;
    _sortOrder = sortOrder;
}

  CountryData_list.fromJson(dynamic json) {
    _countryId = json['country_id'];
    _countryName = json['country_name'];
    _countryIso = json['country_iso'];
    _imageloc = json['imageloc'];
    _countryBanner = json['country_banner'];
    _domainName = json['domain_name'];
    _sortOrder = json['sort_order'];
  }
  String? _countryId;
  String? _countryName;
  String? _countryIso;
  String? _imageloc;
  String? _countryBanner;
  String? _domainName;
  String? _sortOrder;

  String? get countryId => _countryId;
  String? get countryName => _countryName;
  String? get countryIso => _countryIso;
  String? get imageloc => _imageloc;
  String? get countryBanner => _countryBanner;
  String? get domainName => _domainName;
  String? get sortOrder => _sortOrder;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['country_id'] = _countryId;
    map['country_name'] = _countryName;
    map['country_iso'] = _countryIso;
    map['imageloc'] = _imageloc;
    map['country_banner'] = _countryBanner;
    map['domain_name'] = _domainName;
    map['sort_order'] = _sortOrder;
    return map;
  }

}