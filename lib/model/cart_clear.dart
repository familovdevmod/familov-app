import 'dart:convert';

class CartClear {
  CartClear({
      bool? status, 
      String? message, 
      List<dynamic>? data, 
      dynamic errorCode, 
      dynamic errorMsg, 
      dynamic meta,}){
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
}

  CartClear.fromJson(dynamic valueMap) {
        Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CartClear.fromJson(v));
      });
    }
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  List<dynamic>? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  
CartClear copyWith({  bool? status,
  String? message,
  List<dynamic>? data,
  dynamic errorCode,
  dynamic errorMsg,
  dynamic meta,
}) => CartClear(  status: status ?? _status,
  message: message ?? _message,
  data: data ?? _data,
  errorCode: errorCode ?? _errorCode,
  errorMsg: errorMsg ?? _errorMsg,
  meta: meta ?? _meta,
);
  bool? get status => _status;
  String? get message => _message;
  List<dynamic>? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }

}