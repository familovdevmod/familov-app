import 'dart:convert';

class AddBeneficiary {
  AddBeneficiary({
      bool? status, 
      String? message, 
      Data? data, 
      dynamic errorCode, 
      dynamic errorMsg, 
      dynamic meta,}){
    _status = status;
    _message = message;
    //_data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
}

  AddBeneficiary.fromJson(dynamic valueMap) {
     Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
 //   _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
 // Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
 // Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    // if (_data != null) {
    //   map['data'] = _data?.toJson();
    // }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }

}

class Data {
  Data({
      Data_list? dataList,}){
    _dataList = dataList;
}

  Data.fromJson(dynamic json) {
    _dataList = json['data_list'];
  }
  Data_list? _dataList;

  Data_list? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['data_list'] = _dataList;
    return map;
  }

}

class Data_list {
  Data_list({
      int? customerId,
      String? phoneCode, 
      String? recipientInformation, 
      String? recipientPhone, 
      String? anotherPhoneNumber, 
      String? createdAt,}){
    _customerId = customerId;
    _recipientInformation = recipientInformation;
    _recipientPhone = recipientPhone;
    _anotherPhoneNumber = anotherPhoneNumber;
    _createdAt = createdAt;
}

  Data_list.fromJson(dynamic json) {
    _customerId = json['customer_id'];
    _recipientInformation = json['recipient_information'];
    _recipientPhone = json['recipient_phone'];
    _anotherPhoneNumber = json['another_phone_number'];
    _createdAt = json['created_at'];
  }
  int? _customerId;
  String? _recipientInformation;
  String? _recipientPhone;
  String? _anotherPhoneNumber;
  String? _createdAt;

  int? get customerId => _customerId;
  String? get recipientInformation => _recipientInformation;
  String? get recipientPhone => _recipientPhone;
  String? get anotherPhoneNumber => _anotherPhoneNumber;
  String? get createdAt => _createdAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['customer_id'] = _customerId;
    map['recipient_information'] = _recipientInformation;
    map['recipient_phone'] = _recipientPhone;
    map['another_phone_number'] = _anotherPhoneNumber;
    map['created_at'] = _createdAt;
    return map;
  }

}