import 'dart:convert';

class ProductModel {
  ProductModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  ProductModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    String? perPage,
    int? offset,
    String? currentSort,
    List<Product_Data_list>? dataList,
  }) {
    _count = count;
    _perPage = perPage;
    _offset = offset;
    _currentSort = currentSort;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    _perPage = json['per_page'];
    _offset = json['offset'];
    _currentSort = json['current_sort'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Product_Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  String? _perPage;
  int? _offset;
  String? _currentSort;
  List<Product_Data_list>? _dataList;

  int? get count => _count;
  String? get perPage => _perPage;
  int? get offset => _offset;
  String? get currentSort => _currentSort;
  List<Product_Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['per_page'] = _perPage;
    map['offset'] = _offset;
    map['current_sort'] = _currentSort;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

// ignore: camel_case_types
class Product_Data_list {
  Product_Data_list({
    String? productId,
    String? shopId,
    String? categoryId,
    String? customerId,
    dynamic managerId,
    String? productName,
    String? productShortDesc,
    String? productDesc,
    String? productImage,
    String? productPriceCurrencyId,
    String? isProposedByManager,
    String? productPrices,
    dynamic productPriceCurrencyIdManager,
    dynamic productPricesManager,
    String? specialProductPrices,
    String? productStatus,
    String? productAvailability,
    String? isRead,
    dynamic createdAt,
    dynamic updatedAt,
    dynamic productUniqueId,
    String? mainCategoryId,
    String? isMarkedTop,
  }) {
    _productId = productId;
    _shopId = shopId;
    _categoryId = categoryId;
    _customerId = customerId;
    _managerId = managerId;
    _productName = productName;
    _productShortDesc = productShortDesc;
    _productDesc = productDesc;
    _productImage = productImage;
    _productPriceCurrencyId = productPriceCurrencyId;
    _isProposedByManager = isProposedByManager;
    _productPrices = productPrices;
    _productPriceCurrencyIdManager = productPriceCurrencyIdManager;
    _productPricesManager = productPricesManager;
    _specialProductPrices = specialProductPrices;
    _productStatus = productStatus;
    _productAvailability = productAvailability;
    _isRead = isRead;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _productUniqueId = productUniqueId;
    _mainCategoryId = mainCategoryId;
    _isMarkedTop = isMarkedTop;
  }

  Product_Data_list.fromJson(dynamic json) {
    _productId = json['product_id'];
    _shopId = json['shop_id'];
    _categoryId = json['category_id'];
    _customerId = json['customer_id'];
    _managerId = json['manager_id'];
    _productName = json['product_name'];
    _productShortDesc = json['product_short_desc'];
    _productDesc = json['product_desc'];
    _productImage = json['product_image'];
    _productPriceCurrencyId = json['product_price_currency_id'];
    _isProposedByManager = json['is_proposed_by_manager'];
    _productPrices = json['product_prices'];
    _productPriceCurrencyIdManager = json['product_price_currency_id_manager'];
    _productPricesManager = json['product_prices_manager'];
    _specialProductPrices = json['special_product_prices'];
    _productStatus = json['product_status'];
    _productAvailability = json['product_availability'];
    _isRead = json['is_read'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _productUniqueId = json['product_unique_id'];
    _mainCategoryId = json['main_category_id'];
    _isMarkedTop = json['is_marked_top'];
  }
  String? _productId;
  String? _shopId;
  String? _categoryId;
  String? _customerId;
  dynamic _managerId;
  String? _productName;
  String? _productShortDesc;
  String? _productDesc;
  String? _productImage;
  String? _productPriceCurrencyId;
  String? _isProposedByManager;
  String? _productPrices;
  dynamic _productPriceCurrencyIdManager;
  dynamic _productPricesManager;
  String? _specialProductPrices;
  String? _productStatus;
  String? _productAvailability;
  String? _isRead;
  dynamic _createdAt;
  dynamic _updatedAt;
  dynamic _productUniqueId;
  String? _mainCategoryId;
  String? _isMarkedTop;

  String? get productId => _productId;
  String? get shopId => _shopId;
  String? get categoryId => _categoryId;
  String? get customerId => _customerId;
  dynamic get managerId => _managerId;
  String? get productName => _productName;
  String? get productShortDesc => _productShortDesc;
  String? get productDesc => _productDesc;
  String? get productImage => _productImage;
  String? get productPriceCurrencyId => _productPriceCurrencyId;
  String? get isProposedByManager => _isProposedByManager;
  String? get productPrices => _productPrices;
  dynamic get productPriceCurrencyIdManager => _productPriceCurrencyIdManager;
  dynamic get productPricesManager => _productPricesManager;
  String? get specialProductPrices => _specialProductPrices;
  String? get productStatus => _productStatus;
  String? get productAvailability => _productAvailability;
  String? get isRead => _isRead;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  dynamic get productUniqueId => _productUniqueId;
  String? get mainCategoryId => _mainCategoryId;
  String? get isMarkedTop => _isMarkedTop;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['product_id'] = _productId;
    map['shop_id'] = _shopId;
    map['category_id'] = _categoryId;
    map['customer_id'] = _customerId;
    map['manager_id'] = _managerId;
    map['product_name'] = _productName;
    map['product_short_desc'] = _productShortDesc;
    map['product_desc'] = _productDesc;
    map['product_image'] = _productImage;
    map['product_price_currency_id'] = _productPriceCurrencyId;
    map['is_proposed_by_manager'] = _isProposedByManager;
    map['product_prices'] = _productPrices;
    map['product_price_currency_id_manager'] = _productPriceCurrencyIdManager;
    map['product_prices_manager'] = _productPricesManager;
    map['special_product_prices'] = _specialProductPrices;
    map['product_status'] = _productStatus;
    map['product_availability'] = _productAvailability;
    map['is_read'] = _isRead;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['product_unique_id'] = _productUniqueId;
    map['main_category_id'] = _mainCategoryId;
    map['is_marked_top'] = _isMarkedTop;
    return map;
  }
}
