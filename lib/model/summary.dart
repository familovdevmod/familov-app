import 'dart:convert';

class Summary {
  Summary({
    bool? status,
    String? message,
    Data11? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Summary.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data11.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data11? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data11? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data11 {
  Data11({
    double? subTotal,
    double? serviceCharge,
    String? deliveryMode,
    String? deliveryCharge,
    String? promoCode,
    dynamic promoCodeName,
    int? total,
  }) {
    _subTotal = subTotal;
    _serviceCharge = serviceCharge;
    _deliveryMode = deliveryMode;
    _deliveryCharge = deliveryCharge;
    _promoCode = promoCode;
    _promoCodeName = promoCodeName;
    _total = total;
  }

  Data11.fromJson(dynamic json) {
    _subTotal = json['sub_total'];
    _serviceCharge = json['service_charge'];
    _deliveryMode = json['delivery_mode'];
    _deliveryCharge = json['delivery_charge'];
    _promoCode = json['promo_code'];
    _promoCodeName = json['promo_code_name']  != null ? Data11.fromJson(json['promo_code_name']) : '';
    // _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _total = json['total'];
  }
  var _subTotal;
  double? _serviceCharge;
  String? _deliveryMode;
  String? _deliveryCharge;
  String? _promoCode;
  dynamic _promoCodeName;
  var _total;

  double? get subTotal => _subTotal;
  double? get serviceCharge => _serviceCharge;
  String? get deliveryMode => _deliveryMode;
  String? get deliveryCharge => _deliveryCharge;
  String? get promoCode => _promoCode;
  dynamic get promoCodeName => _promoCodeName;
  int? get total => _total;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['sub_total'] = _subTotal;
    map['service_charge'] = _serviceCharge;
    map['delivery_mode'] = _deliveryMode;
    map['delivery_charge'] = _deliveryCharge;
    map['promo_code'] = _promoCode;
    map['promo_code_name'] = _promoCodeName;
    map['total'] = _total;
    return map;
  }
}
