import 'dart:convert';


class CurrencyModel {
  CurrencyModel({
      bool? status, 
      String? message, 
      Data? data, 
      dynamic errorCode, 
      dynamic errorMsg, 
      dynamic meta,}){
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
}

  CurrencyModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    print('jsonresponse currency ${json['status']}');
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }

}

/// count : 4
/// data_list : [{"iCurrencyId":"1","vName":"USD","vCode":"USD","vLangCode":"en","vLanguage":"English","rate":"1.25","vSymbol":"$","en_rate":"1","eDefault":"No"},{"iCurrencyId":"2","vName":"EUR","vCode":"EUR","vLangCode":"fr","vLanguage":"French","rate":"1","vSymbol":"&euro;","en_rate":"1.06","eDefault":"Yes"},{"iCurrencyId":"3","vName":"CAD","vCode":"CAD","vLangCode":"en","vLanguage":"English","rate":"1.57","vSymbol":"c$","en_rate":"1","eDefault":"No"},{"iCurrencyId":"4","vName":"GBP","vCode":"GBP","vLangCode":"en","vLanguage":"English","rate":"0.93","vSymbol":"£","en_rate":"1","eDefault":"No"}]

class Data {
  Data({
      int? count, 
      List<Data_list>? dataList,}){
    _count = count;
    _dataList = dataList;
}

  Data.fromJson(dynamic json) {

    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_list>? _dataList;

  int? get count => _count;
  List<Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// iCurrencyId : "1"
/// vName : "USD"
/// vCode : "USD"
/// vLangCode : "en"
/// vLanguage : "English"
/// rate : "1.25"
/// vSymbol : "$"
/// en_rate : "1"
/// eDefault : "No"

class Data_list {
  Data_list({
      String? iCurrencyId, 
      String? vName, 
      String? vCode, 
      String? vLangCode, 
      String? vLanguage, 
      String? rate, 
      String? vSymbol, 
      String? enRate, 
      String? eDefault,}){
    _iCurrencyId = iCurrencyId;
    _vName = vName;
    _vCode = vCode;
    _vLangCode = vLangCode;
    _vLanguage = vLanguage;
    _rate = rate;
    _vSymbol = vSymbol;
    _enRate = enRate;
    _eDefault = eDefault;
}

  Data_list.fromJson(dynamic json) {

    _iCurrencyId = json['iCurrencyId'];
    _vName = json['vName'];
    _vCode = json['vCode'];
    _vLangCode = json['vLangCode'];
    _vLanguage = json['vLanguage'];
    _rate = json['rate'];
    _vSymbol = json['vSymbol'];
    _enRate = json['en_rate'];
    _eDefault = json['eDefault'];
  }
  String? _iCurrencyId;
  String? _vName;
  String? _vCode;
  String? _vLangCode;
  String? _vLanguage;
  String? _rate;
  String? _vSymbol;
  String? _enRate;
  String? _eDefault;

  String? get iCurrencyId => _iCurrencyId;
  String? get vName => _vName;
  String? get vCode => _vCode;
  String? get vLangCode => _vLangCode;
  String? get vLanguage => _vLanguage;
  String? get rate => _rate;
  String? get vSymbol => _vSymbol;
  String? get enRate => _enRate;
  String? get eDefault => _eDefault;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['iCurrencyId'] = _iCurrencyId;
    map['vName'] = _vName;
    map['vCode'] = _vCode;
    map['vLangCode'] = _vLangCode;
    map['vLanguage'] = _vLanguage;
    map['rate'] = _rate;
    map['vSymbol'] = _vSymbol;
    map['en_rate'] = _enRate;
    map['eDefault'] = _eDefault;
    return map;
  }

}