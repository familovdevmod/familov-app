import 'dart:convert';

class ShopModel {
  ShopModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  ShopModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<Data_lists>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_lists.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_lists>? _dataList;

  int? get count => _count;
  List<Data_lists>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['Data_lists'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

// ignore: camel_case_types
class Data_lists {
  Data_lists({
    String? shopId,
    String? slug,
    String? shopName,
    String? shopAddress,
    String? shopLogo,
    String? shopBanner,
    String? cityId,
    String? countryId,
    dynamic minimumOrderAmount,
    String? minAmountPickup,
    String? pickupFromShopPrice,
    String? homeDeliveryPrice,
    String? homeDeliveryStatus,
    String? shopStatus,
    String? pickupFromShopStatus,
    String? parentShopId,
  }) {
    _shopId = shopId;
    _slug = slug;
    _shopName = shopName;
    _shopAddress = shopAddress;
    _shopLogo = shopLogo;
    _shopBanner = shopBanner;
    _cityId = cityId;
    _countryId = countryId;
    _minimumOrderAmount = minimumOrderAmount;
    _minAmountPickup = minAmountPickup;
    _pickupFromShopPrice = pickupFromShopPrice;
    _homeDeliveryPrice = homeDeliveryPrice;
    _homeDeliveryStatus = homeDeliveryStatus;
    _shopStatus = shopStatus;
    _pickupFromShopStatus = pickupFromShopStatus;
    _parentShopId = parentShopId;
  }

  Data_lists.fromJson(dynamic json) {
   // print('shop ka naam : ${json['shop_name']}');
    _shopId = json['shop_id'];
    _slug = json['slug'];
    _shopName = json['shop_name'];
    _shopAddress = json['shop_address'];
    _shopLogo = json['shop_logo'];
    _shopBanner = json['shop_banner'];
    _cityId = json['city_id'];
    _countryId = json['country_id'];
    _minimumOrderAmount = json['minimum_order_amount'];
    _minAmountPickup = json['min_amount_pickup'];
    _pickupFromShopPrice = json['pickup_from_shop_price'];
    _homeDeliveryPrice = json['home_delivery_price'];
    _homeDeliveryStatus = json['home_delivery_status'];
    _shopStatus = json['shop_status'];
    _pickupFromShopStatus = json['pickup_from_shop_status'];
    _parentShopId = json['parent_shop_id'];
  }
  String? _shopId;
  String? _slug;
  String? _shopName;
  String? _shopAddress;
  String? _shopLogo;
  String? _shopBanner;
  String? _cityId;
  String? _countryId;
  dynamic _minimumOrderAmount;
  String? _minAmountPickup;
  String? _pickupFromShopPrice;
  String? _homeDeliveryPrice;
  String? _homeDeliveryStatus;
  String? _shopStatus;
  String? _pickupFromShopStatus;
  String? _parentShopId;

  String? get shopId => _shopId;
  String? get slug => _slug;
  String? get shopName => _shopName;
  String? get shopAddress => _shopAddress;
  String? get shopLogo => _shopLogo;
  String? get shopBanner => _shopBanner;
  String? get cityId => _cityId;
  String? get countryId => _countryId;
  dynamic get minimumOrderAmount => _minimumOrderAmount;
  String? get minAmountPickup => _minAmountPickup;
  String? get pickupFromShopPrice => _pickupFromShopPrice;
  String? get homeDeliveryPrice => _homeDeliveryPrice;
  String? get homeDeliveryStatus => _homeDeliveryStatus;
  String? get shopStatus => _shopStatus;
  String? get pickupFromShopStatus => _pickupFromShopStatus;
  String? get parentShopId => _parentShopId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shop_id'] = _shopId;
    map['slug'] = _slug;
    map['shop_name'] = _shopName;
    map['shop_address'] = _shopAddress;
    map['shop_logo'] = _shopLogo;
    map['shop_banner'] = _shopBanner;
    map['city_id'] = _cityId;
    map['country_id'] = _countryId;
    map['minimum_order_amount'] = _minimumOrderAmount;
    map['min_amount_pickup'] = _minAmountPickup;
    map['pickup_from_shop_price'] = _pickupFromShopPrice;
    map['home_delivery_price'] = _homeDeliveryPrice;
    map['home_delivery_status'] = _homeDeliveryStatus;
    map['shop_status'] = _shopStatus;
    map['pickup_from_shop_status'] = _pickupFromShopStatus;
    map['parent_shop_id'] = _parentShopId;
    return map;
  }
}
