import 'dart:convert';

class ReorderModel {
  bool? status;
  String? message;
  bool? data;
  dynamic errorCode;
  dynamic errorMsg;
  dynamic meta;

  ReorderModel(
      {required this.status,
      required this.message,
      required this.data,
      this.errorCode,
      this.errorMsg,
      this.meta});

  ReorderModel.fromJson(dynamic jsonn) {
    Map json = jsonDecode(jsonn);
    if (json["status"] is bool) this.status = json["status"];
    if (json["message"] is String) this.message = json["message"];
    if (json["data"] is bool) this.data = json["data"];
    this.errorCode = json["error_code"];
    this.errorMsg = json["error_msg"];
    this.meta = json["meta"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["status"] = this.status;
    data["message"] = this.message;
    data["data"] = this.data;
    data["error_code"] = this.errorCode;
    data["error_msg"] = this.errorMsg;
    data["meta"] = this.meta;
    return data;
  }
}
