import 'dart:convert';

class Totalcustomer {
  Totalcustomer({
    bool? status,
    String? message,
    int? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Totalcustomer.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'];
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  int? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;
  Totalcustomer copyWith({
    bool? status,
    String? message,
    int? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) =>
      Totalcustomer(
        status: status ?? _status,
        message: message ?? _message,
        data: data ?? _data,
        errorCode: errorCode ?? _errorCode,
        errorMsg: errorMsg ?? _errorMsg,
        meta: meta ?? _meta,
      );
  bool? get status => _status;
  String? get message => _message;
  int? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    map['data'] = _data;
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}
