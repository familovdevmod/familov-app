import 'dart:convert';

class BeneficiaryUpdate {
  BeneficiaryUpdate({
    bool? status,
    String? message,
  //  Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
  //  _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  BeneficiaryUpdate.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    print('check update');
    _status = json['status'];
    _message = json['message'];
  //  _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  //Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  //Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    // if (_data != null) {
    //   map['data'] = _data?.toJson();
    // }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

// class Data {
//   Data({
//     Data_list? dataList,
//   }) {
//     _dataList = dataList;
//   }

//   Data.fromJson(dynamic valueMap) {
//     print('check update 2');
//         Map json = jsonDecode(valueMap);
//     _dataList = json['data_list'];
//     print('check update 3');
//   }
//   Data_list? _dataList;

//   Data_list? get dataList => _dataList;

//   Map<String, dynamic> toJson() {
//     print('check update 4');
//     final map = <String, dynamic>{};
//     map['data_list'] = _dataList;
//     return map;
//   }
// }

// ignore: camel_case_types
class Data_list {
  Data_list({
    int? customerId,
    String? recipientInformation,
    String? recipientPhone,
    String? anotherPhoneNumber,
    String? modifiedAt,
  }) {
    _customerId = customerId;
    _recipientInformation = recipientInformation;
    _recipientPhone = recipientPhone;
    _anotherPhoneNumber = anotherPhoneNumber;
    _modifiedAt = modifiedAt;
  }

  Data_list.fromJson(dynamic json) {
    _customerId = json['customer_id'];
    _recipientInformation = json['recipient_information'];
    _recipientPhone = json['recipient_phone'];
    _anotherPhoneNumber = json['another_phone_number'];
    _modifiedAt = json['modified_at'];
  }
  int? _customerId;
  String? _recipientInformation;
  String? _recipientPhone;
  String? _anotherPhoneNumber;
  String? _modifiedAt;

  int? get customerId => _customerId;
  String? get recipientInformation => _recipientInformation;
  String? get recipientPhone => _recipientPhone;
  String? get anotherPhoneNumber => _anotherPhoneNumber;
  String? get modifiedAt => _modifiedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['customer_id'] = _customerId;
    map['recipient_information'] = _recipientInformation;
    map['recipient_phone'] = _recipientPhone;
    map['another_phone_number'] = _anotherPhoneNumber;
    map['modified_at'] = _modifiedAt;
    return map;
  }
}
