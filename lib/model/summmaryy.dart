import 'dart:convert';

class Summmaryy {
  Summmaryy({
    bool? status,
    String? message,
    Data12? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Summmaryy.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != '' ? Data12.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data12? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data12? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data12 {
  Data12({
    dynamic totalAmount,
    dynamic subTotal,
    dynamic serviceCharge,
    dynamic deliveryCharge,
    dynamic promoDiscount,
    dynamic promoCode,
    dynamic deliveryMode,
    PromocodeDetails? promocodeDetails,
  }) {
    _totalAmount = totalAmount;
    _subTotal = subTotal;
    _serviceCharge = serviceCharge;
    _deliveryCharge = deliveryCharge;
    _promoDiscount = promoDiscount;
    _promoCode = promoCode;
    _deliveryMode = deliveryMode;
    _promocodeDetails = promocodeDetails;
  }

  Data12.fromJson(dynamic json) {
    _totalAmount = json['total_amount'];
    _subTotal = json['sub_total'];
    _serviceCharge = json['service_charge'];
    _deliveryCharge = json['delivery_charge'];
    _promoDiscount = json['promo_discount'];
    _promoCode = json['promo_code'];
    _deliveryMode = json['delivery_mode'];
    _promocodeDetails = json['promocode_details'] != ''
        ? PromocodeDetails.fromJson(json['promocode_details'])
        //    _data = json['data'] != '' ? Data12.fromJson(json['data']) : null;
        : null;
  }
  dynamic _totalAmount;
  dynamic _subTotal;
  dynamic _serviceCharge;
  dynamic _deliveryCharge;
  dynamic _promoDiscount;
  dynamic _promoCode;
  dynamic _deliveryMode;
  PromocodeDetails? _promocodeDetails;

  dynamic get totalAmount => _totalAmount;
  dynamic get subTotal => _subTotal;
  dynamic get serviceCharge => _serviceCharge;
  dynamic get deliveryCharge => _deliveryCharge;
  dynamic get promoDiscount => _promoDiscount;
  dynamic get promoCode => _promoCode;
  dynamic get deliveryMode => _deliveryMode;
  PromocodeDetails? get promocodeDetails => _promocodeDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total_amount'] = _totalAmount;
    map['sub_total'] = _subTotal;
    map['service_charge'] = _serviceCharge;
    map['delivery_charge'] = _deliveryCharge;
    map['promo_discount'] = _promoDiscount;
    map['promo_code'] = _promoCode;
    map['delivery_mode'] = _deliveryMode;
    if (_promocodeDetails != null) {
      map['promocode_details'] = _promocodeDetails?.toJson();
    }
    return map;
  }
}

class PromocodeDetails {
  PromocodeDetails({
    bool? isApplied,
    String? msg,
    Promocode? promocode,
    dynamic discount,
  }) {
    _isApplied = isApplied;
    _msg = msg;
    _promocode = promocode;
    _discount = discount;
  }

  PromocodeDetails.fromJson(dynamic json) {
    _isApplied = json['is_applied'];
    _msg = json['msg'];
    _promocode = json['promocode'] != null
        ? Promocode.fromJson(json['promocode'])
        : null;
    _discount = json['discount'];

    print('discount price ${_discount}');
  }
  bool? _isApplied;
  String? _msg;
  Promocode? _promocode;
  dynamic _discount;

  bool? get isApplied => _isApplied;
  String? get msg => _msg;
  Promocode? get promocode => _promocode;
  dynamic get discount => _discount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['is_applied'] = _isApplied;
    map['msg'] = _msg;
    if (_promocode != null) {
      map['promocode'] = _promocode?.toJson();
    }
    map['discount'] = _discount;
    return map;
  }
}

class Promocode {
  Promocode({
    String? promoId,
    String? vName,
    String? vCode,
    String? vDescription,
    String? iAmount,
    String? amountType,
    String? eStatus,
    String? startDate,
    String? endDate,
    String? minOrderAmount,
    String? maxUse,
    String? couponType,
  }) {
    _promoId = promoId;
    _vName = vName;
    _vCode = vCode;
    _vDescription = vDescription;
    _iAmount = iAmount;
    _amountType = amountType;
    _eStatus = eStatus;
    _startDate = startDate;
    _endDate = endDate;
    _minOrderAmount = minOrderAmount;
    _maxUse = maxUse;
    _couponType = couponType;
  }

  Promocode.fromJson(dynamic json) {
    _promoId = json['promo_id'];
    _vName = json['vName'];
    _vCode = json['vCode'];
    _vDescription = json['vDescription'];
    _iAmount = json['iAmount'];
    _amountType = json['amount_type'];
    _eStatus = json['eStatus'];
    _startDate = json['start_date'];
    _endDate = json['end_date'];
    _minOrderAmount = json['min_order_amount'];
    _maxUse = json['max_use'];
    _couponType = json['coupon_type'];
  }
  String? _promoId;
  String? _vName;
  String? _vCode;
  String? _vDescription;
  String? _iAmount;
  String? _amountType;
  String? _eStatus;
  String? _startDate;
  String? _endDate;
  String? _minOrderAmount;
  String? _maxUse;
  String? _couponType;

  String? get promoId => _promoId;
  String? get vName => _vName;
  String? get vCode => _vCode;
  String? get vDescription => _vDescription;
  String? get iAmount => _iAmount;
  String? get amountType => _amountType;
  String? get eStatus => _eStatus;
  String? get startDate => _startDate;
  String? get endDate => _endDate;
  String? get minOrderAmount => _minOrderAmount;
  String? get maxUse => _maxUse;
  String? get couponType => _couponType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['promo_id'] = _promoId;
    map['vName'] = _vName;
    map['vCode'] = _vCode;
    map['vDescription'] = _vDescription;
    map['iAmount'] = _iAmount;
    map['amount_type'] = _amountType;
    map['eStatus'] = _eStatus;
    map['start_date'] = _startDate;
    map['end_date'] = _endDate;
    map['min_order_amount'] = _minOrderAmount;
    map['max_use'] = _maxUse;
    map['coupon_type'] = _couponType;
    return map;
  }
}
