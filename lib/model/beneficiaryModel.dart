import 'dart:convert';

class BeneficiaryModel {
  BeneficiaryModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  BeneficiaryModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    String? pageTitle,
    List<Data_list>? dataList,
  }) {
    _count = count;
    _pageTitle = pageTitle;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    _pageTitle = json['page_title'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  String? _pageTitle;
  List<Data_list>? _dataList;

  int? get count => _count;
  String? get pageTitle => _pageTitle;
  List<Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['page_title'] = _pageTitle;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data_list {
  Data_list({
    String? beneficiaryId,
    String? beneficiaryCustomerId,
    String? beneficiaryFullName,
    String? beneficiaryMainPhoneNo,
    String? beneficiaryAltPhoneNo,
  }) {
    _beneficiaryId = beneficiaryId;
    _beneficiaryCustomerId = beneficiaryCustomerId;
    _beneficiaryFullName = beneficiaryFullName;
    _beneficiaryMainPhoneNo = beneficiaryMainPhoneNo;
    _beneficiaryAltPhoneNo = beneficiaryAltPhoneNo;
  }

  Data_list.fromJson(dynamic json) {
    _beneficiaryId = json['beneficiary_id'];
    _beneficiaryCustomerId = json['beneficiary_customer_id'];
    _beneficiaryFullName = json['beneficiary_full_name'];
    _beneficiaryMainPhoneNo = json['beneficiary_main_phone_no'];
    _beneficiaryAltPhoneNo = json['beneficiary_alt_phone_no'];
  }
  String? _beneficiaryId;
  String? _beneficiaryCustomerId;
  String? _beneficiaryFullName;
  String? _beneficiaryMainPhoneNo;
  String? _beneficiaryAltPhoneNo;

  String? get beneficiaryId => _beneficiaryId;
  String? get beneficiaryCustomerId => _beneficiaryCustomerId;
  String? get beneficiaryFullName => _beneficiaryFullName;
  String? get beneficiaryMainPhoneNo => _beneficiaryMainPhoneNo;
  String? get beneficiaryAltPhoneNo => _beneficiaryAltPhoneNo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['beneficiary_id'] = _beneficiaryId;
    map['beneficiary_customer_id'] = _beneficiaryCustomerId;
    map['beneficiary_full_name'] = _beneficiaryFullName;
    map['beneficiary_main_phone_no'] = _beneficiaryMainPhoneNo;
    map['beneficiary_alt_phone_no'] = _beneficiaryAltPhoneNo;
    return map;
  }
}
