import 'dart:convert';

class Checkout {
  Checkout({
    bool? status,
    String? message,
    List<Data90>? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Checkout.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data90.fromJson(v));
      });
    }
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  List<Data90>? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  List<Data90>? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data90 {
  Data90({
    String? pmId,
    String? pmName,
    String? pmSlug,
    String? pmImage,
    String? isActive,
  }) {
    _pmId = pmId;
    _pmName = pmName;
    _pmSlug = pmSlug;
    _pmImage = pmImage;
    _isActive = isActive;
  }

  Data90.fromJson(dynamic json) {
    _pmId = json['pm_id'];
    _pmName = json['pm_name'];
    _pmSlug = json['pm_slug'];
    _pmImage = json['pm_image'];
    _isActive = json['isActive'];
  }
  String? _pmId;
  String? _pmName;
  String? _pmSlug;
  String? _pmImage;
  String? _isActive;

  String? get pmId => _pmId;
  String? get pmName => _pmName;
  String? get pmSlug => _pmSlug;
  String? get pmImage => _pmImage;
  String? get isActive => _isActive;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pm_id'] = _pmId;
    map['pm_name'] = _pmName;
    map['pm_slug'] = _pmSlug;
    map['pm_image'] = _pmImage;
    map['isActive'] = _isActive;
    return map;
  }
}
