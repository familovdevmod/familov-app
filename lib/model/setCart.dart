import 'dart:convert';

class SetCart {
  SetCart({
    bool? status,
    String? message,
    dynamic data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  SetCart.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
   _data=json['data'];
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  dynamic _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  dynamic get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    map['data'] = _data;
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    String? cartId,
    String? parentOrderId,
    String? customerId,
    String? ipAddress,
    String? productId,
    String? price,
    dynamic priceTemp,
    dynamic sku,
    dynamic sendCurrencyIso,
    dynamic providerCode,
    dynamic phoneNum,
    dynamic phoneCode,
    String? orderFor,
    String? quantity,
    dynamic countryId,
    dynamic cityId,
    String? shopId,
    String? productName,
    String? productImage,
    String? categoryName,
    String? categoryNameEn,
  }) {
    _cartId = cartId;
    _parentOrderId = parentOrderId;
    _customerId = customerId;
    _ipAddress = ipAddress;
    _productId = productId;
    _price = price;
    _priceTemp = priceTemp;
    _sku = sku;
    _sendCurrencyIso = sendCurrencyIso;
    _providerCode = providerCode;
    _phoneNum = phoneNum;
    _phoneCode = phoneCode;
    _orderFor = orderFor;
    _quantity = quantity;
    _countryId = countryId;
    _cityId = cityId;
    _shopId = shopId;
    _productName = productName;
    _productImage = productImage;
    _categoryName = categoryName;
    _categoryNameEn = categoryNameEn;
  }

  Data.fromJson(dynamic json) {
    _cartId = json['cart_id'];
    _parentOrderId = json['parent_order_id'];
    _customerId = json['customer_id'];
    _ipAddress = json['ip_address'];
    _productId = json['product_id'];
    _price = json['price'];
    _priceTemp = json['price_temp'];
    _sku = json['sku'];
    _sendCurrencyIso = json['send_currency_iso'];
    _providerCode = json['provider_code'];
    _phoneNum = json['phone_num'];
    _phoneCode = json['phone_code'];
    _orderFor = json['order_for'];
    _quantity = json['quantity'];
    _countryId = json['country_id'];
    _cityId = json['city_id'];
    _shopId = json['shop_id'];
    _productName = json['product_name'];
    _productImage = json['product_image'];
    _categoryName = json['category_name'];
    _categoryNameEn = json['category_name_en'];
  }
  String? _cartId;
  String? _parentOrderId;
  String? _customerId;
  String? _ipAddress;
  String? _productId;
  String? _price;
  dynamic _priceTemp;
  dynamic _sku;
  dynamic _sendCurrencyIso;
  dynamic _providerCode;
  dynamic _phoneNum;
  dynamic _phoneCode;
  String? _orderFor;
  String? _quantity;
  dynamic _countryId;
  dynamic _cityId;
  String? _shopId;
  String? _productName;
  String? _productImage;
  String? _categoryName;
  String? _categoryNameEn;

  String? get cartId => _cartId;
  String? get parentOrderId => _parentOrderId;
  String? get customerId => _customerId;
  String? get ipAddress => _ipAddress;
  String? get productId => _productId;
  String? get price => _price;
  dynamic get priceTemp => _priceTemp;
  dynamic get sku => _sku;
  dynamic get sendCurrencyIso => _sendCurrencyIso;
  dynamic get providerCode => _providerCode;
  dynamic get phoneNum => _phoneNum;
  dynamic get phoneCode => _phoneCode;
  String? get orderFor => _orderFor;
  String? get quantity => _quantity;
  dynamic get countryId => _countryId;
  dynamic get cityId => _cityId;
  String? get shopId => _shopId;
  String? get productName => _productName;
  String? get productImage => _productImage;
  String? get categoryName => _categoryName;
  String? get categoryNameEn => _categoryNameEn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['cart_id'] = _cartId;
    map['parent_order_id'] = _parentOrderId;
    map['customer_id'] = _customerId;
    map['ip_address'] = _ipAddress;
    map['product_id'] = _productId;
    map['price'] = _price;
    map['price_temp'] = _priceTemp;
    map['sku'] = _sku;
    map['send_currency_iso'] = _sendCurrencyIso;
    map['provider_code'] = _providerCode;
    map['phone_num'] = _phoneNum;
    map['phone_code'] = _phoneCode;
    map['order_for'] = _orderFor;
    map['quantity'] = _quantity;
    map['country_id'] = _countryId;
    map['city_id'] = _cityId;
    map['shop_id'] = _shopId;
    map['product_name'] = _productName;
    map['product_image'] = _productImage;
    map['category_name'] = _categoryName;
    map['category_name_en'] = _categoryNameEn;
    return map;
  }
}
