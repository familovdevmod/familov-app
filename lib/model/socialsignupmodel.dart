// import 'dart:convert';
//
// class SocialSignUpModel
//
//

import 'dart:convert';

/// status : true
/// message : "Logged in successfully!"
/// data : {"customer_id":"13089","oauth_provider":null,"oauth_uid":null,"sender_phone":"","if_vendor":"0","username":"Jash","lastname":"","recp_name":"","password":"","email_address":"oraissn@gmai.com","home_address":"","address_line2":null,"phone_number":"","city_id":"","country_id":"","shop_name":"","shop_address":"","shop_logo":"","shop_banner":"","status":"active","referral":"","maturity_status":"0","iGift":"0","eUsed":"unused","myReferral":"0","totalReferral":"0","dob":null,"notification_email":"0","notification_text":"0","country_code":null,"home_gender":null,"postal_code":null,"phone_code":"","stripe_id":null,"pwd_token":null,"signup_date":"2022-06-15","ewallet_in":"0.00","ewallet_out":"0.00","social_login_type":null,"fb_id":"f15d4f7f7ce2221d1d97623b778c27b3","google_id":null,"locale":null,"profile_photo":"","id_passport":null,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTMwODksImVtYWlsIjoib3JhaXNzbkBnbWFpLmNvbSIsImlhdCI6MTY1NTI5OTk2MywiZXhwIjoxNjU1NDcyNzYzfQ.GmWPmjcxL1RnSO2kjcRNxsxsWS6b7GRlaT0Iqs-Uy2U"}
/// error_code : null
/// error_msg : null
/// meta : null
class SocialSignUpModel {
  SocialSignUpModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }
  SocialSignUpModel.fromJson(dynamic jsonn) {
    Map json = jsonDecode(jsonn);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != '' ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;
  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

/// customer_id : "13089"
/// oauth_provider : null
/// oauth_uid : null
/// sender_phone : ""
/// if_vendor : "0"
/// username : "Jash"
/// lastname : ""
/// recp_name : ""
/// password : ""
/// email_address : "oraissn@gmai.com"
/// home_address : ""
/// address_line2 : null
/// phone_number : ""
/// city_id : ""
/// country_id : ""
/// shop_name : ""
/// shop_address : ""
/// shop_logo : ""
/// shop_banner : ""
/// status : "active"
/// referral : ""
/// maturity_status : "0"
/// iGift : "0"
/// eUsed : "unused"
/// myReferral : "0"
/// totalReferral : "0"
/// dob : null
/// notification_email : "0"
/// notification_text : "0"
/// country_code : null
/// home_gender : null
/// postal_code : null
/// phone_code : ""
/// stripe_id : null
/// pwd_token : null
/// signup_date : "2022-06-15"
/// ewallet_in : "0.00"
/// ewallet_out : "0.00"
/// social_login_type : null
/// fb_id : "f15d4f7f7ce2221d1d97623b778c27b3"
/// google_id : null
/// locale : null
/// profile_photo : ""
/// id_passport : null
/// access_token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTMwODksImVtYWlsIjoib3JhaXNzbkBnbWFpLmNvbSIsImlhdCI6MTY1NTI5OTk2MywiZXhwIjoxNjU1NDcyNzYzfQ.GmWPmjcxL1RnSO2kjcRNxsxsWS6b7GRlaT0Iqs-Uy2U"
class Data {
  Data({
    String? customerId,
    dynamic oauthProvider,
    dynamic oauthUid,
    String? senderPhone,
    String? ifVendor,
    String? username,
    String? lastname,
    String? recpName,
    String? password,
    String? emailAddress,
    String? homeAddress,
    dynamic addressLine2,
    String? phoneNumber,
    String? cityId,
    String? countryId,
    String? shopName,
    String? shopAddress,
    String? shopLogo,
    String? shopBanner,
    String? status,
    String? referral,
    String? maturityStatus,
    String? iGift,
    String? eUsed,
    String? myReferral,
    String? totalReferral,
    dynamic dob,
    String? notificationEmail,
    String? notificationText,
    dynamic countryCode,
    dynamic homeGender,
    dynamic postalCode,
    String? phoneCode,
    dynamic stripeId,
    dynamic pwdToken,
    String? signupDate,
    String? ewalletIn,
    String? ewalletOut,
    dynamic socialLoginType,
    String? fbId,
    dynamic googleId,
    dynamic locale,
    String? profilePhoto,
    dynamic idPassport,
    String? accessToken,
  }) {
    _customerId = customerId;
    _oauthProvider = oauthProvider;
    _oauthUid = oauthUid;
    _senderPhone = senderPhone;
    _ifVendor = ifVendor;
    _username = username;
    _lastname = lastname;
    _recpName = recpName;
    _password = password;
    _emailAddress = emailAddress;
    _homeAddress = homeAddress;
    _addressLine2 = addressLine2;
    _phoneNumber = phoneNumber;
    _cityId = cityId;
    _countryId = countryId;
    _shopName = shopName;
    _shopAddress = shopAddress;
    _shopLogo = shopLogo;
    _shopBanner = shopBanner;
    _status = status;
    _referral = referral;
    _maturityStatus = maturityStatus;
    _iGift = iGift;
    _eUsed = eUsed;
    _myReferral = myReferral;
    _totalReferral = totalReferral;
    _dob = dob;
    _notificationEmail = notificationEmail;
    _notificationText = notificationText;
    _countryCode = countryCode;
    _homeGender = homeGender;
    _postalCode = postalCode;
    _phoneCode = phoneCode;
    _stripeId = stripeId;
    _pwdToken = pwdToken;
    _signupDate = signupDate;
    _ewalletIn = ewalletIn;
    _ewalletOut = ewalletOut;
    _socialLoginType = socialLoginType;
    _fbId = fbId;
    _googleId = googleId;
    _locale = locale;
    _profilePhoto = profilePhoto;
    _idPassport = idPassport;
    _accessToken = accessToken;
  }
  Data.fromJson(dynamic json) {
    _customerId = json['customer_id'];
    _oauthProvider = json['oauth_provider'];
    _oauthUid = json['oauth_uid'];
    _senderPhone = json['sender_phone'];
    _ifVendor = json['if_vendor'];
    _username = json['username'];
    _lastname = json['lastname'];
    _recpName = json['recp_name'];
    _password = json['password'];
    _emailAddress = json['email_address'];
    _homeAddress = json['home_address'];
    _addressLine2 = json['address_line2'];
    _phoneNumber = json['phone_number'];
    _cityId = json['city_id'];
    _countryId = json['country_id'];
    _shopName = json['shop_name'];
    _shopAddress = json['shop_address'];
    _shopLogo = json['shop_logo'];
    _shopBanner = json['shop_banner'];
    _status = json['status'];
    _referral = json['referral'];
    _maturityStatus = json['maturity_status'];
    _iGift = json['iGift'];
    _eUsed = json['eUsed'];
    _myReferral = json['myReferral'];
    _totalReferral = json['totalReferral'];
    _dob = json['dob'];
    _notificationEmail = json['notification_email'];
    _notificationText = json['notification_text'];
    _countryCode = json['country_code'];
    _homeGender = json['home_gender'];
    _postalCode = json['postal_code'];
    _phoneCode = json['phone_code'];
    _stripeId = json['stripe_id'];
    _pwdToken = json['pwd_token'];
    _signupDate = json['signup_date'];
    _ewalletIn = json['ewallet_in'];
    _ewalletOut = json['ewallet_out'];
    _socialLoginType = json['social_login_type'];
    _fbId = json['fb_id'];
    _googleId = json['google_id'];
    _locale = json['locale'];
    _profilePhoto = json['profile_photo'];
    _idPassport = json['id_passport'];
    _accessToken = json['access_token'];
  }
  String? _customerId;
  dynamic _oauthProvider;
  dynamic _oauthUid;
  String? _senderPhone;
  String? _ifVendor;
  String? _username;
  String? _lastname;
  String? _recpName;
  String? _password;
  String? _emailAddress;
  String? _homeAddress;
  dynamic _addressLine2;
  String? _phoneNumber;
  String? _cityId;
  String? _countryId;
  String? _shopName;
  String? _shopAddress;
  String? _shopLogo;
  String? _shopBanner;
  String? _status;
  String? _referral;
  String? _maturityStatus;
  String? _iGift;
  String? _eUsed;
  String? _myReferral;
  String? _totalReferral;
  dynamic _dob;
  String? _notificationEmail;
  String? _notificationText;
  dynamic _countryCode;
  dynamic _homeGender;
  dynamic _postalCode;
  String? _phoneCode;
  dynamic _stripeId;
  dynamic _pwdToken;
  String? _signupDate;
  String? _ewalletIn;
  String? _ewalletOut;
  dynamic _socialLoginType;
  String? _fbId;
  dynamic _googleId;
  dynamic _locale;
  String? _profilePhoto;
  dynamic _idPassport;
  String? _accessToken;
  String? get customerId => _customerId;
  dynamic get oauthProvider => _oauthProvider;
  dynamic get oauthUid => _oauthUid;
  String? get senderPhone => _senderPhone;
  String? get ifVendor => _ifVendor;
  String? get username => _username;
  String? get lastname => _lastname;
  String? get recpName => _recpName;
  String? get password => _password;
  String? get emailAddress => _emailAddress;
  String? get homeAddress => _homeAddress;
  dynamic get addressLine2 => _addressLine2;
  String? get phoneNumber => _phoneNumber;
  String? get cityId => _cityId;
  String? get countryId => _countryId;
  String? get shopName => _shopName;
  String? get shopAddress => _shopAddress;
  String? get shopLogo => _shopLogo;
  String? get shopBanner => _shopBanner;
  String? get status => _status;
  String? get referral => _referral;
  String? get maturityStatus => _maturityStatus;
  String? get iGift => _iGift;
  String? get eUsed => _eUsed;
  String? get myReferral => _myReferral;
  String? get totalReferral => _totalReferral;
  dynamic get dob => _dob;
  String? get notificationEmail => _notificationEmail;
  String? get notificationText => _notificationText;
  dynamic get countryCode => _countryCode;
  dynamic get homeGender => _homeGender;
  dynamic get postalCode => _postalCode;
  String? get phoneCode => _phoneCode;
  dynamic get stripeId => _stripeId;
  dynamic get pwdToken => _pwdToken;
  String? get signupDate => _signupDate;
  String? get ewalletIn => _ewalletIn;
  String? get ewalletOut => _ewalletOut;
  dynamic get socialLoginType => _socialLoginType;
  String? get fbId => _fbId;
  dynamic get googleId => _googleId;
  dynamic get locale => _locale;
  String? get profilePhoto => _profilePhoto;
  dynamic get idPassport => _idPassport;
  String? get accessToken => _accessToken;
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['customer_id'] = _customerId;
    map['oauth_provider'] = _oauthProvider;
    map['oauth_uid'] = _oauthUid;
    map['sender_phone'] = _senderPhone;
    map['if_vendor'] = _ifVendor;
    map['username'] = _username;
    map['lastname'] = _lastname;
    map['recp_name'] = _recpName;
    map['password'] = _password;
    map['email_address'] = _emailAddress;
    map['home_address'] = _homeAddress;
    map['address_line2'] = _addressLine2;
    map['phone_number'] = _phoneNumber;
    map['city_id'] = _cityId;
    map['country_id'] = _countryId;
    map['shop_name'] = _shopName;
    map['shop_address'] = _shopAddress;
    map['shop_logo'] = _shopLogo;
    map['shop_banner'] = _shopBanner;
    map['status'] = _status;
    map['referral'] = _referral;
    map['maturity_status'] = _maturityStatus;
    map['iGift'] = _iGift;
    map['eUsed'] = _eUsed;
    map['myReferral'] = _myReferral;
    map['totalReferral'] = _totalReferral;
    map['dob'] = _dob;
    map['notification_email'] = _notificationEmail;
    map['notification_text'] = _notificationText;
    map['country_code'] = _countryCode;
    map['home_gender'] = _homeGender;
    map['postal_code'] = _postalCode;
    map['phone_code'] = _phoneCode;
    map['stripe_id'] = _stripeId;
    map['pwd_token'] = _pwdToken;
    map['signup_date'] = _signupDate;
    map['ewallet_in'] = _ewalletIn;
    map['ewallet_out'] = _ewalletOut;
    map['social_login_type'] = _socialLoginType;
    map['fb_id'] = _fbId;
    map['google_id'] = _googleId;
    map['locale'] = _locale;
    map['profile_photo'] = _profilePhoto;
    map['id_passport'] = _idPassport;
    map['access_token'] = _accessToken;
    return map;
  }
}
