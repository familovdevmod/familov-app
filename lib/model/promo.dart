import 'dart:convert';

class Promo {
  Promo({
    bool? status,
    String? message,
    Data1? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Promo.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data1.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data1? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data1? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data1 {
  Data1({
    String? promoId,
    String? vName,
    String? vCode,
    String? vDescription,
    String? iAmount,
    String? amountType,
    String? eStatus,
    String? startDate,
    String? endDate,
    String? minOrderAmount,
    String? maxUse,
    String? couponType,
  }) {
    _promoId = promoId;
    _vName = vName;
    _vCode = vCode;
    _vDescription = vDescription;
    _iAmount = iAmount;
    _amountType = amountType;
    _eStatus = eStatus;
    _startDate = startDate;
    _endDate = endDate;
    _minOrderAmount = minOrderAmount;
    _maxUse = maxUse;
    _couponType = couponType;
  }

  Data1.fromJson(dynamic json) {
    _promoId = json['promo_id'];
    _vName = json['vName'];
    _vCode = json['vCode'];
    _vDescription = json['vDescription'];
    _iAmount = json['iAmount'];
    _amountType = json['amount_type'];
    _eStatus = json['eStatus'];
    _startDate = json['start_date'];
    _endDate = json['end_date'];
    _minOrderAmount = json['min_order_amount'];
    _maxUse = json['max_use'];
    _couponType = json['coupon_type'];
  }
  String? _promoId;
  String? _vName;
  String? _vCode;
  String? _vDescription;
  String? _iAmount;
  String? _amountType;
  String? _eStatus;
  String? _startDate;
  String? _endDate;
  String? _minOrderAmount;
  String? _maxUse;
  String? _couponType;

  String? get promoId => _promoId;
  String? get vName => _vName;
  String? get vCode => _vCode;
  String? get vDescription => _vDescription;
  String? get iAmount => _iAmount;
  String? get amountType => _amountType;
  String? get eStatus => _eStatus;
  String? get startDate => _startDate;
  String? get endDate => _endDate;
  String? get minOrderAmount => _minOrderAmount;
  String? get maxUse => _maxUse;
  String? get couponType => _couponType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['promo_id'] = _promoId;
    map['vName'] = _vName;
    map['vCode'] = _vCode;
    map['vDescription'] = _vDescription;
    map['iAmount'] = _iAmount;
    map['amount_type'] = _amountType;
    map['eStatus'] = _eStatus;
    map['start_date'] = _startDate;
    map['end_date'] = _endDate;
    map['min_order_amount'] = _minOrderAmount;
    map['max_use'] = _maxUse;
    map['coupon_type'] = _couponType;
    return map;
  }
}
