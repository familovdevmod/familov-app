import 'dart:convert';

class Getrechargeitem {
  Getrechargeitem({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  Getrechargeitem.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != '' ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    int? totalCount,
    int? perPage,
    int? prevPage,
    int? currentPage,
    int? nextPage,
    List<DataList>? dataList,
    OperatorDetails? operatorDetails,
  }) {
    _count = count;
    _totalCount = totalCount;
    _perPage = perPage;
    _prevPage = prevPage;
    _currentPage = currentPage;
    _nextPage = nextPage;
    _dataList = dataList;
    _operatorDetails = operatorDetails;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    _totalCount = json['total_count'];
    _perPage = json['per_page'];
    _prevPage = json['prev_page'];
    _currentPage = json['current_page'];
    _nextPage = json['next_page'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(DataList.fromJson(v));
      });
    }
    _operatorDetails = json['operator_details'] != null
        ? OperatorDetails.fromJson(json['operator_details'])
        : null;
  }
  int? _count;
  int? _totalCount;
  int? _perPage;
  int? _prevPage;
  int? _currentPage;
  int? _nextPage;
  List<DataList>? _dataList;
  OperatorDetails? _operatorDetails;

  int? get count => _count;
  int? get totalCount => _totalCount;
  int? get perPage => _perPage;
  int? get prevPage => _prevPage;
  int? get currentPage => _currentPage;
  int? get nextPage => _nextPage;
  List<DataList>? get dataList => _dataList;
  OperatorDetails? get operatorDetails => _operatorDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['total_count'] = _totalCount;
    map['per_page'] = _perPage;
    map['prev_page'] = _prevPage;
    map['current_page'] = _currentPage;
    map['next_page'] = _nextPage;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    if (_operatorDetails != null) {
      map['operator_details'] = _operatorDetails?.toJson();
    }
    return map;
  }
}

class OperatorDetails {
  OperatorDetails({
    String? name,
    String? countryIso,
    String? validationRegex,
    String? providerCode,
    String? regionCode,
    String? providerImage,
    String? img,
  }) {
    _name = name;
    _countryIso = countryIso;
    _validationRegex = validationRegex;
    _providerCode = providerCode;
    _regionCode = regionCode;
    _providerImage = providerImage;
    _img = img;
  }

  OperatorDetails.fromJson(dynamic json) {
    //Map json = jsonDecode(valueMap);
    _name = json['Name'];
    _countryIso = json['CountryIso'];
    _validationRegex = json['ValidationRegex'];
    _providerCode = json['ProviderCode'];
    _regionCode = json['region_code'];
    _providerImage = json['provider_image'];
    _img = json['img'];
  }
  String? _name;
  String? _countryIso;
  String? _validationRegex;
  String? _providerCode;
  String? _regionCode;
  String? _providerImage;
  String? _img;

  String? get name => _name;
  String? get countryIso => _countryIso;
  String? get validationRegex => _validationRegex;
  String? get providerCode => _providerCode;
  String? get regionCode => _regionCode;
  String? get providerImage => _providerImage;
  String? get img => _img;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Name'] = _name;
    map['CountryIso'] = _countryIso;
    map['ValidationRegex'] = _validationRegex;
    map['ProviderCode'] = _providerCode;
    map['region_code'] = _regionCode;
    map['provider_image'] = _providerImage;
    map['img'] = _img;
    return map;
  }
}

class DataList {
  DataList({
    String? displayText,
    String? descriptionMarkdown,
    String? readMoreMarkdown,
    String? providerCode,
    String? skuCode,
    String? maximumReceiveValue,
    String? maximumReceiveCurrencyIso,
    String? maximumReceiveValueExcludingTax,
    String? maximumSendValue,
    String? maximumSendCurrencyIso,
    String? minimumReceiveValue,
    String? minimumReceiveCurrencyIso,
    String? minimumSendValue,
    String? minimumSendCurrencyIso,
    String? benefits,
    dynamic validityPeriodIso,
    String? regionCode,
    String? localizationKey,
  }) {
    _displayText = displayText;
    _descriptionMarkdown = descriptionMarkdown;
    _readMoreMarkdown = readMoreMarkdown;
    _providerCode = providerCode;
    _skuCode = skuCode;
    _maximumReceiveValue = maximumReceiveValue;
    _maximumReceiveCurrencyIso = maximumReceiveCurrencyIso;
    _maximumReceiveValueExcludingTax = maximumReceiveValueExcludingTax;
    _maximumSendValue = maximumSendValue;
    _maximumSendCurrencyIso = maximumSendCurrencyIso;
    _minimumReceiveValue = minimumReceiveValue;
    _minimumReceiveCurrencyIso = minimumReceiveCurrencyIso;
    _minimumSendValue = minimumSendValue;
    _minimumSendCurrencyIso = minimumSendCurrencyIso;
    _benefits = benefits;
    _validityPeriodIso = validityPeriodIso;
    _regionCode = regionCode;
    _localizationKey = localizationKey;
  }

  DataList.fromJson(dynamic json) {
    _displayText = json['DisplayText'];
    _descriptionMarkdown = json['DescriptionMarkdown'];
    _readMoreMarkdown = json['ReadMoreMarkdown'];
    _providerCode = json['ProviderCode'];
    _skuCode = json['SkuCode'];
    _maximumReceiveValue = json['Maximum_ReceiveValue'];
    _maximumReceiveCurrencyIso = json['Maximum_ReceiveCurrencyIso'];
    _maximumReceiveValueExcludingTax = json['Maximum_ReceiveValueExcludingTax'];
    _maximumSendValue = json['Maximum_SendValue'];
    _maximumSendCurrencyIso = json['Maximum_SendCurrencyIso'];
    _minimumReceiveValue = json['Minimum_ReceiveValue'];
    _minimumReceiveCurrencyIso = json['Minimum_ReceiveCurrencyIso'];
    _minimumSendValue = json['Minimum_SendValue'];
    _minimumSendCurrencyIso = json['Minimum_SendCurrencyIso'];
    _benefits = json['Benefits'];
    _validityPeriodIso = json['ValidityPeriodIso'];
    _regionCode = json['RegionCode'];
    _localizationKey = json['LocalizationKey'];
  }
  String? _displayText;
  String? _descriptionMarkdown;
  String? _readMoreMarkdown;
  String? _providerCode;
  String? _skuCode;
  String? _maximumReceiveValue;
  String? _maximumReceiveCurrencyIso;
  String? _maximumReceiveValueExcludingTax;
  String? _maximumSendValue;
  String? _maximumSendCurrencyIso;
  String? _minimumReceiveValue;
  String? _minimumReceiveCurrencyIso;
  String? _minimumSendValue;
  String? _minimumSendCurrencyIso;
  String? _benefits;
  dynamic _validityPeriodIso;
  String? _regionCode;
  String? _localizationKey;

  String? get displayText => _displayText;
  String? get descriptionMarkdown => _descriptionMarkdown;
  String? get readMoreMarkdown => _readMoreMarkdown;
  String? get providerCode => _providerCode;
  String? get skuCode => _skuCode;
  String? get maximumReceiveValue => _maximumReceiveValue;
  String? get maximumReceiveCurrencyIso => _maximumReceiveCurrencyIso;
  String? get maximumReceiveValueExcludingTax =>
      _maximumReceiveValueExcludingTax;
  String? get maximumSendValue => _maximumSendValue;
  String? get maximumSendCurrencyIso => _maximumSendCurrencyIso;
  String? get minimumReceiveValue => _minimumReceiveValue;
  String? get minimumReceiveCurrencyIso => _minimumReceiveCurrencyIso;
  String? get minimumSendValue => _minimumSendValue;
  String? get minimumSendCurrencyIso => _minimumSendCurrencyIso;
  String? get benefits => _benefits;
  dynamic get validityPeriodIso => _validityPeriodIso;
  String? get regionCode => _regionCode;
  String? get localizationKey => _localizationKey;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['DisplayText'] = _displayText;
    map['DescriptionMarkdown'] = _descriptionMarkdown;
    map['ReadMoreMarkdown'] = _readMoreMarkdown;
    map['ProviderCode'] = _providerCode;
    map['SkuCode'] = _skuCode;
    map['Maximum_ReceiveValue'] = _maximumReceiveValue;
    map['Maximum_ReceiveCurrencyIso'] = _maximumReceiveCurrencyIso;
    map['Maximum_ReceiveValueExcludingTax'] = _maximumReceiveValueExcludingTax;
    map['Maximum_SendValue'] = _maximumSendValue;
    map['Maximum_SendCurrencyIso'] = _maximumSendCurrencyIso;
    map['Minimum_ReceiveValue'] = _minimumReceiveValue;
    map['Minimum_ReceiveCurrencyIso'] = _minimumReceiveCurrencyIso;
    map['Minimum_SendValue'] = _minimumSendValue;
    map['Minimum_SendCurrencyIso'] = _minimumSendCurrencyIso;
    map['Benefits'] = _benefits;
    map['ValidityPeriodIso'] = _validityPeriodIso;
    map['RegionCode'] = _regionCode;
    map['LocalizationKey'] = _localizationKey;
    return map;
  }
}
