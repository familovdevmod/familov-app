import 'dart:convert';

class GetuserTransaction {
  GetuserTransaction({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  GetuserTransaction.fromJson(dynamic valueMap) {
    print('jsonresponse ${valueMap}');
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    int? totalCount,
    int? perPage,
    dynamic offset,
    List<Data_list>? dataList,
  }) {
    _count = count;
    _totalCount = totalCount;
    _perPage = perPage;
    //   _offset = offset;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    _totalCount = json['total_count'];
    _perPage = json['per_page'];
    //   _offset = json['offset'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  int? _totalCount;
  int? _perPage;
  // dynamic _offset;
  List<Data_list>? _dataList;

  int? get count => _count;
  int? get totalCount => _totalCount;
  int? get perPage => _perPage;
  //dynamic get offset => _offset;
  List<Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['total_count'] = _totalCount;
    map['per_page'] = _perPage;

    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data_list {
  Data_list({
    String? orderId,
    String? parentOrderId,
    String? customerId,
    String? ipAddress,
    String? randomRick,
    String? dateTime,
    String? generateCode,
    String? orderStatus,
    dynamic selectedAdmin,
    String? deliveryCharges,
    String? orderView,
    String? countryId,
    String? cityId,
    String? shopId,
    String? contactId,
    String? receiverName,
    String? receiverPhone,
    String? paymentMethod,
    String? datePayment,
    String? discount,
    String? welcomeDiscount,
    String? promoId,
    String? totalOrder,
    String? serviceTax,
    String? deliveryOption,
    String? deliveryPrice,
    dynamic homeDeliveryAddress,
    String? paymentCurr,
    String? greetingMsg,
    String? subTotal,
    String? promoDiscountAmount,
    String? grandTotal,
    String? stripeToken,
    String? promoCode,
    String? paymentCurrCode,
    dynamic uploadBilling,
    dynamic orderBillingPrice,
    String? anotherPhoneNumber,
    String? isRead,
    dynamic stripeCustId,
    String? amount,
    dynamic amountRefunded,
    dynamic application,
    String? paymentApplicationFee,
    dynamic applicationFeeAmount,
  }) {
    _orderId = orderId;
    _parentOrderId = parentOrderId;
    _customerId = customerId;
    _ipAddress = ipAddress;
    _randomRick = randomRick;
    _dateTime = dateTime;
    _generateCode = generateCode;
    _orderStatus = orderStatus;
    _selectedAdmin = selectedAdmin;
    _deliveryCharges = deliveryCharges;
    _orderView = orderView;
    _countryId = countryId;
    _cityId = cityId;
    _shopId = shopId;
    _contactId = contactId;
    _receiverName = receiverName;
    _receiverPhone = receiverPhone;
    _paymentMethod = paymentMethod;
    _datePayment = datePayment;
    _discount = discount;
    _welcomeDiscount = welcomeDiscount;
    _promoId = promoId;
    _totalOrder = totalOrder;
    _serviceTax = serviceTax;
    _deliveryOption = deliveryOption;
    _deliveryPrice = deliveryPrice;
    _homeDeliveryAddress = homeDeliveryAddress;
    _paymentCurr = paymentCurr;
    _greetingMsg = greetingMsg;
    _subTotal = subTotal;
    _promoDiscountAmount = promoDiscountAmount;
    _grandTotal = grandTotal;
    _stripeToken = stripeToken;
    _promoCode = promoCode;
    _paymentCurrCode = paymentCurrCode;
    _uploadBilling = uploadBilling;
    _orderBillingPrice = orderBillingPrice;
    _anotherPhoneNumber = anotherPhoneNumber;
    _isRead = isRead;
    _stripeCustId = stripeCustId;
    _amount = amount;
    _amountRefunded = amountRefunded;
    _application = application;
    _paymentApplicationFee = paymentApplicationFee;
    _applicationFeeAmount = applicationFeeAmount;
  }

  Data_list.fromJson(dynamic json) {
    _orderId = json['order_id'];
    _parentOrderId = json['parent_order_id'];
    _customerId = json['customer_id'];
    _ipAddress = json['ip_address'];
    _randomRick = json['random_rick'];
    _dateTime = json['date_time'];
    _generateCode = json['generate_code'];
    _orderStatus = json['order_status'];
    _selectedAdmin = json['selected_admin'];
    _deliveryCharges = json['delivery_charges'];
    _orderView = json['order_view'];
    _countryId = json['country_id'];
    _cityId = json['city_id'];
    _shopId = json['shop_id'];
    _contactId = json['contact_id'];
    _receiverName = json['receiver_name'];
    _receiverPhone = json['receiver_phone'];
    _paymentMethod = json['payment_method'];
    _datePayment = json['date_payment'];
    _discount = json['discount'];
    _welcomeDiscount = json['welcome_discount'];
    _promoId = json['promo_id'];
    _totalOrder = json['total_order'];
    _serviceTax = json['service_tax'];
    _deliveryOption = json['delivery_option'];
    _deliveryPrice = json['delivery_price'];
    _homeDeliveryAddress = json['home_delivery_address'];
    _paymentCurr = json['payment_curr'];
    _greetingMsg = json['greeting_msg'];
    _subTotal = json['sub_total'];
    _promoDiscountAmount = json['promo_discount_amount'];
    _grandTotal = json['grand_total'];
    _stripeToken = json['stripe_token'];
    _promoCode = json['promo_code'];
    _paymentCurrCode = json['payment_curr_code'];
    _uploadBilling = json['upload_billing'];
    _orderBillingPrice = json['order_billing_price'];
    _anotherPhoneNumber = json['another_phone_number'];
    _isRead = json['is_read'];
    _stripeCustId = json['stripe_cust_id'];
    _amount = json['amount'];
    _amountRefunded = json['amount_refunded'];
    _application = json['application'];
    _paymentApplicationFee = json['payment_application_fee'];
    _applicationFeeAmount = json['application_fee_amount'];
  }
  String? _orderId;
  String? _parentOrderId;
  String? _customerId;
  String? _ipAddress;
  String? _randomRick;
  String? _dateTime;
  String? _generateCode;
  String? _orderStatus;
  dynamic _selectedAdmin;
  String? _deliveryCharges;
  String? _orderView;
  String? _countryId;
  String? _cityId;
  String? _shopId;
  String? _contactId;
  String? _receiverName;
  String? _receiverPhone;
  String? _paymentMethod;
  String? _datePayment;
  String? _discount;
  String? _welcomeDiscount;
  String? _promoId;
  String? _totalOrder;
  String? _serviceTax;
  String? _deliveryOption;
  String? _deliveryPrice;
  dynamic _homeDeliveryAddress;
  String? _paymentCurr;
  String? _greetingMsg;
  String? _subTotal;
  String? _promoDiscountAmount;
  String? _grandTotal;
  String? _stripeToken;
  String? _promoCode;
  String? _paymentCurrCode;
  dynamic _uploadBilling;
  dynamic _orderBillingPrice;
  String? _anotherPhoneNumber;
  String? _isRead;
  dynamic _stripeCustId;
  String? _amount;
  dynamic _amountRefunded;
  dynamic _application;
  String? _paymentApplicationFee;
  dynamic _applicationFeeAmount;

  String? get orderId => _orderId;
  String? get parentOrderId => _parentOrderId;
  String? get customerId => _customerId;
  String? get ipAddress => _ipAddress;
  String? get randomRick => _randomRick;
  String? get dateTime => _dateTime;
  String? get generateCode => _generateCode;
  String? get orderStatus => _orderStatus;
  dynamic get selectedAdmin => _selectedAdmin;
  String? get deliveryCharges => _deliveryCharges;
  String? get orderView => _orderView;
  String? get countryId => _countryId;
  String? get cityId => _cityId;
  String? get shopId => _shopId;
  String? get contactId => _contactId;
  String? get receiverName => _receiverName;
  String? get receiverPhone => _receiverPhone;
  String? get paymentMethod => _paymentMethod;
  String? get datePayment => _datePayment;
  String? get discount => _discount;
  String? get welcomeDiscount => _welcomeDiscount;
  String? get promoId => _promoId;
  String? get totalOrder => _totalOrder;
  String? get serviceTax => _serviceTax;
  String? get deliveryOption => _deliveryOption;
  String? get deliveryPrice => _deliveryPrice;
  dynamic get homeDeliveryAddress => _homeDeliveryAddress;
  String? get paymentCurr => _paymentCurr;
  String? get greetingMsg => _greetingMsg;
  String? get subTotal => _subTotal;
  String? get promoDiscountAmount => _promoDiscountAmount;
  String? get grandTotal => _grandTotal;
  String? get stripeToken => _stripeToken;
  String? get promoCode => _promoCode;
  String? get paymentCurrCode => _paymentCurrCode;
  dynamic get uploadBilling => _uploadBilling;
  dynamic get orderBillingPrice => _orderBillingPrice;
  String? get anotherPhoneNumber => _anotherPhoneNumber;
  String? get isRead => _isRead;
  dynamic get stripeCustId => _stripeCustId;
  String? get amount => _amount;
  dynamic get amountRefunded => _amountRefunded;
  dynamic get application => _application;
  String? get paymentApplicationFee => _paymentApplicationFee;
  dynamic get applicationFeeAmount => _applicationFeeAmount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['order_id'] = _orderId;
    map['parent_order_id'] = _parentOrderId;
    map['customer_id'] = _customerId;
    map['ip_address'] = _ipAddress;
    map['random_rick'] = _randomRick;
    map['date_time'] = _dateTime;
    map['generate_code'] = _generateCode;
    map['order_status'] = _orderStatus;
    map['selected_admin'] = _selectedAdmin;
    map['delivery_charges'] = _deliveryCharges;
    map['order_view'] = _orderView;
    map['country_id'] = _countryId;
    map['city_id'] = _cityId;
    map['shop_id'] = _shopId;
    map['contact_id'] = _contactId;
    map['receiver_name'] = _receiverName;
    map['receiver_phone'] = _receiverPhone;
    map['payment_method'] = _paymentMethod;
    map['date_payment'] = _datePayment;
    map['discount'] = _discount;
    map['welcome_discount'] = _welcomeDiscount;
    map['promo_id'] = _promoId;
    map['total_order'] = _totalOrder;
    map['service_tax'] = _serviceTax;
    map['delivery_option'] = _deliveryOption;
    map['delivery_price'] = _deliveryPrice;
    map['home_delivery_address'] = _homeDeliveryAddress;
    map['payment_curr'] = _paymentCurr;
    map['greeting_msg'] = _greetingMsg;
    map['sub_total'] = _subTotal;
    map['promo_discount_amount'] = _promoDiscountAmount;
    map['grand_total'] = _grandTotal;
    map['stripe_token'] = _stripeToken;
    map['promo_code'] = _promoCode;
    map['payment_curr_code'] = _paymentCurrCode;
    map['upload_billing'] = _uploadBilling;
    map['order_billing_price'] = _orderBillingPrice;
    map['another_phone_number'] = _anotherPhoneNumber;
    map['is_read'] = _isRead;
    map['stripe_cust_id'] = _stripeCustId;
    map['amount'] = _amount;
    map['amount_refunded'] = _amountRefunded;
    map['application'] = _application;
    map['payment_application_fee'] = _paymentApplicationFee;
    map['application_fee_amount'] = _applicationFeeAmount;
    return map;
  }
}
