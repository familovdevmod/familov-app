class Pdf {
  Pdf({
      bool? status, 
      String? message, 
      Data? data, 
      dynamic errorCode, 
      dynamic errorMsg, 
      dynamic meta,}){
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
}

  Pdf.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;
Pdf copyWith({  bool? status,
  String? message,
  Data? data,
  dynamic errorCode,
  dynamic errorMsg,
  dynamic meta,
}) => Pdf(  status: status ?? _status,
  message: message ?? _message,
  data: data ?? _data,
  errorCode: errorCode ?? _errorCode,
  errorMsg: errorMsg ?? _errorMsg,
  meta: meta ?? _meta,
);
  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }

}

class Data {
  Data({
      String? fileName, 
      String? fileData,}){
    _fileName = fileName;
    _fileData = fileData;
}

  Data.fromJson(dynamic json) {
    _fileName = json['file_name'];
    _fileData = json['file_data'];
  }
  String? _fileName;
  String? _fileData;
Data copyWith({  String? fileName,
  String? fileData,
}) => Data(  fileName: fileName ?? _fileName,
  fileData: fileData ?? _fileData,
);
  String? get fileName => _fileName;
  String? get fileData => _fileData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['file_name'] = _fileName;
    map['file_data'] = _fileData;
    return map;
  }

}