import 'dart:convert';

class MobileInvoices {
  MobileInvoices({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  MobileInvoices.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<Data_list>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_list>? _dataList;

  int? get count => _count;
  List<Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data_list {
  Data_list({
    String? orderitemId,
    String? orderId,
    String? productId,
    String? productPrize,
    String? totalPrize,
    String? quantity,
    String? shopId,
    String? categoryId,
    String? customerId,
    dynamic managerId,
    String? productName,
    String? productShortDesc,
    String? productDesc,
    dynamic productImage,
    String? productPriceCurrencyId,
    String? isProposedByManager,
    String? productPrices,
    dynamic productPriceCurrencyIdManager,
    dynamic productPricesManager,
    String? specialProductPrices,
    String? productStatus,
    String? productAvailability,
    String? isRead,
    dynamic createdAt,
    dynamic updatedAt,
    dynamic productUniqueId,
    String? mainCategoryId,
    String? isMarkedTop,
    Recharge_item_details? rechargeItemDetails,
  }) {
    _orderitemId = orderitemId;
    _orderId = orderId;
    _productId = productId;
    _productPrize = productPrize;
    _totalPrize = totalPrize;
    _quantity = quantity;
    _shopId = shopId;
    _categoryId = categoryId;
    _customerId = customerId;
    _managerId = managerId;
    _productName = productName;
    _productShortDesc = productShortDesc;
    _productDesc = productDesc;
    _productImage = productImage;
    _productPriceCurrencyId = productPriceCurrencyId;
    _isProposedByManager = isProposedByManager;
    _productPrices = productPrices;
    _productPriceCurrencyIdManager = productPriceCurrencyIdManager;
    _productPricesManager = productPricesManager;
    _specialProductPrices = specialProductPrices;
    _productStatus = productStatus;
    _productAvailability = productAvailability;
    _isRead = isRead;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _productUniqueId = productUniqueId;
    _mainCategoryId = mainCategoryId;
    _isMarkedTop = isMarkedTop;
    _rechargeItemDetails = rechargeItemDetails;
  }

  Data_list.fromJson(dynamic json) {
    _orderitemId = json['orderitem_id'];
    _orderId = json['order_id'];
    _productId = json['product_id'];
    _productPrize = json['product_prize'];
    _totalPrize = json['total_prize'];
    _quantity = json['quantity'];
    _shopId = json['shop_id'];
    _categoryId = json['category_id'];
    _customerId = json['customer_id'];
    _managerId = json['manager_id'];
    _productName = json['product_name'];
    _productShortDesc = json['product_short_desc'];
    _productDesc = json['product_desc'];
    _productImage = json['product_image'];
    _productPriceCurrencyId = json['product_price_currency_id'];
    _isProposedByManager = json['is_proposed_by_manager'];
    _productPrices = json['product_prices'];
    _productPriceCurrencyIdManager = json['product_price_currency_id_manager'];
    _productPricesManager = json['product_prices_manager'];
    _specialProductPrices = json['special_product_prices'];
    _productStatus = json['product_status'];
    _productAvailability = json['product_availability'];
    _isRead = json['is_read'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _productUniqueId = json['product_unique_id'];
    _mainCategoryId = json['main_category_id'];
    _isMarkedTop = json['is_marked_top'];
    _rechargeItemDetails = json['recharge_item_details'] != null
        ? Recharge_item_details.fromJson(json['recharge_item_details'])
        : null;
  }
  String? _orderitemId;
  String? _orderId;
  String? _productId;
  String? _productPrize;
  String? _totalPrize;
  String? _quantity;
  String? _shopId;
  String? _categoryId;
  String? _customerId;
  dynamic _managerId;
  String? _productName;
  String? _productShortDesc;
  String? _productDesc;
  dynamic _productImage;
  String? _productPriceCurrencyId;
  String? _isProposedByManager;
  String? _productPrices;
  dynamic _productPriceCurrencyIdManager;
  dynamic _productPricesManager;
  String? _specialProductPrices;
  String? _productStatus;
  String? _productAvailability;
  String? _isRead;
  dynamic _createdAt;
  dynamic _updatedAt;
  dynamic _productUniqueId;
  String? _mainCategoryId;
  String? _isMarkedTop;
  Recharge_item_details? _rechargeItemDetails;

  String? get orderitemId => _orderitemId;
  String? get orderId => _orderId;
  String? get productId => _productId;
  String? get productPrize => _productPrize;
  String? get totalPrize => _totalPrize;
  String? get quantity => _quantity;
  String? get shopId => _shopId;
  String? get categoryId => _categoryId;
  String? get customerId => _customerId;
  dynamic get managerId => _managerId;
  String? get productName => _productName;
  String? get productShortDesc => _productShortDesc;
  String? get productDesc => _productDesc;
  dynamic get productImage => _productImage;
  String? get productPriceCurrencyId => _productPriceCurrencyId;
  String? get isProposedByManager => _isProposedByManager;
  String? get productPrices => _productPrices;
  dynamic get productPriceCurrencyIdManager => _productPriceCurrencyIdManager;
  dynamic get productPricesManager => _productPricesManager;
  String? get specialProductPrices => _specialProductPrices;
  String? get productStatus => _productStatus;
  String? get productAvailability => _productAvailability;
  String? get isRead => _isRead;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  dynamic get productUniqueId => _productUniqueId;
  String? get mainCategoryId => _mainCategoryId;
  String? get isMarkedTop => _isMarkedTop;
  Recharge_item_details? get rechargeItemDetails => _rechargeItemDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['orderitem_id'] = _orderitemId;
    map['order_id'] = _orderId;
    map['product_id'] = _productId;
    map['product_prize'] = _productPrize;
    map['total_prize'] = _totalPrize;
    map['quantity'] = _quantity;
    map['shop_id'] = _shopId;
    map['category_id'] = _categoryId;
    map['customer_id'] = _customerId;
    map['manager_id'] = _managerId;
    map['product_name'] = _productName;
    map['product_short_desc'] = _productShortDesc;
    map['product_desc'] = _productDesc;
    map['product_image'] = _productImage;
    map['product_price_currency_id'] = _productPriceCurrencyId;
    map['is_proposed_by_manager'] = _isProposedByManager;
    map['product_prices'] = _productPrices;
    map['product_price_currency_id_manager'] = _productPriceCurrencyIdManager;
    map['product_prices_manager'] = _productPricesManager;
    map['special_product_prices'] = _specialProductPrices;
    map['product_status'] = _productStatus;
    map['product_availability'] = _productAvailability;
    map['is_read'] = _isRead;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['product_unique_id'] = _productUniqueId;
    map['main_category_id'] = _mainCategoryId;
    map['is_marked_top'] = _isMarkedTop;
    if (_rechargeItemDetails != null) {
      map['recharge_item_details'] = _rechargeItemDetails?.toJson();
    }
    return map;
  }
}

class Recharge_item_details {
  Recharge_item_details({
    String? id,
    String? orderId,
    String? providerCode,
    dynamic countryCode,
    String? skucode,
    String? productName,
    String? sendValue,
    String? sendCurrencyIso,
    String? accountNumber,
    String? phoneNum,
    String? phoneCode,
    String? distributorRef,
    dynamic isRechargeDone,
    dynamic paymentStatus,
    dynamic rechargeErrors,
    dynamic isRead,
    String? createdAt,
    dynamic modifiedAt,
  }) {
    _id = id;
    _orderId = orderId;
    _providerCode = providerCode;
    _countryCode = countryCode;
    _skucode = skucode;
    _productName = productName;
    _sendValue = sendValue;
    _sendCurrencyIso = sendCurrencyIso;
    _accountNumber = accountNumber;
    _phoneNum = phoneNum;
    _phoneCode = phoneCode;
    _distributorRef = distributorRef;
    _isRechargeDone = isRechargeDone;
    _paymentStatus = paymentStatus;
    _rechargeErrors = rechargeErrors;
    _isRead = isRead;
    _createdAt = createdAt;
    _modifiedAt = modifiedAt;
  }

  Recharge_item_details.fromJson(dynamic json) {
    _id = json['id'];
    _orderId = json['order_id'];
    _providerCode = json['ProviderCode'];
    //_countryCode = json['country_code'];
    _skucode = json['skucode'];
    _productName = json['product_name'];
    _sendValue = json['SendValue'];
    _sendCurrencyIso = json['SendCurrencyIso'];
    _accountNumber = json['AccountNumber'];
    _phoneNum = json['phone_num'];
    _phoneCode = json['phone_code'];
    _distributorRef = json['DistributorRef'];
    //_isRechargeDone = json['is_recharge_done'];
  //  _paymentStatus = json['payment_status'];
   // _rechargeErrors = json['recharge_errors'];
   // _isRead = json['is_read'];
    _createdAt = json['created_at'];
   // _modifiedAt = json['modified_at'];
  }
  String? _id;
  String? _orderId;
  String? _providerCode;
  dynamic _countryCode;
  String? _skucode;
  String? _productName;
  String? _sendValue;
  String? _sendCurrencyIso;
  String? _accountNumber;
  String? _phoneNum;
  String? _phoneCode;
  String? _distributorRef;
  dynamic _isRechargeDone;
  dynamic _paymentStatus;
  dynamic _rechargeErrors;
  dynamic _isRead;
  String? _createdAt;
  dynamic _modifiedAt;

  String? get id => _id;
  String? get orderId => _orderId;
  String? get providerCode => _providerCode;
  dynamic get countryCode => _countryCode;
  String? get skucode => _skucode;
  String? get productName => _productName;
  String? get sendValue => _sendValue;
  String? get sendCurrencyIso => _sendCurrencyIso;
  String? get accountNumber => _accountNumber;
  String? get phoneNum => _phoneNum;
  String? get phoneCode => _phoneCode;
  String? get distributorRef => _distributorRef;
  dynamic get isRechargeDone => _isRechargeDone;
  dynamic get paymentStatus => _paymentStatus;
  dynamic get rechargeErrors => _rechargeErrors;
  dynamic get isRead => _isRead;
  String? get createdAt => _createdAt;
  dynamic get modifiedAt => _modifiedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['order_id'] = _orderId;
    map['ProviderCode'] = _providerCode;
  //  map['country_code'] = _countryCode;
    map['skucode'] = _skucode;
    map['product_name'] = _productName;
    map['SendValue'] = _sendValue;
    map['SendCurrencyIso'] = _sendCurrencyIso;
    map['AccountNumber'] = _accountNumber;
    map['phone_num'] = _phoneNum;
    map['phone_code'] = _phoneCode;
    map['DistributorRef'] = _distributorRef;
   // map['is_recharge_done'] = _isRechargeDone;
   // map['payment_status'] = _paymentStatus;
   // map['recharge_errors'] = _rechargeErrors;
   // map['is_read'] = _isRead;
    map['created_at'] = _createdAt;
   // map['modified_at'] = _modifiedAt;
    return map;
  }
}
