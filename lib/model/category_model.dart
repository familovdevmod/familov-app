import 'dart:convert';

class CategoryModel {
  CategoryModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  CategoryModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<Data_lisst>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_lisst.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_lisst>? _dataList;

  int? get count => _count;
  List<Data_lisst>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

// ignore: camel_case_types
class Data_lisst {
  Data_lisst({
    String? mainCategoryId,
    String? mainCategoryName,
    String? mainCategoryImage,
    String? slug,
    String? mainCategoryBannerImage,
    String? countryMainCategoryId,
    String? countryId,
    String? createdAt,
    String? updatedAt,
    dynamic shouldActive
  }) {
    _mainCategoryId = mainCategoryId;
    _mainCategoryName = mainCategoryName;
    _mainCategoryImage = mainCategoryImage;
    _slug = slug;
    _mainCategoryBannerImage = mainCategoryBannerImage;
    _countryMainCategoryId = countryMainCategoryId;
    _countryId = countryId;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _shouldActive = shouldActive;
  }

  Data_lisst.fromJson(dynamic json) {
    _mainCategoryId = json['main_category_id'];
    _mainCategoryName = json['main_category_name'];
    _mainCategoryImage = json['main_category_image'];
    _slug = json['slug'];
    _mainCategoryBannerImage = json['main_category_banner_image'];
    _countryMainCategoryId = json['country_main_category_id'];
    _countryId = json['country_id'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  String? _mainCategoryId;
  String? _mainCategoryName;
  String? _mainCategoryImage;
  String? _slug;
  String? _mainCategoryBannerImage;
  String? _countryMainCategoryId;
  String? _countryId;
  String? _createdAt;
  String? _updatedAt;
  String? _shouldActive;

  String? get mainCategoryId => _mainCategoryId;
  String? get mainCategoryName => _mainCategoryName;
  String? get mainCategoryImage => _mainCategoryImage;
  String? get slug => _slug;
  String? get mainCategoryBannerImage => _mainCategoryBannerImage;
  String? get countryMainCategoryId => _countryMainCategoryId;
  String? get countryId => _countryId;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  dynamic get shouldActive => _shouldActive;


  set shouldActive(dynamic value) {
    _shouldActive = value;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['main_category_id'] = _mainCategoryId;
    map['main_category_name'] = _mainCategoryName;
    map['main_category_image'] = _mainCategoryImage;
    map['slug'] = _slug;
    map['main_category_banner_image'] = _mainCategoryBannerImage;
    map['country_main_category_id'] = _countryMainCategoryId;
    map['country_id'] = _countryId;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }
}
