import 'dart:convert';

class CityModel {
  CityModel({
    bool? status,
    String? message,
    Data? data,
    dynamic errorCode,
    dynamic errorMsg,
    dynamic meta,
  }) {
    _status = status;
    _message = message;
    _data = data;
    _errorCode = errorCode;
    _errorMsg = errorMsg;
    _meta = meta;
  }

  CityModel.fromJson(dynamic valueMap) {
    Map json = jsonDecode(valueMap);

    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _errorCode = json['error_code'];
    _errorMsg = json['error_msg'];
    _meta = json['meta'];
  }
  bool? _status;
  String? _message;
  Data? _data;
  dynamic _errorCode;
  dynamic _errorMsg;
  dynamic _meta;

  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;
  dynamic get errorCode => _errorCode;
  dynamic get errorMsg => _errorMsg;
  dynamic get meta => _meta;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['error_code'] = _errorCode;
    map['error_msg'] = _errorMsg;
    map['meta'] = _meta;
    return map;
  }
}

class Data {
  Data({
    int? count,
    List<Data_list>? dataList,
  }) {
    _count = count;
    _dataList = dataList;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['data_list'] != null) {
      _dataList = [];
      json['data_list'].forEach((v) {
        _dataList?.add(Data_list.fromJson(v));
      });
    }
  }
  int? _count;
  List<Data_list>? _dataList;

  int? get count => _count;
  List<Data_list>? get dataList => _dataList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_dataList != null) {
      map['data_list'] = _dataList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data_list {
  Data_list({
    String? cityId,
    String? countryId,
    String? cityName,
    String? citySlug,
  }) {
    _cityId = cityId;
    _countryId = countryId;
    _cityName = cityName;
    _citySlug = citySlug;
  }

  Data_list.fromJson(dynamic json) {
    _cityId = json['city_id'];
    _countryId = json['country_id'];
    _cityName = json['city_name'];
    _citySlug = json['city_slug'];
  }
  String? _cityId;
  String? _countryId;
  String? _cityName;
  String? _citySlug;

  String? get cityId => _cityId;
  String? get countryId => _countryId;
  String? get cityName => _cityName;
  String? get citySlug => _citySlug;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['city_id'] = _cityId;
    map['country_id'] = _countryId;
    map['city_name'] = _cityName;
    map['city_slug'] = _citySlug;
    return map;
  }
}
