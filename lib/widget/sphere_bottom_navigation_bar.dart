import 'package:flutter/material.dart';

class BuildNavigationItem {
  final String icon;
  final Color selectedItemColor;
  final Color itemColor;
  final String tooltip;
  final String title;

  BuildNavigationItem(
      {required this.icon,
      required this.tooltip,
      required this.selectedItemColor,
      required this.itemColor,
      required this.title});
}

class SphereBottomNavigationBar extends StatefulWidget {
  final int defaultSelectedItem;
  Color sheetBackgroundColor;
  final BorderRadius sheetRadius;
  final List<BuildNavigationItem> navigationItems;
  final ValueChanged<int> onItemPressed;
  final ValueChanged<int> onItemLongPressed;

  SphereBottomNavigationBar(
      {required this.defaultSelectedItem,
      required this.sheetBackgroundColor,
      required this.sheetRadius,
      required this.onItemLongPressed,
      required this.navigationItems,
      required this.onItemPressed}) {
    assert(onItemPressed != null, 'You must implement onItemPressed ');
  }

  @override
  _SphereBottomNavigationBarState createState() {
    return _SphereBottomNavigationBarState(
        items: navigationItems,
        sheetRadius: sheetRadius,
        onItemLongPressed: onItemLongPressed,
        sheetBackgroundColor: sheetBackgroundColor,
        defaultSelectedItem: defaultSelectedItem,
        onItemPressed: onItemPressed);
  }
}

class _SphereBottomNavigationBarState extends State<SphereBottomNavigationBar> {
  final int defaultSelectedItem;
  List<BuildNavigationItem> items;
  int selectedItemIndex = 0;
  BorderRadius sheetRadius;
  var sheetHieght;
  var sheetWidth;
  Color sheetBackgroundColor;
  ValueChanged<int> onItemLongPressed;
  ValueChanged<int> onItemPressed;

  _SphereBottomNavigationBarState(
      {required this.items,
      required this.onItemLongPressed,
      this.defaultSelectedItem = 0,
      required this.sheetRadius,
      required this.sheetBackgroundColor,
      //this.iconSize,
      required this.onItemPressed}) {
    selectedItemIndex = defaultSelectedItem;
    assert(items.length > 1, 'Atleast 2 item required. ');
    assert(items.length <= 5, 'You can add Maximum 5 Item');
  }

  Widget itemBuilder(BuildNavigationItem item, bool isSelected) {
    var containerHieght = ((sheetHieght + sheetWidth) / 7.63);
    var containerWidth = ((sheetHieght + sheetWidth) / 5);
    var tooltip =
        item.tooltip == null ? selectedItemIndex.toString() : item.tooltip;
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 0),
      child: Tooltip(
          verticalOffset: 50,
          message: '$tooltip',
          child: Column(
            children: <Widget>[
              Container(
                width: containerWidth,
                height: containerHieght,
                decoration: BoxDecoration(
                  color: isSelected
                      ? Color(0xFF6020BD).withOpacity(
                          0.1) //item.selectedItemColor.withOpacity(1)
                      : null, //item.itemColor.withOpacity(0.4),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(100),
                      bottomRight: Radius.circular(100)),
                ),
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          IconTheme(
                            data: IconThemeData(
                                size: ((containerHieght + containerWidth) / 5),
                                color: isSelected
                                    ? Colors.red
                                    : item.itemColor == null
                                        ? Colors.red
                                        : item.itemColor),
                            child: isSelected
                                ? Stack(
                                    children: [
                                      Image.asset(
                                        "assets/images/${item.icon}.png",
                                        width: 25,
                                        height: 25,
                                        color: Color(0xFFF93E6C),
                                      ),
                                    ],
                                  )
                                : Stack(
                                    children: [
                                      Image.asset(
                                        "assets/images/${item.icon}.png",
                                        width: 25,
                                        height: 25,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            item.title,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 10,
                                color: isSelected
                                    ? Color(0xFFF93E6C)
                                    : Colors.black),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              // Text(item.title)
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    sheetHieght = MediaQuery.of(context).size.height / 11;
    sheetWidth = MediaQuery.of(context).size.width;
    sheetBackgroundColor = (sheetBackgroundColor == null)
        ? Theme.of(context).bottomAppBarColor
        : sheetBackgroundColor;
    return Container(
      width: sheetWidth,
      height: sheetHieght,
      color: Colors.transparent,
      child: Container(
        // padding: EdgeInsets.only(top: 6),
        width: sheetWidth,
        height: sheetHieght,
        decoration: BoxDecoration(
            color: sheetBackgroundColor,
            borderRadius: sheetRadius,
            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 3)]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: items.map((item) {
            var index = items.indexOf(item);
            return GestureDetector(
              onTap: () {
                onItemPressed(index);
                setState(() {
                  selectedItemIndex = index;
                });
              },
              onLongPress: () {
                onItemLongPressed(index);
                setState(() {
                  selectedItemIndex = index;
                });
              },
              child: itemBuilder(item, selectedItemIndex == index),
            );
          }).toList(),
        ),
      ),
    );
  }
}
