import 'package:flutter/material.dart';

class AppColors {
  final blue = const Color(0xff5E6DF2); //Color(0xffF93E6C)
  final stepperColor = const Color(0xff5E6DF2); //Color(0xffF93E6C)
  final orange = const Color(0xffFF971D); //Color(0xffF93E6C)
  final darkgrey = const Color(0xff4F4F4F); //Color(0xffF93E6C)
  final greyText = const Color(0xff828282); //Color(0xffF93E6C)
  final blackText = const Color(0xff333333); //Color(0xffF93E6C)
  final white = const Color(0xffFFFFFF); //Color(0xffF93E6C)
  final bluelight =
      const Color(0xffAFB6F8).withOpacity(0.2); //Color(0xffF93E6C)
  final green = const Color(0xff219653); //Color(0xffF93E6C)
  final dividercolor = const Color(0xffEFEFEF); //Color(0xffF93E6C)
  final red = const Color(0xffEB5757); //Color(0xffF93E6C)
  final lightGrey = const Color(0xffBDBDBD); //Color(0xffF93E6C)
  final bgGreen = const Color(0xff27AE60); //Color(0xffF93E6C)
  final yellow = const Color(0xffE6BA16); //Color(0xffF93E6C)
  final lightGray = const Color(0xFF7F7F7F);
  final lightred = const Color(0xFFB00020);
  final lightgreen = const Color(0xFF0FB537);
  final appaccent = Color(0xffDA3C5F);
  final toasterror = Colors.red.shade400;
  final toastsuccess = Color(0xff00DBA7);
  final background = Colors.grey.shade100;

  AppColors();
}
