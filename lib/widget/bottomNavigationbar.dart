// import 'package:familov/widget/sphere_bottom_navigation_bar.dart';
// import 'package:flutter/material.dart';

// class NavigationBar extends StatefulWidget {
//   const NavigationBar({Key? key}) : super(key: key);

//   @override
//   _NavigationBarState createState() => _NavigationBarState();
// }

// class _NavigationBarState extends State<NavigationBar> {
//   var _selectedIndexs = 0;
//   int currentIndex = 0;
//   Color backgroudColor = Colors.white;

//   void _onItemTapped(int index) {
//     setState(() {
//       _selectedIndexs = index;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: SphereBottomNavigationBar(
//         defaultSelectedItem: 0,
//         sheetBackgroundColor: Colors.white,
//         sheetRadius: BorderRadius.only(
//             topLeft: Radius.circular(10), topRight: Radius.circular(10)),
//         onItemPressed: (index) {
//           _onItemTapped(index);
//         },
//         onItemLongPressed: (index) => setState(() {
//           backgroudColor = Color(0xFF44D6B2);
//         }),
//         navigationItems: [
//           BuildNavigationItem(
//               tooltip: 'Home',
//               itemColor: Colors.blue,
//               icon: 'homes',
//               selectedItemColor: Color(0xFFFFB2D9),
//               title: 'Home'),
//           BuildNavigationItem(
//               tooltip: 'Recharge',
//               itemColor: Color(0xFFB2F4FF),
//               icon: 'rechargeicon',
//               selectedItemColor: Color(0xFFFFB2D9),
//               title: 'Recharge'),
//           BuildNavigationItem(
//               tooltip: 'Cart',
//               itemColor: Color(0xFFCDB2FF),
//               icon: 'rechargeicon',
//               selectedItemColor: Color(0xFFFFB2D9),
//               title: 'Cart'),
//           BuildNavigationItem(
//               tooltip: 'Profile',
//               itemColor: Color(0xFFB2F4FF),
//               icon: 'group1',
//               selectedItemColor: Color(0xFFFFB2D9),
//               title: 'Profile'),
//         ],
//       ),
//     );
//   }
// }
