import 'package:flutter/material.dart';

Widget buttonWidget(String text, BuildContext ctx, Function onTap) {
  return Container(
    child: FlatButton(
        height: 48,
        minWidth: MediaQuery.of(ctx).size.width,
        // minWidth: MediaQuery.of(context).size.width,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.horizontal(
                left: Radius.circular(10), right: Radius.circular(10))),
        color: Color(0xff00DBA7),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(text,
                style: new TextStyle(fontSize: 18.0, color: Colors.white)),
          ],
        ),
        onPressed: () {
          onTap();
        }),
  );
}
