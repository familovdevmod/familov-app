// ignore: import_of_legacy_library_into_null_safe
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CarouselPage extends StatefulWidget {
  List<String> textList;

  CarouselPage({required this.textList});
  @override
  _CarouselPageState createState() => _CarouselPageState();
}

class _CarouselPageState extends State<CarouselPage> {
  int _current = 0;
  int cha = 1;
  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = widget.textList
        .map((item) => Container(
              width: 300,
              decoration: BoxDecoration(
                color: Color.fromRGBO(239, 233, 248, 1),
                border: Border.all(
                  color: Colors.black12,
                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.elliptical(32, 32)),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
                child: Column(
                  children: [
                    Container(
                      // height: 40,
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          Container(
                            width: 32,
                            // height: 32,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(239, 233, 248, 1),
                              border: Border.all(
                                color: Colors.black12,
                                width: 1,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(32, 32)),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Positioned(bottom: 5, child: Text('$cha')),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      cha == 1
                          ? '\n1\nChoose store'
                          : cha == 2
                              ? '\n2\nPay securely'
                              : "\n3\nYour package are delivered",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(96, 32, 189, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 16,
                          letterSpacing: 0,
                          fontWeight: FontWeight.normal,
                          height: 1.5),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      '${widget.textList[cha - 1]}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(130, 130, 130, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.normal,
                          height: 1.5),
                    )
                  ],
                ),
              ),
            ))
        .toList();

    return Column(
      children: [
        CarouselSlider(
          items: imageSliders,
          options: CarouselOptions(
              autoPlay: true,
              enlargeCenterPage: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                  if (cha < 3) {
                    cha++;
                  } else {
                    cha = 1;
                  }
                });
              }),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.textList.map((url) {
            int index = widget.textList.indexOf(url);
            return Container(
              width: 8,
              height: 8,
              margin: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 3,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? Color.fromRGBO(0, 0, 0, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4),
              ),
            );
          }).toList(),
        )
      ],
    );
  }
}
