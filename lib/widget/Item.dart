import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/deleteCart.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/place.dart';
import 'package:familov/presenter/cartcontroller.dart';
import 'package:familov/presenter/cartprovider.dart';
import 'package:familov/presenter/deleteCart_presenter.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/screen/productDetail.dart';
import 'package:familov/utils/urlcollection.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screen/CartScreen.dart';

class Item extends StatefulWidget {
  final String name,
      price,
      image,
      productId,
      specialproductprices,
      availability,
      shopid;
  const Item({
    Key? key,
    required this.name,
    required this.price,
    required this.image,
    required this.productId,
    required this.specialproductprices,
    required this.availability,
    required this.shopid,
  }) : super(key: key);
  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item>
    implements SetCartContract, GetCartContract, DeleteCartContract {
  var price;
  bool isAdded = false;
  // int localcount = 0;
  var load = false;
  var msg;
  double? discountPrice;
  String? disP;
  var percentage;
  var per;
  SetCartPresenter? presenter;
  GetCartPresenter? presenter1;
  DeleteCartPresenter? delpresenter;
  var valueKey;
  List<Data9> dataList2 = [];
  var len;
  List cartproductid = [];
  var newcount = 0;
  int localcount = 0;
  var localcartproductid;
  List dataList = [];
  int length = 0;
  List<Place> items = [];
  var deletedatabase;

  CartController cartController = Get.put(CartController());
  @override
  void initState() {
    print('local count ${localcount}');
    shared();
    database();
    presenter1!.getCart();
    super.initState();
  }

  var sho;
  pref(sho) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('shop', sho);
    print('sho  is : $sho');
  }

  var checkCurrency;
  var rate;
  shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    checkCurrency = preferences.getString('currency');
    rate = preferences.getString('rate');
    valueKey = preferences.getString('value_key');
    sho = preferences.getString('shop');
    print('currency is $checkCurrency and $sho');
    setState(() {});
  }

  _ItemState() {
    presenter = new SetCartPresenter(this);
    presenter1 = new GetCartPresenter(this);
    delpresenter = new DeleteCartPresenter(this);
  }
  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<CartProvider>(context);
    // URLCollection().sharedP0();
    discountPrice =
        double.parse(widget.price) - double.parse(widget.specialproductprices);
    // print('disxount ${discountPrice}');
    print(rate);
    // print('rate-------->');
    disP = discountPrice.toString();
    // percentage = ((double.parse(widget.price) - discountPrice) * 100) /
    //     double.parse(widget.price);
    percentage = discountPrice! * 100;
    per = percentage / double.parse(widget.price);
    // print(
    //     'percenteage is  $per  and original ${double.parse(widget.price)} and discount is ${double.parse(widget.specialproductprices)}');
    // var specialproductprices = double.parse(widget.price) * double.parse(rate);
    // String specialprice2 = specialproductprices.toString();
    // var specialproductprices =
    //     '${double.parse('${double.parse(widget.price) * double.parse(rate)}').toStringAsFixed(2)}';
    // var parsedspecialproductprices = num.parse(  );

    // var anotherprice = double.parse(widget.price) * double.parse(rate);
    // var parsedanotherprice = num.parse(anotherprice.toStringAsFixed(2));
    return InkWell(
      highlightColor: Colors.transparent,
      splashFactory: NoSplash.splashFactory,
      onLongPress: () {
        print('');
        print('from here');
        print(widget.productId);
        print(widget.specialproductprices);
        print(widget.image);
        print(widget.price);
        print(discountPrice.toString());
        print(per.toString());
        print(widget.shopid);
        print(localcount);
      },
      onTap: () {
        widget.availability == 'AVAILABLE'
            ? Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductDetail(
                          productId: widget.productId,
                          specialPrice: widget.specialproductprices,
                          image: widget.image,
                          price: widget.price,
                          discount: discountPrice.toString(),
                          disPercentage: per.toString(),
                          shopId: widget.shopid,
                          totalNumber:
                              localcount == 0 ? localcount + 1 : localcount,
                        ))).then((value) {
                database();
                presenter1!.getCart();
                setState(() {});
              })
            : BotToast.showSimpleNotification(
                onTap: () {},
                backgroundColor: Colors.red.shade400,
                title: "Product Not Available");
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          boxShadow: [
            BoxShadow(
              color: widget.availability == 'AVAILABLE'
                  ? Colors.grey.shade300
                  : Colors.white,
              blurRadius: 2.0,
            ),
          ],
          color:
              widget.availability != 'AVAILABLE' ? Colors.white : Colors.white,
        ),
        child: Stack(
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 111,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Center(
                      child: widget.image != ''
                          ? Stack(
                              children: [
                                widget.availability != 'AVAILABLE'
                                    ? Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(2),
                                            topRight: Radius.circular(2),
                                            bottomLeft: Radius.circular(2),
                                            bottomRight: Radius.circular(2),
                                          ),
                                          color:
                                              Color.fromRGBO(224, 224, 224, 1),
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8, vertical: 4),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Text(
                                              'Not Available',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      51, 51, 51, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 12,
                                                  letterSpacing:
                                                      0 /*percentages not used in flutter. defaulting to zero*/,
                                                  fontWeight: FontWeight.w500,
                                                  height:
                                                      1.5 /*PERCENT not supported*/
                                                  ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : InkWell(
                                        child: Image.network(
                                          '${widget.image}',
                                          alignment: Alignment.center,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container(
                                              // color: Colors.red,
                                              margin: EdgeInsets.only(left: 10),
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                'assets/images/fami.png',
                                                scale: 1,
                                                alignment: Alignment.center,
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                              ],
                            )
                          : InkWell(
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'assets/images/fami.png',
                                  scale: 1,
                                  alignment: Alignment.center,
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pushNamed(
                                  '/ProductDetail',
                                  arguments: widget.productId,
                                );
                              },
                            ),
                    ),
                  ),
                  Container(
                    width: 100,
                    alignment: Alignment.center,
                    child: Center(
                      child: Text(
                        widget.name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontWeight: FontWeight.w500,
                            color: widget.availability == 'AVAILABLE'
                                ? Colors.black
                                : Colors.black.withOpacity(.4)),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      widget.specialproductprices == '0.00'
                          ? Text(
                              '${checkCurrency.toString().substring(0, 2)}${widget.price}',
                              // '${double.parse('$rate')}',
                              // '${checkCurrency.toString().substring(0, 2)} ${double.parse('${widget.price.toString()}') * double.parse('$rate')}',
                              // ' ${widget.price}  ',
                              // maxLines: 1,

                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: TextStyle(
                                  overflow: TextOverflow.fade,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black87.withOpacity(0.8),
                                  fontFamily: 'Gilroy',
                                  fontSize: 13),
                            )
                          : Container(
                              child: Text(
                                '${checkCurrency.toString().substring(0, 2)}${widget.price}',
                                // '${checkCurrency.toString().substring(0, 2)}${parsedanotherprice}',
                                // '${checkCurrency.toString().substring(0, 2)} ${double.parse('${widget.price.toString().substring(0, 4)}') * double.parse('$rate')}',
                                // ' ${double.parse('${widget.price.toString()}')}',
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  color: Color(0xff828282),
                                  fontSize: 12,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                      SizedBox(
                        width: 10,
                      ),
                      widget.specialproductprices == '0.00'
                          ? Container(
                              width: 0,
                              height: 0,
                            )
                          : InkWell(
                              onTap: () {
                                print(widget.price);
                              },
                              child: Text(
                                '${checkCurrency.toString().substring(0, 2)}${widget.specialproductprices}',
                                // '${checkCurrency.toString().substring(0, 2)}${disP!.length > 5 ? disP!.substring(0, 5) : disP}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  fontWeight: FontWeight.bold,
                                  color: widget.availability == 'AVAILABLE'
                                      ? Colors.black87.withOpacity(0.8)
                                      : Colors.black87.withOpacity(0.5),
                                ),
                              ),
                            ),
                    ],
                  ),
                  isAdded || newcount != 0 || localcount != 0
                      ? Container(
                          height: 24,
                          width: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              valueKey == null
                                  ? InkWell(
                                      onTap: () {
                                        print('local count ${localcount}');
                                        if (localcount == 0) {
                                          isAdded = false;
                                          setState(() {
                                            print('isadddddded');
                                          });
                                          pref('');
                                        } else {
                                          pref('${widget.shopid}');
                                        }
                                        if (localcount == 0) {
                                          isAdded = false;
                                          setState(() {});
                                          pref('');
                                        }
                                        price = widget.price * localcount;
                                        print('price is  $price');

                                        if (localcount > 0) {
                                          localcount--;
                                          isAdded = false;
                                          deletesDatabase(
                                              widget.productId.toString());
                                          print('isadddddded');
                                        }
                                        database();
                                        if (localcount > 0) {
                                          DBHelper.insert('thecart', {
                                            'id': widget.productId.toString(),
                                            'quantity': localcount.toString(),
                                            'shopId': widget.shopid.toString(),
                                          });
                                        }
                                        setState(() {
                                          BotToast.showSimpleNotification(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CartScreen()));
                                              },
                                              backgroundColor:
                                                  Color(0xff00DBA7),
                                              title:
                                                  "Product Quantity Updated");
                                        });
                                      },
                                      child: Container(
                                        width: 24,
                                        height: 24,
                                        child: const Center(
                                            child: Text(
                                          "-",
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        )),
                                        decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.grey),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: Colors.white),
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () {
                                        setState(() {
                                          if (localcount == 0) {
                                            pref('');
                                          } else {
                                            pref('${widget.shopid}');
                                          }
                                          if (newcount > 0) {
                                            newcount--;
                                            isAdded = false;
                                            setState(() {
                                              newcount == 0
                                                  ? delpresenter!.deleteCart(
                                                      widget.productId)
                                                  : print('object');
                                            });
                                          }

                                          presenter!.setCart(
                                              widget.shopid,
                                              widget.productId,
                                              newcount.toString(),
                                              '',
                                              '',
                                              '',
                                              '');
                                          BotToast.showSimpleNotification(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CartScreen()));
                                              },
                                              backgroundColor:
                                                  Color(0xff00DBA7),
                                              title:
                                                  "Product Quantity Updated");
                                        });
                                      },
                                      child: Container(
                                        width: 24,
                                        height: 24,
                                        child: const Center(
                                            child: Text(
                                          "-",
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        )),
                                        decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.grey),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: Colors.white),
                                      ),
                                    ),
                              Container(
                                  alignment: Alignment.center,
                                  width: 20,
                                  height: 24,
                                  child: Text(
                                    valueKey != null
                                        ? '${newcount.toString()}'
                                        : '${localcount.toString()}',
                                    maxLines: 1,
                                  )),
                              valueKey == null
                                  ? InkWell(
                                      onTap: () {
                                        localcount++;
                                        if (localcount == 0) {
                                          pref('');
                                        } else {
                                          pref('${widget.shopid}');
                                        }
                                        print(
                                            'id is  :::: ${widget.productId}');
                                        DBHelper.insert('thecart', {
                                          'id': widget.productId.toString(),
                                          'quantity': localcount == 0
                                              ? localcount.toString()
                                              : localcount,
                                          'shopId': widget.shopid.toString(),
                                        });
                                        database();
                                        setState(() {
                                          print('orailnoor');
                                          BotToast.showSimpleNotification(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CartScreen()));
                                              },
                                              backgroundColor:
                                                  Color(0xff00DBA7),
                                              title:
                                                  "Product Quantity Updated");
                                        });
                                      },
                                      child: Container(
                                        width: 24,
                                        height: 24,
                                        child: const Center(
                                            child: Text(
                                          "+",
                                          style: TextStyle(color: Colors.white),
                                        )),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: const Color(0xff00DBA7)),
                                      ),
                                    )
                                  : InkWell(
                                      highlightColor: Colors.transparent,
                                      splashFactory: NoSplash.splashFactory,
                                      onTap: () {
                                        newcount++;
                                        print(
                                            ' aho $sho and ${widget.productId} and $localcount');
                                        presenter!.setCart(
                                            widget.shopid,
                                            widget.productId,
                                            newcount.toString(),
                                            '',
                                            '',
                                            '',
                                            '');
                                        // setState(() {
                                        //   if (localcount == 0) {
                                        //     pref('');
                                        //   } else {
                                        //     pref('${widget.shopid}');
                                        //   }
                                        // });
                                        BotToast.showSimpleNotification(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          CartScreen()));
                                            },
                                            backgroundColor: Color(0xff00DBA7),
                                            title: "Product Quantity Updated");
                                      },
                                      child: Container(
                                        width: 24,
                                        height: 24,
                                        child: const Center(
                                            child: Text(
                                          "+",
                                          style: TextStyle(color: Colors.white),
                                        )),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: const Color(0xff00DBA7)),
                                      ),
                                    ),
                            ],
                          ),
                        )
                      : valueKey == null
                          ? InkWell(
                              highlightColor: Colors.transparent,
                              splashFactory: NoSplash.splashFactory,
                              onTap: widget.availability == 'AVAILABLE'
                                  ? () {
                                      isAdded = true;
                                      localcount++;

                                      if (localcount == 0) {
                                        pref('');
                                      } else {
                                        pref('${widget.shopid}');
                                      }
                                      print('id is  :::: ${widget.productId}');
                                      DBHelper.insert('thecart', {
                                        'id': widget.productId.toString(),
                                        'quantity': localcount.toString(),
                                        'shopId': widget.shopid.toString(),
                                      });

                                      setState(
                                        () {
                                          database();
                                          BotToast.showSimpleNotification(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CartScreen()));
                                              },
                                              backgroundColor:
                                                  Color(0xff00DBA7),
                                              title:
                                                  "Product Added Sucessfully");
                                        },
                                      );
                                    }
                                  : () {
                                      BotToast.showSimpleNotification(
                                          onTap: () {},
                                          backgroundColor: Colors.red.shade400,
                                          title: "Product Not Available");
                                    },
                              child: Container(
                                width: 70,
                                height: 24,
                                child: const Center(
                                    child: Text(
                                  "+ Add",
                                  style: TextStyle(color: Colors.white),
                                )),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: widget.availability == 'AVAILABLE'
                                        ? Color(0xff00DBA7)
                                        : Color(0xff00DBA7).withOpacity(.3)),
                              ),
                            )
                          : InkWell(
                              highlightColor: Colors.transparent,
                              splashFactory: NoSplash.splashFactory,
                              // here i am
                              onTap: widget.availability == 'AVAILABLE'
                                  ? () {
                                      isAdded = true;
                                      newcount++;
                                      if (localcount == 0) {
                                        pref('');
                                      } else {
                                        pref('${widget.shopid}');
                                      }
                                      print(
                                          ' ====aho $sho and ${widget.productId} and $localcount');
                                      presenter!.setCart(
                                          widget.shopid,
                                          widget.productId,
                                          newcount.toString(),
                                          '',
                                          '',
                                          '',
                                          '');
                                      setState(() {});
                                    }
                                  : () {
                                      BotToast.showSimpleNotification(
                                          backgroundColor: Colors.red.shade400,
                                          title: "Product Not Available");
                                    },
                              child: Container(
                                width: 70,
                                height: 24,
                                child: const Center(
                                    child: Text(
                                  "+ Add",
                                  style: TextStyle(color: Colors.white),
                                )),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: widget.availability == 'AVAILABLE'
                                        ? Color(0xff00DBA7)
                                        : Color(0xff00DBA7).withOpacity(0.3)),
                              ),
                            ),
                ],
              ),
            ),
            Positioned(
                child: double.parse(widget.specialproductprices) != 0
                    ? Container(
                        width: 40,
                        height: 35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(4),
                            topRight: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(0),
                          ),
                          color: widget.availability == 'AVAILABLE'
                              ? Color.fromRGBO(249, 62, 108, 1)
                              : Color.fromRGBO(249, 62, 108, 1).withOpacity(.4),
                          // border: Border.all(
                          //   color: Color.fromRGBO(249, 62, 108, 1),
                          //   width: 0.6000000238418579,
                          // ),
                        ),
                        child: Center(
                          child: Text(
                            '${per.toString().substring(0, 3)}%\noff',
                            textAlign: TextAlign.center,
                            maxLines: 2,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      )
                    : Container(
                        width: 31,
                        height: 28,
                        child: Container(
                            width: 31,
                            height: 28,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(4),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(0),
                                bottomRight: Radius.circular(0),
                              ),
                            )))),
          ],
        ),
      ),
    );
  }

  @override
  void onSetCartError(String error) {
    print('error iss ${error}');
  }

  @override
  void onSetCartSuccess(SetCart setCartModel) {
    presenter1!.getCart();
    if (setCartModel.status == true) {
      newcount > 1
          ? print('object')
          : BotToast.showSimpleNotification(
              // onTap: () {
              // };
              backgroundColor: Color(0xff00DBA7),
              title: "Product Added Sucessfully");
      // Fluttertoast.showToast(
      //   textColor: Colors.black,
      //   msg: 'Product Added Sucessfully',
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM_RIGHT,
      //   backgroundColor: Colors.lightGreen.shade100,
      //   fontSize: 18,
      // );
    }
    print('Erroe message is : ${setCartModel.status}');
    setState(() {
      if (setCartModel.errorMsg == "Different shop") {
        BotToast.showSimpleNotification(
            // onTap: () {
            // };
            backgroundColor: AppColors().toasterror,
            title: "${setCartModel.errorMsg}");
      }
    });
  }

  @override
  void onGetCartError(String error) {
    // TODO: implement onGetCartError
  }

  @override
  onGetCartSuccess(GetCart getCartModel) {
    // TODO: implement onGetCartSuccess
    localcount = 0;
    print('orailnoor --item cart screen $len');
    dataList2 = getCartModel.data!;
    len = dataList2.length;
    cartController.getcountapi.value = len;
    print('orailnoor --item cart screen $len');
    for (var i = 0; i < len; i++) {
      widget.productId == dataList2[i].productId
          ? newcount = int.parse(dataList2[i].quantity.toString())
          : print('object');

      cartproductid =
          List<String>.generate(len, (counter) => "${dataList2[i].productId}");
      print('cart quantity :${dataList2[i].productId}');
    }
    print(cartproductid[0].toString());
    print('blank');
    setState(() {});
  }

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;
    cartController.getcount.value = length;
    print('Lenght of local cart $length');
    items = dataList
        .map(
          (item) => Place(
              id: item['id'],
              quantity: item['quantity'],
              shopId: item['shopId']),
        )
        .toList();
    for (int i = 0; i < length; i++) {
      widget.productId == items[i].id
          ? localcount = int.parse(items[i].quantity)
          : print('');
      print('items ID: ${items[i].id}\n item quantity : ${items[i].quantity}');
    }
    print('here it id $items');
    setState(() {
      // print('shopIdshopId is : ${dataList[0]['shopId'].toString()}');
      // print('id is : ${dataList[0]['id'].toString()}');
      // print('quantity is : ${dataList[0]['quantity'].toString()}');
    });
  }

  deletesDatabase(String idToDelete) async {
    print('del done');
    deletedatabase = await DBHelper.delete(
      int.parse('$idToDelete'),
    );
    length = dataList.length;
    print('length');
    setState(() {});
  }

  @override
  void onDeleteCartError(String error) {
    // TODO: implement onDeleteCartError
  }

  @override
  void onDeleteCartSuccess(DeleteCart deleteCartModel) {
    // TODO: implement onDeleteCartSuccess
  }
}
