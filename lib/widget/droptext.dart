import 'package:flutter/material.dart';

class Dropdown extends StatefulWidget {
  const Dropdown({Key? key}) : super(key: key);
  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  var _dropDownValue;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Text(
            ' € EUR',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(79, 79, 79, 1),
                fontFamily: 'Gilroy',
                fontSize: 14,
                letterSpacing: 0,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Text(
          '\$ USD',
          textAlign: TextAlign.left,
          style: TextStyle(
              color: Color.fromRGBO(79, 79, 79, 1),
              fontFamily: 'Gilroy',
              fontSize: 14,
              letterSpacing: 0,
              fontWeight: FontWeight.normal,
              height: 1),
        ),
        Text(
          'c \$ CAD',
          textAlign: TextAlign.left,
          style: TextStyle(
              color: Color.fromRGBO(79, 79, 79, 1),
              fontFamily: 'Gilroy',
              fontSize: 14,
              letterSpacing: 0,
              fontWeight: FontWeight.normal,
              height: 1),
        ),
        Text(
          '£ GBP',
          textAlign: TextAlign.left,
          style: TextStyle(
              color: Color.fromRGBO(79, 79, 79, 1),
              fontFamily: 'Gilroy',
              fontSize: 14,
              letterSpacing: 0,
              fontWeight: FontWeight.normal,
              height: 1),
        )
      ],
    );

 
  }
}
