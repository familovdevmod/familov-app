import 'package:flutter/material.dart';

Widget getBottomButton(String text, Function onTap) {
  return BottomAppBar(
    color: Colors.white,
    child: Container(
      height: 48,
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: InkWell(
                    child: Container(
                      width: 360,
                      height: 48,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                          //  bottomLeft: Radius.circular(8),
                          //  bottomRight: Radius.circular(8),
                        ),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          text,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      onTap();
                    }),
                flex: 1,
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
