import 'package:familov/model/getUserTransaction.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionBox extends StatefulWidget {
  final String data;
  final List<Data_list>? dataList;

  const TransactionBox({Key? key, required this.data, this.dataList})
      : super(key: key);
  @override
  _TransactionBoxState createState() => _TransactionBoxState();
}

class _TransactionBoxState extends State<TransactionBox> {
  @override
  Widget build(BuildContext context) {
    return widget.dataList?.length == 0
        ? Container(
            child: Image.asset(
              'assets/images/transaction.png',
              width: 200,
              height: 400,
            ),
          )
        : ListView.builder(
         //  separatorBuilder: (BuildContext context, int index) { return  },
            itemCount: widget.dataList?.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    widget.data == ""
                        ? Container()
                        : Container(
                            width: 100,
                            height: 16,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                topRight: Radius.circular(8),
                                bottomLeft: Radius.circular(8),
                                bottomRight: Radius.circular(8),
                              ),
                              color: widget.dataList![index].orderStatus ==
                                      'Wait'
                                  ? Color.fromRGBO(242, 201, 76, 1)
                                  : widget.dataList![index].orderStatus ==
                                          'Delivered'
                                      ? Color(0xff00DBA7)
                                      : widget.dataList![index].orderStatus ==
                                              'Cancel'
                                          ? Colors.red
                                          : Color.fromRGBO(242, 201, 76, 1),
                            ),
                            child: Text(
                              '${widget.dataList![index].orderStatus}',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                  height: 1.4),
                            )),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 5.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(4),
                            topRight: Radius.circular(4),
                            bottomLeft: Radius.circular(4),
                            bottomRight: Radius.circular(4),
                          ),
                          border: Border.all(
                            color: Color.fromRGBO(189, 189, 189, 1),
                            width: 0.5,
                          ),
                        ),
                        child: Column(children: <Widget>[
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 15),
                                child: Text(
                                  '${widget.dataList![index].generateCode}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 16,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 15, top: 15),
                                child: Text(
                                  '${(DateFormat("yyyy-MM-dd hh:mm:ss").parse('${widget.dataList![index].dateTime}'))}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 12,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  'Payment : ${widget.dataList![index].paymentMethod}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 13,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 5, top: 10),
                                child: Text(
                                  '${widget.dataList![index].paymentCurrCode}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(0, 219, 167, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 15, top: 10),
                                child: Text(
                                  '${widget.dataList![index].grandTotal}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(0, 219, 167, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Transform.rotate(
                              angle: 0.000005008956130975318,
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 4),
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'Invoice',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(77, 77, 77, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 12,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),
                                ),
                                SizedBox(width: 30),
                                Text(
                                  'Delivery Note',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(96, 32, 189, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 12,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width /
                                          2.7),
                                  child: Text(
                                    'Reorder',
                                    style: TextStyle(
                                        color: Color.fromRGBO(249, 62, 108, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 12,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ])),
                  ],
                ),
              );
            });
  }

  // DateTime getddMMyyyyDateFormatToDateTime(String stringDateTime) {
  //   return DateFormat('dd-MM-yyyy').parse(stringDateTime);
  // }

  // widgetShowDateNTime(String Date, Time) {
  //   var timeFrom = DateFormat('HH:mm a').parse(Time);
  //   var dateFormat = DateFormat("HH:mm");
  //   return Container(
  //     padding: EdgeInsets.only(left: 10, right: 10),
  //     margin: new EdgeInsets.only(top: 1.0),
  //     child: Align(
  //       alignment: Alignment.centerLeft,
  //       child: Text(
  //         Date + " , " + Time,
  //         //dateFormat.format(timeFrom)
  //         maxLines: 1,
  //         textAlign: TextAlign.center,
  //         // style: GoogleFonts.poppins(
  //         //   fontSize: 10.0,
  //         //   color: AppColors().color3,
  //         // ),
  //       ),
  //     ),
  //   );
  // }
}
