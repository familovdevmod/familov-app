import 'package:familov/widget/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget getBottomButtonWithTwoFill(BuildContext context, String text1,
    String text2, Function firstMethod, Function secondMethod, var value) {
  double width1 = 0;

  double width2 = 0;
  if (value == 1) {
    width1 = MediaQuery.of(context).size.width / 3 - 8;
    width2 = MediaQuery.of(context).size.width / 1.5;
  } else {
    width1 = MediaQuery.of(context).size.width / 3;
    width2 = MediaQuery.of(context).size.width / 3;
  }
  return Container(
    color: Colors.transparent,
    height: 48,
    child: Row(
      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
            child: Container(
              width: width1,
              // height: width1,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromRGBO(0, 219, 167, 1),
                ),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                color: Colors.white,
              ),
              child: Align(
                  alignment: Alignment.center,
                  child: value == 1
                      ? Icon(
                          Icons.share_outlined,
                          color: Color.fromRGBO(0, 219, 167, 1),
                        )
                      : Text(
                          '$text1',
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        )),
            ),
            onTap: () {}),
        Spacer(),
        SizedBox(
          width: 8,
        ),
        InkWell(
            child: Container(
              width: width2,
              height: 48,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                color: Color.fromRGBO(0, 219, 167, 1),
              ),
              child: Center(
                  child: Text(
                text2,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w600,
                    fontSize: 18),
              )),
              // child: Align(
              //   alignment: Alignment.center,
              //   child: Text(
              //     text2,
              //     style: GoogleFonts.montserrat(
              //         color: AppColors().white,
              //         fontSize: 18,
              //         letterSpacing: 1.2,
              //         fontWeight: FontWeight.w600,
              //         height: 1.5),
              //     textAlign: TextAlign.center,
              //   ),
              // ),
            ),
            onTap: () {
              // if (value == 2) {
              secondMethod();
              // }
            }),
      ],
    ),
  );
}

Widget getBottomButtonWithTwoFillcheckout(
  BuildContext context,
  String text1,
  String text2,
  Function firstMethod,
  Function secondMethod,
) {
  // double width1 = 0;
  // double width2 = 0;
  // if (value == 1) {
  //   width1 = 48;
  //   width2 = 250;
  // } else {
  //   width1 = 48;
  //   width2 = 195;
  // }
  return Container(
    color: Colors.transparent,
    height: 48,
    child: Row(
      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromRGBO(0, 219, 167, 1),
                ),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  // topRight: Radius.circular(8),
                ),
                color: Colors.white,
              ),
              child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    '$text1',
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  )),
            ),
            onTap: () {
              firstMethod();
            }),
        InkWell(
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              height: 48,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  // topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                color: Color.fromRGBO(0, 219, 167, 1),
              ),
              child: Center(
                  child: Text(
                text2,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w600,
                    fontSize: 18),
              )),
              // child: Align(
              //   alignment: Alignment.center,
              //   child: Text(
              //     text2,
              //     style: GoogleFonts.montserrat(
              //         color: AppColors().white,
              //         fontSize: 18,
              //         letterSpacing: 1.2,
              //         fontWeight: FontWeight.w600,
              //         height: 1.5),
              //     textAlign: TextAlign.center,
              //   ),
              // ),
            ),
            onTap: () {
              // if (value == 2) {
              secondMethod();
              // }
            }),
      ],
    ),
  );
}
