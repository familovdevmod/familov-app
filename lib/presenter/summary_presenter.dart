import 'dart:convert';
import 'package:familov/model/summary.dart';
import 'package:familov/model/summmaryy.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class SummaryContract {
  void onSummarySuccess(Summmaryy summaryModel);
  void onSummaryError(String error);
}

class SummaryPresenter {
  SummaryContract _view;
  URLCollection api = new URLCollection();
  SummaryPresenter(this._view);

  summary(
    String delivery,
    String promo,
  ) {
    api
        .summary(
      delivery,
      promo,
    )
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onSummaryError(res.errorHuman);
      } else {
        print('res.summary ${jsonEncode(res.mappedResult)}');
        _view.onSummarySuccess(Summmaryy.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
