import 'dart:convert';
import 'package:familov/model/deleteCart.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class DeleteCartContract {
  void onDeleteCartSuccess(DeleteCart deleteCartModel);
  void onDeleteCartError(String error);
}

class DeleteCartPresenter {
  DeleteCartContract _view;
  URLCollection api = new URLCollection();
  DeleteCartPresenter(this._view);

  deleteCart(
    String productId,
  ) {
    api
        .deleteCart(
      productId,
    )
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onDeleteCartError(res.errorHuman);
      } else {
        print('res.deleteCartResult ${jsonEncode(res.mappedResult)}');
        print('Delete Success');
        _view.onDeleteCartSuccess(
            DeleteCart.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
