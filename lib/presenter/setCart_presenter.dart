import 'dart:convert';
import 'package:familov/model/setCart.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class SetCartContract {
  void onSetCartSuccess(SetCart setCartModel);
  void onSetCartError(String error);
}

class SetCartPresenter {
  SetCartContract _view;
  URLCollection api = new URLCollection();
  SetCartPresenter(this._view);

  setCart(String shopId, String productId, String quantity, String countryCode,
      String phoneNumber, String skuCode, String isRecharge) {
    // {{BaseURL}}/api_v2/set-cart?shop_id=2&product_id=10&quantity=50&is_recharge=r&country_code=91&sku_code=RJININ77785&phone_number=8840993989
    print('idddds $shopId ,  $productId and , $quantity');
    api
        .setCart(shopId, productId, quantity, countryCode, phoneNumber, skuCode,
            isRecharge)
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onSetCartError(res.errorHuman);
      } else {
        print('res.setCartResult ${jsonEncode(res.mappedResult)}');
        _view.onSetCartSuccess(SetCart.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
