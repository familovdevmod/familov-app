import 'dart:convert';
import 'package:familov/model/recommendation.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class RecommendationContract {
  void onRecommendationSuccess(Recommendation recommendation);
  void onRecommendationError(String error);
}

class RecommendationPresenter {
  RecommendationContract _view;
  URLCollection api = new URLCollection();
  RecommendationPresenter(this._view);
  recommendation() {
    api.recommendation().then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onRecommendationError(res.errorHuman);
      } else {
        print('res.recommendation ${jsonEncode(res.mappedResult)}');
        _view.onRecommendationSuccess(
            Recommendation.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
