import 'dart:convert';
import 'package:familov/model/shopModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ShopContract {
  void onShopSuccess(ShopModel shopModel);
  void onShopError(String error);
}

class ShopPresenter {
  ShopContract _view;
  URLCollection api = new URLCollection();
  ShopPresenter(this._view);
  shop(data1 , data2) {
    api
        .shopList(data1,data2)
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onShopError(res.errorHuman);
      } else {
        print('res.shop ${jsonEncode(res.mappedResult)}');
        _view.onShopSuccess(ShopModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
