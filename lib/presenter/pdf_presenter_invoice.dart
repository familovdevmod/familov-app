import 'dart:convert';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

import '../model/pdf_model.dart';

abstract class GetPdfDataContactInvoice {
  void onPdfSuccess(PdfModel invoicesModel);
  void onPdfError(String error);
}

class PdfPresenterInvoice {
  GetPdfDataContactInvoice _view;
  URLCollection api = new URLCollection();

  PdfPresenterInvoice(this._view);

  getPdfInvoicedata(orderId) {
    print('urlData pdf invoice: $orderId');
    api.pdfUrlInvoice(orderId).then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onPdfError(res.errorHuman);
      } else {
        print('res.pdf ${jsonEncode(res.mappedResult)}');

        _view.onPdfSuccess(PdfModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
