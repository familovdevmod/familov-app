import 'dart:convert';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ClearCartContract {
  void onClearCartSuccess(CartClear clearCart);
  void onClearCartError(String error);
}

class ClearCartPresenter {
  ClearCartContract _view;
  URLCollection api = new URLCollection();
  ClearCartPresenter(this._view);

  clearCart() {
    api.clearCart().then((MappedNetworkServiceResponse res) async {
      print('${res.toString()}--clear cart');
      if (!res.success) {
        _view.onClearCartError(res.errorHuman);
      } else {
        print('res.clearCart ${jsonEncode(res.mappedResult)}');
        _view.onClearCartSuccess(
            CartClear.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
