import 'dart:convert';
import 'package:familov/model/category_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CategoryContract {
  void onCategorySuccess(CategoryModel categoryModel);
  void onCategoryError(String error);
}

class CategoryPresenter {
  CategoryContract _view;
  URLCollection api = new URLCollection();
  CategoryPresenter(this._view);
  category(data1 , data2) {
    api
        .categoryList(data1,data2)
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onCategoryError(res.errorHuman);
      } else {
        print('res.category ${jsonEncode(res.mappedResult)}');
        _view.onCategorySuccess(CategoryModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
