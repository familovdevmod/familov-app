import 'dart:convert';
import 'package:familov/model/topRated_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetProductContract {
  void onProductSuccess(TopProduct productModel);
  void onProductError(String error);
}

class UserProductPresenter {
  GetProductContract _view;
  URLCollection api = new URLCollection();

  UserProductPresenter(this._view);

  getUserproduct(
    data1,
    data2,
    data3,
    data4,
    data5,
  ) {
    // print('all 5 data is  :  A $data1, B  $data2,  C    $data3 ,  D    $data4,   E   $data5,');
    //  print('The value is :  $a');
    api
        .product(
      data1,
      data2,
      data3,
      data4,
      data5,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onProductError(res.errorHuman);
      } else {
        print('res.product ${jsonEncode(res.mappedResult)}');
        _view.onProductSuccess(
            TopProduct.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
