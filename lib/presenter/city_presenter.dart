import 'dart:convert';
import 'package:familov/model/cityModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CityContract {
  void onCitySuccess(CityModel cityModel);
  void onCityError(String error);
}

class CityPresenter {
  CityContract _view;
  URLCollection api = new URLCollection();

  CityPresenter(this._view);

  city(data) {
    api
        .city('?country_id=$data')
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onCityError(res.errorHuman);
      } else {
        print('res.city ${jsonEncode(res.mappedResult)}');
        _view.onCitySuccess(CityModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
