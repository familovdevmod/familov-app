import 'dart:convert';
import 'package:familov/model/getCart.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetCartContract {
  void onGetCartSuccess(GetCart getCartModel);
  void onGetCartError(String error);
}

class GetCartPresenter {
  GetCartContract _view;
  URLCollection api = new URLCollection() ;
  GetCartPresenter(this._view);

  getCart() {
    api.getCart().then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onGetCartError(res.errorHuman);
      } else {
        print('res.getCartResult ${jsonEncode(res.mappedResult)}');
        _view.onGetCartSuccess(GetCart.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
