import 'dart:convert';
import 'package:familov/model/getUserTransaction.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetUserTransactionContract {
  void onTransactionSuccess(GetuserTransaction transactionModel);
  void onTransactionError(String error);
}

class UserTransactionPresenter {
  GetUserTransactionContract _view;
  URLCollection api = new URLCollection();
  UserTransactionPresenter(this._view);

  getUserTransaction(data, page, recharge) {
    api
        .getUserTransaction(data, page, recharge)
        .then((MappedNetworkServiceResponse res) async {
      print('MYTRANSACTION  ${res.toString()}');
      if (!res.success) {
        _view.onTransactionError(res.errorHuman);
      } else {
        print('res.transactionResult ${jsonEncode(res.mappedResult)}');

        _view.onTransactionSuccess(
            GetuserTransaction.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
