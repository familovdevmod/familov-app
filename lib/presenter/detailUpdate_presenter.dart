import 'dart:convert';
import 'package:familov/model/detailUpdate_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class DetailUpdateScreenContract {
  void onDetailUpdateSuccess(Detailupdatemodel detailupdatemodel);
  void onDetailUpdateError(String error);
}

class DetailUpdateScreenPresenter {
  DetailUpdateScreenContract _view;
  URLCollection api = new URLCollection();

  DetailUpdateScreenPresenter(this._view);

  detailUpdates(
    String username,
    String lastname,
    String cityId,
    String homeaddress,
    String addressline2,
    String postalcode,
    String phonecode,
    String senderphone,
    String notificationemail,
    String notificationtext,
    dynamic imageUrl
  ) {
    api
        .detailUpdate(
      username,
      lastname,
      cityId,
      homeaddress,
      addressline2,
      postalcode,
      phonecode,
      senderphone,
      notificationemail,
      notificationtext,
      imageUrl,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onDetailUpdateError(
            Detailupdatemodel.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');

        _view.onDetailUpdateSuccess(
            Detailupdatemodel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
