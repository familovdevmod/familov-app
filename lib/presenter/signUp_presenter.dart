import 'dart:convert';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/signUp_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';
import 'package:familov/widget/app_colors.dart';

abstract class SignUpScreenContract {
  void onSignUpSuccess(SignupModel signupmodel);
  void onSignUpError(String error);
}

class SignUpScreenPresenter {
  SignUpScreenContract _view;
  URLCollection api = new URLCollection();
  SignUpScreenPresenter(this._view);
  signUp(
    String email,
    String password,
    String firstname,
    String lastname,
    String phonecode,
    String phonenumber,
    String friendreferralid,
    String grecaptcharesponsetoken,
  ) {
    api
        .signup(
      email,
      password,
      firstname,
      lastname,
      phonecode,
      phonenumber,
      friendreferralid,
      grecaptcharesponsetoken,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onSignUpError(
            SignupModel.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        // BotToast.showSimpleNotification(
        //     backgroundColor: AppColors().toastsuccess,
        //     title: 'Email already registered with us  ');
        // print('res.mappedResult ${jsonEncode(res.mappedResult)[5]}');

        _view.onSignUpSuccess(
            SignupModel.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
