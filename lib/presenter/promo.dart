import 'dart:convert';
import 'package:familov/model/promo.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class StripeafterContract {
  void onStripeafter(Promo promo);
  void onStripeerror(String error);
}

class Stripeafter {
  StripeafterContract _view;
  URLCollection api = new URLCollection();
  Stripeafter(this._view);

  stripe(promo) {
    api.promo(promo).then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onStripeerror(res.errorHuman);
      } else {
        print('res.Promo ${jsonEncode(res.mappedResult)}');
        print('here i am');
        _view.onStripeafter(Promo.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
