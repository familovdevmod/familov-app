import 'dart:convert';
import 'package:familov/model/country_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CountryContract {
  void onCountrySuccess(CountryModel login);
  void onCountryError(String error);
}

class CountryPresenter {
  CountryContract _view;
  URLCollection api = new URLCollection();
  CountryPresenter(this._view);

  getCountry() {
    api.countryList().then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onCountryError(res.errorHuman);
      } else {
        print('res.country ${jsonEncode(res.mappedResult)}');

        // print('res.mappedResult ${res}');

        _view.onCountrySuccess(
            CountryModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
