import 'dart:convert';
import 'package:familov/model/mobile_invoice.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetMobileInvoiceContract {
  void onInvoiceSuccess(MobileInvoices invoicesModel);
  void onInvoiceError(String error);
}

class UserMobileInvoicePresenter {
  GetMobileInvoiceContract _view;
  URLCollection api = new URLCollection();

  UserMobileInvoicePresenter(this._view);

  getMobileInvoice(urlData) {
    print('urlData  : $urlData');
    api.mobileInvoice(urlData).then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onInvoiceError(res.errorHuman);
      } else {
        print('res.mobileInvoice ${jsonEncode(res.mappedResult)}');

        _view.onInvoiceSuccess(
            MobileInvoices.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
