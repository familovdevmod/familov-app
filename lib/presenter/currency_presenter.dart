import 'dart:convert';
import 'package:familov/model/currency_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CurrencyContract {
  void onCurrencySuccess(CurrencyModel login);
  void onCurrencyError(String error);
}

class CurrencyPresenter {
  CurrencyContract _view;
  URLCollection api = new URLCollection();

  CurrencyPresenter(this._view);

  getCurrency() {
    api.currencyList().then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onCurrencyError(res.errorHuman);
      } else {
        print('res.mappedResult ${res}');

        _view.onCurrencySuccess(CurrencyModel.fromJson(
            jsonEncode(res.mappedResult)  ));
      }
    });
  }
}
