import 'dart:convert';
import 'package:familov/model/socialsignupmodel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class SocialLoginSignUpContract {
  void onSocialLogin(SocialSignUpModel socialSignUpModel);
  void onSocialLoginerror(String error);
}

class SocialLoginSignUpPresenter {
  SocialLoginSignUpContract _view;
  URLCollection api = new URLCollection();
  SocialLoginSignUpPresenter(this._view);
  getData(
    String friend_referral_id,
    String platform,
    String username,
    String email,
    String uid,
    String photo_url,
    String phone_code,
    String phone_number,
  ) {
    api
        .socialsignup(
      friend_referral_id,
      platform,
      username,
      email,
      uid,
      photo_url,
      phone_code,
      phone_number,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onSocialLoginerror(
            SocialSignUpModel.fromJson(jsonEncode(res.mappedResult))
                .toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');

        _view.onSocialLogin(
            SocialSignUpModel.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
