import 'dart:convert';
import 'package:familov/model/language.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class LanguageContract {
  void onLanguageSuccess(Language language);
  void onLanguageError(String error);
}

class LanguagePresenter {
  LanguageContract _view;
  URLCollection api = new URLCollection();
  LanguagePresenter(this._view);
  language(lang) {
    api.languageIs(lang).then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onLanguageError(res.errorHuman);
      } else {
        print('res.language ${jsonEncode(res.mappedResult)}');
        _view.onLanguageSuccess(
            Language.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
