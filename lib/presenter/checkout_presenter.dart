import 'dart:convert';
import 'package:familov/model/checkout.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CheckoutContract {
  void onCheckoutSuccess(Checkout checkoutModel);
  void onCheckoutError(String error);
}

class CheckoutPresenter {
  CheckoutContract _view;
  URLCollection api = new URLCollection();
  CheckoutPresenter(this._view);
  checkout() {
    api
        .checkout()
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onCheckoutError(res.errorHuman);
      } else {
        print('res.checkout ${jsonEncode(res.mappedResult)}');
        _view.onCheckoutSuccess(Checkout.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
