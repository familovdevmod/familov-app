import 'dart:convert';
import 'package:familov/model/homeModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class HomeContract {
  void onHomeSuccess(HomeModel homeModel);
  void onHomeError(String error);
}

class HomePresenter {
  HomeContract _view;
  URLCollection api = new URLCollection();
  HomePresenter(this._view);
  home(data1, data2) {
    print('$data1   ,  $data2');
    api.home(data1, data2).then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onHomeError(res.errorHuman);
      } else {
        print('res.home ${jsonEncode(res.mappedResult)}');
        _view.onHomeSuccess(HomeModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
