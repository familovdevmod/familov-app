import 'dart:convert';
import 'package:familov/model/product_detail_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ProductDetailClass {
  void onproductDetailSuccess(ProductDetailModel detailModel);
  void onproductDetailError(String error);
}

class ProductDetailPresenter {
  ProductDetailClass _view;
  URLCollection api = new URLCollection();
  ProductDetailPresenter(this._view);
  productDetail(var productId) {
    api
        .productDetail('?product_id=$productId')
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onproductDetailError(res.errorHuman);
      } else {
        print('res.productDetail ${jsonEncode(res.mappedResult)}');
        _view.onproductDetailSuccess(
            ProductDetailModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
