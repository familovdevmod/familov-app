import 'dart:convert';
import 'package:familov/model/shop_invoiceModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetShopInvoiceContract {
  void onShopInvoiceSuccess(ShopInvoice shopinvoiceModel);
  void onShopInvoiceError(String error);
}

class UserShopInvoicePresenter {
  GetShopInvoiceContract _view;
  URLCollection api = new URLCollection();

  UserShopInvoicePresenter(this._view);

  getShopInvoice(urlData) {
    print('ShopurlData  : $urlData');
    api.shopInvoice(urlData).then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onShopInvoiceError(res.errorHuman);
      } else {
        print('res.shopInvoice ${jsonEncode(res.mappedResult)}');

        _view.onShopInvoiceSuccess(
            ShopInvoice.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
