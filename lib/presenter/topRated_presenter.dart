import 'dart:convert';
import 'package:familov/model/topRated_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class TopRatedContract {
  void onTopRatedSuccess(TopProduct shopModel);
  void onTopRatedShopError(String error);
}

class TopRatedPresenter {
  TopRatedContract _view;
  URLCollection api = new URLCollection();
  TopRatedPresenter(this._view);
  topRated(String data) {
    api
        .topRated('?shop_id=$data')
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onTopRatedShopError(res.errorHuman);
      } else {
        print('res.topProduct ${jsonEncode(res.mappedResult)}');
        _view.onTopRatedSuccess(TopProduct.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
