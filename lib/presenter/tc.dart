import 'dart:convert';
import 'package:familov/model/totalcustomer.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class TCContract {
  void onTCSuccess(Totalcustomer tc);
  void onTCError(String error);
}

class TCPresenter {
  TCContract _view;
  URLCollection api = new URLCollection();
  TCPresenter(this._view);
  totalCount() {
    api.totalC().then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onTCError(res.errorHuman);
      } else {
        print('res.total_Count ${jsonEncode(res.mappedResult)}');
        _view.onTCSuccess(Totalcustomer.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
