import 'dart:convert';
import 'package:familov/model/language.dart';
import 'package:familov/model/languageversion.dart';
import 'package:familov/model/pdf.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class LanguageVersionContract {
  void onLanguageVersionSuccess(Languageversion pdf);
  void onLanguageVersionError(String error);
}

class LanguageVersionPresenter {
  LanguageVersionContract _view;
  URLCollection api = new URLCollection();
  LanguageVersionPresenter(this._view);
  languageVersion() {
    api.langVersion().then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onLanguageVersionError(res.errorHuman);
      } else { 
        print('res.lanVersion ${jsonEncode(res.mappedResult)}');
        _view.onLanguageVersionSuccess(
            Languageversion.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
