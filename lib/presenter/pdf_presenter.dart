import 'dart:convert';
import 'package:familov/model/mobile_invoice.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

import '../model/pdf_model.dart';

abstract class GetPdfDataContract {
  void onPdfSuccess(PdfModel invoicesModel);
  void onPdfError(String error);
}

class PdfPresenter {
  GetPdfDataContract _view;
  URLCollection api = new URLCollection();

  PdfPresenter(this._view);

  getPdfString(orderId) {
    print('urlData pdf : $orderId');
    api.pdfUrl(orderId).then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onPdfError(res.errorHuman);
      } else {
        print('res.pdf ${jsonEncode(res.mappedResult)}');

        _view.onPdfSuccess(
            PdfModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
