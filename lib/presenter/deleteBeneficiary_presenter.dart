import 'dart:convert';
import 'package:familov/model/deleteBeneficiaryModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class DeleteBeneficiaryContract {
  void onDeleteBeneficiarySuccess(DeleteBeneficiary deleteBeneficiaryModel);
  void onDeleteBeneficiaryError(String error);
}

class DeleteBeneficiaryPresenter {
  DeleteBeneficiaryContract _view;
  URLCollection api = new URLCollection();

  DeleteBeneficiaryPresenter(this._view);

   deleteBeneficiary(data) {
    api
        .deletebeneficiary('?recipient_id=$data')
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onDeleteBeneficiaryError(res.errorHuman);
      } else {
        print('res.transactionResult ${jsonEncode(res.mappedResult)}');

        _view.onDeleteBeneficiarySuccess(
            DeleteBeneficiary.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
