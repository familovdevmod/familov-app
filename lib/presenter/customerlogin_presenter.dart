import 'dart:convert';
import 'package:familov/model/login.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class CustomerLoginScreenContract {
  void onLoginSuccess(Loginjson login);
  void onLoginError(String error);
}

class CustomerLoginScreenPresenter {
  CustomerLoginScreenContract _view;
  URLCollection api = new URLCollection();

  CustomerLoginScreenPresenter(this._view);

  doLogin(String email, String password, String token) {
    api
        .customerLogin(email, password, token)
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onLoginError(
            Loginjson.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');

        _view.onLoginSuccess(Loginjson.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
