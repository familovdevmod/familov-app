import 'dart:convert';
import 'package:familov/model/changePassword.dart';
import 'package:familov/model/signUp_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ChangePasswordScreenContract {
  void onChangePassSuccess(Test testModel);
  void onChangePassError(String error);
}

class ChangePasswordScreenPresenter {
  ChangePasswordScreenContract _view;
  URLCollection api = new URLCollection();
  ChangePasswordScreenPresenter(this._view);
  changePassword(
    String currentPassword,
    String newPassword,
   String confirmPassword
  ) {
    api
        .changePassword(
      currentPassword,
      newPassword,
      confirmPassword,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onChangePassError(
            Test.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');

        _view.onChangePassSuccess(Test.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
