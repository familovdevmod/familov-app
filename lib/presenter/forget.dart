import 'dart:convert';
import 'package:familov/model/changePassword.dart';
import 'package:familov/model/forget.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ForgetPasswordScreenContract {
  void onForgetPassSuccess(Forget forgetModel);
  void onForgetPassError(String error);
}

class ForgetPasswordScreenPresenter {
  ForgetPasswordScreenContract _view;
  URLCollection api = new URLCollection();
  ForgetPasswordScreenPresenter(this._view);
  forgetPassword(
    String email,
    String otp,
    String newpassword,
    String confirmpassword,
    String stage,
  ) {
    api
        .forgetPassword(
      email,
      otp,
      newpassword,
      confirmpassword,
      stage,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onForgetPassError(
            Test.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');
        _view
            .onForgetPassSuccess(Forget.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
