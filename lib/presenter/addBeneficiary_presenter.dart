import 'dart:convert';
import 'package:familov/model/addBeneficiary_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class AddBeneficiaryScreenContract {
  void onaddBeneficiarySuccess(AddBeneficiary addBeneficiarymodel);
  void onaddBeneficiaryError(String error);
}

class AddBeneficiaryScreenPresenter {
  AddBeneficiaryScreenContract _view;
  URLCollection api = new URLCollection();

  AddBeneficiaryScreenPresenter(this._view);

  addBeneficiary(
    String recpName,
    String phoneCode,
    String phoneNumber,
    String anotherPhoneCode,
    String anotherPhoneNumber,
  ) {
    api
        .addBeneficiary(
      recpName,
      phoneCode,
      phoneNumber,
      anotherPhoneCode,
      anotherPhoneNumber,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onaddBeneficiaryError(
            AddBeneficiary.fromJson(jsonEncode(res.mappedResult)).toString());
        print('res.AddBeneficiary ${jsonEncode(res.mappedResult)}');
      } else {
        print('res.AddBeneficiary ${jsonEncode(res.mappedResult)}');

        _view.onaddBeneficiarySuccess(
            AddBeneficiary.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
