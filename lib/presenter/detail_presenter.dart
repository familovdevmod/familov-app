import 'dart:convert';
import 'package:familov/model/detail_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class DetailContract {
  void onDetailSuccess(DetailModel detailModel);
  void onDetailError(String error);
}

class DetailPresenter {
  DetailContract _view;
  URLCollection api = new URLCollection();

  DetailPresenter(this._view);

  getDetail() {
    api.detailList().then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onDetailError(res.errorHuman);
      } else {
        print('res.detailResult ${jsonEncode(res.mappedResult)}');
        _view.onDetailSuccess(DetailModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
