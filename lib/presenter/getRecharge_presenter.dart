import 'dart:convert';
import 'package:familov/model/getrechargeitem.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetRechargeItemContract {
  void onGetRechargeItemSuccess(Getrechargeitem recharge);
  void onGetRechargeItemError(String error);
}

class GetRechargeItemPresenter {
  GetRechargeItemContract _view;
  URLCollection api = new URLCollection();
  GetRechargeItemPresenter(this._view);

  getRechargeItem(mobile, code, currentpage) {
    api
        .getRechargeItem(mobile, code, currentpage)
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onGetRechargeItemError(res.errorHuman);
      } else {
        print('res.getRechargeItem ${jsonEncode(res.mappedResult)}');
        _view.onGetRechargeItemSuccess(
            Getrechargeitem.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
