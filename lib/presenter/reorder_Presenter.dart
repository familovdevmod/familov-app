import 'dart:convert';
import 'package:familov/model/reorder_model.dart';
import 'package:familov/model/topRated_Model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class ReorderPresenterContract {
  void onReoderSuccess(ReorderModel reoder_model);
  void onReoderFail(String error);
}

class ReorderPresenter {
  ReorderPresenterContract _view;
  URLCollection api = new URLCollection();

  ReorderPresenter(this._view);

  Orderagain(
    orderid,
  ) {
    // print('all 5 data is  :  A $data1, B  $data2,  C    $data3 ,  D    $data4,   E   $data5,');
    //  print('The value is :  $a');
    api
        .reorder(
      orderid,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onReoderFail(res.errorHuman);
      } else {
        print('res.product ${jsonEncode(res.mappedResult)}');
        _view.onReoderSuccess(
            ReorderModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
