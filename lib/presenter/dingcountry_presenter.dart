import 'dart:convert';
import 'package:familov/model/dingcountry.dart';
import 'package:familov/model/product_detail_model.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class DingCountryClass {
  void onDingCountrySuccess(Dingcountry dingModel);
  void onDingCountryError(String error);
}

class DingCountryPresenter {
  DingCountryClass _view;
  URLCollection api = new URLCollection();
  DingCountryPresenter(this._view);
  dingCountry() {
    api
        .dingCountry()
        .then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onDingCountryError(res.errorHuman);
      } else {
        print('res.DingCountry ${jsonEncode(res.mappedResult)}');
        _view.onDingCountrySuccess(
            Dingcountry.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
