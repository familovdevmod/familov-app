import 'dart:convert';
import 'package:familov/model/beneficiaryModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class GetBeneficiaryContract {
  void onBeneficiarySuccess(BeneficiaryModel beneficiaryModel);
  void onBeneficiaryError(String error);
}

class UserBeneficiaryPresenter {
  GetBeneficiaryContract _view;
  URLCollection api = new URLCollection();

  UserBeneficiaryPresenter(this._view);

  getBeneficiary() {
    api.beneficiary().then((MappedNetworkServiceResponse res) async {
      print(res.toString());
      if (!res.success) {
        _view.onBeneficiaryError(res.errorHuman);
      } else {
        print('res.Beneficiary ${jsonEncode(res.mappedResult)}');

        _view.onBeneficiarySuccess(
            BeneficiaryModel.fromJson(jsonEncode(res.mappedResult)));
      }
    });
  }
}
