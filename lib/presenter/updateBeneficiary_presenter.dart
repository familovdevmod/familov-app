import 'dart:convert';
import 'package:familov/model/updateBeneficiaryModel.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class UpdateBeneficiaryScreenContract {
  void onBeneficiaryUpdateSuccess(BeneficiaryUpdate beneficiaryupdatemodel);
  void onBeneficiaryUpdateError(String error);
}

class BeneficiaryUpdateScreenPresenter {
  UpdateBeneficiaryScreenContract _view;
  URLCollection api = new URLCollection();

  BeneficiaryUpdateScreenPresenter(this._view);

  beneficiaryUpdates(
    String recipientId,
    String recpName,
    String phoneCode,
    String phoneNumber,
    String anotherPhoneCode,
    String anotherPhoneNumber,
  ) {
    api
        .updateBeneficiary(
      recipientId,
      recpName,
      phoneCode,
      phoneNumber,
      anotherPhoneCode,
      anotherPhoneNumber,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        _view.onBeneficiaryUpdateError(
            BeneficiaryUpdate.fromJson(jsonEncode(res.mappedResult))
                .toString());
      } else {
        print('res.mappedResult ${jsonEncode(res.mappedResult)}');

        _view.onBeneficiaryUpdateSuccess(
            BeneficiaryUpdate.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
