import 'dart:convert';
import 'package:familov/model/stripecheckout.dart';
import 'package:familov/network/network_service_response.dart';
import 'package:familov/utils/urlcollection.dart';

abstract class StripeCheckoutContract {
  void onStripeCheckoutSuccess(Stripecheckout stripeCheckout);
  void onStripeCheckoutError(String error);
}

class StripeCheckoutPresenter {
  StripeCheckoutContract _view;
  URLCollection api = new URLCollection();
  StripeCheckoutPresenter(this._view);
  stripeCheckout(
    String deliverymode,
    String homedeliveryaddress,
    String pmslug,
    String recipientid,
    String recipientname,
    String phonenumber,
    String phonenumber2,
    String currency,
    String promocode,
    String greetingmsg,
    String parentorderid,
    String isrecharge,
  ) {
    api
        .stripeCheckout(
      deliverymode,
      homedeliveryaddress,
      pmslug,
      recipientid,
      recipientname,
      phonenumber,
      phonenumber2,
      currency,
      promocode,
      greetingmsg,
      parentorderid,
      isrecharge,
    )
        .then((MappedNetworkServiceResponse res) async {
      if (!res.success) {
        print('pre8 checkout');
        _view.onStripeCheckoutError(
            Stripecheckout.fromJson(jsonEncode(res.mappedResult)).toString());
      } else {
        print('res.stripe ${jsonEncode(res.mappedResult)}');
        _view.onStripeCheckoutSuccess(
            Stripecheckout.fromJson(jsonEncode(res.mappedResult)));
        print('done sucess');
      }
    });
  }
}
