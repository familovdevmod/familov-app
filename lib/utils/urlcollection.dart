import 'package:familov/network/network_service_response.dart';
import 'package:familov/network/network_util.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class URLCollection {
  NetworkUtil _netUtil = new NetworkUtil();
  // sharedP0() async {
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   // preferences.setString('currency', _currencySelectedController);
  //   checkCurrency = preferences.getString('currency');
  //   print('${checkCurrency} currrency is here');
  //   // rate1 = preferences.getString('rate');

  //   if (checkCurrency == null) {
  //     preferences.setString('currency', '\$ USD');
  //     preferences.setString('rate', '1');

  //     checkCurrency = preferences.getString('currency');
  //   }
  // }

  // ignore: non_constant_identifier_names
  String Tokensave = '';
  var checkCurrency;
  String urlAdd = '';

  static const String baseUrl = "http://apptest.familov.com/api_v2/";
  static const String login_Url = baseUrl + "login";
  static const String currencyList_Url = baseUrl + "currency-list";
  static const String countryDetail_Url = baseUrl + "get-user-details";
  static const String detailUpdate_Url = baseUrl + "update-user-details";
  static const String getUserTransaction_Url = baseUrl + "get-user-transaction";
  static const String getMobileInvoice_Url = baseUrl + "get-user-transaction";
  static const String getShopInvoice_Url = baseUrl + "get-user-transaction";
  static const String getbeneficiary_Url = baseUrl + "get-user-recipient";
  static const String addbeneficiary_Url = baseUrl + "add-user-recipient";
  static const String updatebeneficiary_Url = baseUrl + "update-user-recipient";
  static const String getdeletebeneficiary_Url =
      baseUrl + "delete-user-recipient";
  static const String city_Url = baseUrl + "cities";
  static const String countryList_Url = baseUrl + "countries";
  static const String shopList_Url = baseUrl + "shops";
  static const String categoryList_Url = baseUrl + "main-category";
  static const String topRated_Url = baseUrl + "top-marked-product";
  static const String shopDetail_Url = baseUrl + "shops-details";
  static const String getProduct_Url = baseUrl + "product-list";
  static const String ProductDetail_Url = baseUrl + "product-details";
  static const String signup_Url = baseUrl + "sign-up";
  static const String getCart_Url = baseUrl + "get-cart";
  static const String setCart_Url = baseUrl + "set-cart";
  static const String reorder_Url = baseUrl + "reorder";
  static const String deleteCart_Url = baseUrl + "remove-cart";
  static const String dingCountries_Url = baseUrl + "ding-countries";
  static const String getRechargeItem_Url = baseUrl + "get-recharge-item";
  static const String recommendation_Url =
      baseUrl + "user-product-recommendation";
  static const String stripesuccess =
      baseUrl + "validate-promo-code"; //validate-promo-code?promo_code=APPTEST
  static const String changepassword_Url = baseUrl + "change-password";
  static const String forgetpassword_Url = baseUrl + "forget-password";
  static const String checkout_Url = baseUrl + "get-checkout-options";
  static const String language_Url = baseUrl + "get-all-language-label";
  static const String summary_url = baseUrl + "get-checkout-summary";
  static const String clear_url = baseUrl + "make-cart-empty";
  static const String pdf_url = baseUrl + "user-order-bill";
  static const String pdf_url_invoice = baseUrl + "user-order-bill-details";
  static const String total_count_url = baseUrl + "total-customer";
  static const String langVersion_url = baseUrl + "language-version";
  static const String stripeCheckout_url = baseUrl + "checkout";
  static const String socialsignup_url = baseUrl + "social-signup";

  Future<MappedNetworkServiceResponse> customerLogin(
      String username, String password, String token) async {
    var request = http.MultipartRequest('POST', Uri.parse('$login_Url'));
    request.fields
        .addAll({'email': username, 'password': password, 'Language': 'fr'});
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> currencyList() async {
    var request = http.Request(
      'GET',
      Uri.parse('$currencyList_Url'),
    );
    var headers = {'Language': 'fr'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> detailList() async {
    var request = http.Request(
      'GET',
      Uri.parse('$countryDetail_Url'),
    );

    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');

    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> detailUpdate(
      String username,
      String lastname,
      String cityId,
      String homeaddress,
      String addressline2,
      String postalcode,
      String phonecode,
      String senderphone,
      String notificationemail,
      String notificationtext,
      dynamic imageUrl) async {
    var request = http.MultipartRequest('POST', Uri.parse('$detailUpdate_Url'));
    print('Test Image Url  : $imageUrl');
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    if (imageUrl != "") {
      print('imageurl val=$imageUrl=');
      request.files
          .add(await http.MultipartFile.fromPath('profile_photo', imageUrl));
    }

    print('notificationemail  :  $notificationemail');
    print('notificationtext  :  $notificationtext');
    print('phone no : $senderphone');
    request.fields.addAll({
      'username': username,
      'lastname': lastname,
      'city_id': cityId,
      'home_address': homeaddress,
      'address_line2': addressline2,
      'postal_code': postalcode,
      'phone_code': phonecode,
      'sender_phone': senderphone,
      'notification_email': notificationemail,
      'notification_text': notificationtext,
    });
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> getUserTransaction(
      data, page, recharge) async {
    print('dataCheck  $data  and page  $page');
    var newUrl = getUserTransaction_Url +
        '?app_order_status=$data' +
        '&page=$page' +
        '&only_recharge=$recharge';
    var request = http.Request(
      'GET',
      Uri.parse('$newUrl'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('urlsss  :  $newUrl');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> mobileInvoice(String urlData) async {
    var request = http.Request(
      'GET',
      Uri.parse('${getMobileInvoice_Url + urlData}'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('urlsss  :  ${getMobileInvoice_Url + urlData}');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      //  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> shopInvoice(String urlData) async {
    var request = http.Request(
      'GET',
      Uri.parse('${getShopInvoice_Url + urlData}'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('urlsss  :  ${getShopInvoice_Url + urlData}');

    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      //  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> beneficiary() async {
    var request = http.Request(
      'GET',
      Uri.parse('$getbeneficiary_Url'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('url is  :  $getbeneficiary_Url');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> addBeneficiary(
    String recpName,
    String phoneCode,
    String phoneNumber,
    String anotherPhoneCode,
    String anotherPhoneNumber,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    var request =
        http.MultipartRequest('POST', Uri.parse('$addbeneficiary_Url'));
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      //'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };

    print(
        'Test demo:=== : $recpName phoneCode $phoneCode phoneNumber $phoneNumber anotherPhoneCode $anotherPhoneCode anotherPhoneNumber $anotherPhoneNumber');

    request.headers.addAll(headers);
    request.fields.addAll({
      'recp_name': recpName,
      'phone_code': phoneCode,
      'phone_number': phoneNumber,
      'another_phone_code': anotherPhoneCode,
      'another_phone_number': anotherPhoneNumber,
    });
    print('request data is  : ${request.fields}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> updateBeneficiary(
    String recipientId,
    String recpName,
    String phoneCode,
    String phoneNumber,
    String anotherPhoneCode,
    String anotherPhoneNumber,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    var request =
        http.MultipartRequest('POST', Uri.parse('$updatebeneficiary_Url'));
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      //'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };

    print(
        'Test demo:=== :$recipientId  id   $recpName phoneCode $phoneCode phoneNumber $phoneNumber anotherPhoneCode $anotherPhoneCode anotherPhoneNumber $anotherPhoneNumber');

    request.headers.addAll(headers);
    request.fields.addAll({
      'recipient_id': recipientId,
      'recp_name': recpName,
      'phone_code': phoneCode,
      'phone_number': phoneNumber,
      'another_phone_code': anotherPhoneCode,
      'another_phone_number': anotherPhoneNumber,
    });
    print('request data is  : ${request.fields}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      print('return res');
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> deletebeneficiary(data) async {
    var request = http.Request(
      'GET',
      Uri.parse('${getdeletebeneficiary_Url + data}'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('url is  :  $getbeneficiary_Url');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
      // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI5NzgsImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsImlhdCI6MTYzNzkwNjgzNSwiZXhwIjoxNjM4MDc5NjM1fQ.umcqSRQdtXfqZQrtM37CL1DXR5z_1xexyYhwQG6gWnA',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> city(data) async {
    var request = http.Request(
      'GET',
      Uri.parse('${city_Url + data}'),
    );
    final prefs = await SharedPreferences.getInstance();
    // Tokensave = prefs.getString('value_key')!;
    // print('Token is coming $Tokensave');
    // print('url is  :  $city_Url');
    var headers = {
      'Language': 'fr',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> countryList() async {
    var request = http.Request(
      'GET',
      Uri.parse('$countryList_Url'),
    );
    var headers = {'Language': 'fr'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> shopList(data1, data2) async {
    var request = http.Request(
      'GET',
      Uri.parse(
          '${shopList_Url + '?city_id=$data1' + '&main_category_id=$data2'}'),
    );
    var headers = {'Language': 'fr'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> categoryList(data1, data2) async {
    var request = http.Request(
      'GET',
      Uri.parse(
          '${categoryList_Url + '?country_id=$data1' + '&city_id=$data2'}'),
    );
    final prefs = await SharedPreferences.getInstance();
    // Tokensave = prefs.getString('value_key')!;
    var lang = prefs.getString('languages');
    print(request);
    var headers = {
      'Language': '$lang',
      // 'Accesstoken': Tokensave,
    };

    // var headers = {'Language': 'fr'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> topRated(data) async {
    var request = http.Request(
      'GET',
      Uri.parse('${topRated_Url + data}'),
    );
    var headers = {'Language': 'fr'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> home(data1, data2) async {
    var request = http.Request(
      'GET',
      Uri.parse(
          '${shopDetail_Url + '?shop_id=$data1' + '&main_category_id=$data2'}'),
    );
    print(request);
    var headers = {'Language': 'en'};
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> product(
    data1,
    data2,
    data3,
    data4,
    data5,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    checkCurrency = preferences.getString('currency');
    // // print('${checkCurrency} currrency is here');
    // print(
    //     'values shop_id ${data1} category_id=$data2 search_keyword=$data3 &sort_by=$data4 &page=$data5');
    var newUrl = getProduct_Url +
            '?shop_id=$data1' +
            '&category_id=$data2' +
            '&search_keyword=$data3' +
            '&sort_by=$data4' +
            '&page=${data5}'
        //  '&limit=$data5' +
        // '&offset=$data6'
        ;
    // print('${newUrl}newUrl=====');

    var request = http.Request(
      'GET',
      Uri.parse('$newUrl'),
    );

    var headers = {
      'Language': 'fr',
      'Currency': '${checkCurrency.toString().substring(2)}'
    };
    // print('currrency is here 1 $headers');
    request.headers.addAll(headers);

    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

//////

  Future<MappedNetworkServiceResponse> reorder(
    orderid,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    Tokensave = preferences.getString('value_key')!;
    checkCurrency = preferences.getString('currency');
    // // print('${checkCurrency} currrency is here');
    // print(
    //     'values shop_id ${data1} category_id=$data2 search_keyword=$data3 &sort_by=$data4 &page=$data5');
    var newUrl = reorder_Url + '?order_id=$orderid';

    var request = http.Request(
      'GET',
      Uri.parse('$newUrl'),
    );

    var headers = {
      'Language': 'english',
      'Currency': '${checkCurrency.toString().substring(2)}',
      'Accesstoken': Tokensave,
    };
    // print('currrency is here 1 $headers');
    request.headers.addAll(headers);

    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

/////

//{{BaseURL}}/api_v2/product-details?product_id=22034
  Future<MappedNetworkServiceResponse> productDetail(productId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    checkCurrency = preferences.getString('currency');
    var request = http.Request(
      'GET',
      Uri.parse('${ProductDetail_Url + '$productId'}'),
    );
    print(request);
    var headers = {
      'Language': 'fr',
      'Currency': '${checkCurrency.toString().substring(2)}'
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> signup(
    String email,
    String password,
    String firstname,
    String lastname,
    String phonecode,
    String phonenumber,
    String friendreferralid,
    String grecaptcharesponsetoken,
  ) async {
    var request = http.MultipartRequest('POST', Uri.parse('$signup_Url'));
    var headers = {
      'Language': 'english',
      'Accesstoken': Tokensave,
    };

    request.headers.addAll(headers);
    request.fields.addAll({
      'email': email,
      'password': password,
      'first_name': firstname,
      'last_name': lastname,
      'phone_code': phonecode,
      'phone_number': phonenumber,
      'friend_referral_id': friendreferralid,
      'g_recaptcha_response_token': grecaptcharesponsetoken
    });
    print('request data is  : ${request.fields}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      //  print('return res');
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> getCart() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    checkCurrency = preferences.getString('currency');
    print('${checkCurrency} currrency is here');
    var request = http.Request(
      'GET',
      Uri.parse('$getCart_Url'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',
      'Currency': '${checkCurrency.toString().substring(2)}',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> setCart(
      String shopId,
      String productId,
      String quantity,
      String countryCode,
      String phoneNumber,
      String skuCode,
      String isRecharge) async {
    var request = http.Request(
      'GET',
      Uri.parse('$setCart_Url' +
          '?shop_id=$shopId&product_id=$productId&quantity=$quantity&country_code=$countryCode&phone_number=$phoneNumber&sku_code=$skuCode&is_recharge=$isRecharge'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

//remove-cart?product_id=9344
  Future<MappedNetworkServiceResponse> deleteCart(
    String productId,
  ) async {
    var request = http.Request(
      'GET',
      Uri.parse('$deleteCart_Url' + '?product_id=$productId'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      // 'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> dingCountry() async {
    var request = http.Request(
      'GET',
      Uri.parse('$dingCountries_Url'),
    );
    // final prefs = await SharedPreferences.getInstance();
    // Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',

      //   'Accesstoken': Tokensave,
    };
    // print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> getRechargeItem(
      mobile, code, currentpage) async {
    var request = http.Request(
      'GET',
      Uri.parse('$getRechargeItem_Url' +
          '?number=$mobile' +
          '&country_code=$code' +
          '&page=$currentpage'),
    );
    // final prefs = await SharedPreferences.getInstance();
    // Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',
      'Currency': 'USD',
      // 'Accesstoken': Tokensave,
    };
    // print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> recommendation() async {
    var request = http.Request(
      'GET',
      Uri.parse('$recommendation_Url'),
    );
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('prefs $prefs');
    Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> promo(promo) async {
    var request = http.Request(
      'GET',
      Uri.parse('$stripesuccess' + '?promo_code=$promo'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('stripe checkout');
    print(request);
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> changePassword(
    String currentPassword,
    String newPassword,
    String confirmPassword,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    var request =
        http.MultipartRequest('POST', Uri.parse('$changepassword_Url'));
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print(
        'currentPassword:=== : $currentPassword newPassword is =   $newPassword confirmPassword is =  $confirmPassword ');
    request.headers.addAll(headers);
    request.fields.addAll({
      'current_password': currentPassword,
      'new_password': newPassword,
      'confirm_password': confirmPassword,
    });
    print('request data is  : ${request.fields}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> forgetPassword(
    String email,
    String otp,
    String newpassword,
    String confirmpassword,
    String stage,
  ) async {
    var request =
        http.MultipartRequest('POST', Uri.parse('$forgetpassword_Url'));
    print(
        'email:=== : $email token is =   $otp  newpassword: $newpassword confirmPassword : $confirmpassword   stage :  $stage');
    request.fields.addAll({
      'email': email,
      'otp': otp,
      'new_password': newpassword,
      'confirm_password': confirmpassword,
      'stage': stage
    });
    print(
        'emailis:=== : $email otp is =   $otp  newpassword: $newpassword confirmPassword : $confirmpassword   stage :  $stage');
    print('request forget  : ${request.fields}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      print('-----------${res.mappedResult["status"]}');
      print(request.fields);

      return res;
    });
  }

  Future<MappedNetworkServiceResponse> checkout() async {
    var request = http.Request(
      'GET',
      Uri.parse('$checkout_Url'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print(request);
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> languageIs(lang) async {
    var request = http.Request(
      'GET',
      Uri.parse('$language_Url'),
    );
    print('langggggg $request  lang is : $lang');
    var headers = {
      'Language': '$lang',
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> summary(deilevey, promo) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    checkCurrency = preferences.getString('currency');
    print('${checkCurrency} currrency is here');
    var request = http.Request(
      'GET',
      Uri.parse(
          '$summary_url' + '?delivery_mode=$deilevey' + '&promo_code=$promo'),
      // Uri.parse('$summary_url'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    var lang = prefs.getString('languages');
    print(request);
    var headers = {
      'Currency': '${checkCurrency.toString().substring(2)}',
      'Language': '$lang',
      'Accesstoken': Tokensave,
    };

    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      print("$MappedNetworkServiceResponse--checkout summary");
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> clearCart() async {
    var request = http.Request(
      'GET',
      Uri.parse('$clear_url'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    // var lang = prefs.getString('languages');
    print(request);
    var headers = {
      //  'Language': '$lang',
      'Accesstoken': Tokensave,
    };
    print('Saved token is :  $Tokensave');
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  // Future<MappedNetworkServiceResponse> pdf(id) async {
  //   var request = http.Request(
  //     'GET',
  //     Uri.parse('$pdf_url' + '?order_id=$id'),
  //   );
  //   final prefs = await SharedPreferences.getInstance();
  //   Tokensave = prefs.getString('value_key')!;
  //   // var lang = prefs.getString('languages');
  //   print(request);
  //   var headers = {
  //     //  'Language': '$lang',
  //     'Accesstoken': Tokensave,
  //   };
  //   print('Saved token is :  $Tokensave');
  //   request.headers.addAll(headers);
  //   return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
  //     return res;
  //   });
  // }

  Future<MappedNetworkServiceResponse> totalC() async {
    var request = http.Request(
      'GET',
      Uri.parse('$total_count_url'),
    );
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> langVersion() async {
    var request = http.Request(
      'GET',
      Uri.parse('$langVersion_url'),
    );
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> pdfUrl(String oderId) async {
    var request = http.Request(
      'GET',
      Uri.parse('${pdf_url + oderId}'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('urlsss180  :  ${getMobileInvoice_Url + oderId}');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> pdfUrlInvoice(String oderId) async {
    var request = http.Request(
      'GET',
      Uri.parse('${pdf_url_invoice + oderId}'),
    );
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    print('urlsss180  :  ${getMobileInvoice_Url + oderId}');
    var headers = {
      'Language': 'fr',
      'Accesstoken': Tokensave,
    };
    request.headers.addAll(headers);
    return _netUtil.get(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> stripeCheckout(
    String deliverymode,
    String homedeliveryaddress,
    String pmslug,
    String recipientid,
    String recipientname,
    String phonenumber,
    String phonenumber2,
    String currency,
    String promocode,
    String greetingmsg,
    String parentorderid,
    String isrecharge,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    Tokensave = prefs.getString('value_key')!;
    print('Token is coming $Tokensave');
    var request =
        http.MultipartRequest('POST', Uri.parse('$stripeCheckout_url'));
    var headers = {
      'Accesstoken': Tokensave,
    };
    print(
        '$deliverymode $homedeliveryaddress $pmslug $recipientid  $recipientname  $phonenumber  $phonenumber2  $currency $promocode  $greetingmsg  $parentorderid');
    request.headers.addAll(headers);
    request.fields.addAll({
      'delivery_mode': deliverymode,
      'home_delivery_address': homedeliveryaddress,
      'pm_slug': pmslug,
      'recipient_id': recipientid,
      'recipient_name': recipientname,
      'phone_number': phonenumber,
      'phone_number2': phonenumber2,
      'currency': currency,
      'promo_code': promocode,
      'greeting_msg': greetingmsg,
      'parent_order_id': parentorderid,
      'is_recharge': isrecharge,
    });
    print('request checkout data  : ${request.fields}');
    print('request checkout data  : ${request.url}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }

  Future<MappedNetworkServiceResponse> socialsignup(
    String friend_referral_id,
    String platform,
    String username,
    String email,
    String uid,
    String photo_url,
    String phone_code,
    String phone_number,
  ) async {
    print('Token is coming $Tokensave');
    var request = http.MultipartRequest('POST', Uri.parse('$socialsignup_url'));
    var headers = {
      'Language': 'english',
    };
    request.headers.addAll(headers);
    request.fields.addAll({
      'friend_referral_id': friend_referral_id,
      'platform': platform,
      'username': username,
      'email': email,
      'uid': uid,
      'photo_url': photo_url,
      'phone_code': phone_code,
      'phone_number': phone_number
    });
    print('request checkout data  : ${request.fields}');
    print('request checkout data  : ${request.url}');
    return _netUtil.postNew(request).then((MappedNetworkServiceResponse res) {
      return res;
    });
  }
}
