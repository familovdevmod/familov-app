import 'package:bot_toast/bot_toast.dart';
import 'package:familov/screen/home.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FailScreen extends StatefulWidget {
  const FailScreen(String value, bool bool, {Key? key}) : super(key: key);

  @override
  State<FailScreen> createState() => _FailScreenState();
}

var name;

class _FailScreenState extends State<FailScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    shared();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * .9,
          child: Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xffFF3B5E), shape: BoxShape.circle),
                  height: 100,
                  width: 100,
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 70,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Oops !! ${name}',
                  style: TextStyle(
                    color: Color(0xff6020BD),
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget arcu pellentesque hendrerit auctor',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: 22),
                // Text(
                //   'Thank You !',
                //   style: TextStyle(
                //     color: Color(0xff4D4D4D),
                //     fontFamily: 'Gilroy',
                //     fontWeight: FontWeight.w600,
                //     fontSize: 20,
                //   ),
                // ),
                // SizedBox(height: 22),
                Text(
                  'Lorem ipsum dolor sit amet',
                  style: TextStyle(
                    color: Color(0xff4D4D4D),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 22,
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width - 15,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8),
                      ),
                      color: Color.fromRGBO(0, 219, 167, 1),
                    ),
                    child: Align(
                        alignment: Alignment.center,
                        child: InkWell(
                          onTap: () {
                            BotToast.showSimpleNotification(
                                onTap: () {},
                                backgroundColor: Color(0xffFF3B5E),
                                title: "It'll be there soon.");
                          },
                          child: Text(
                            'Retry',
                            style: TextStyle(
                              color: Colors.white.withOpacity(.95),
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        )),
                  ),
                ),
                InkWell(
                  onTap: () {
                    // Navigator.pop(context,
                    //     MaterialPageRoute(builder: (context) => Home()));
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => HomeScreen(0,'')),
                        (Route<dynamic> route) => false);
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width - 15,
                    height: 48,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1.4,
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8),
                      ),
                    ),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Back to Home',
                          style: TextStyle(
                            color: AppColors().toastsuccess,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(color: AppColors().background),
        ),
      ),
    );
  }

  void shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = preferences.getString('nameis');
    setState(() {});
  }
}
