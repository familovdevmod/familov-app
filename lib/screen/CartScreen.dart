import 'dart:convert';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/model/topRated_Model.dart';
import 'package:familov/presenter/cartcontroller.dart';
import 'package:familov/presenter/clearCart+presenter.dart';
import 'package:familov/presenter/product_presenter.dart';
import 'package:familov/screen/loginScreen.dart';
import 'package:familov/screen/transactionCheckout.dart';
import 'package:familov/screen/transactionDetailScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/deleteCart.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/place.dart';
import 'package:familov/model/product_detail_model.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/model/summmaryy.dart';
import 'package:familov/presenter/deleteCart_presenter.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/presenter/productDetail.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:familov/presenter/summary_presenter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen>
    implements
        GetProductContract,
        ClearCartContract,
        ProductDetailClass,
        GetCartContract,
        SetCartContract,
        DeleteCartContract,
        SummaryContract {
  List dataList = [];
  List<Data9> dataList2 = [];
  int? length = 0;
  var stuck = true;
  var icheck = 0;
  var quan1;
  var ind = 0;
  double? calculation;
  List ids = [];
  List prices = [];
  List imagesIs = [];
  List productIds = [];
  int? selectedIndex1 = 0;
  String? selectValue;

  Data12? list1 = Data12();
  CartController cartController = Get.put(CartController());

  List<Place> items = [];
  var price;

  ProductDetailPresenter? presenter;
  var rechargeshopid;
  GetCartPresenter? presenter1;
  UserProductPresenter? _presenters;
  SetCartPresenter? presenter2;
  ClearCartPresenter? cartemptypresenter;

  DeleteCartPresenter? presenter3;

  SummaryPresenter? presenter4;

  _CartScreenState() {
    presenter = new ProductDetailPresenter(this);
    presenter1 = new GetCartPresenter(this);
    presenter2 = new SetCartPresenter(this);
    presenter3 = new DeleteCartPresenter(this);
    presenter4 = new SummaryPresenter(this);
    cartemptypresenter = new ClearCartPresenter(this);
  }

  bool loadcheck = false;
  // bool loadcheck1 = false;
  // bool load3 = false;

  var chk = false;
  var deletedatabase;
  var updatedatabase;
  String? imageRecharge,
      skuCode,
      data1,
      data2,
      mobile,
      code,
      maximumSendCurrencyIso,
      currencySymbol,
      ratex,
      pricex;

  @override
  void initState() {
    shared();
    database();
    print('key $valueKey');

    Future.delayed(Duration(milliseconds: 1500), () {
      presenter1!.getCart();
      presenter4!.summary('', '');
    });
    super.initState();
  }

  var summarycheck = false;
  var checkCurrency;
  var rate;
  var valueKey;
  var len;
  shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var checkCurrencypre =
        preferences.getString('currency').toString().substring(0, 1);
    checkCurrency = checkCurrencypre == 'c'
        ? preferences.getString('currency').toString().substring(0, 2)
        : preferences.getString('currency').toString().substring(0, 1);
    rate = preferences.getString('rate');
    valueKey = preferences.getString('value_key');
    print('key1 $valueKey');

    if (valueKey != null) {
      presenter4!.summary('', '');

      presenter1!.getCart();
    }
    setState(() {
      if (valueKey == null) {
        loadcheck = true;
      }
    });
    // print('currency is $checkCurrency and rate is :  $rate ');
  }

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;
    cartController.getcount.value=length!;

    items = dataList
        .map(
          (item) => Place(
              id: item['id'],
              quantity: item['quantity'],
              shopId: item['shopId']),
        )
        .toList();
    setState(() {
      // print('shopIdshopId is : ${dataList[0]['shopId'].toString()}');
      // print('id is : ${dataList[0]['id'].toString()}');
      // print('quantity is : ${dataList[0]['quantity'].toString()}');
    });
  }

  deletesDatabase(String idToDelete) async {
    print('del done');
    deletedatabase = await DBHelper.delete(
      int.parse('$idToDelete'),
    );
    length = dataList.length;
    print('length');
    setState(() {});
  }

  var totalCost;

  apicall(int index) {
    ind = index;
    for (int i = 0; i < dataList.length; i++) {
      icheck = i;
      presenter!.productDetail('${dataList[i]['id']}');
    }
    stuck = false;
  }

  final List<String> countList = <String>[
    ("1"),
    ('2'),
    ('3'),
    ('4'),
    ('5'),
    ("6"),
    ('7'),
    ('8'),
    ('9'),
    ('10'),
    ("11"),
    ('12'),
    ('13'),
    ('14'),
    ('15'),
    ("16"),
    ('17'),
    ('18'),
    ('19'),
    ('20'),
    ("21"),
    ('22'),
    ('23'),
    ('24'),
    ('25'),
    ("26"),
    ('27'),
    ('28'),
    ('29'),
    ('30'),
    ("31"),
    ('32'),
    ('33'),
    ('34'),
    ('35'),
    ("36"),
    ('37'),
    ('38'),
    ('39'),
    ('40'),
    ("41"),
    ('42'),
    ('43'),
    ('44'),
    ('45'),
    ("46"),
    ('47'),
    ('48'),
    ('49'),
    ('50')
  ];
  var load = false;

  updateDatabase(
    String idToUpdate,
    quan,
  ) async {
    updatedatabase = await DBHelper.updateData(
      {
        'id': idToUpdate,
        'quantity': quan,
      },
      int.parse('$idToUpdate'),
    );
  }

  refreshPage() {
    dataList = [];
    dataList2 = [];
    length = 0;
    stuck = true;
    icheck = 0;
    quan1 = null;
    ind = 0;
    calculation = 0;
    ids = [];
    prices = [];
    imagesIs = [];
    productIds = [];
    selectedIndex1 = 0;
    selectValue = null;
    list1 = Data12();
    items = [];
    price = null;
    loadcheck = false;
    chk = false;
    deletedatabase = false;
    updatedatabase = false;
    summarycheck = false;
    checkCurrency = null;
    rate = null;
    valueKey = null;
    len = null;
    totalCost = null;
    load = false;

    shared();
    database();
  }

  @override
  Widget build(BuildContext context) {
    // ScreenUtil.init(
    //     BoxConstraints(
    //         maxWidth: MediaQuery.of(context).size.width,
    //         maxHeight: MediaQuery.of(context).size.height),
    //     designSize: Size(400, 650),
    //     orientation: Orientation.portrait);
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            '${AppLocalizations.of(context)!.cartapp}',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 25,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        backgroundColor: Colors.white,
        body: (loadcheck == true)
            ? length! > 0
                ? Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            ListView.builder(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: valueKey == null
                                    ? length
                                    : dataList2.length,
                                itemBuilder: (context, index) {
                                  stuck == true
                                      ? apicall(index)
                                      : CircularProgressIndicator(
                                          color: AppColors().appaccent,
                                        );
                                  return Visibility(
                                    visible: calculation == null ? false : true,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 14, right: 14, bottom: 14),
                                      child: Card(
                                        elevation: 4,
                                        shadowColor:
                                            Color.fromARGB(153, 245, 245, 245),
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                children: [
                                                  valueKey == null
                                                      ? Container(
                                                          width: 100,
                                                          height: 100,
                                                          alignment:
                                                              Alignment.topLeft,
                                                          child:
                                                              imagesIs.length ==
                                                                      0
                                                                  ? Image.asset(
                                                                      'assets/images/fami.png',
                                                                      scale: 1,
                                                                      alignment:
                                                                          Alignment
                                                                              .center,
                                                                      errorBuilder: (context,
                                                                          error,
                                                                          stackTrace) {
                                                                        return Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 10),
                                                                          alignment:
                                                                              Alignment.center,
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/images/fami.png',
                                                                            scale:
                                                                                1,
                                                                            alignment:
                                                                                Alignment.center,
                                                                          ),
                                                                        );
                                                                      },
                                                                    )
                                                                  : Image
                                                                      .network(
                                                                      '${imagesIs[index]}',
                                                                      errorBuilder: (context,
                                                                          error,
                                                                          stackTrace) {
                                                                        return Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 10),
                                                                          alignment:
                                                                              Alignment.center,
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/images/fami.png',
                                                                            scale:
                                                                                1,
                                                                            alignment:
                                                                                Alignment.center,
                                                                          ),
                                                                        );
                                                                      },
                                                                    ))
                                                      : Container(
                                                          width: 100,
                                                          height: 100,
                                                          alignment:
                                                              Alignment.topLeft,
                                                          child: dataList2
                                                                      .length ==
                                                                  0
                                                              ? Image.asset(
                                                                  'assets/images/fami.png',
                                                                  scale: 1,
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  errorBuilder:
                                                                      (context,
                                                                          error,
                                                                          stackTrace) {
                                                                    return Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              10),
                                                                      alignment:
                                                                          Alignment
                                                                              .center,
                                                                      child: Image
                                                                          .asset(
                                                                        'assets/images/fami.png',
                                                                        scale:
                                                                            1,
                                                                        alignment:
                                                                            Alignment.center,
                                                                      ),
                                                                    );
                                                                  },
                                                                )
                                                              : Image.network(
                                                                  '${dataList2[index].productImage}',
                                                                  errorBuilder:
                                                                      (context,
                                                                          error,
                                                                          stackTrace) {
                                                                    return Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              10),
                                                                      alignment:
                                                                          Alignment
                                                                              .center,
                                                                      child: Image
                                                                          .asset(
                                                                        'assets/images/fami.png',
                                                                        scale:
                                                                            1,
                                                                        alignment:
                                                                            Alignment.center,
                                                                      ),
                                                                    );
                                                                  },
                                                                )),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            3,
                                                    child: valueKey == null
                                                        ? Text(
                                                            ids.length == 0
                                                                ? 'Food product'
                                                                : '${ids[index]}',
                                                            textAlign:
                                                                TextAlign.left,
                                                            maxLines: 2,
                                                            style: TextStyle(
                                                                color: Color
                                                                    .fromRGBO(
                                                                        79,
                                                                        79,
                                                                        79,
                                                                        1),
                                                                fontFamily:
                                                                    'Gilroy',
                                                                fontSize: 18,
                                                                letterSpacing:
                                                                    0,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                height: 1.5),
                                                          )
                                                        : Text(
                                                            dataList2.length ==
                                                                    0
                                                                ? 'Food product'
                                                                : '${dataList2[index].productName}',
                                                            textAlign:
                                                                TextAlign.left,
                                                            maxLines: 2,
                                                            style: TextStyle(
                                                                color: Color
                                                                    .fromRGBO(
                                                                        79,
                                                                        79,
                                                                        79,
                                                                        1),
                                                                fontFamily:
                                                                    'Gilroy',
                                                                fontSize: 18,
                                                                letterSpacing:
                                                                    0,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                height: 1.5),
                                                          ),
                                                  ),
                                                  Spacer(),
                                                  valueKey == null
                                                      ? rechargeshopid == '179'
                                                          ? Container()
                                                          : Container(
                                                              width: 54,
                                                              height: 30,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          10),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          4),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          4),
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          4),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          4),
                                                                ),
                                                                color: Colors
                                                                    .white,
                                                                border:
                                                                    Border.all(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          189,
                                                                          189,
                                                                          189,
                                                                          1),
                                                                  width: 0.5,
                                                                ),
                                                              ),
                                                              child: InkWell(
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10),
                                                                  child:
                                                                      DropdownButtonHideUnderline(
                                                                          child:
                                                                              DropdownButton<String>(
                                                                    value: dataList[
                                                                            index]
                                                                        [
                                                                        'quantity'],
                                                                    menuMaxHeight:
                                                                        450,
                                                                    onChanged:
                                                                        (String?
                                                                            newValue) {
                                                                      setState(
                                                                          () async {
                                                                        print(
                                                                            'newvalue is- ${newValue} id- ${dataList[index]['id']} index ${index}');
                                                                        updateDatabase(
                                                                            dataList[index]['id'],
                                                                            newValue);
                                                                        // database();

                                                                        dataList =
                                                                            await DBHelper.getData('thecart');
                                                                        print(
                                                                            'Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
                                                                        length =
                                                                            dataList.length;
                                                                        items = dataList
                                                                            .map(
                                                                              (item) => Place(id: item['id'], quantity: item['quantity'], shopId: item['shopId']),
                                                                            )
                                                                            .toList();
                                                                        setState(
                                                                            () {
                                                                          // print('shopIdshopId is : ${dataList[0]['shopId'].toString()}');
                                                                          // print('id is : ${dataList[0]['id'].toString()}');
                                                                          // print('quantity is : ${dataList[0]['quantity'].toString()}');
                                                                        });

                                                                        calculation =
                                                                            0;
                                                                        // print("before calculation.. $calculation ${dataList.length}");
                                                                        for (int i =
                                                                                0;
                                                                            i < dataList.length;
                                                                            i++) {
                                                                          var matttt =
                                                                              double.parse(prices[i]) * double.parse(dataList[i]['quantity']);
                                                                          calculation =
                                                                              double.parse('${double.parse('${calculation! + matttt} ').toStringAsFixed(2)}');
                                                                          print(
                                                                              'mathMaticsss $matttt');
                                                                          print(
                                                                              "innnn calculation.. $calculation");
                                                                        }
                                                                        setState(
                                                                            () {
                                                                          calculation;
                                                                        });
                                                                        // print("calculation $calculation");
                                                                        print(
                                                                            "prices $prices");
                                                                        print(
                                                                            "dataList $dataList");
                                                                      });
                                                                    },
                                                                    hint:
                                                                        new Text(
                                                                      '${dataList[index]['quantity']}',
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.black),
                                                                    ),
                                                                    items: countList
                                                                        .map((String
                                                                            value) {
                                                                      return new DropdownMenuItem<
                                                                          String>(
                                                                        value:
                                                                            value,
                                                                        child:
                                                                            new Text(
                                                                          value,
                                                                          style:
                                                                              TextStyle(
                                                                            fontFamily:
                                                                                'Gilroy',
                                                                            fontSize:
                                                                                10.0,
                                                                            color:
                                                                                const Color(0xFF111111),
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      );
                                                                    }).toList(),
                                                                  )),
                                                                ),
                                                                onTap: () {},
                                                              ),
                                                            ) //hhelllllllllo1
                                                      : rechargeshopid == '179'
                                                          ? Container()
                                                          : Container(
                                                              width: 54,
                                                              height: 30,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          10),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          4),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          4),
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          4),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          4),
                                                                ),
                                                                color: Colors
                                                                    .white,
                                                                border:
                                                                    Border.all(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          189,
                                                                          189,
                                                                          189,
                                                                          1),
                                                                  width: 0.5,
                                                                ),
                                                              ),
                                                              child: InkWell(
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10),
                                                                  child:
                                                                      DropdownButtonHideUnderline(
                                                                          child:
                                                                              DropdownButton<String>(
                                                                    value: dataList2[
                                                                            index]
                                                                        .quantity,
                                                                    menuMaxHeight:
                                                                        450,
                                                                    onChanged:
                                                                        (String?
                                                                            newValue) {
                                                                      setState(
                                                                          () {
                                                                        setState(
                                                                            () {});
                                                                      });
                                                                      print(
                                                                          "indexxxxxxx $index");
                                                                      print(
                                                                          "indexxxxxxx ${dataList2[index].productId}");

                                                                      presenter2!.setCart(
                                                                          dataList2[index]
                                                                              .shopId
                                                                              .toString(),
                                                                          dataList2[index]
                                                                              .productId
                                                                              .toString(),
                                                                          newValue
                                                                              .toString(),
                                                                          '',
                                                                          '',
                                                                          '',
                                                                          '');
                                                                    },
                                                                    hint:
                                                                        new Text(
                                                                      '${dataList2[index].quantity}',
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.black),
                                                                    ),
                                                                    items: countList
                                                                        .map((String
                                                                            value) {
                                                                      return new DropdownMenuItem<
                                                                          String>(
                                                                        value:
                                                                            value,
                                                                        child:
                                                                            new Text(
                                                                          value,
                                                                          style:
                                                                              TextStyle(
                                                                            fontFamily:
                                                                                'Gilroy',
                                                                            fontSize:
                                                                                10.0,
                                                                            color:
                                                                                const Color(0xFF111111),
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      );
                                                                    }).toList(),
                                                                  )),
                                                                ),
                                                                onTap: () {
                                                                  setState(
                                                                      () {});
                                                                },
                                                              ),
                                                            )
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Spacer(),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                        right: 15,
                                                      ),
                                                      child: valueKey == null
                                                          ? InkWell(
                                                              onTap: (() {
                                                                print(
                                                                    productIds[
                                                                        index]);
                                                              }),
                                                              child: Text(
                                                                prices.length ==
                                                                        0
                                                                    ? 'null'
                                                                    : '${checkCurrency.toString()}${double.parse(prices[index]) * double.parse(dataList[index]['quantity'])}',

                                                                //  '${checkCurrency.toString()}${double.parse('${double.parse(prices[index]) * double.parse(dataList[index]['quantity'])}').toStringAsFixed(2)}',
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    color: Color
                                                                        .fromRGBO(
                                                                            0,
                                                                            219,
                                                                            167,
                                                                            1),
                                                                    fontFamily:
                                                                        'Gilroy',
                                                                    fontSize:
                                                                        18,
                                                                    letterSpacing:
                                                                        0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                    height:
                                                                        1.5),
                                                              ),
                                                            )
                                                          : Text(
                                                              dataList2.length ==
                                                                      0
                                                                  ? 'No amount yet'
                                                                  : priceValue(
                                                                      '${checkCurrency.toString()}${double.parse('${double.parse(dataList2[index].price.toString()) * double.parse(dataList2[index].quantity.toString())}').toStringAsFixed(2)}'),
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          219,
                                                                          167,
                                                                          1),
                                                                  fontFamily:
                                                                      'Gilroy',
                                                                  fontSize: 18,
                                                                  letterSpacing:
                                                                      0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                  height: 1.5),
                                                            )),
                                                ],
                                              ),
                                              Container(
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 10),
                                                child: Transform.rotate(
                                                  angle:
                                                      0.000005008956130975318,
                                                  child: Divider(
                                                      color: Color.fromRGBO(
                                                          189, 189, 189, 1),
                                                      thickness: 0.2),
                                                ),
                                              ),
                                              valueKey == null
                                                  ? Container(
                                                      height: 15,
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      // color: Colors.red,
                                                      alignment:
                                                          Alignment.center,
                                                      margin: EdgeInsets.only(
                                                        top: 4,
                                                        bottom: 4,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 15),
                                                            child: InkWell(
                                                              child: Text(
                                                                'Remove',
                                                                style: TextStyle(
                                                                    color: Color
                                                                        .fromRGBO(
                                                                            249,
                                                                            62,
                                                                            108,
                                                                            1),
                                                                    fontFamily:
                                                                        'Gilroy',
                                                                    fontSize:
                                                                        12,
                                                                    letterSpacing:
                                                                        0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                    height: 1),
                                                              ),
                                                              onTap: () {
                                                                _localdialog(
                                                                    context,
                                                                    index);
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  : Container(
                                                      height: 15,
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      alignment:
                                                          Alignment.center,
                                                      margin: EdgeInsets.only(
                                                        top: 4,
                                                        bottom: 4,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          //
                                                          InkWell(
                                                            child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          15),
                                                              child: Text(
                                                                'Remove',
                                                                style: TextStyle(
                                                                    color: Color
                                                                        .fromRGBO(
                                                                            249,
                                                                            62,
                                                                            108,
                                                                            1),
                                                                    fontFamily:
                                                                        'Gilroy',
                                                                    fontSize:
                                                                        12,
                                                                    letterSpacing:
                                                                        0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                    height: 1),
                                                              ),
                                                            ),
                                                            onTap: () {
                                                              _onlinediaglog(
                                                                  context,
                                                                  index);
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                              SizedBox(
                                                height: 10,
                                              )
                                            ]),
                                      ),
                                    ),
                                  );
                                }),
                            calculation == null
                                ? Container()
                                : Container(
                                    //  height: 100,
                                    margin: EdgeInsets.only(bottom: 20),
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(left: 16, top: 25),
                                    child: Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Order Details',
                                            style: GoogleFonts.montserrat(
                                                fontSize: 20,
                                                color: AppColors().blackText,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Sub Total',
                                                style: GoogleFonts.montserrat(
                                                    fontSize: 16,
                                                    color: AppColors().darkgrey,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              valueKey == null
                                                  ? Container(
                                                      margin: EdgeInsets.only(
                                                          top: 18, right: 16),
                                                      child: Text(
                                                        '${checkCurrency.toString()}$calculation',
                                                        style: GoogleFonts
                                                            .montserrat(
                                                                fontSize: 16,
                                                                color: AppColors()
                                                                    .darkgrey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                      ),
                                                    )
                                                  : Container(
                                                      margin: EdgeInsets.only(
                                                          top: 18, right: 16),
                                                      child: Text(
                                                        list1 == null
                                                            ? ''
                                                            : '${checkCurrency.toString()}${list1!.subTotal}',
                                                        style: GoogleFonts
                                                            .montserrat(
                                                                fontSize: 16,
                                                                color: AppColors()
                                                                    .darkgrey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                      ),
                                                    ),
                                            ],
                                          ),
                                          valueKey == null
                                              ? Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      'Service Charges',
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              fontSize: 16,
                                                              color: AppColors()
                                                                  .darkgrey,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                    ),
                                                    valueKey == null
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16),
                                                            child: Text(
                                                              '${checkCurrency.toString()}${2.50 * double.parse(rate)}',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 16,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          )
                                                        : Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16),
                                                            child: Text(
                                                              '${checkCurrency.toString()}${list1!.serviceCharge}',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 16,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          ),
                                                  ],
                                                )
                                              : Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      'Service Charges',
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              fontSize: 16,
                                                              color: AppColors()
                                                                  .darkgrey,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                    ),
                                                    valueKey == null
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16),
                                                            child: Text(
                                                              '${checkCurrency.toString()}01.00',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 16,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          )
                                                        : Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16),
                                                            child: Text(
                                                              '${checkCurrency.toString()}${list1!.serviceCharge}',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 16,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          ),
                                                  ],
                                                ),
                                          Divider(
                                              color: AppColors()
                                                  .dividercolor
                                                  .withOpacity(1),
                                              thickness: 2.0),
                                          valueKey == null
                                              ? Container()
                                              : Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      'Total Price',
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              fontSize: 18,
                                                              color: AppColors()
                                                                  .darkgrey,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                    ),
                                                    valueKey == null
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16,
                                                                    bottom: 10),
                                                            child: Text(
                                                              '${checkCurrency.toString()} 11.00',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 18,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          )
                                                        : Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 16,
                                                                    bottom: 10),
                                                            child: Text(
                                                              '${checkCurrency.toString()}${list1!.totalAmount}',
                                                              style: GoogleFonts.montserrat(
                                                                  fontSize: 18,
                                                                  color: AppColors()
                                                                      .darkgrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                          ),
                                                  ],
                                                ),
                                        ],
                                      ),
                                    ),
                                  ),
                            calculation == null
                                ? Container()
                                : InkWell(
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 20),
                                      width: MediaQuery.of(context).size.width -
                                          15,
                                      height: 48,
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8),
                                          bottomLeft: Radius.circular(8),
                                          bottomRight: Radius.circular(8),
                                        ),
                                        color: Color.fromRGBO(0, 219, 167, 1),
                                      ),
                                      child: Align(
                                          alignment: Alignment.center,
                                          child: valueKey == null
                                              ? Text(
                                                  'Proceed to checkout ${checkCurrency.toString()}${(calculation! + 2.5).toStringAsFixed(2)}',
                                                  style: GoogleFonts.montserrat(
                                                      color: AppColors().white,
                                                      fontSize: 16,
                                                      letterSpacing: 1.2,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      height: 1.5),
                                                  textAlign: TextAlign.center,
                                                )
                                              : Text(
                                                  'Proceed to checkout ${checkCurrency.toString()}${list1!.totalAmount}',
                                                  style: GoogleFonts.montserrat(
                                                      color: AppColors().white,
                                                      fontSize: 16,
                                                      letterSpacing: 1.2,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      height: 1.5),
                                                  textAlign: TextAlign.center,
                                                )),
                                    ),
                                    onTap: rechargeshopid == '179'
                                        ? () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TransactionCheckOut(
                                                            imageRecharge:
                                                                imageRecharge,
                                                            skuCode: skuCode,
                                                            data1: data1,
                                                            data2: data2,
                                                            mobile: mobile,
                                                            code: code,
                                                            price: pricex,
                                                            maximumSendCurrencyIso:
                                                                maximumSendCurrencyIso,
                                                            currencySymbol:
                                                                '$checkCurrency',
                                                            rate: rate)));
                                          }
                                        : () {
                                            print(
                                                'recharge shop id $shopIdddd');
                                            if (valueKey != null) {
                                              if (dataList2.isNotEmpty) {
                                                if (dataList2[0].shopId !=
                                                    null) {
                                                  shopIdddd = dataList2[0]
                                                      .shopId
                                                      .toString();
                                                }
                                              }
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TransactionDetailScreen(
                                                          address: '',
                                                          promo: '',
                                                        )),
                                              );
                                            } else {
                                              _showDialog(context);
                                            }
                                          }),
                          ],
                        ),
                      ),
                      calculation == null
                          ? Center(
                              child: CircularProgressIndicator(
                                color: AppColors().appaccent,
                              ),
                            )
                          : Container()
                    ],
                  )
                : Column(
                    children: [
                      SizedBox(
                        height: 16.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 140),
                        child: Center(
                          child: Container(
                            height: 200,
                            child: Image.asset('assets/images/cart4x.png'),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        '${AppLocalizations.of(context)!.emptycart}',
                        style: TextStyle(
                          fontFamily: 'Gilroy',
                          color: Color(0xff6020BD),
                          fontSize: 24.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  )
            : Center(
                child: CircularProgressIndicator(
                  color: AppColors().appaccent,
                ),
              ));
  }

  @override
  void onproductDetailError(String error) {}

  @override
  void onproductDetailSuccess(ProductDetailModel detailModels) {
    print("hellllllllllllllllllll");
    if (detailModels.status == true) {
      DataList productDetails = detailModels.data!.dataList!;
      for (int i = 0; i < dataList.length; i++) {
        imagesIs.add("value");
        ids.add("value");
        prices.add("value");
        productIds.add("value");
      }
      for (int i = 0; i < dataList.length; i++) {
        if (dataList[i]['id'] == productDetails.productId) {
          imagesIs.removeAt(i);
          imagesIs.insert(i, productDetails.productImage);
          ids.removeAt(i);
          ids.insert(i, productDetails.productName);
          prices.removeAt(i);
          prices.insert(i, productDetails.productPrices);
          productIds.removeAt(i);
          productIds.insert(i, productDetails.productId);

          print("Calculation Before $calculation");
          if (calculation == null) {
            calculation = 0;
          }
          var matttt =
              double.parse(prices[i]) * double.parse(dataList[i]['quantity']);
          calculation = double.parse(
              '${double.parse('${calculation! + matttt}').toStringAsFixed(2)}');
        }
      }
    }
    if (icheck == dataList.length - 1) {
      print('datalist length 695');
      setState(() {});
    } else {
      print('datalist length 699');
    }
    setState(() {
      //    load3 = true;
    });
  }

  @override
  void onGetCartError(String error) {}

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    if (getCartModel.status == true) {
      dataList2 = getCartModel.data!;
      len = dataList2.length;
      cartController.getcountapi.value=len;
      if (getCartModel.data?.isEmpty ?? true) {
        print('orailnoor --get cart cart screen');
      } else {
        print('orailnoor --else cart cart screen');
        if (getCartModel.data![0].orderFor == 'recharge') {
          rechargeshopid = getCartModel.data![0].shopId;

          imageRecharge = getCartModel.data![0].productImage;
          skuCode = getCartModel.data![0].sku;
          data1 = getCartModel.data![0].productImage;
          print('$data1 product name');
          data2 = getCartModel.data![0].price;
          mobile = getCartModel.data![0].phoneNum;
          code = getCartModel.data![0].phoneCode;
          pricex = getCartModel.data![0].price;
          maximumSendCurrencyIso = getCartModel.data![0].sendCurrencyIso;
        }
      }

      delCart();
      loadcheck = true;

      currencySymbol;
    }
    calculation = 0;
    setState(() {});
    if (valueKey != null) {
      length = len;
      setState(() {
        presenter4!.summary('', '');
      });
    }

    //  print('refreshs getCart ${jsonEncode(dataList2)}');
    setState(() {});
  }

  @override
  void onSetCartError(String error) {}

  @override
  void onSetCartSuccess(SetCart setCartModel) {
    refreshPage();
  }

  @override
  void onDeleteCartError(String error) {}

  @override
  void onDeleteCartSuccess(DeleteCart deleteCartModel) {
    if (deleteCartModel.status == true) {
      presenter1!.getCart();
    }
    presenter4!.summary('', '');
    setState(() {});
    //  print('Delete done success');
  }

  String getPrice(String parse, dataList, int index) {
    //var setvalue = '€ ${double.parse(prices[index]) * double.parse(dataList[index]['quantity'])}';
    return '€ ${double.parse(prices[index]) * double.parse(dataList[index]['quantity'])}';
  }

  String priceValue(String s) {
    price = s;
    return price;
  }

  @override
  void onSummaryError(String error) {}

  @override
  void onSummarySuccess(Summmaryy summaryModel) {
    list1 = summaryModel.data;
    if (summaryModel.status == true) {
      summarycheck = true;
    }
    // print('${summaryModel.data!.totalAmount}orailnoor--checkout summary');
    setState(() {});
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              // width: 233.0,
              // height: 22.0,
              child: Center(
                  child: Text(
            'Login or Register',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color.fromRGBO(249, 62, 108, 1),
                fontFamily: 'Gilroy',
                fontSize: 20,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1.5 /*PERCENT not supported*/
                ),
          ))),
          content: Text(
            'Please login or register to continue your purchase. ',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color.fromRGBO(130, 130, 130, 1),
                fontFamily: 'Gilroy',
                fontSize: 16,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1.5 /*PERCENT not supported*/
                ),
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      // Navigator.of(context).pushReplacementNamed(
                      //   '/loginScreen',
                      // );
                      Navigator.pop(context);
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new LoginScreen(samePage: 'yes')));
                      // Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //             builder: (context) => LoginScreen('yes')))
                      //     .then((value) {
                      //   presenter1!.getCart();
                      //   presenter4!.summary('', '');
                      //   setState(() {
                      //     refreshPage();
                      //   });
                      // });
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                        // border: Border.all(
                        //   width: 1.0,
                        // ),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                            letterSpacing: 0,
                            fontWeight: FontWeight.w600,
                            height: 1),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onProductError(String error) {
    // TODO: implement onProductError
  }

  @override
  void onProductSuccess(TopProduct productModel) {
    // TODO: implement onProductSuccess
  }

  delCart() async {
    print(' orailnoor yea its deleted');
    await DBHelper.fulldelete();
  }

  void _localdialog(BuildContext context, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              width: 233.0,
              height: 22.0,
              child: Center(
                child: Text(
                  'Remove Item',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.right,
                ),
              )),
          content: new Text("Are you sure you want to remove this item",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                      print('remove481 ${index}');
                      setState(() {
                        deletesDatabase(productIds[index]);
                        database();
                        calculation = 0;
                        for (int i = 1; i < dataList.length; i++) {
                          var matttt = double.parse(prices[i]) *
                              double.parse(dataList[i]['quantity']);
                          print('aur ye chal gya');
                          calculation = double.parse(
                              '${double.parse('${calculation! + matttt}').toStringAsFixed(2)}');
                          print('aur ye chal gya');
                          setState(() {});
                        }
                      });
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  void _onlinediaglog(BuildContext context, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              width: 233.0,
              height: 22.0,
              child: Center(
                child: Text(
                  'Remove Item',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.right,
                ),
              )),
          content: new Text("Are you sure you want to remove this item",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: rechargeshopid == '179'
                        ? () {
                            print('recharge remove');

                            setState(() {
                              cartemptypresenter!.clearCart();
                              Navigator.of(context).pop();
                            });
                          }
                        : () {
                            print(
                                'pId is ${dataList2[index].productId.toString()}');
                            presenter3!.deleteCart(
                                dataList2[index].productId.toString());
                            setState(() {
                              Navigator.of(context).pop();
                            });
                          },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onClearCartError(String error) {
    // TODO: implement onClearCartError
  }

  @override
  void onClearCartSuccess(CartClear clearCart) {
    // TODO: implement onClearCartSuccess
    if (clearCart.status == true) {
      presenter1!.getCart();
    }
    presenter4!.summary('', '');
    setState(() {});
  }
}

String shopIdddd = '';
