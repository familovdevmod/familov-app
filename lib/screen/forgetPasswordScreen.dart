import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/forget.dart';
import 'package:familov/presenter/forget.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../widget/app_colors.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);
  @override
  State<ForgetPassword> createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword>
    implements ForgetPasswordScreenContract {
  ForgetPasswordScreenPresenter? presenter;

  _ForgetPasswordState() {
    presenter = new ForgetPasswordScreenPresenter(this);
  }

  final _emailController = TextEditingController();

  var status;
  String? message;
  int stage = 1;
  var tIs = '';
  var eIs = '';
  var email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
         toolbarHeight:30,
        foregroundColor: Color(0xff4D4D4D),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.only(top: 80, left: 16, right: 16),
          child: Column(children: [
            Center(
              child: Text(
                'Forgot Password',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  color: Color(0xffF93E6C),
                  fontSize: 32.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 14),
              child: Text(
                'Please enter your email address. You will receive a link to create a new password via email.',
                style: TextStyle(
                  letterSpacing: 1,
                  height: 1.5,
                  fontFamily: 'Gilroy',
                  color: Color(0xff828282),
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(
                top: 25,
              ),
              child: Text('Email'),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 5,
              ),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: 'Your Email',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                validator: null,
                onSaved: null,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            buttonWidget('Submit', context, () async {
              presenter!.forgetPassword(
                  '${_emailController.text}', //test@mail.com
                  '', //3c2e9986735470724b2369a4b2f9688d
                  //dGVzdEBtYWlsLmNvbQ==
                  '', //123456789
                  '', //123456789
                  '$stage');
              // Future.delayed(const Duration(milliseconds: 1500), () {

              // }
              // );
            }),
            SizedBox(height: 20),
            // buttonWidget('stage 2', context, () {
            //   if (message != '' && message != null) {
            //     print('M12 -$message');
            //     if (status == true) {
            //       print('M13 -');
            //       String s = "$message";
            //       int idx = s.indexOf("?");
            //       List parts = [
            //         s.substring(0, idx).trim(),
            //         s.substring(idx + 1).trim()
            //       ];
            //       print('M12 -  $parts');
            //       String d = parts[1];
            //       print('d is =  $d');

            //       int idx2 = d.indexOf('&');
            //       List parts1 = [
            //         d.substring(0, idx2).trim(),
            //         d.substring(idx2 + 1).trim()
            //       ];
            //       print('part2 is = $parts1');
            //       // [t=0d3f269e8be64f8b5d71fcb6bf5bee6f, e=dGVzdEBtYWlsLmNvbQ==]
            //       String r = parts1[0];
            //       String r2 = parts1[1];
            //       tIs = r.substring(2).trim();
            //       eIs = r2.substring(2).trim();
            //       print('TokenIs = $tIs  and EIs =  $eIs');
            //     }
            //     print('${_emailController.text} and  $tIs and $eIs');
            //   }
            //   stage = 2;
            //   presenter!.forgetPassword(
            //       '$eIs', //test@mail.com
            //       '$tIs', //3c2e9986735470724b2369a4b2f9688d
            //       //dGVzdEBtYWlsLmNvbQ==
            //       '', //123456789
            //       '', //123456789
            //       '$stage'); //t=f0862a26b0e63bfefe8179df1fe9d85b&e=dGVzdEBtYWlsLmNvbQ==
            //   if (status == true && stage == 2) {
            //     Navigator.of(context)
            //         .pushReplacementNamed('/Forget2', arguments: {
            //       'email': eIs,
            //       'token': tIs,
            //     });
            //   }
            // })
          ]),
        ));
  }

  @override
  void onForgetPassError(String error) {}

  @override
  void onForgetPassSuccess(Forget forgetModel) {
    status = forgetModel.status;
    message = forgetModel.message;
    status == true
        ? Navigator.of(context).pushNamed('/Otp', arguments: {
            'email': _emailController.text,
          })
        : BotToast.showSimpleNotification(
            backgroundColor: AppColors().toasterror,
            title: "Enter valid Email");
    // Fluttertoast.showToast(
    //     msg: 'Enter valid Email',
    //     toastLength: Toast.LENGTH_SHORT,
    //     gravity: ToastGravity.BOTTOM,
    //     backgroundColor: Colors.black45,
    //     fontSize: 18,
    //   );
    if (status == true) {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess, title: "$message");
      // Fluttertoast.showToast(
      //   msg: '$message',
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM,
      //   backgroundColor: Colors.black45,
      //   fontSize: 18,
      // );
    }
    print(message);
  }
}
