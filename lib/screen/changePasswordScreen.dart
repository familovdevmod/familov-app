import 'package:bot_toast/bot_toast.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/getBottomButton.dart';
import 'package:familov/model/changePassword.dart';
import 'package:familov/presenter/changePassword.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen>
    implements ChangePasswordScreenContract {
  TextEditingController currentpassword = new TextEditingController();
  TextEditingController newpassword = new TextEditingController();
  TextEditingController confirmpassword = new TextEditingController();

  bool _obscureText = true;
  bool _obscureTextcpwd = true;
  bool _obscureTextnpwd = true;

  ChangePasswordScreenPresenter? presenter;

  _ChangePasswordScreenState() {
    presenter = new ChangePasswordScreenPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 80,
        automaticallyImplyLeading: true,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Change Password',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                      bottom: 10,
                    ),
                    child: Text('Current Password'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: new TextFormField(
                      decoration: new InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                        ),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffDA3C5F)),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Color(0xffDA3C5F))),
                        hintText: "Current Password",
                        hintStyle: TextStyle(fontWeight: FontWeight.w500),
                        suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: _obscureText
                                ? Image.asset(
                                    'assets/images/eyeoff.png',
                                    height: 17,
                                    width: 17,
                                    scale: 2,
                                  )
                                : Image.asset(
                                    'assets/images/visibility.png',
                                    height: 17,
                                    width: 17,
                                    scale: 2,
                                  )),
                      ),
                      obscureText: _obscureText,
                      controller: currentpassword,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(bottom: 10, top: 20),
                    child: Text('New Password'),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          new TextFormField(
                            decoration: new InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide(
                                  color: Color(0xFF7F7F7F),
                                  width: 0.5,
                                ),
                              ),
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F)),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 14),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F))),
                              hintText: "New Password",
                              // helperText: "This password should be of minimum 8 digits",
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _obscureTextcpwd = !_obscureTextcpwd;
                                    print('Status : ${_obscureTextcpwd}');
                                  });
                                },
                                child: _obscureTextcpwd
                                    ? Image.asset(
                                        'assets/images/eyeoff.png',
                                        height: 17,
                                        width: 17,
                                        scale: 2,
                                      )
                                    : Image.asset(
                                        'assets/images/visibility.png',
                                        height: 0,
                                        width: 0,
                                        scale: 2,
                                      ),
                              ),
                            ),
                            obscureText: _obscureTextcpwd,
                            controller: newpassword,
                          ),
                        ],
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(bottom: 10, top: 20),
                    child: Text('Confirm Password'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: new TextFormField(
                      decoration: new InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                        ),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffDA3C5F)),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Color(0xffDA3C5F))),
                        hintText: "Confirm Password",
                        // suffixIcon: GestureDetector(
                        //     onTap: () {
                        //       setState(() {
                        //         _obscureTextnpwd = !_obscureTextnpwd;
                        //       });
                        //     },
                        //     child: _obscureTextnpwd
                        //         ? Image.asset(
                        //             'assets/images/eyeoff.png',
                        //             height: 17,
                        //             width: 17,
                        //             scale: 2,
                        //           )
                        //         : Image.asset(
                        //             'assets/images/visibility.png',
                        //             height: 0,
                        //             width: 0,
                        //             scale: 2,
                        //           )),
                      ),
                      obscureText: _obscureTextnpwd,
                      controller: confirmpassword,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.bottomCenter,
              child: getBottomButton(
                  'Save Changes',
                  currentpassword.text.isEmpty ||
                          newpassword.text.isEmpty ||
                          confirmpassword.text.isEmpty
                      ? () {
                          BotToast.showSimpleNotification(
                              onTap: () {},
                              backgroundColor: AppColors().toastsuccess,
                              title: "Password can't be empty");
                        }
                      : () {
                          setState(() {
                            if (currentpassword.text == '' &&
                                newpassword.text == '' &&
                                confirmpassword.text == '') {
                              BotToast.showSimpleNotification(
                                  onTap: () {},
                                  backgroundColor: AppColors().toastsuccess,
                                  title: "Password can't be empty");
                            } else if (newpassword.text !=
                                confirmpassword.text) {
                              BotToast.showSimpleNotification(
                                  onTap: () {},
                                  backgroundColor: AppColors().toastsuccess,
                                  title: "Confirm password not match");
                            } else if (currentpassword.text.isEmpty &&
                                newpassword.text.isEmpty &&
                                confirmpassword.text.isEmpty) {
                              BotToast.showSimpleNotification(
                                  onTap: () {},
                                  backgroundColor: AppColors().toastsuccess,
                                  title: "Enter a valid password");
                            } else {
                              presenter!.changePassword(
                                  '${currentpassword.text}',
                                  '${newpassword.text}',
                                  '${confirmpassword.text}');
                            }
                          });
                        }),
            )
          ],
        ),
      ),
    );
  }

  @override
  void onChangePassError(String error) {
    Fluttertoast.showToast(
      msg: '$error',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.black45,
      fontSize: 18,
    );
  }

  @override
  void onChangePassSuccess(Test testModel) {
    if (testModel.status == true) {
      BotToast.showSimpleNotification(
          onTap: () {},
          backgroundColor: AppColors().toastsuccess,
          title: "${testModel.message}");
      Navigator.of(context).pop();
    } else {
      BotToast.showSimpleNotification(
          onTap: () {},
          backgroundColor: AppColors().toastsuccess,
          title: "${testModel.errorMsg}");
    }
  }
}
