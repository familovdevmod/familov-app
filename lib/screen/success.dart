import 'package:familov/screen/home.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/screen/mobileRechargeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/customvariable.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SuccessScreen extends StatefulWidget {
  const SuccessScreen(String value, bool bool, {Key? key}) : super(key: key);

  @override
  State<SuccessScreen> createState() => _SuccessScreenState();
}

var name;

class _SuccessScreenState extends State<SuccessScreen> {
  @override
  void initState() {
    shared();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * .9,
          child: Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xff00DBA7), shape: BoxShape.circle),
                  height: 100,
                  width: 100,
                  child: Icon(
                    Icons.done,
                    color: Colors.white,
                    size: 70,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Yipee !!! $name',
                  style: TextStyle(
                    color: Color(0xff6020BD),
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget arcu pellentesque hendrerit auctor',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Thank You !',
                  style: TextStyle(
                    color: Color(0xff4D4D4D),
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 22),
                Text(
                  'Lorem ipsum dolor sit amet',
                  style: TextStyle(
                    color: Color(0xff4D4D4D),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 22,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MobileScreenRecharge()));
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width - 15,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8),
                      ),
                      color: Color.fromRGBO(0, 219, 167, 1),
                    ),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Send a Mobile Recharge',
                          style: TextStyle(
                            color: Colors.white.withOpacity(.95),
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => HomeScreen(0,'')),
                        (Route<dynamic> route) => false);
                    // Navigator.pop(context,
                    //     MaterialPageRoute(builder: (context) => Home()));
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width - 15,
                    height: 48,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1.4,
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8),
                      ),
                    ),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Back to Home',
                          style: TextStyle(
                            color: AppColors().toastsuccess,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(color: AppColors().background),
        ),
      ),
    );
  }

  shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      name = preferences.getString('nameis');
    });
  }
}
