import 'dart:convert';
import 'dart:io';
import 'package:familov/model/socialsignupmodel.dart';
import 'package:familov/presenter/socialLoginsignuppresenter.dart';
import 'package:familov/screen/home.dart';
import 'package:http/http.dart' as http;
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/screen/registerScreen.dart';
import 'package:familov/model/login.dart';
import 'package:familov/model/totalcustomer.dart';
import 'package:familov/presenter/customerlogin_presenter.dart';
import 'package:familov/presenter/tc.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../model/place.dart';
import '../presenter/clearCart+presenter.dart';
import '../widget/customvariable.dart';
import '../widget/db_helper.dart';

class LoginScreen extends StatefulWidget {
  String samePage;

  LoginScreen({Key? key, required this.samePage}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    implements
        CustomerLoginScreenContract,
        SocialLoginSignUpContract,
        TCContract,
        ClearCartContract,
        SetCartContract {
  dynamic flag;
  late CustomerLoginScreenPresenter _presenter;
  late TCPresenter presenter1;
  late SetCartPresenter _setCartPresenter;
  late ClearCartPresenter? presenterclear;
  late SocialLoginSignUpPresenter? socialloginpresenter;
  final _auth = FirebaseAuth.instance;

  _LoginScreenState() {
    socialloginpresenter = SocialLoginSignUpPresenter(this);
    presenterclear = ClearCartPresenter(this);
    _presenter = new CustomerLoginScreenPresenter(this);
    presenter1 = new TCPresenter(this);
    _setCartPresenter = SetCartPresenter(this);
  }
  final FirebaseAuth auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _obscureText = true;
  bool _obscureTextnpwd = true;
  String? name = '';

  tokenSave(dynamic token, bool flag) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('value_key', token.toString());
    preferences.setString(
      'nameis',
      name!,
    );
    preferences.setBool('valBool', flag);
    bool? check = preferences.getBool('valBool');
    String? valueKey = preferences.getString('value_key');
    if (check == true) {
      print('boolData :  ${valueKey.toString()}');
      print('boolData11 :  ${check.toString()}');
    }
  }

  var socialname;
  var socialemail;
  var socialprofile;
  var socilatoken;
  var socialusername;
  int? total_count;
  bool load = false;

  @override
  void initState() {
    presenter1.totalCount();
    super.initState();
  }

  // final GoogleSignIn _googleSignIn = GoogleSignIn(
  //   scopes: [
  //     'email',
  //     'https://www.googleapis.com/auth/contacts.readonly',
  //   ],
  // );
  // Future<void> _handleSignIn() async {
  //   try {
  //     GoogleSignInAccount? googleSignInAccount = await _googleSignIn.signIn();

  //     GoogleSignInAuthentication googleSignInAuthentication =
  //         await googleSignInAccount!.authentication;
  //     AuthCredential credential = GoogleAuthProvider.credential(
  //         accessToken: googleSignInAuthentication.accessToken,
  //         idToken: googleSignInAuthentication.idToken);

  //     await FirebaseAuth.instance.signInWithCredential(credential);
  //     print('Signed in succesFull');
  //   } catch (error) {
  //     print(error);
  //     // Fluttertoast.showToast(msg: "Error while login");
  //     BotToast.showSimpleNotification(
  //         backgroundColor: AppColors().toasterror, title: "$error");
  //   }
  // }

  // signInWithFacebook() async {
  //   final LoginResult loginResult = await FacebookAuth.instance.login();
  //   print(
  //       "loginResult.... ${loginResult.status} ${loginResult.message} ${loginResult.accessToken!.token}");
  //   final OAuthCredential facebookAuthCredential =
  //       FacebookAuthProvider.credential(loginResult.accessToken!.token);
  //   return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 30,
          foregroundColor: Color(0xff4D4D4D),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: load == true
            ? SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 80, left: 16, right: 16),
                    child: Column(
                      children: [
                        Text(
                          'Log In',
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            color: Color(0xffF93E6C),
                            fontSize: 32.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Text(
                          'Join $total_count happy families!',
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                // signInWithFacebook();
                                facebookSignin();
                              },
                              child: Container(
                                padding: const EdgeInsets.only(
                                    right: 20, top: 20, bottom: 20),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                                child: Icon(
                                  Icons.facebook,
                                  color: Color(0xff324BCC),
                                  size: 45,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            FittedBox(
                              child: InkWell(
                                onTap: () {
                                  signup(context);
                                },
                                child: Container(
                                    width: 40,
                                    padding: const EdgeInsets.all(5.0),
                                    decoration: new BoxDecoration(
                                      border: Border.all(color: Colors.black38),
                                      shape: BoxShape.circle,
                                    ),
                                    child: Image.asset(
                                      'assets/images/google.png',
                                    )),
                              ),
                            ),
                            // SizedBox(
                            //   width: 30,
                            // ),
                            // InkWell(
                            //     onTap: () {
                            //       signOut(context);
                            //     },
                            //     child: Icon(Icons.logout))
                            // SizedBox(
                            //   width: 30,
                            // ),
                            // FittedBox(
                            //   child: Container(
                            //       width: 40,
                            //       padding: const EdgeInsets.all(5.0),
                            //       decoration: new BoxDecoration(
                            //         border: Border.all(color: Colors.black38),
                            //         shape: BoxShape.circle,
                            //       ),
                            //       child: Image.asset(
                            //         'assets/images/apple.png',
                            //       )),
                            // ),
                          ],
                        ),
                        Platform.isAndroid
                            ? Container()
                            : Container(
                                margin: EdgeInsets.only(
                                    left: 16, right: 16, top: 16, bottom: 16),
                                child: SignInWithAppleButton(
                                  onPressed: () async {
                                    final appleCredential =
                                        await SignInWithApple
                                            .getAppleIDCredential(
                                      scopes: [
                                        AppleIDAuthorizationScopes.email,
                                        AppleIDAuthorizationScopes.fullName,
                                      ],
                                    );
                                    var oAuthProvider =
                                        OAuthProvider("apple.com");
                                    var credential = oAuthProvider.credential(
                                        accessToken:
                                            appleCredential.authorizationCode,
                                        idToken: appleCredential.identityToken);
                                    await FirebaseAuth.instance
                                        .signInWithCredential(credential);
                                    print('credential $credential');
                                  },
                                ),
                              ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: new Container(
                                    child: Divider(
                                  color: Colors.black45,
                                  height: 36,
                                )),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Or",
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: new Container(
                                    // margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                                    child: Divider(
                                  color: Colors.black45,
                                  height: 36,
                                )),
                              ),
                            ]),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(
                            top: 25,
                          ),
                          child: Text(
                            'Email',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff4D4D4D)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          child: TextFormField(
                              decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    borderSide: BorderSide(
                                      color: Color(0xFF7F7F7F),
                                      width: 0.5,
                                    ),
                                  ),
                                  hintStyle: TextStyle(
                                      fontSize: 16,
                                      fontFamily: "Gilroy",
                                      color: Color(0xff828282)),
                                  hintText: 'Your Email',
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffDA3C5F)),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 14),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                      borderSide: BorderSide(
                                          color: Color(0xffDA3C5F)))),
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Email is empty!';
                                }
                                if (!value.contains('@')) {
                                  return 'Invalid email!';
                                }
                                return null;
                              },
                              onSaved: null),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(
                            top: 25,
                          ),
                          child: Text(
                            'Password',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff4D4D4D)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          child: TextFormField(
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide(
                                  color: Color(0xFF7F7F7F),
                                  width: 0.5,
                                ),
                              ),
                              hintStyle: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "Gilroy",
                                  color: Color(0xff828282)),
                              hintText: 'Your Password',
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F)),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 14),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F))),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _obscureText = !_obscureText;
                                    print('Status : ${_obscureText}');
                                  });
                                },
                                child: _obscureText
                                    ? Image.asset(
                                        'assets/images/eyeoff.png',
                                        height: 17,
                                        width: 17,
                                        scale: 2,
                                      )
                                    : Image.asset(
                                        'assets/images/visibility.png',
                                        height: 0,
                                        width: 0,
                                        scale: 2,
                                      ),
                              ),
                            ),
                            controller: _passwordController,
                            obscureText: _obscureText,
                            keyboardType: TextInputType.visiblePassword,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Password is empty!';
                              }
                              if (value.length < 5) {
                                return 'Password is too short!';
                              }
                            },
                            onSaved: null,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        GestureDetector(
                          child: Container(
                            alignment: Alignment.centerRight,
                            // margin: EdgeInsets.only(left: 210),
                            child: Text(
                              'Forget Password ?',
                              style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  color: Color(0xff6020BD),
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pushNamed('/ForgetScreen');
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        buttonWidget("Log In", context, () {
                          if (!_formKey.currentState!.validate()) {
                            return;
                          }

                          print(
                              'test11  :  ${_emailController.text} and ${_passwordController.text}');
                          _presenter.doLogin("${_emailController.text}",
                              "${_passwordController.text}", "");
                        }),
                        SizedBox(
                          height: 35,
                        ),
                        Text(
                          'New to FAMILOV?',
                          style: TextStyle(
                              fontFamily: 'Gilroy',
                              color: Color(0xff4D4D4D),
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              child: new Container(
                                child: new Text.rich(
                                  TextSpan(
                                    children: [
                                      TextSpan(
                                        text: 'Register',
                                        style: TextStyle(
                                            fontFamily: 'Gilroy',
                                            color: Color(0xff00DBA7),
                                            fontWeight: FontWeight.w500),
                                      ),
                                      TextSpan(
                                        text: ' in less than a minute',
                                        style: TextStyle(
                                            fontFamily: 'Gilroy',
                                            color: Color(0xff4D4D4D),
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterScreen()));
                                //Navigator.of(context).pushNamed('/RegisterScreen');
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        )
                      ],
                    ),
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              )));
  }

  @override
  void onLoginError(String error) {
    // Fluttertoast.showToast(
    //   msg: '$error',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toasterror, title: "$error");
    print('login res error $error');
  }

  @override
  void onLoginSuccess(Loginjson login) {
    if (login.status == true) {
      flag = true;
      tokenSave(login.data!.accessToken, flag);
      print('login res success ${login.data!.accessToken}');
      name = login.data!.username;
      print('name is :  $name');

      database();

      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess, title: "${login.message}");
    } else {
      // presenterclear?.clearCart();
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toasterror, title: "${login.errorMsg}");
    }
    widget.samePage == 'recharge' ? Navigator.pop(context) : print('object');
    // presenterclear?.clearCart();
  }

  database() async {
    var dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    var length = dataList.length;
    var items = dataList
        .map(
          (item) => Place(
              id: item['id'],
              quantity: item['quantity'],
              shopId: item['shopId']),
        )
        .toList();

    items.isEmpty
        ? Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomeScreen(0, '')))
        : presenterclear?.clearCart();

    // for (int i = 0; i < dataList.length; i++) {
    //   _setCartPresenter.setCart(
    //       dataList[i]['shopId'].toString(),
    //       dataList[i]['id'].toString(),
    //       dataList[i]['quantity'].toString(),
    //       '',
    //       '',
    //       '',
    //       '');
    // }
  }

  @override
  void onTCError(String error) {}

  @override
  void onTCSuccess(Totalcustomer tc) {
    total_count = tc.data;
    load = true;
    setState(() {});
  }

  @override
  void onSetCartError(String error) {
    print('orailnoor setcart error');

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                new HomeScreen(widget.samePage.isNotEmpty ? 2 : 0, '')));

    // TODO: implement onSetCartError
  }

  @override
  void onSetCartSuccess(SetCart setCartModel) {
    print("print('orailnoor setcart success'); ${setCartModel.message}");
    // delCart();
  }

  @override
  void onClearCartError(String error) {
    print('orailnoor clearcart error');
    // database();
  }
  // TODO: implement onClearCartError

  @override
  void onClearCartSuccess(CartClear clearCart) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                new HomeScreen(widget.samePage.isNotEmpty ? 2 : 0, '')));
    print('orailnoor clearcart success');

    // database();

    // TODO: implement onClearCartSuccess
  }

  // delCart() async {
  //   print(' orailnoor yea its deleted');
  //   await DBHelper.fulldelete();
  // }

  Future<void> signup(BuildContext context) async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      // Getting users credential
      UserCredential result = await auth.signInWithCredential(authCredential);
      // User user = result.user;

      if (result != null) {
        print('from here dude');
        print(result.user!.displayName);

        print(result.additionalUserInfo);
        print(result.user!.displayName);
        print(result.user!.photoURL);
        print(result.user!.uid);
        print(result.additionalUserInfo!.profile!['email']);
        socialname = result.user!.displayName;
        socialemail = result.additionalUserInfo!.profile!['email'];
        socialprofile = result.user!.photoURL;
        socilatoken = result.user!.uid;
        print('''from here''');
        print(socialemail + socialname + socialname + socialprofile);
        // socialloginAPI();
        socialloginpresenter!.getData(
          '',
          'g',
          socialname.toString(),
          socialemail.toString(),
          socilatoken.toString(),
          socialprofile.toString(),
          '',
          '',
        );
      }
    }
  }

  // socialloginAPI() async {
  //   var headers = {'Language': 'english'};
  //   var request = http.MultipartRequest(
  //       'POST', Uri.parse('http://apptest.familov.com/api_v2/social-signup'));
  //   request.fields.addAll({
  //     'friend_referral_id': '',
  //     'platform': 'f',
  //     'username': socialname,
  //     'email': socialemail,
  //     'uid': socilatoken,
  //     'photo_url': socilatoken,
  //     'phone_code': '',
  //     'phone_number': ''
  //   });

  //   request.headers.addAll(headers);

  //   http.StreamedResponse response = await request.send();

  //   if (response.statusCode == 200) {
  //     print(await response.stream.bytesToString());
  //     print('google signup successfull');
  //     sociallogingoogle();
  //     BotToast.showSimpleNotification(
  //         backgroundColor: AppColors().toastsuccess,
  //         title: "Google authenticate");
  //   } else {
  //     print(response.reasonPhrase);
  //     sociallogingoogle();
  //     BotToast.showSimpleNotification(
  //         backgroundColor: AppColors().toastsuccess,
  //         title: "Something went wrong");
  //   }
  // }

  // sociallogingoogle() async {
  //   var headers = {'Language': 'english'};
  //   var request = http.MultipartRequest(
  //       'POST', Uri.parse('http://apptest.familov.com/api_v2/social-login'));
  //   request.fields
  //       .addAll({'platform': 'g', 'email': socialemail, 'uid': socilatoken});

  //   request.headers.addAll(headers);

  //   http.StreamedResponse response = await request.send();

  //   if (response.statusCode == 200) {
  //     print(await response.stream.bytesToString());
  //     BotToast.showSimpleNotification(
  //         backgroundColor: AppColors().toastsuccess, title: "Google logged in");
  //     // Navigator.push(
  //     //       context, MaterialPageRoute(builder: (context) => HomeScreen(0)));
  //   } else {
  //     BotToast.showSimpleNotification(
  //         backgroundColor: AppColors().toastsuccess,
  //         title: "${response.reasonPhrase}");
  //     print(response.reasonPhrase);
  //   }
  // }

  @override
  void onSignUpError(String error) {
    // TODO: implement onSignUpError
  }

  @override
  void onSignUpSuccess(SocialSignUpModel socialSignUpModel) {
    print('sign up with google called ${socialSignUpModel.message.toString()}');
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toastsuccess,
        title: "${socialSignUpModel.message.toString()}");
    socialSignUpModel.message.toString();
    // TODO: implement onSignUpSuccess
  }

  Future<String> facebookSignin() async {
    try {
      final _instance = FacebookAuth.instance;
      final result = await _instance.login(permissions: ['email']);

      print('logged inn using facebook${result.status}');
      if (result.status == LoginStatus.success) {
        print('logged inn using facebook');
        final OAuthCredential credential =
            FacebookAuthProvider.credential(result.accessToken!.token);
        final a = await _auth.signInWithCredential(credential);
        print(a.additionalUserInfo!);
        print('facebook username = ${a.additionalUserInfo!.username}');
        print(auth.currentUser!.displayName);
        socialname = auth.currentUser!.displayName;
        socialemail = auth.currentUser!.email;
        socialprofile = auth.currentUser!.photoURL;
        socilatoken = auth.currentUser!.uid;
        // socialusername =  auth.currentUser!.
        print('facebook name : ${auth.currentUser!.displayName}');
        print('facebook uid ${auth.currentUser!.uid}');
        print('facebook mail ${auth.currentUser!.email}');
        print('facebook profile ${auth.currentUser!.photoURL}');
        socialloginpresenter!.getData(
            '',
            'f',
            socialname.toString(),
            socialemail.toString(),
            socilatoken.toString(),
            socialprofile.toString(),
            '',
            '');
        await _instance.getUserData().then((userData) async {
          await _auth.currentUser!.updateEmail(userData['email']);
        });
        return 'Login successful';
      } else if (result.status == LoginStatus.cancelled) {
        print(auth.currentUser);
        return 'Login cancelled';
      } else {
        print(auth.currentUser);
        return 'Error';
      }
    } catch (e) {
      print("catch  $e");
      return e.toString();
    }
  }

  Future signOut(BuildContext context) async {
    await FirebaseAuth.instance.signOut().then((value) =>
        BotToast.showSimpleNotification(
            backgroundColor: AppColors().toasterror, title: "logged out"));

    // print(user);
  }

  @override
  void onSocialLogin(SocialSignUpModel socialSignUpModel) {
    // TODO: implement onSocialLogin

    if (socialSignUpModel.status == true) {
      flag = true;
      tokenSave(socialSignUpModel.data!.accessToken, flag);
      print('login res success ${socialSignUpModel.data!.accessToken}');
      name = socialSignUpModel.data!.username;
      print('name is :  $name');

      database();

      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${socialSignUpModel.message}");
    } else {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${socialSignUpModel.errorMsg}");
    }
  }

  @override
  void onSocialLoginerror(String error) {
    // TODO: implement onSocialLoginerror
  }
}
