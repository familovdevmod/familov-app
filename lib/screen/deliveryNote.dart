// import 'package:familov/model/mobile_invoice.dart';
// import 'package:familov/model/shop_invoiceModel.dart';
// import 'package:familov/presenter/mobileInvoice_presenter.dart';
// import 'package:familov/presenter/shopInvoice_presenter.dart';
// import 'package:flutter/material.dart';

// class DeliveryNote extends StatefulWidget {
//   const DeliveryNote({Key? key}) : super(key: key);
//   @override
//   _DeliveryNoteState createState() => _DeliveryNoteState();
// }

// class _DeliveryNoteState extends State<DeliveryNote>
//     implements GetShopInvoiceContract, GetMobileInvoiceContract {
//   List? statusList = [];
//   List? mobileList = [];
//   var load = false;
//   var state = false;
 

//   UserShopInvoicePresenter? _presenter;
//   UserMobileInvoicePresenter? presenter;

//   _DeliveryNoteState() {
//     _presenter = new UserShopInvoicePresenter(this);
//     presenter = new UserMobileInvoicePresenter(this);
//   }

//   @override
//   widget build(BuildContext context) {
//     final arguments = ModalRoute.of(context)!.settings.arguments as Map;
//     if (state == false) {
//       setState(() {
//         print('the Data is : ${arguments['urlData']}');
//         arguments['generatedArgument'].toString().startsWith('F')
//             ? _presenter!.getShopInvoice(arguments['urlData'])
//             : presenter!.getMobileInvoice(arguments['urlData']);
//       });
//     }
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         iconTheme: IconThemeData(color: Colors.black),
//         toolbarHeight: 60,
//         centerTitle: true,
//         backgroundColor: Colors.white,
//         elevation: 0,
//         title: Text(
//           'Delivery Note',
//           style: TextStyle(
//             fontFamily: 'Gilroy',
//             fontSize: 20,
//             fontWeight: FontWeight.w500,
//             color: Color(0xffF93E6C),
//           ),
//         ),
//       ),
//       body: load == true 
//           ? SingleChildScrollView(
//               child:
//                   Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
//                 Container(
//                   margin: EdgeInsets.only(right: 10, bottom: 5, top: 10),
//                   width: 120,
//                   height: 24,
//                   child: Row(children: <widget>[
//                     Container(
//                       width: 24,
//                       height: 24,
//                       decoration: BoxDecoration(
//                         color: Color.fromRGBO(255, 255, 255, 1),
//                       ),
//                       child: Image.asset('assets/images/download.png'),
//                     ),
//                     Text(
//                       'Download Bill',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                           color: Color.fromRGBO(77, 77, 77, 1),
//                           fontFamily: 'Gilroy',
//                           fontSize: 14,
//                           letterSpacing: 0,
//                           fontWeight: FontWeight.normal,
//                           height: 1.5),
//                     ),
//                   ]),
//                 ),
//                 Container(
//                     margin: EdgeInsets.only(
//                         right: 16, left: 16, bottom: 32, top: 12),
//                     width: MediaQuery.of(context).size.width,
//                     height: MediaQuery.of(context).size.height / 1.3,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.only(
//                         topLeft: Radius.circular(4),
//                         topRight: Radius.circular(4),
//                         bottomLeft: Radius.circular(4),
//                         bottomRight: Radius.circular(4),
//                       ),
//                       color: Colors.white,
//                       border: Border.all(
//                         color: Color.fromRGBO(189, 189, 189, 1),
//                         width: 0.5,
//                       ),
//                     ),
//                     child: Padding(
//                         padding: const EdgeInsets.all(10.0),
//                         child: Column(
//                           children: [
//                             Row(
//                               children: [
//                                 Image.asset(
//                                   'assets/images/familovlogo.png',
//                                 ),
//                                 Spacer(),
//                                 Column(
//                                   crossAxisAlignment: CrossAxisAlignment.end,
//                                   children: [
//                                     // Familov Mobile Date: Aug 7, 2021 / 19:56
//                                     Text(
//                                       'CODE: ${arguments['generatedArgument']}',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w500),
//                                     ),
//                                     SizedBox(
//                                       height: 4,
//                                     ),
//                                     arguments['generatedArgument']
//                                             .toString()
//                                             .startsWith('M')
//                                         ? Text(
//                                             'Familov Mobile',
//                                             style: TextStyle(
//                                                 fontSize: 12,
//                                                 fontFamily: 'Gilroy',
//                                                 fontWeight: FontWeight.w500),
//                                           )
//                                         : SizedBox(
//                                             width: 185,
//                                             child: Text(
//                                               'Delivery: ${arguments['delivery_option']}',
//                                               textAlign: TextAlign.end,
//                                               //  overflow: TextOverflow.ellipsis,
//                                               style: TextStyle(
//                                                   fontSize: 12,
//                                                   fontFamily: 'Gilroy',
//                                                   fontWeight: FontWeight.w500),
//                                             ),
//                                           ),
//                                     SizedBox(
//                                       height: 4,
//                                     ),
//                                     Text(
//                                       'Date: ${arguments['date_time']}',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w500),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                             Padding(
//                               padding:
//                                   const EdgeInsets.only(top: 12, bottom: 10),
//                               child: Container(
//                                 // margin: EdgeInsets.symmetric(horizontal: 10),
//                                 child: Transform.rotate(
//                                   angle: 0.000005008956130975318,
//                                   child: Divider(
//                                       color: Color.fromRGBO(189, 189, 189, 1),
//                                       thickness: 0.5),
//                                 ),
//                               ),
//                             ),
//                             Column(
//                               children: [
//                                 Row(
//                                   children: [
//                                     Text(
//                                       'Name         :',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w600),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.only(left: 5),
//                                       child: Text(
//                                         'Ashutosh Tripathi',
//                                         style: TextStyle(
//                                             fontSize: 12,
//                                             fontFamily: 'Gilroy',
//                                             fontWeight: FontWeight.w500),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                                 SizedBox(
//                                   height: 5,
//                                 ),
//                                 Row(
//                                   children: [
//                                     Text(
//                                       'For              :',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w600),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.only(left: 5),
//                                       child: Text(
//                                         '${arguments['receiver_name']}',
//                                         style: TextStyle(
//                                             fontSize: 12,
//                                             fontFamily: 'Gilroy',
//                                             fontWeight: FontWeight.w500),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                                 SizedBox(
//                                   height: 5,
//                                 ),
//                                 Row(
//                                   children: [
//                                     Text(
//                                       'Contact     :',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w600),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.only(left: 5),
//                                       child: Text(
//                                         '${arguments['receiver_phone']}',
//                                         style: TextStyle(
//                                             fontSize: 12,
//                                             fontFamily: 'Gilroy',
//                                             fontWeight: FontWeight.w500),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                                 SizedBox(
//                                   height: 5,
//                                 ),
//                                 Row(
//                                   children: [
//                                     Text(
//                                       'Message    :',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontFamily: 'Gilroy',
//                                           fontWeight: FontWeight.w600),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.only(left: 5),
//                                       child: Text(
//                                         'Message will be display here',
//                                         maxLines: 3,
//                                         style: TextStyle(
//                                             fontSize: 12,
//                                             fontFamily: 'Gilroy',
//                                             fontWeight: FontWeight.w500),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                             Padding(
//                               padding:
//                                   const EdgeInsets.only(top: 16, bottom: 10),
//                               child: Container(
//                                 child: Transform.rotate(
//                                   angle: 0.000005008956130975318,
//                                   child: Divider(
//                                       color: Color.fromRGBO(189, 189, 189, 1),
//                                       thickness: 0.5),
//                                 ),
//                               ),
//                             ),
//                             Container(
//                               child: Row(
//                                 children: [
//                                   Text(
//                                     'Products',
//                                     textAlign: TextAlign.left,
//                                     style: TextStyle(
//                                         color: Color.fromRGBO(79, 79, 79, 1),
//                                         fontFamily: 'Gilroy',
//                                         fontSize: 14,
//                                         letterSpacing: 0,
//                                         fontWeight: FontWeight.normal,
//                                         height: 1.5),
//                                   ),
//                                   Spacer(),
//                                   Text(
//                                     'Qty',
//                                     textAlign: TextAlign.left,
//                                     style: TextStyle(
//                                         color: Color.fromRGBO(79, 79, 79, 1),
//                                         fontFamily: 'Gilroy',
//                                         fontSize: 14,
//                                         letterSpacing: 0,
//                                         fontWeight: FontWeight.normal,
//                                         height: 1.5),
//                                   )
//                                 ],
//                               ),
//                             ),
//                             SizedBox(
//                               height: 10,
//                             ),
//                             ListView.builder(
//                               physics: BouncingScrollPhysics(),
//                               scrollDirection: Axis.vertical,
//                               shrinkWrap: true,
//                               itemCount: arguments['generatedArgument']
//                                       .toString()
//                                       .startsWith('F')
//                                   ? statusList!.length
//                                   : mobileList!.length,
//                               itemBuilder: (context, index) {
//                                 return Container(
//                                   padding: EdgeInsets.only(top: 10, bottom: 5),
//                                   child: Row(
//                                     children: [
//                                       Text(
//                                         arguments['generatedArgument']
//                                                 .toString()
//                                                 .startsWith('F')
//                                             ? '${statusList![index].productName}'
//                                             : '${mobileList![0].rechargeItemDetails!.productName}',
//                                         style: TextStyle(
//                                             fontWeight: FontWeight.w500,
//                                             fontSize: 14),
//                                       ),
//                                       Spacer(),
//                                       Text(
//                                         arguments['generatedArgument']
//                                                 .toString()
//                                                 .startsWith('F')
//                                             ? '${statusList![index].quantity}'
//                                             : '${mobileList![index].quantity}',
//                                         style: TextStyle(
//                                             fontWeight: FontWeight.w500,
//                                             fontSize: 14),
//                                       ),
//                                     ],
//                                   ),
//                                 );
//                               },
//                             ),
//                           ],
//                         ))),
//               ]),
//             )
//           : Scaffold(
//               body: Center(
//                 child: CircularProgressIndicator(
//  color: Color.fromRGBO(249, 62, 108, 1)),
//               ),
//             ),
//     );
//   }

//   @override
//   void onShopInvoiceError(String error) {}

//   @override
//   void onShopInvoiceSuccess(ShopInvoice shopinvoiceModel) {
//     setState(() {
//       load = true;
//       state = true;
//     });
//     statusList = shopinvoiceModel.data!.dataList!;
//   }

//   @override
//   void onInvoiceError(String error) {}

//   @override
//   void onInvoiceSuccess(MobileInvoices invoicesModel) {
//     setState(() {
//       load = true;
//       state = true;
//     });
//     mobileList = invoicesModel.data!.dataList!;
//   }
// }
