
import 'dart:io' show Platform;

import 'package:bot_toast/bot_toast.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:familov/model/signUp_Model.dart';
import 'package:familov/model/totalcustomer.dart';
import 'package:familov/presenter/signUp_presenter.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:email_validator/email_validator.dart';
// import 'package:familov/Screen/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:g_captcha/g_captcha.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../presenter/tc.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen>
    implements SignUpScreenContract, TCContract {
  TextEditingController _passwordController = new TextEditingController();
  SignUpScreenPresenter? presenter;
  TCPresenter? presenter1;

  _RegisterScreenState() {
    presenter = new SignUpScreenPresenter(this);
    presenter1 = new TCPresenter(this);
  }
  dynamic flag;
  var code1 = '+91';
  var name = '';
  bool privacycheck = false;

  var firstName;
  var email;
  var mobileNumber;
  var lastName;
  var createPassword;
  var confirmPassword;
  Color nameborder = Color(0xFF7F7F7F);
  Color lastnameborder = Color(0xFF7F7F7F);
  Color emailborder = Color(0xFF7F7F7F);
  Color phoneborder = Color(0xFF7F7F7F);
  Color passwordborder = Color(0xFF7F7F7F);
  Color conformborder = Color(0xFF7F7F7F);
  bool? ismailvalid;

  int? totalc;

  final List<String> genderList = <String>[
    ("Select Gender"),
    ('Male'),
    ('Female')
  ];
  bool _obscureText = true;
  bool _obscureTextnpwd = true;

  final myController1 = TextEditingController();
  final myController2 = TextEditingController();
  final myController3 = TextEditingController();
  final myController4 = TextEditingController();
  final myController5 = TextEditingController();
  final emailController = TextEditingController();

  @override
  void dispose() {
    myController1.dispose();
    myController2.dispose();
    myController3.dispose();
    myController4.dispose();
    myController5.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // _openReCaptcha();

    // createPassword.addListener(() {
    //   setState(() {
    //     passwordcolor = Colors.green;
    //   });
    // });
    // confirmPassword.addListener(() {
    //   setState(() {
    //     passwordcolor = Colors.red;
    //   });
    // });
    presenter1!.totalCount();
    super.initState();
  }

  final GlobalKey<FormState> _formKey = GlobalKey();
  Future<void> _saveForm() async {
    // final isValid = _formKey.currentState!.validate();
    // if (!isValid) {
    //   return;
    // }
    presenter!.signUp(
      '$email',
      '$confirmPassword',
      '$firstName',
      '$lastName',
      '$code1',
      '$mobileNumber',
      '',
      '123456',
    );
  }

  Color passwordcolor = Color(0xffF93E6C);
  bool? load;
  String CAPTCHA_SITE_KEY = "6Lf2VqEfAAAAAGpX_xR5pNY44M8PEus5X1UADnCK";

  // _openReCaptcha() async {
  //   String tokenResult = await GCaptcha.reCaptcha(CAPTCHA_SITE_KEY);
  //   print('tokenResult: $tokenResult');
  //   Fluttertoast.showToast(msg: tokenResult, timeInSecForIosWeb: 4);

  //   // setState
  // }

  webViewScreen() {
    return WebView(
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (controller) {
        controller.loadFlutterAsset('assets/webpages/index.html');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var deviceVertical =
        MediaQuery.of(context).orientation == Orientation.portrait;
    print('deviceVertical   =   $deviceVertical');
    final FocusScopeNode _node = FocusScopeNode();
    // var heights = AppBar().preferredSize.height;
    // final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 30,
          foregroundColor: Color(0xff4D4D4D),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Container(
            child: load == true
                ? Form(
                    key: _formKey,
                    child: FocusScope(
                      node: _node,
                      child: SingleChildScrollView(
                        // reverse: true,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 80,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            // height:
                            //     MediaQuery.of(context).size.height - heights - statusBarHeight,
                            child: Column(
                              children: [
                                Text(
                                  'Register',
                                  style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    color: Color(0xffF93E6C),
                                    fontSize: 32.0,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                Text(
                                  'Join $totalc happy families!',
                                  style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    color: Colors.black,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        // signInWithFacebook();
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            right: 20, top: 20, bottom: 20),
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                        child: Icon(
                                          Icons.facebook,
                                          color: Color(0xff324BCC),
                                          size: 45,
                                        ),
                                      ),
                                    ),
                                    FittedBox(
                                      child: InkWell(
                                        onTap: () {
                                          // _handleSignIn();
                                        },
                                        child: Container(
                                            width: 40,
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: new BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.black38),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Image.asset(
                                              'assets/images/google.png',
                                            )),
                                      ),
                                    ),
                                    Platform.isIOS
                                        ? Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: FittedBox(
                                              child: Container(
                                                  width: 40,
                                                  padding:
                                                      const EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.black38),
                                                    shape: BoxShape.circle,
                                                  ),
                                                  child: Image.asset(
                                                    'assets/images/apple.png',
                                                  )),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(
                                                left: 20.0, right: 20.0),
                                            child: Divider(
                                              color: Colors.black45,
                                              height: 36,
                                            )),
                                      ),
                                      Text(
                                        "Or",
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black),
                                      ),
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(
                                                left: 20.0, right: 20.0),
                                            child: Divider(
                                              color: Colors.black45,
                                              height: 36,
                                            )),
                                      ),
                                    ]),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 20,
                                  ),
                                  child: Text(
                                    'First Name',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 10,
                                  ),
                                  child: TextFormField(
                                    textInputAction: TextInputAction.next,
                                    onChanged: (val) {
                                      setState(() {
                                        print(
                                            '${myController1.text}here it is');
                                        // ignore: unnecessary_statements
                                        myController1.text.isNotEmpty
                                            ? nameborder = Colors.green
                                            : nameborder = Color(0xffDA3C5F);
                                      });
                                    },
                                    onEditingComplete: _node.nextFocus,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          borderSide: BorderSide(
                                            color: nameborder,
                                            width: 0.5,
                                          ),
                                        ),
                                        hintStyle: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Gilroy",
                                            color: Color(0xff828282)),
                                        hintText: 'My first name',
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xffDA3C5F)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 16,
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            borderSide:
                                                BorderSide(color: nameborder))),
                                    controller: myController1,
                                    keyboardType: TextInputType.text,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r'[A-Za-z]')),
                                    ],
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Name is empty!';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {},
                                    // onTap: () {
                                    //   firstName = myController.text;
                                    //   print('text1 ${myController.text}');
                                    //   setState(() {});
                                    // },
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 20,
                                  ),
                                  child: Text(
                                    'Last Name',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 10,
                                  ),
                                  child: TextFormField(
                                    onChanged: (val) {
                                      setState(() {
                                        print(
                                            '${myController1.text}here it is');
                                        // ignore: unnecessary_statements
                                        myController2.text.isNotEmpty
                                            ? lastnameborder = Colors.green
                                            : lastnameborder =
                                                Color(0xffDA3C5F);
                                      });
                                    },
                                    textInputAction: TextInputAction.next,
                                    onEditingComplete: _node.nextFocus,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          borderSide: BorderSide(
                                            color: lastnameborder,
                                            width: 0.5,
                                          ),
                                        ),
                                        hintStyle: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Gilroy",
                                            color: Color(0xff828282)),
                                        hintText: 'My last name',
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xffDA3C5F)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 16,
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            borderSide: BorderSide(
                                                color: lastnameborder))),
                                    controller: myController2,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r'[A-Za-z]')),
                                    ],
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Name is empty!';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {},
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 20,
                                  ),
                                  child: Text(
                                    'Email',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 10,
                                  ),
                                  child: TextFormField(
                                    onChanged: (val) {
                                      ismailvalid = EmailValidator.validate(
                                          emailController.text);
                                      setState(() {
                                        print(
                                            '${myController1.text}here it is');
                                        // ignore: unnecessary_statements
                                        emailController.text.isNotEmpty &&
                                                ismailvalid == true
                                            ? emailborder = Colors.green
                                            : emailborder = Color(0xffDA3C5F);
                                      });
                                    },
                                    textInputAction: TextInputAction.next,
                                    onEditingComplete: _node.nextFocus,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          borderSide: BorderSide(
                                            color: emailborder,
                                            width: 0.5,
                                          ),
                                        ),
                                        hintStyle: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Gilroy",
                                            color: Color(0xff828282)),
                                        hintText: 'My Email',
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xffDA3C5F)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 16,
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            borderSide: BorderSide(
                                                color: lastnameborder))),
                                    controller: emailController,
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Email is empty!';
                                      } else {
                                        if (ismailvalid == false) {
                                          return 'Enter a valid Email!';
                                        }
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {},
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 25,
                                  ),
                                  child: Text(
                                    'Mobile Number',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        //  height: MediaQuery.of(context).size.height / 18,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3.5,
                                        child: Container(
                                          // height: MediaQuery.of(context).size.height / 17.35,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3.5,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                color: phoneborder,
                                                width: 0.5,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0))),
                                          child: CountryCodePicker(
                                            // ignore: avoid_print
                                            onChanged: (e) =>
                                                code1 = e.dialCode.toString(),
                                            //print(e.dialCode),
                                            initialSelection: 'IN',
                                            showCountryOnly: false,
                                            showOnlyCountryWhenClosed: false,
                                            favorite: const ['+39', 'FR'],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 18,
                                      ),
                                      Expanded(
                                        child: TextFormField(
                                          onChanged: (val) {
                                            setState(() {
                                              print(
                                                  '${myController4.text}here it is');
                                              // ignore: unnecessary_statements
                                              // myController4.text.isNotEmpty &&
                                              myController4.text.length >= 10
                                                  ? phoneborder = Colors.green
                                                  : phoneborder =
                                                      Color(0xffDA3C5F);
                                            });
                                          },
                                          // maxLength: 10,
                                          keyboardType: TextInputType.number,
                                          textInputAction: TextInputAction.next,
                                          onEditingComplete: _node.nextFocus,
                                          decoration: InputDecoration(
                                              counterText: "",
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                borderSide: BorderSide(
                                                  color: phoneborder,
                                                  width: 0.5,
                                                ),
                                              ),
                                              hintStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: "Gilroy",
                                                  color: Color(0xff828282)),
                                              hintText: 'My mobile number',
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Color(0xffDA3C5F)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8))),
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 16),
                                              focusedBorder: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8)),
                                                  borderSide: BorderSide(
                                                      color: phoneborder))),
                                          inputFormatters: [
                                            FilteringTextInputFormatter.allow(
                                                RegExp(r'[0-9]')),
                                          ],
                                          controller: myController4,
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return 'Mobile number is empty!';
                                            }
                                            return null;
                                          },
                                          onSaved: null,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 25,
                                  ),
                                  child: Text(
                                    'Create Password',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 5,
                                  ),
                                  // height: 48.0,
                                  child: new TextFormField(
                                    onChanged: (val) {
                                      setState(() {
                                        print(
                                            '${myController4.text}here it is');
                                        // ignore: unnecessary_statements
                                        // myController4.text.isNotEmpty &&
                                        _passwordController.text ==
                                                myController5.text
                                            ? passwordborder = Colors.green
                                            : passwordborder =
                                                Color(0xffDA3C5F);
                                        _passwordController.text ==
                                                myController5.text
                                            ? conformborder = Colors.green
                                            : conformborder = Color(0xffDA3C5F);
                                      });
                                    },
                                    textInputAction: TextInputAction.next,
                                    onEditingComplete: _node.nextFocus,
                                    decoration: new InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        borderSide: BorderSide(
                                          color: conformborder,
                                          width: 0.5,
                                        ),
                                      ),
                                      hintStyle: TextStyle(
                                          fontSize: 16,
                                          fontFamily: "Gilroy",
                                          color: Color(0xff828282)),
                                      hintText: 'Create password',
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffDA3C5F)),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8))),
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 16,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8)),
                                          borderSide: BorderSide(
                                              color: passwordborder)),
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _obscureText = !_obscureText;
                                            print('Status : ${_obscureText}');
                                          });
                                        },
                                        child: _obscureText
                                            ? Image.asset(
                                                'assets/images/eyeoff.png',
                                                height: 17,
                                                width: 17,
                                                scale: 2,
                                              )
                                            : Image.asset(
                                                'assets/images/visibility.png',
                                                height: 0,
                                                width: 0,
                                                scale: 2,
                                              ),
                                      ),
                                    ),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Password is empty!';
                                      }
                                      return null;
                                    },
                                    obscureText: _obscureText,
                                    controller: _passwordController,
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(
                                    top: 25,
                                  ),
                                  child: Text(
                                    'Confirm Password',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xff4D4D4D)),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                      top: 5,
                                    ),
                                    // height: 60.0,
                                    child: new TextFormField(
                                      onChanged: (val) {
                                        setState(() {
                                          print(
                                              '${myController4.text}here it is');
                                          // ignore: unnecessary_statements
                                          // myController4.text.isNotEmpty &&
                                          _passwordController.text ==
                                                  myController5.text
                                              ? conformborder = Colors.green
                                              : conformborder =
                                                  Color(0xffDA3C5F);
                                        });
                                      },
                                      textInputAction: TextInputAction.next,
                                      onEditingComplete: _node.nextFocus,
                                      decoration: new InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          borderSide: BorderSide(
                                            color: conformborder,
                                            width: 0.5,
                                          ),
                                        ),
                                        hintStyle: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Gilroy",
                                            color: Color(0xff828282)),
                                        hintText: 'Confirm password',
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xffDA3C5F)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 16,
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            borderSide: BorderSide(
                                                color: conformborder)),
                                        // suffixIcon: GestureDetector(
                                        //     onTap: () {
                                        //       setState(() {
                                        //         _obscureTextnpwd =
                                        //             !_obscureTextnpwd;
                                        //       });
                                        //     },
                                        //     child: _obscureTextnpwd
                                        //         ? Image.asset(
                                        //             'assets/images/eyeoff.png',
                                        //             height: 17,
                                        //             width: 17,
                                        //             color: null,
                                        //             scale: 2,
                                        //           )
                                        //         : Image.asset(
                                        //             'assets/images/visibility.png',
                                        //             height: 0,
                                        //             width: 0,
                                        //             color: null,
                                        //             scale: 2,
                                        //           )),
                                      ),
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return 'Password is empty!';
                                        }
                                        return null;
                                      },
                                      obscureText: _obscureTextnpwd,
                                      controller: myController5,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 14,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      new Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 20,
                                              width: 20,
                                              child: Checkbox(
                                                  activeColor: Colors.grey,
                                                  value: privacycheck,
                                                  onChanged: (value) {
                                                    privacycheck =
                                                        privacycheck == false
                                                            ? true
                                                            : false;
                                                    setState(() {});
                                                  }),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                                'By submitting this form, you accept FAMILOVs'),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              launchUrl(Uri.parse(
                                                  'https://familov.com/terms-conditions'));
                                            },
                                            child: Text(
                                              'Terms of use ',
                                              style: TextStyle(
                                                  fontFamily: 'Gilroy',
                                                  color: Color(0xff6020BD),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Text('and its '),
                                          InkWell(
                                            onTap: () {
                                              launchUrl(Uri.parse(
                                                  'https://www.familov.com/privacy-policy'));
                                            },
                                            child: Text(
                                              'Privacy Policy',
                                              style: TextStyle(
                                                  fontFamily: 'Gilroy',
                                                  color: Color(0xff6020BD),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 30),
                                  child: FlatButton(
                                    height:
                                        MediaQuery.of(context).size.height / 14,
                                    minWidth: MediaQuery.of(context).size.width,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: BorderRadius.horizontal(
                                            left: Radius.circular(10),
                                            right: Radius.circular(10))),
                                    color: Color(0xff00DBA7),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('Register',
                                            style: new TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.white)),
                                      ],
                                    ),
                                    onPressed: () {
                                      print('code of country is :  $code1');
                                      final isValid =
                                          _formKey.currentState!.validate();
                                      if (!isValid) {
                                        return;
                                      }
                                      ismailvalid = EmailValidator.validate(
                                          emailController.text);
                                      firstName = myController1.text;
                                      lastName = myController2.text;
                                      email = emailController.text;
                                      mobileNumber = myController4.text;
                                      createPassword = _passwordController.text;
                                      confirmPassword = myController5.text;
                                      if (privacycheck == true) {
                                        if (createPassword == confirmPassword &&
                                            createPassword != null &&
                                            confirmPassword != null) {
                                          print(
                                              'text1 : $firstName   $lastName  $email  $mobileNumber  $createPassword  $confirmPassword');
                                          print('ckeck signup');
                                          _saveForm();
                                        } else {
                                          BotToast.showSimpleNotification(
                                              onTap: () {},
                                              backgroundColor:
                                                  AppColors().toastsuccess,
                                              title: "Password does not match");
                                        }
                                      } else {
                                        BotToast.showSimpleNotification(
                                            onTap: () {},
                                            backgroundColor:
                                                AppColors().appaccent,
                                            title:
                                                "Please accept privacy policy first");
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(
                    color: Color(0xffDA3C5F),
                  ))));
  }

  @override
  void onSignUpError(String error) {
    print('orailnoor signups');
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toasterror, title: "$error");
    // Fluttertoast.showToast(
    //   msg: '$error',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
  }

  @override
  void onSignUpSuccess(SignupModel signupmodel) {
    print('---here we go ${signupmodel.data.toString()}');
    if (signupmodel.status == true) {
      print('orailnoor signups');
      flag = true;
      tokenSave(signupmodel.data!.accessToken, flag);
      print('login res success ${signupmodel.data!.accessToken}');
      name = signupmodel.data!.username!;
      print('name is :  $name');
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${signupmodel.message}");

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen(0, '')));
    } else {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${signupmodel.errorMsg}");
      // Fluttertoast.showToast(
      //   msg: '${signupmodel.errorMsg}',
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM,
      //   backgroundColor: Colors.black45,
      //   fontSize: 18,

    }
  }

  @override
  void onTCError(String error) {}

  @override
  void onTCSuccess(Totalcustomer tc) {
    load = true;
    totalc = tc.data;
    setState(() {});
  }

  tokenSave(dynamic token, bool flag) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('value_key', token.toString());
    preferences.setString(
      'nameis',
      name,
    );
    preferences.setBool('valBool', flag);
    bool? check = preferences.getBool('valBool');
    String? valueKey = preferences.getString('value_key');
    if (check == true) {
      print('boolData :  ${valueKey.toString()}');
      print('boolData11 :  ${check.toString()}');
    }
  }
}
