import 'package:familov/screen/beneficiaryScreen.dart';
import 'package:familov/screen/changePasswordScreen.dart';
import 'package:familov/screen/changelangscreen.dart';
import 'package:familov/screen/loginScreen.dart';
import 'package:familov/screen/myDetailsScreen.dart';
import 'package:familov/screen/transactionScreen.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/presenter/clearCart+presenter.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import '../widget/buttonWidget.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    implements ClearCartContract {
  var local;

  late File jsonFile;
  late Directory dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  var fileContent;
  var items;
  ClearCartPresenter? presenterclear;
  _ProfileScreenState() {
    presenterclear = new ClearCartPresenter(this);
  }

  var load = false;

  @override
  void initState() {
    // Future.delayed(Duration(milliseconds: 20000), () {
    //   // Do something
    // });
    getApplicationDocumentsDirectory().then((Directory directory) async {
      dir = directory;
      jsonFile = new File(dir.path + "/" + fileName);
      fileExists = jsonFile.existsSync();

      print("fileExists $fileExists");
      if (fileExists) {
        setState(() {
          final data = json.decode(jsonFile.readAsStringSync());
          dynamic _items = data;
          items = jsonDecode(_items);
          print('items data value ${items['MSG_PRODUCT_DETAIL_04']}');
        });
      }
      if (items != null) {
        if (items['LBL_MENU_07'] != '' && items['LBL_MENU_07'] != null) {
          load = true;
        }
      }
    });
    tokenCheck();
    setState(() {});
    super.initState();
  }

  delCart() async {
    await DBHelper.fulldelete();
  }

  String? valueKey;

  tokenCheck() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    if (valueKey != '' && valueKey != null) {
      print('tokenCheckIs :  ${valueKey.toString()}');
    }
  }

  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<LocaleProvider>(context);
    // var locale = provider.locale ?? Locale('en');
    return WillPopScope(
      onWillPop: () async {
        //debugPrint("popping from route 2 disabled");
        Navigator.pop(context);
        return false;
      },
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            //check lang API or just remove
            body: load == true
                ? valueKey == null
                    ? showNotLoginPage()
                    : _bodyData()
                : Center(
                    child: CircularProgressIndicator(
                      color: Color(0xffDA3C5F),
                    ),
                  )),
      ),
    );
  }

  _bodyData() {
    return ListView(physics: BouncingScrollPhysics(), children: <Widget>[
      Column(children: <Widget>[
        Column(crossAxisAlignment: CrossAxisAlignment.center, children: <
            Widget>[
          SizedBox(
            height: 30,
          ),
          Container(
            child: Text(
              'Profile',
              style: TextStyle(
                color: Color(0xffF93E6C),
                fontFamily: 'Gilroy',
                fontSize: 22,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox(
            height: 16.0,
          ),
          new Container(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  if (valueKey != '' && valueKey != null) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new TransactionScreen()));
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 28),
                  height: 45.0,
                  child: Row(
                    children: <Widget>[
                      expandStyle(
                          1, //${AppLocalizations.of(context)!.tran}
                          widgetText(
                              '${items['LBL_MENU_07']}  ', "transcation")),
                      expandStyle(0, widgetIconsNext()),
                    ],
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  if (valueKey != '' && valueKey != null) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyDetailScreen()));
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45.0,
                  child: Row(
                    children: <Widget>[
                      //Mes Details
                      expandStyle(
                          1, //${AppLocalizations.of(context)!.detail}
                          widgetText(
                              '${items['LBL_MENU_04']} ', //
                              "details")),
                      expandStyle(0, widgetIconsNext()),
                    ],
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  if (valueKey != '' && valueKey != null) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Beneficiary()));
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45.0,
                  child: Row(
                    children: <Widget>[
                      expandStyle(
                          //Mes bénéficiaires
                          1,
                          widgetText('${AppLocalizations.of(context)!.bene}',
                              "clipboard")),
                      expandStyle(0, widgetIconsNext()),
                    ],
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  if (valueKey != '' && valueKey != null) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangePasswordScreen()));
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45.0,
                  child: Row(
                    children: <Widget>[
                      //Changer le mot de passe
                      expandStyle(
                          1, widgetText('${items['LBL_MENU_05']}', "lock")),
                      expandStyle(0, widgetIconsNext()),
                    ],
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChangeLanguageScreen()));
                  //   Navigator.of(context).pushNamed('/LanguageScreen');
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45.0,
                  // color: Colors.red,
                  child: Align(
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        expandStyle(
                            //Changer de langue
                            1,
                            widgetText('${AppLocalizations.of(context)!.lang}',
                                "Langusage")),
                        expandStyle(0, widgetIconsNext()),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  alignment: Alignment.center,
                  height: 45.0,
                  // color: Colors.red,
                  child: InkWell(
                    onTap: () {
                      launchUrl(Uri.parse('http://devtest1.familov.com/faq'));
                    },
                    child: Row(
                      children: <Widget>[
                        //Aider
                        expandStyle(
                            1,
                            widgetText('${AppLocalizations.of(context)!.help}',
                                "lock")),
                        expandStyle(0, widgetIconsNext()),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.white,
                splashColor: Colors.white,
                onTap: () {
                  if (valueKey != '' && valueKey != null) {
                    _showDialog(context);
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45.0,
                  child: Row(
                    children: <Widget>[
                      expandStyle(
                          1, widgetText('${items['LBL_MENU_06']}', 'log')),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
            ],
          )),
        ])
      ])
    ]);
  }

  showNotLoginPage() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            SizedBox(
              height: 150,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Image.asset('assets/images/splash_400.png'),
            ),
            SizedBox(
              height: 80,
            ),
            buttonWidget('Login', context, () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginScreen(
                            samePage: '',
                          )));
            }),
            SizedBox(
              height: 80,
            ),
            InkWell(
                onTap: () {
                  launchUrl(Uri.parse('https://familov.com/terms-conditions'));
                },
                child: Text('Terms & Condition')),
            SizedBox(
              height: 20,
            ),
            InkWell(
                onTap: () {
                  //
                  launchUrl(Uri.parse('https://familov.com/faq'));
                },
                child: Text('Support')),
            SizedBox(
              height: 20,
            ),
            InkWell(
                onTap: () {
                  launchUrl(Uri.parse('https://familov.com/faq'));
                },
                child: Text('FAQ')),
          ],
        ),
      ),
    );
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              // width: 233.0,
              // height: 22.0,
              child: Center(
            child: Text(
              'Logout',
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
              textAlign: TextAlign.right,
            ),
          )),
          content: new Text("Are you sure you want to logout?",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  InkWell(
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w600,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    onTap: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      await preferences.clear();
                      presenterclear!.clearCart();
                      delCart();
                      setState(() {
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => LoginScreen('yes')));
                        Navigator.of(context).pushNamed('/HomeScreen');
                      });
                    },
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  expandStyle(int flex, Widget child) => Expanded(flex: flex, child: child);

  widgetText(String name, String imagename) => new Container(
        margin: new EdgeInsets.only(left: 18.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Row(
            children: [
              Image.asset(
                "assets/images/${imagename}.png",
                fit: BoxFit.contain,
                width: 25,
                height: 25,
              ),
              Container(
                margin: new EdgeInsets.only(left: 24.0),
                child: Text(
                  name,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ),
      );

  widgetIconsNext() => new Container(
        margin: new EdgeInsets.only(right: 16.0),
        child: Image.asset(
          "assets/images/lessarrowblack.png",
          width: 24,
          height: 24,
        ),
      );

  @override
  void onClearCartError(String error) {
    // TODO: implement onClearCartError
  }

  @override
  void onClearCartSuccess(CartClear clearCart) {
    // TODO: implement onClearCartSuccess
  }
}
