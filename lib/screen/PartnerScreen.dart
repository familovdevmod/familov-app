import 'package:familov/widget/buttonWidget.dart';
import 'package:flutter/material.dart';

class PartnerScreen extends StatefulWidget {
  const PartnerScreen({Key? key}) : super(key: key);
  @override
  _PartnerScreenState createState() => _PartnerScreenState();
}

class _PartnerScreenState extends State<PartnerScreen> {
  final List<String> genderList = <String>[
    ("Select Gender"),
    ('Male'),
    ('Female')
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 80,
        automaticallyImplyLeading: true,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'For Partners',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Center(
                child: Text(
                  'Please fill below form to become a Partner',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 30,
                ),
                child: Text('First Name'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'My First Name',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  controller: null,
                  keyboardType: TextInputType.text,
                  validator: null,
                  onSaved: null,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('Last Name'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'My Last Name',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  controller: null,
                  keyboardType: TextInputType.text,
                  validator: null,
                  onSaved: null,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('Email'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'My Email',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  controller: null,
                  keyboardType: TextInputType.text,
                  validator: null,
                  onSaved: null,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 15,
                ),
                child: Text('Mobile Number'),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 18,
                    width: MediaQuery.of(context).size.width / 3.5,
                    child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            onChanged: (value) {},
                            //   hint: new Text("Select Gender"),
                            items: genderList.map((String user) {
                              return new DropdownMenuItem<String>(
                                value: user,
                                child: new Text(
                                  user,
                                  style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    fontSize: 10.0,
                                    color: const Color(0xFF111111),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        )),
                  ),
                  SizedBox(
                    width: 17,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 18,
                    width: MediaQuery.of(context).size.width / 1.80,
                    child: Container(
                      child: TextFormField(
                        decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(
                                color: Color(0xFF7F7F7F),
                                width: 0.5,
                              ),
                            ),
                            hintText: 'My mobile number',
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xffDA3C5F)),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 16),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                borderSide:
                                    BorderSide(color: Color(0xffDA3C5F)))),
                        controller: null,
                        keyboardType: TextInputType.emailAddress,
                        validator: null,
                        onSaved: null,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('Country'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 16,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Container(
                        margin: EdgeInsets.only(left: 15, right: 20),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            onChanged: (value) {},
                            hint: new Text(
                              "Country",
                            ),
                            items: genderList.map((String user) {
                              return new DropdownMenuItem<String>(
                                value: user,
                                child: new Text(
                                  user,
                                  style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    fontSize: 10.0,
                                    color: const Color(0xFF111111),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('City'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 16,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Container(
                        margin: EdgeInsets.only(left: 15, right: 20),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            onChanged: (value) {},
                            hint: new Text(
                              "City",
                            ),
                            items: genderList.map((String user) {
                              return new DropdownMenuItem<String>(
                                value: user,
                                child: new Text(
                                  user,
                                  style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    fontSize: 10.0,
                                    color: const Color(0xFF111111),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('Area'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'Area',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  controller: null,
                  keyboardType: TextInputType.text,
                  validator: null,
                  onSaved: null,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Text('Category'),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'Eg : category1,category2,... etc',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  controller: null,
                  keyboardType: TextInputType.text,
                  validator: null,
                  onSaved: null,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 14,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      child: new Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  'By submitting this form, you accept FAMILOVs',
                              style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  color: Color(0xff828282),
                                  fontWeight: FontWeight.w500),
                            ),
                            TextSpan(
                              text: ' Terms of use ',
                              style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  color: Color(0xff6020BD),
                                  fontWeight: FontWeight.w500),
                            ),
                            TextSpan(
                              text: 'and its ',
                              style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  color: Color(0xff828282),
                                  fontWeight: FontWeight.w500),
                            ),
                            TextSpan(
                              text: 'Privacy Policy',
                              style: TextStyle(
                                  fontFamily: 'Gilroy',
                                  color: Color(0xff6020BD),
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 25,
              ),
              buttonWidget('Get Started', context, () {}),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
