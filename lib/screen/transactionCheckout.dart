// ignore_for_file: deprecated_member_use

import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/model/stripecheckout.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:familov/presenter/stripeCheckout.dart';
import 'package:familov/screen/loginScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/model/checkout.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/summmaryy.dart';
import 'package:familov/presenter/checkout_presenter.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:im_stepper/stepper.dart';
import 'package:dotted_decoration/dotted_decoration.dart' as Shape;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
import '../presenter/getCart_Presenter.dart';
import '../presenter/summary_presenter.dart';

class TransactionCheckOut extends StatefulWidget {
  final String? imageRecharge,
      skuCode,
      data1,
      data2,
      mobile,
      code,
      price,
      maximumSendCurrencyIso,
      currencySymbol,
      rate;

  TransactionCheckOut({
    Key? key,
    required this.imageRecharge,
    required this.skuCode,
    required this.data1,
    required this.data2,
    required this.mobile,
    required this.code,
    required this.price,
    required this.maximumSendCurrencyIso,
    required this.currencySymbol,
    required this.rate,
  }) : super(key: key);

  @override
  _TransactionCheckOutState createState() => _TransactionCheckOutState();
}

class _TransactionCheckOutState extends State<TransactionCheckOut>
    implements
        CheckoutContract,
        GetCartContract,
        SummaryContract,
        SetCartContract,
        StripeCheckoutContract {
  int activeStep = 0;
  var CheckStripe;
  List<Data90> statusList = [];

  var cases = 0;

  CheckoutPresenter? presenter;
  GetCartPresenter? presenter1;
  SummaryPresenter? presenter4;
  SetCartPresenter? _setCartPresenter;

  StripeCheckoutPresenter? _stripeCheckoutPresenter;

  _TransactionCheckOutState() {
    presenter = new CheckoutPresenter(this);
    presenter1 = new GetCartPresenter(this);
    presenter4 = new SummaryPresenter(this);
    _setCartPresenter = new SetCartPresenter(this);
    _stripeCheckoutPresenter = new StripeCheckoutPresenter(this);
  }
  var idx;
  Data12? list1;
  var charge;
  final myController1 = TextEditingController();

  // var price;

  @override
  void initState() {
    print('hhhhhhhhhhhheeeeeeeeeee ');
    print(widget.price!.toString());
    print(widget.rate.toString());
    // print(double.parse(widget.price!.toString())*double.parse(widget.rate.toString()));
    // price=double.parse('${double.parse(widget.price!.toString())*double.parse(widget.rate.toString())}').toStringAsFixed(2);
    tokenCheck();
    presenter1!.getCart();
    presenter!.checkout();
    presenter4!.summary('', '');

    super.initState();
  }

  String? valueKey;

  tokenCheck() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    if (valueKey != '' && valueKey != null) {
      print('tokenCheckIs :  ${valueKey.toString()}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          bottomNavigationBar: FlatButton(
            height: 48,
            minWidth: MediaQuery.of(context).size.width,
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            color: Color(0xff00DBA7),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Continue',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                )
              ],
            ),
            onPressed: () async {
              if (valueKey != null) {
                cases++;
                if (cases == 1) {
                  print("Beforesetcart ");
                  _setCartPresenter!.setCart('', '', '', widget.code.toString(),
                      widget.mobile.toString(), widget.skuCode.toString(), 'r');
                  activeStep = cases;

                  print('case is : $cases  and $activeStep');
                  setPreference();
                }
                if (cases == 2) {
                  if (myController1.text.isNotEmpty) {
                    activeStep = cases;
                  } else {
                    BotToast.showSimpleNotification(
                        backgroundColor: AppColors().toastsuccess,
                        title: "Write beneficiary name");
                  }
                  print('case is :2 $cases  and $activeStep');
                }
                if (cases == 3) {
                  print("Beforesetcart ");
                  finalcheckout();
                  print("Beforesetcart ");
                }
                print("casssssse $cases amd $activeStep");
                setState(() {});
              } else {
                _showDialog(context);
              }
            },
          ),
          backgroundColor: Colors.white,
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            toolbarHeight: 80,
            automaticallyImplyLeading: true,
            centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 0,
            title: Text(
              'Checkout',
              style: TextStyle(
                fontFamily: 'Gilroy',
                fontSize: 25,
                fontWeight: FontWeight.w500,
                color: Color(0xffF93E6C),
              ),
            ),
          ),
          body: SingleChildScrollView(
              child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 80,
                          child: NumberStepper(
                            enableStepTapping:
                                myController1.text == '' ? false : true,
                            lineColor:
                                Color.fromRGBO(0, 219, 167, 1).withOpacity(.4),
                            //   activeStep == 0 ? Colors.green : Colors.grey,
                            lineLength: 100,
                            alignment: Alignment.center,
                            stepColor: Colors.grey.shade200,
                            activeStepColor: Color.fromRGBO(0, 219, 167, 1),
                            activeStepBorderColor: Colors.grey,
                            direction: Axis.horizontal,
                            stepRadius: 19,
                            numbers: [
                              1,
                              2,
                              3,
                            ],
                            lineDotRadius: 2,
                            enableNextPreviousButtons: false,
                            scrollingDisabled: true,
                            activeStep: activeStep,
                            onStepReached: (index) {
                              setState(() {
                                activeStep = index;
                                print('step $activeStep');
                              });
                            },
                          ),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 12,
                            ),
                            Text(
                              'Transaction \nDetails',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: activeStep != 0
                                      ? Color.fromRGBO(130, 130, 130, 1)
                                      : Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            Spacer(),
                            Text(
                              'Beneficiaries \nInformation',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: activeStep != 1
                                      ? Color.fromRGBO(130, 130, 130, 1)
                                      : Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            Spacer(),
                            Text(
                              'Mode of \nPayment',
                              style: TextStyle(
                                  color: activeStep != 2
                                      ? Color.fromRGBO(130, 130, 130, 1)
                                      : Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                          ],
                        ),

                        // Row(
                        //   children: [
                        //     SizedBox(
                        //       width: 12,
                        //     ),
                        //     Text(
                        //       'Transaction \nDetails',
                        //       textAlign: TextAlign.center,
                        //       style: TextStyle(
                        //           color: activeStep == 0
                        //               ? Color.fromRGBO(96, 32, 189, 1)
                        //               : Color.fromRGBO(130, 130, 130, 1),
                        //           fontFamily: 'Gilroy',
                        //           fontSize: 12,
                        //           letterSpacing: 0,
                        //           fontWeight: FontWeight.normal,
                        //           height: 1.5),
                        //     ),
                        //     Spacer(),
                        //     Text(
                        //       'Beneficiaries \nInformation',
                        //       textAlign: TextAlign.center,
                        //       style: TextStyle(
                        //           color: activeStep == 1
                        //               ? Color.fromRGBO(96, 32, 189, 1)
                        //               : Color.fromRGBO(130, 130, 130, 1),
                        //           fontFamily: 'Gilroy',
                        //           fontSize: 12,
                        //           letterSpacing: 0,
                        //           fontWeight: FontWeight.normal,
                        //           height: 1.5),
                        //     ),
                        //     Spacer(),
                        //     Text(
                        //       'Mode of \nPayment',
                        //       textAlign: TextAlign.center,
                        //       style: TextStyle(
                        //           color: activeStep == 2
                        //               ? Color.fromRGBO(96, 32, 189, 1)
                        //               : Color.fromRGBO(130, 130, 130, 1),
                        //           fontFamily: 'Gilroy',
                        //           fontSize: 12,
                        //           letterSpacing: 0,
                        //           fontWeight: FontWeight.normal,
                        //           height: 1.5),
                        //     ),
                        //     SizedBox(
                        //       width: 40,
                        //     ),
                        //   ],
                        // ),

                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                selected(),
                              ],
                            ),
                          ),
                        ),
                      ])))),
    );
  }

  selected() {
    switch (activeStep) {
      case 0:
        cases = 0;
        return Container(
            child: Column(
          children: [
            Container(
                // margin: EdgeInsets.only(left: 0, right: 0, bottom: 15),
                // width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height / 6,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    topRight: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                    bottomRight: Radius.circular(4),
                  ),
                  border: Border.all(
                    color: Color.fromRGBO(189, 189, 189, 1),
                    width: 0.5,
                  ),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 5),
                                width: 80,
                                height: 60,
                                alignment: Alignment.topLeft,
                                child: Image.network(
                                    widget.imageRecharge.toString()
                                    // 'assets/images/airtel.png',
                                    )),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              // mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${widget.data1}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.w600,
                                      height: 1.5),
                                ),
                                SizedBox(
                                  child: Text(
                                    '${widget.data2}',
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 18,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                                ),
                                Text(
                                  'Mob : +${widget.code}-${widget.mobile}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ])),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: Shape.DottedDecoration(
                shape: Shape.Shape.line,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 16, top: 25),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Order Details',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(51, 51, 51, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          letterSpacing:
                              0 /*percentages not used in flutter. defaulting to zero*/,
                          fontWeight: FontWeight.w600,
                          height: 1.5 /*PERCENT not supported*/
                          ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Sub Total',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 14,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 18, right: 16),
                          child: Text(
                            '${widget.currencySymbol} ${double.parse('${double.parse(widget.price!.toString()) * double.parse(widget.rate.toString())}').toStringAsFixed(2)}',
                            style: GoogleFonts.montserrat(
                                fontSize: 14,
                                color: AppColors().darkgrey,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                    charge != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Service Charges',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing:
                                        0 /*percentages not used in flutter. defaulting to zero*/,
                                    fontWeight: FontWeight.w600,
                                    height: 1.5 /*PERCENT not supported*/
                                    ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10, right: 16),
                                child: Text(
                                  '${widget.currencySymbol} ${double.parse('${double.parse(charge!.toString()) * double.parse(widget.rate.toString())}').toStringAsFixed(2)}',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      color: AppColors().darkgrey,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        : Container(
                            height: 0,
                          ),
                    Divider(
                        color: AppColors().dividercolor.withOpacity(1),
                        thickness: 2.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total Price',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 10, right: 16, bottom: 10),
                          child: Text(
                            charge == null
                                ? '${widget.currencySymbol} ${double.parse('${double.parse(widget.price!.toString()) * double.parse(widget.rate.toString())}').toStringAsFixed(2)}'
                                : '${widget.currencySymbol} ${(double.parse('${(double.parse(widget.price!.toString()) * double.parse(widget.rate.toString())) + (double.parse(charge!.toString()) * double.parse(widget.rate.toString()))}').toStringAsFixed(2))}',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                color: Color.fromRGBO(79, 79, 79, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 16,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w600,
                                height: 1.5),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
      case 1:
        cases = 1;
        return Container(
            child: Column(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
              top: 25,
            ),
            child: Text(
              'Name of Beneficiary',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
          ),
          Container(
              margin: EdgeInsets.only(
                top: 5,
              ),
              child: TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                      ),
                      hintText: 'Name of Beneficiary',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffDA3C5F)),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                  keyboardType: TextInputType.emailAddress,
                  controller: myController1,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Beneficiary is empty!';
                    }
                    return null;
                  },
                  onSaved: (value) {}))
        ]));

      case 2:
        cases = 2;
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Easy isn\'t it? Choose the payment method that suits you, accept the terms of service and pay securely.',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(130, 130, 130, 1),
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                    letterSpacing: 0,
                    fontWeight: FontWeight.normal,
                    height: 1.5),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10),
                alignment: Alignment.center,
                child: Text(
                  'Select a payment ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.w600,
                      height: 1.5),
                ),
              ),
              // SingleChildScrollView(
              //     scrollDirection: Axis.horizontal,
              //     child: Container(
              //       margin: EdgeInsets.only(top: 15),
              //       height: 220,
              //       width: MediaQuery.of(context).size.width/.2,
              //       child: Row(children: [
              //       ]),
              //     ))

              SizedBox(
                height: 220,
                child: GridView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: statusList.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 0,
                    mainAxisSpacing: 1,
                    childAspectRatio: 0.95,
                  ),
                  itemBuilder: (context, index) {
                    return InkWell(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            topRight: Radius.circular(8),
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                          border: Border.all(
                            color: idx == index ? Colors.red : Colors.black45,
                            width: 1,
                          ),
                        ),
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        child: Card(
                            shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.circular(1.0),
                            ),
                            child: Image.network(
                              '${statusList[index].pmImage}',
                              errorBuilder: (context, error, stackTrace) {
                                return Container(
                                  margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    'assets/images/fami.png',
                                    scale: 1,
                                    alignment: Alignment.center,
                                  ),
                                );
                              },
                            )),
                      ),
                      onTap: () {
                        setState(() {
                          print('Slug $index');
                          idx = index;
                          CheckStripe = statusList[idx].pmSlug;
                        });
                      },
                    );
                  },
                ),
              )
            ],
          ),
        );
    }
  }

  @override
  void onCheckoutError(String error) {}

  @override
  void onCheckoutSuccess(Checkout checkoutModel) {
    statusList = checkoutModel.data!;
    setState(() {});
  }

  @override
  void onGetCartError(String error) {}

  @override
  void onGetCartSuccess(GetCart getCartModel) {}

  @override
  void onSummaryError(String error) {}

  @override
  void onSummarySuccess(Summmaryy summaryModel) {
    print('qqqqqqqqqqqqqqqqqqq ');
    list1 = summaryModel.data;
    print('qqqqqqqqqqqqqqqqqqq ${list1!.serviceCharge}');

    if (summaryModel.status == true) {
      charge = list1!.serviceCharge;
      print('service $charge');
    }
    setState(() {});
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              // width: 233.0,
              // height: 22.0,
              child: Center(
                  child: Text(
            'Login or Register',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color.fromRGBO(249, 62, 108, 1),
                fontFamily: 'Gilroy',
                fontSize: 20,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1.5 /*PERCENT not supported*/
                ),
          ))),
          content: Text(
            'Please login or register to continue your purchase. ',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color.fromRGBO(130, 130, 130, 1),
                fontFamily: 'Gilroy',
                fontSize: 16,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1.5 /*PERCENT not supported*/
                ),
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      // Navigator.of(context).pushReplacementNamed(
                      //   '/loginScreen',
                      // );
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen(
                                    samePage: 'recharge',
                                  ))).then((value) {
                        print("sadsa");
                        tokenCheck();
                        presenter!.checkout();
                      });
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                        // border: Border.all(
                        //   width: 1.0,
                        // ),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                            letterSpacing: 0,
                            fontWeight: FontWeight.w600,
                            height: 1),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onSetCartError(String error) {
    // TODO: implement onSetCartError
  }

  @override
  Future<void> onSetCartSuccess(SetCart setCartModel) async {
    // TODO: implement onSetCartSuccess
    print("setCartSuccess");
    if (setCartModel.status == true) {
      // Fluttertoast.showToast(
      //   msg: setCartModel.message.toString(),
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM,
      //   backgroundColor: Colors.black45,
      //   fontSize: 18,
      // );
      BotToast.showSimpleNotification(
          backgroundColor: Color(0xff00DBA7),
          title: setCartModel.message.toString());
    }
  }

  @override
  void onStripeCheckoutError(String error) {
    print('${error}');
    // TODO: implement onStripeCheckoutError
  }

  @override
  void onStripeCheckoutSuccess(Stripecheckout stripeCheckout) {
    // TODO: implement onStripeCheckoutSuccess
    print('onStripeCheckoutSuccess ${stripeCheckout.status}');
    print('onStripeCheckoutSuccess ${stripeCheckout.data!.checkoutUrl}');

    launch('${stripeCheckout.data!.checkoutUrl}');
  }

  finalcheckout() async {
    print('From here' +
        '' +
        '$CheckStripe' +
        '' +
        '${myController1.text}' +
        '+' +
        widget.code.toString() +
        ' ' +
        widget.mobile.toString() +
        '+' +
        widget.code.toString() +
        ' ' +
        widget.mobile.toString() +
        '{}' +
        '' +
        '' +
        '' +
        'true');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var currency = preferences.getString('currency')!.split(' ')[1];
    print('CheckStripe=$CheckStripe, Currency= $currency');
    _stripeCheckoutPresenter!.stripeCheckout(
        '',
        '',
        '$CheckStripe',
        '',
        '${myController1.text}',
        '+' + widget.code.toString() + ' ' + widget.mobile.toString(),
        '+' + widget.code.toString() + ' ' + widget.mobile.toString(),
        currency,
        '',
        '',
        '',
        'true');
  }

  void setPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('myShopID', '179');
  }
}
