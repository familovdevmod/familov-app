import 'package:bot_toast/bot_toast.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:familov/presenter/beneficiary_presenter.dart';
import 'package:familov/screen/transactionDetailScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/getBottomButton.dart';
import 'package:familov/model/addBeneficiary_Model.dart';
import 'package:familov/presenter/addBeneficiary_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:fluttertoast/fluttertoast.dart';

class BeneficiaryAddScreen extends StatefulWidget {
  String samePage = '';
  BeneficiaryAddScreen(this.samePage, {Key? key}) : super(key: key);
  @override
  _BeneficiaryAddScreenState createState() => _BeneficiaryAddScreenState();
}

UserBeneficiaryPresenter? presenters;

class _BeneficiaryAddScreenState extends State<BeneficiaryAddScreen>
    implements AddBeneficiaryScreenContract {
  final GlobalKey<FormState> _formKey = GlobalKey();

  AddBeneficiaryScreenPresenter? _presenter;
  var code1 = '+91';
  var code2 = '+91';

  _BeneficiaryAddScreenState() {
    _presenter = new AddBeneficiaryScreenPresenter(this);
  }

  // ignore: unused_field
  var _getBeneficiary = Data_list(
    recipientInformation: '',
    recipientPhone: '',
    anotherPhoneNumber: '',
  );

  Future<void> _saveForm() async {
    print('${widget.samePage}----here');
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState!.save();

    _presenter!.addBeneficiary(
      // 'recpName',
      // 'phoneCode',
      // 'phoneNumber',
      // 'anotherPhoneCode',
      // 'anotherPhoneNumber',
      _getBeneficiary.recipientInformation!,
      code1,
      _getBeneficiary.recipientPhone!,
      code2,
      _getBeneficiary.anotherPhoneNumber!,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: getBottomButton('Add Beneficiary', () {
        if (widget.samePage.isNotEmpty) {
          _saveForm();

          Navigator.of(context).pop('yes');

          // Navigator.pushReplacement(
          //     context,
          //     MaterialPageRoute(
          //         builder: (BuildContext context) =>
          //             new TransactionDetailScreen(
          //               address: '',
          //               promo: '',
          //             )));
        } else {
          _saveForm();
        }
      }),
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Add Beneficiary',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(249, 62, 108, 1),
                fontFamily: 'Gilroy',
                fontSize: 20,
                letterSpacing: 0,
                fontWeight: FontWeight.normal,
                height: 1),
          )),
      body: Form(
        key: _formKey,
        child: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 16, left: 16, right: 16),
                child: Text(
                  'Please enter the full name of the recipient as indicated on his CNI.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 32, left: 16, right: 16),
                child: Text(
                  'Add Beneficiary Name',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color.fromRGBO(77, 77, 77, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 16, right: 16),
                child: TextFormField(

                    //   initialValue: _initValues['username'],
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                        ),
                        hintText: 'Add Beneficiary Name',
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffDA3C5F)),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 16,
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                    keyboardType: TextInputType.text,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[A-Za-z]')),
                    ],
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Name is empty!';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      print('text1 : $value');
                      _getBeneficiary = Data_list(
                          anotherPhoneNumber:
                              _getBeneficiary.anotherPhoneNumber,
                          // createdAt: _getBeneficiary.createdAt,
                          //  customerId: _getBeneficiary.customerId,
                          recipientInformation: value,
                          recipientPhone: _getBeneficiary.recipientPhone);
                      // postalCode: _editedProduct.postalCode,
                    }),
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 30, left: 16, right: 16),
                  child: Text(
                    'Main Number',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Color.fromRGBO(77, 77, 77, 1),
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        letterSpacing:
                            0 /*percentages not used in flutter. defaulting to zero*/,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Container(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // height: MediaQuery.of(context).size.height / 17.35,
                          width: MediaQuery.of(context).size.width / 3.5,
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: Color(0xFF7F7F7F),
                                width: 0.5,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          child: Container(
                            child: CountryCodePicker(
                              padding: EdgeInsets.all(1),
                              // ignore: avoid_print
                              onChanged: (e) => code1 = e.dialCode.toString(),
                              //print(e.dialCode),
                              initialSelection: 'IN',
                              showCountryOnly: false,
                              showOnlyCountryWhenClosed: false,
                              favorite: const ['+39', 'FR'],
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 18,
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height / 17.35,
                          width: MediaQuery.of(context).size.width / 1.75,
                          child: Container(
                              child: TextFormField(
                            maxLength: 15,
                            keyboardType: TextInputType.number,
                            // initialValue: _initValues['phone_number'],
                            decoration: InputDecoration(
                                counterText: "",
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  borderSide: BorderSide(
                                    color: Color(0xFF7F7F7F),
                                    width: 0.5,
                                  ),
                                ),
                                hintText: 'My mobile number',
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xffDA3C5F)),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8))),
                                contentPadding: EdgeInsets.symmetric(
                                  horizontal: 16,
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    borderSide:
                                        BorderSide(color: Color(0xffDA3C5F)))),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]')),
                            ],
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Mobile number is empty!';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              print('text1 : $value');
                              _getBeneficiary = Data_list(
                                  anotherPhoneNumber:
                                      _getBeneficiary.anotherPhoneNumber,
                                  // createdAt: _getBeneficiary.createdAt,
                                  //  customerId: _getBeneficiary.customerId,
                                  recipientInformation:
                                      _getBeneficiary.recipientInformation,
                                  recipientPhone: value);
                              // postalCode: _editedProduct.postalCode,
                            },
                          )),
                        ),
                      ]),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 30, right: 16, left: 16),
                  child: Text(
                    'Other Number',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Color.fromRGBO(77, 77, 77, 1),
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        // height: MediaQuery.of(context).size.height / 17.35,
                        width: MediaQuery.of(context).size.width / 3.5,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        child: CountryCodePicker(
                          padding: EdgeInsets.all(1),
                          // ignore: avoid_print
                          onChanged: (e) => code2 = e.dialCode.toString(),
                          //print(e.toLongString()),
                          initialSelection: 'IN',
                          showCountryOnly: false,
                          showOnlyCountryWhenClosed: false,
                          favorite: const ['+39', 'FR'],
                        ),
                      ),
                      SizedBox(
                        width: 18,
                      ),
                      Container(
                        // height: MediaQuery.of(context).size.height / 17.35,
                        width: MediaQuery.of(context).size.width / 1.75,
                        child: Container(
                            child: TextFormField(
                          maxLength: 15,

                          // initialValue: _initValues['phone_number'],
                          decoration: InputDecoration(
                              counterText: "",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide(
                                  color: Color(0xFF7F7F7F),
                                  width: 0.5,
                                ),
                              ),
                              hintText: 'My mobile number',
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F)),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              contentPadding: EdgeInsets.symmetric(
                                horizontal: 16,
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  borderSide:
                                      BorderSide(color: Color(0xffDA3C5F)))),

                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          ],
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Mobile number is empty!';
                            }
                          },
                          onSaved: (value) {
                            print('text1 : $value');
                            _getBeneficiary = Data_list(
                                anotherPhoneNumber: value,
                                // createdAt: _getBeneficiary.createdAt,
                                //  customerId: _getBeneficiary.customerId,
                                recipientInformation:
                                    _getBeneficiary.recipientInformation,
                                recipientPhone: _getBeneficiary.recipientPhone);
                            // postalCode: _editedProduct.postalCode,
                          },
                        )),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onaddBeneficiaryError(String error) {
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toasterror, title: "$error");
    // Fluttertoast.showToast(
    //   msg: '$error',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
  }

  @override
  void onaddBeneficiarySuccess(AddBeneficiary addBeneficiarymodel) {
    // Fluttertoast.showToast(
    //   msg: '${addBeneficiarymodel.message}',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toastsuccess,
        title: "${addBeneficiarymodel.message}");
    Navigator.of(context).pushNamed('/BeneficiaryScreen');
  }
}
