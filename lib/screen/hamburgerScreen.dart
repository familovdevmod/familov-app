import 'package:familov/screen/web.dart';
import 'package:familov/model/currency_model.dart';
import 'package:familov/presenter/currency_presenter.dart';
import 'package:familov/widget/locale_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Handburger extends StatefulWidget {
  const Handburger({Key? key}) : super(key: key);
  @override
  _HandburgerState createState() => _HandburgerState();
}

class _HandburgerState extends State<Handburger> implements CurrencyContract {
  late CurrencyPresenter _presenter;
  List<Data_list> currencyValueList = [];
  bool currencyDropDown = false, selectCurrency = false;
  var languagedropdown = false;
  var _currencySelectedController = '\$ USD';
  var _currencySymbol = '';
  var rate;

  _HandburgerState() {
    _presenter = new CurrencyPresenter(this);
  }
  var languageval;
  late File jsonFile;
  late Directory dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  var fileContent;
  var items;
  bool currencychecks = true;

  @override
  void initState() {
    sharedP();
    getApplicationDocumentsDirectory().then((Directory directory) async {
      dir = directory;
      jsonFile = new File(dir.path + "/" + fileName);
      fileExists = jsonFile.existsSync();

      if (fileExists) {
        setState(() {
          final data = json.decode(jsonFile.readAsStringSync());
          dynamic _items = data;
          items = jsonDecode(_items);
          print('items data value ${items['MSG_PRODUCT_DETAIL_04']}');
        });
      }
    });
    //  BotToast.showLoading(duration: Duration(seconds: 2));
    print('hamburger');
    _presenter.getCurrency();
    pref('');
    super.initState();
  }

  // String? checkCurrency = '\$ USD';
  String? checkCurrency = '';
  String? rate1 = '1';
  var currentlanguage = '';
  sharedP() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    currentlanguage = preferences.getString('mylang')!;

    preferences.setString('currencySymbol', _currencySymbol);
    preferences.setString('rate', rate);
    checkCurrency = preferences.getString('currency');
    setState(() {});
  }

  chnagecurrency() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('currency', _currencySelectedController);
    preferences.setString('currency', _currencySelectedController);
    checkCurrency = preferences.getString('currency');

    setState(() {
      currencyDropDown = false;
    });
  }

  sharedP0() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // preferences.setString('currency', _currencySelectedController);
    checkCurrency = preferences.getString('currency');
    rate1 = preferences.getString('rate');

    if (checkCurrency == null) {
      preferences.setString('currency', '\$ EUR');
      preferences.setString('rate', '1');

      checkCurrency = preferences.getString('currency');
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LocaleProvider>(context);
    var locale = provider.locale ?? Locale('en');
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // foregroundColor: Colors.transparent,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.close)),
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 80,
        automaticallyImplyLeading: true,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Container(
            height: 33, child: Image.asset('assets/images/familovlogo.png')),
        actions: [
          // IconButton(
          //     icon: GestureDetector(
          //       onLongPress: () {
          //         Navigator.push(context,
          //             MaterialPageRoute(builder: (context) => TestWidget()));
          //       },
          //       child: Icon(
          //         Icons.notifications_none_outlined,
          //         color: Colors.black54,
          //         size: 24,
          //       ),
          //     ),
          //     onPressed: () {}),
        ],
      ),
      body: SingleChildScrollView(
        physics: currencyDropDown
            ? BouncingScrollPhysics()
            : NeverScrollableScrollPhysics(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              // color: Color.fromRGBO(255, 255, 255, 1),
              ),
          child: Column(
            children: <Widget>[
              // Container(
              //   padding: EdgeInsets.only(top: 20),
              //   child: Row(
              //     children: [
              //       GestureDetector(
              //         child: Container(
              //             padding: EdgeInsets.only(left: 20),
              //             child: Icon(Icons.close)),
              //         onTap: () {
              //           Navigator.pop(context);
              //         },
              //       ),
              //       Center(
              //         child: Container(
              //           alignment: Alignment.center,
              //           width: MediaQuery.of(context).size.width / 1.3,
              //           height: 50,
              //           decoration: BoxDecoration(
              //             image: DecorationImage(
              //               image: AssetImage(
              //                 'assets/images/familovlogo.png',
              //               ),
              //             ),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),

              Divider(color: Color.fromRGBO(189, 189, 189, 1), thickness: 0.5),
              Container(
                margin: EdgeInsets.only(left: 32, right: 24, top: 50),
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            currencyDropDown = !currencyDropDown;
                          });
                        },
                        child: Row(
                          children: [
                            currencychecks == true
                                ? Row(
                                    children: [
                                      Text(
                                        '${AppLocalizations.of(context)!.currency} : ',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 16,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                      Text(
                                        '\$ USD',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                    ],
                                  )
                                : Text(
                                    _currencySelectedController == ''
                                        ? '${AppLocalizations.of(context)!.currency} : \$ USD'
                                        : '${AppLocalizations.of(context)!.currency} :',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),

                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Text(
                                checkCurrency!,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1),
                              ),
                            ),
                            // SizedBox(
                            //   width: MediaQuery.of(context).size.width / 2.15,
                            // ),
                            Container(
                              height: 25,
                              child: currencyDropDown
                                  ? Image.asset(
                                      'assets/images/uparrow.png',
                                      scale: 4,
                                    )
                                  : Image.asset(
                                      'assets/images/sidearrow.png',
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    currencyDropDown
                        ? ListView.builder(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: currencyValueList.length,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: EdgeInsets.only(top: 10, bottom: 5),
                                child: GestureDetector(
                                  onTap: () {
                                    setState(
                                      () {
                                        _currencySelectedController =
                                            '${currencyValueList[index].vSymbol} ${currencyValueList[index].vName}';
                                        _currencySymbol =
                                            currencyValueList[index].vSymbol!;
                                        selectCurrency = true;
                                        chnagecurrency();
                                        rate = currencyValueList[index].rate;
                                        print(
                                            'currency11 $checkCurrency  and rate :  $rate');
                                      },
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 250,
                                        child: Text(
                                          '${currencyValueList[index].vSymbol} ${currencyValueList[index].vName}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          )
                        : Container(),
                    InkWell(
                      onTap: () {
                        setState(() {
                          languagedropdown = !languagedropdown;
                        });
                      },
                      child: Row(
                        children: [
                          Container(
                              margin: EdgeInsets.only(),
                              child: Text(
                                //   'Stories',
                                'Language : $currentlanguage',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 16,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1),
                              )),
                          Spacer(),
                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              height: 25,
                              child: languagedropdown
                                  ? Image.asset(
                                      'assets/images/uparrow.png',
                                      scale: 4,
                                    )
                                  : Image.asset(
                                      'assets/images/sidearrow.png',
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    languagedropdown == true
                        ? Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    // color: Colors.red,
                                    width: 250,
                                    child: GestureDetector(
                                      onTap: () {
                                        english(locale);
                                      },
                                      child: Text(
                                        '• English',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Container(
                                    // color: Colors.red,
                                    width: 250,
                                    child: GestureDetector(
                                      onTap: () {
                                        french(locale);
                                      },
                                      child: Text(
                                        '• French',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        : Container(),

                    Container(
                        margin: EdgeInsets.only(
                          top: 32,
                        ),
                        child: Text(
                          //   'Stories',
                          '${items['FOOTER_MENU_STORIES']}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 16,
                        ),
                        child: Text(
                          '${items['FOOTER_MENU_PRESS']}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w500,
                              height: 1),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Web(
                                      name: 'Press / News',
                                      web: 'http://apptest.familov.com/press',
                                    )));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 32,
                        ),
                        child: Text(
                          '${AppLocalizations.of(context)!.faq}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Web(
                                      name: 'FAQ',
                                      web: 'http://apptest.familov.com/faq',
                                    )));
                      },
                    ),
                    // Container(
                    //   margin: EdgeInsets.only(
                    //     top: 16,
                    //   ),
                    //   child: Row(
                    //     children: [
                    //       GestureDetector(
                    //         child: Text(
                    //           '${AppLocalizations.of(context)!.friend}',
                    //           textAlign: TextAlign.left,
                    //           style: TextStyle(
                    //               color: Color.fromRGBO(79, 79, 79, 1),
                    //               fontFamily: 'Gilroy',
                    //               fontSize: 16,
                    //               letterSpacing: 0,
                    //               fontWeight: FontWeight.normal,
                    //               height: 1),
                    //         ),
                    //         onTap: () {
                    //           Navigator.of(context).pushNamed('/Friend');
                    //         },
                    //       ),
                    //       SizedBox(
                    //         width: 15,
                    //       ),
                    //       Text(
                    //         '£ 3.72',
                    //         style: TextStyle(
                    //             color: Color.fromRGBO(0, 219, 167, 1),
                    //             fontFamily: 'Gilroy',
                    //             fontSize: 16,
                    //             letterSpacing: 0,
                    //             fontWeight: FontWeight.w600,
                    //             height: 1),
                    //       )
                    //     ],
                    //   ),

                    // ),

                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 16,
                        ),
                        child: Text(
                          '${items['FOR_PATNERS']}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pushNamed('/Partner');
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 32,
                        ),
                        child: Text(
                          '${AppLocalizations.of(context)!.term}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Web(
                                      name: 'Term of use',
                                      web:
                                          'http://apptest.familov.com/terms-conditions',
                                    )));
                      },
                    ),
                    GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 16,
                          ),
                          child: Text(
                            '${AppLocalizations.of(context)!.privacy}',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(79, 79, 79, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 16,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Web(
                                        name:
                                            '${items['LBL_PRIVACY_POLICY_01']}',
                                        web:
                                            'http://apptest.familov.com/privacy-policy',
                                      )));
                        }),
                    Container(
                      margin: EdgeInsets.only(
                        top: 32,
                      ),
                      child: GestureDetector(
                        child: Text(
                          '${AppLocalizations.of(context)!.followus}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                        onTap: () {},
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 12),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              launchUrl(
                                Uri.parse(
                                    'https://www.facebook.com/Familover/'),
                                mode: LaunchMode.externalNonBrowserApplication,
                              );
                            },
                            child: Container(
                              width: 26,
                              height: 20,
                              child: Image.asset(
                                'assets/images/facebook.png',
                                scale: 4,
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              launchUrl(
                                Uri.parse('https://twitter.com/Familov_world'),
                                mode: LaunchMode.externalNonBrowserApplication,
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(left: 12),
                              width: 26,
                              height: 30,
                              child: Image.asset(
                                'assets/images/twitter.png',
                                scale: 4,
                              ),
                            ),
                          ),
                          Container(
                            width: 26,
                            height: 30,
                            margin: EdgeInsets.only(left: 12),
                            child: Image.asset(
                              'assets/images/linkedin.png',
                              scale: 5,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onCurrencyError(String error) {}

  @override
  void onCurrencySuccess(CurrencyModel currencyList) {
    sharedP0();
    //  print('currencyList ${currencyList.data!.dataList![0].vName}');
    currencyValueList = currencyList.data!.dataList!;
    setState(() {
      currencychecks = false;
    });
  }

  pref(local) async {
    print('locale $local');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (local != null && local != '') {
      preferences.setString('languages', local.toString());
    } else {
      preferences.setString('languages', 'en');
    }
    setState(() {});
  }

  void english(locale) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    languageval = preferences.setInt('Languageinteger', 1);
    preferences.setString('mylang', 'English');

    // isClicked = false;
    locale = Locale('en');
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    print('Local v :  $locale');
    provider.setLocale(locale);
    pref(locale);
    setState(() {
      languagedropdown = !languagedropdown;
      sharedP();
    });
  }

  void french(locale) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    languageval = preferences.setInt('Languageinteger', 2);
    preferences.setString('mylang', 'French');
    locale = Locale('fr');
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    print('Local v :  $locale');
    provider.setLocale(locale);
    pref(locale);
    setState(() {
      languagedropdown = !languagedropdown;
      sharedP();
    });
  }
}
