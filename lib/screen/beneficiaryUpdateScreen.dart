import 'package:bot_toast/bot_toast.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:familov/screen/beneficiaryScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/getBottomButton.dart';
import 'package:familov/model/updateBeneficiaryModel.dart';
import 'package:familov/presenter/updateBeneficiary_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:fluttertoast/fluttertoast.dart';

class BeneficiaryUpdateScreen extends StatefulWidget {
  const BeneficiaryUpdateScreen({Key? key}) : super(key: key);
  @override
  _BeneficiaryAddScreenState createState() => _BeneficiaryAddScreenState();
}

class _BeneficiaryAddScreenState extends State<BeneficiaryUpdateScreen>
    with WidgetsBindingObserver
    implements UpdateBeneficiaryScreenContract {
  final GlobalKey<FormState> _formKey = GlobalKey();

  BeneficiaryUpdateScreenPresenter? _presenter;
  String code1 = '+1684';
  String code2 = '+1684';

  _BeneficiaryAddScreenState() {
    _presenter = new BeneficiaryUpdateScreenPresenter(this);
  }
  @override
  void dispose() {
    print('dispose');

    super.dispose();
  }

  var Id;

  var _getBeneficiary = Data_list(
    recipientInformation: '',
    recipientPhone: '',
    anotherPhoneNumber: '',
  );

  Future<void> _saveForm() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState!.save();
    print(code1);
    print(code2);

    _presenter!.beneficiaryUpdates(
      Id,
      _getBeneficiary.recipientInformation!,
      code1,
      _getBeneficiary.recipientPhone!,
      code2,
      _getBeneficiary.anotherPhoneNumber!,
    );
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   print('AppLifecycleState ${state}');
  //   if (state == AppLifecycleState.resumed) {
  //     print('AppLifecycleState resumed');
  //     setState(() {});
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    // print('${arguments['name']}');
    String var1 = arguments['mainNumber'];
    List<String> var2 = var1.split(" ");
    //  print('var1 at index 1 is ${var2[1]}');
    // print('var1 at index 0 is ${var2[0]}');
    String num1 = var2[1];
    code1 = var2[0];

    Id = arguments['id'];
    //  print('Ids is : ${Id}');

    String var3 = arguments['otherNumber'];
    List<String> var4 = var3.split(" ");
    //  print('var2 is ${var4[1]}');
    //print('var1 is ${var4[0]}');
    String num2 = var4[1];
    code2 = var4[0];

    return Scaffold(
      bottomNavigationBar:
          getBottomButton('Update Beneficiery', () => _saveForm()),
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Update Beneficiery',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(249, 62, 108, 1),
                fontFamily: 'Gilroy',
                fontSize: 20,
                letterSpacing: 0,
                fontWeight: FontWeight.normal,
                height: 1),
          )),
      body: Form(
        key: _formKey,
        child: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 16, left: 16, right: 16),
                child: Text(
                  'Please enter the full name of the recipient as indicated on his CNI.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 32, left: 16, right: 16),
                child: Text(
                  'Add Beneficiary Name',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color.fromRGBO(77, 77, 77, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 14,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 16, right: 16),
                child: TextFormField(
                    initialValue: arguments['name'],
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                        ),
                        hintText: 'Add Beneficiary Name',
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffDA3C5F)),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 16,
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                    keyboardType: TextInputType.text,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[A-Za-z]')),
                    ],
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Name is empty!';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      //    print('text1 : $value');
                      _getBeneficiary = Data_list(
                          anotherPhoneNumber:
                              _getBeneficiary.anotherPhoneNumber,
                          // createdAt: _getBeneficiary.createdAt,
                          //  customerId: _getBeneficiary.customerId,
                          recipientInformation: value,
                          recipientPhone: _getBeneficiary.recipientPhone);
                    }),
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 30, left: 16, right: 16),
                  child: Text(
                    'Main Number',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Color.fromRGBO(77, 77, 77, 1),
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        letterSpacing:
                            0 /*percentages not used in flutter. defaulting to zero*/,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Row(children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 17.35,
                    width: MediaQuery.of(context).size.width / 3.5,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: CountryCodePicker(
                      // ignore: avoid_print
                      onChanged: (e) {
                        code1 = e.dialCode.toString();
                        print('code1$code1');
                      },
                      //print(e.dialCode),
                      initialSelection: code1,
                      showCountryOnly: false,
                      showOnlyCountryWhenClosed: false,
                      favorite: const ['+39', 'FR'],
                    ),
                  ),
                  SizedBox(
                    width: 18,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 17.35,
                    width: MediaQuery.of(context).size.width / 1.75,
                    child: Container(
                        child: TextFormField(
                      initialValue: num1,
                      decoration: InputDecoration(
                          counterText: "",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            borderSide: BorderSide(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                          ),
                          hintText: 'My mobile number',
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffDA3C5F)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 16,
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              borderSide:
                                  BorderSide(color: Color(0xffDA3C5F)))),
                      keyboardType: TextInputType.phone,
                      maxLength: 16,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Mobile number is empty!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        //  print('text1 : $value');
                        _getBeneficiary = Data_list(
                            anotherPhoneNumber:
                                _getBeneficiary.anotherPhoneNumber,
                            // createdAt: _getBeneficiary.createdAt,
                            //  customerId: _getBeneficiary.customerId,
                            recipientInformation:
                                _getBeneficiary.recipientInformation,
                            recipientPhone: value);
                      },
                    )),
                  ),
                ]),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 30, right: 16, left: 16),
                  child: Text(
                    'Other Number',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Color.fromRGBO(77, 77, 77, 1),
                        fontFamily: 'Gilroy',
                        fontSize: 14,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Row(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height / 17.35,
                      width: MediaQuery.of(context).size.width / 3.5,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: CountryCodePicker(
                        // ignore: avoid_print
                        onChanged: (e) => code2 = e.dialCode.toString(),
                        //print(e.toLongString()),
                        initialSelection: '$code2',
                        showCountryOnly: false,
                        showOnlyCountryWhenClosed: false,
                        favorite: const ['+39', 'FR'],
                      ),
                    ),
                    SizedBox(
                      width: 18,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height / 17.35,
                      width: MediaQuery.of(context).size.width / 1.75,
                      child: Container(
                          child: TextFormField(
                        initialValue: num2,
                        decoration: InputDecoration(
                            counterText: "",
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(
                                color: Color(0xFF7F7F7F),
                                width: 0.5,
                              ),
                            ),
                            hintText: 'My mobile number',
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xffDA3C5F)),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 16,
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                borderSide:
                                    BorderSide(color: Color(0xffDA3C5F)))),
                        keyboardType: TextInputType.phone,
                        maxLength: 16,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        ],
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Mobile number is empty!';
                          }
                        },
                        onSaved: (value) {
                          //   print('text1 : $value');
                          _getBeneficiary = Data_list(
                              anotherPhoneNumber: value,
                              // createdAt: _getBeneficiary.createdAt,
                              //  customerId: _getBeneficiary.customerId,
                              recipientInformation:
                                  _getBeneficiary.recipientInformation,
                              recipientPhone: _getBeneficiary.recipientPhone);
                        },
                      )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onBeneficiaryUpdateError(String error) {
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toasterror, title: "$error");
    // Fluttertoast.showToast(
    //   msg: '$error',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
  }

  @override
  void onBeneficiaryUpdateSuccess(BeneficiaryUpdate beneficiaryupdatemodel) {
    print('updateisdone');
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toastsuccess,
        title: "${beneficiaryupdatemodel.message}");
    // Fluttertoast.showToast(
    //   msg: '${beneficiaryupdatemodel.message}',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
    // Navigator.of(context).pushNamed(
    //   '/BeneficiaryScreen',
    // );
    Navigator.pushReplacementNamed(context, '/BeneficiaryScreen');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Beneficiary()));
  }
}
