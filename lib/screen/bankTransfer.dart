import 'package:familov/screen/paymentFailed.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:flutter/material.dart';
import 'package:dotted_line/dotted_line.dart';

class BankTransfer extends StatefulWidget {
  const BankTransfer({Key? key}) : super(key: key);
  @override
  _BankTransferState createState() => _BankTransferState();
}
//test
class _BankTransferState extends State<BankTransfer> {
  var load = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Image.asset(
                    'assets/images/tick.png',
                    height: 150,
                    width: 200,
                  ),
                ),
                Text(
                  'Yipee !!! Sparsh Gupta',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'The withdrawal code is M-1234567890. upon receipt of payment Sparsh will instantly receive the credit in his telephone.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Thank You !',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(77, 77, 77, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.w600,
                      height: 1.5),
                ),
                load == true
                    ? Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 15, bottom: 20),
                            child: DottedLine(
                              lineThickness: 0.1,
                              dashColor: Colors.black,
                            ),
                          ),
                          Text(
                            'Bank Details',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color.fromRGBO(77, 77, 77, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 20,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w600,
                                height: 1.5),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Please transfer (EUR 34.84) in below bank account.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 59, 94, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 14,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w600,
                                height: 1.5),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 18),
                                child: Text(
                                  'Holder Name',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 18),
                                child: Text(
                                  'FAMILOV LIMT',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5)),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 18),
                                child: Text(
                                  'Bank Name',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 18),
                                child: Text(
                                  'Sparkasse Saarbruecken',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5)),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 18),
                                child: Text(
                                  'IBAN',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 18),
                                child: Text(
                                  'DE12839898989989222289',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5)),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 18),
                                child: Text(
                                  'BIC',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 18),
                                child: Text(
                                  'SAKSDE55XXX',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5)),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 18),
                                child: Text(
                                  'Ref/VW',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Spacer(),
                              Container(
                                margin: EdgeInsets.only(right: 18),
                                child: Text(
                                  'F-1627022788 Sparsh Gupta',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(77, 77, 77, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 14,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5)),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      )
                    : Container(
                        margin: EdgeInsets.only(top: 15, bottom: 35),
                        child: Text(
                          'You left with € 1.29 in your wallet',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(77, 77, 77, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 16,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: buttonWidget('Retry to Pay € 1.29', context, () {
                      load = false;
                      setState(() {});
                    })),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    ),
                    border: Border.all(
                      color: Color.fromRGBO(0, 219, 167, 1),
                      width: 1,
                    ),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15, bottom: 30),
                  child: FlatButton(
                      height: 48,
                      minWidth: MediaQuery.of(context).size.width,
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.horizontal(
                        left: Radius.circular(10),
                        right: Radius.circular(10),
                      )),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text('Back to Home',
                              style: new TextStyle(
                                fontSize: 18.0,
                                color: Color(0xff00DBA7),
                              )),
                        ],
                      ),
                      onPressed: () {
                        load = true;
                        setState(() {});
                      }),
                ),
                Container(
                    margin: EdgeInsets.only(top: 16, bottom: 20),
                    child: InkWell(
                      child: Text('Payment failed design check'),
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PaymentFail()));
                      },
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
