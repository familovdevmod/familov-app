import 'package:familov/screen/bankTransfer.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:flutter/material.dart';

class PayByCard extends StatefulWidget {
  const PayByCard({Key? key}) : super(key: key);

  @override
  _PayByCardState createState() => _PayByCardState();
}

class _PayByCardState extends State<PayByCard> {
  bool notifyEmails = false;

  final List<String> genderList = <String>[
    ("Select Gender"),
    ('Male'),
    ('Female')
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        bottomNavigationBar: Container(
          child: buttonWidget('Pay € 1.29', context, () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => BankTransfer()));
          }),
        ),
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'CheckOut',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 25,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              child: Column(children: [
            Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  'Pay with Card',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                )),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                'Enter your card details',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(130, 130, 130, 1),
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                    letterSpacing: 0,
                    fontWeight: FontWeight.normal,
                    height: 1.5),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: Text('Email'),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, bottom: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: 'Email',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: null,
                keyboardType: TextInputType.text,
                validator: null,
                onSaved: null,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: Text('Card Information'),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, bottom: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: '1234 1234 1234 1234',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: null,
                keyboardType: TextInputType.text,
                validator: null,
                onSaved: null,
              ),
            ),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 2, left: 10, bottom: 15),
                  width: MediaQuery.of(context).size.width / 2.2,
                  child: Container(
                    child: TextFormField(
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            borderSide: BorderSide(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                          ),
                          hintText: 'MM/YY',
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffDA3C5F)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          contentPadding: EdgeInsets.symmetric(horizontal: 16),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              borderSide:
                                  BorderSide(color: Color(0xffDA3C5F)))),
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Mobile number is empty!';
                        }
                        return null;
                      },
                      onSaved: null,
                    ),
                  ),
                ),
                SizedBox(
                  width: 18,
                ),
                Container(
                  margin: EdgeInsets.only(top: 2),
                  width: MediaQuery.of(context).size.width / 2.2,
                  child: Container(
                    child: TextFormField(
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            borderSide: BorderSide(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                          ),
                          hintText: 'CVV',
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffDA3C5F)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          contentPadding: EdgeInsets.symmetric(horizontal: 16),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              borderSide:
                                  BorderSide(color: Color(0xffDA3C5F)))),
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Mobile number is empty!';
                        }
                        return null;
                      },
                      onSaved: null,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: Text('Name on card'),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: 'Name on card',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: null,
                keyboardType: TextInputType.text,
                validator: null,
                onSaved: null,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 20, right: 10, left: 10),
              child: Text('Country or Region'),
            ),
            Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    height: MediaQuery.of(context).size.height / 14,
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: InkWell(
                        child: Container(
                            margin: EdgeInsets.only(left: 15, right: 20),
                            child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                              onChanged: (value) {},
                              hint: new Text(
                                "Select",
                                style: TextStyle(color: Color(0xff828282)),
                              ),
                              items: genderList.map((String user) {
                                return new DropdownMenuItem<String>(
                                  value: user,
                                  child: new Text(
                                    user,
                                    style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      fontSize: 10.0,
                                      color: const Color(0xFF111111),
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                );
                              }).toList(),
                            ))),
                        onTap: () {},
                      ),
                    ))),
            Row(
              children: [
                Checkbox(
                  value: notifyEmails,
                  onChanged: (notifyEmails) {
                    setState(() {
                      this.notifyEmails = notifyEmails!;
                      print('email notify : ${this.notifyEmails}');
                    });
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Save information to pay faster next time.',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 12,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ])),
        ),
      ),
    );
  }
}
