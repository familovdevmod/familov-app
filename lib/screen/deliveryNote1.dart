import 'dart:convert';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/mobile_invoice.dart';
import 'package:familov/model/pdf_model.dart';
import 'package:familov/model/shop_invoiceModel.dart';
import 'package:familov/presenter/mobileInvoice_presenter.dart';
import 'package:familov/presenter/shopInvoice_presenter.dart';
import 'package:familov/widget/app_colors.dart';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

import '../presenter/pdf_presenter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:dio/dio.dart';

class DeliveryNote extends StatefulWidget {
  const DeliveryNote({Key? key}) : super(key: key);
  @override
  _DeliveryNoteState createState() => _DeliveryNoteState();
}

class _DeliveryNoteState extends State<DeliveryNote>
    implements
        GetShopInvoiceContract,
        GetMobileInvoiceContract,
        GetPdfDataContract {
  List? statusList = [];
  List? mobileList = [];
  var load = false;
  var state = false;
  var orderId;
  Dio dio = Dio();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  late String finalPdfPath;
  String progress = "";

  UserShopInvoicePresenter? _presenter;
  UserMobileInvoicePresenter? presenter;
  PdfPresenter? presenterPdf;

  _DeliveryNoteState() {
    _presenter = new UserShopInvoicePresenter(this);
    presenter = new UserMobileInvoicePresenter(this);
    presenterPdf = new PdfPresenter(this);
  }

  @override
  void initState() {
    // flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    final android = AndroidInitializationSettings('@drawable/ic_launcher');
    final iOS = IOSInitializationSettings();
    final initSettings = InitializationSettings(android: android, iOS: iOS);

    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: _onSelectNotification);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    if (state == false) {
      setState(() {
        print('the Data is delivery : ${arguments['urlData']}');
        arguments['generatedArgument'].toString().startsWith('F')
            ? _presenter!.getShopInvoice(arguments['urlData'])
            : presenter!.getMobileInvoice(arguments['urlData']);
      });
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 60,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Delivery Note',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
      ),
      body: load == true
          ? SingleChildScrollView(
              child:
                  Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                Container(
                  margin: EdgeInsets.only(right: 10, bottom: 5, top: 10),
                  width: 120,
                  height: 24,
                  child: InkWell(
                    onTap: () {
                      orderId = arguments['urlData'];
                      // List<String> var2 = orderId.split("=");
                      //  print('var1 at index 1 is ${var2[1]}');
                      // print('var1 at index 0 is ${var2[0]}');
                      //  String num1 = var2[1];
                      // code1 = var2[0];

                      print('download pdf 68 ${orderId}');
                      _getStoragePermission();
                      getPdfString(orderId);
                    },
                    child: Row(children: <Widget>[
                      Container(
                        width: 24,
                        height: 24,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 1),
                        ),
                        child: Image.asset('assets/images/download.png'),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          'Download${progress}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(77, 77, 77, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 14,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ]),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(
                        right: 16, left: 16, bottom: 32, top: 12),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 1.3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(4),
                        topRight: Radius.circular(4),
                        bottomLeft: Radius.circular(4),
                        bottomRight: Radius.circular(4),
                      ),
                      color: Colors.white,
                      border: Border.all(
                        color: Color.fromRGBO(189, 189, 189, 1),
                        width: 0.5,
                      ),
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 33,
                                  child: Image.asset(
                                    'assets/images/familovlogo.png',
                                  ),
                                ),
                                Spacer(),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    // Familov Mobile Date: Aug 7, 2021 / 19:56
                                    Text(
                                      'CODE: ${arguments['generatedArgument']}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    arguments['generatedArgument']
                                            .toString()
                                            .startsWith('M')
                                        ? Text(
                                            'Familov Mobile',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontFamily: 'Gilroy',
                                                fontWeight: FontWeight.w500),
                                          )
                                        : SizedBox(
                                            width: 185,
                                            child: Text(
                                              'Delivery: ${arguments['delivery_option']}',
                                              textAlign: TextAlign.end,
                                              //  overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontFamily: 'Gilroy',
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      'Date: ${arguments['date_time']}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      'Address: ${arguments['home_delivery_address'] == 'null' ? arguments['delivery_option'] : arguments['home_delivery_address']}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 12, bottom: 10),
                              child: Container(
                                // margin: EdgeInsets.symmetric(horizontal: 10),
                                child: Transform.rotate(
                                  angle: 0.000005008956130975318,
                                  child: Divider(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      thickness: 0.5),
                                ),
                              ),
                            ),
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      'Name         :',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        'Ashutosh Tripathi',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'For              :',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        '${arguments['receiver_name']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Contact     :',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        '${arguments['receiver_phone']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Autre contact     :',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        '${arguments['another_phone_number']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Message    :',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Gilroy',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        '${arguments['greeting_msg']}',
                                        maxLines: 3,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 16, bottom: 10),
                              child: Container(
                                child: Transform.rotate(
                                  angle: 0.000005008956130975318,
                                  child: Divider(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      thickness: 0.5),
                                ),
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Text(
                                    'Products',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 14,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                                  Spacer(),
                                  Text(
                                    'Qty',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 14,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Expanded(
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: arguments['generatedArgument']
                                        .toString()
                                        .startsWith('F')
                                    ? statusList!.length
                                    : mobileList!.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 5),
                                    child: Row(
                                      children: [
                                        Text(
                                          arguments['generatedArgument']
                                                  .toString()
                                                  .startsWith('F')
                                              ? '${statusList![index].productName}'
                                              : '${mobileList![index].productName}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                        Spacer(),
                                        Text(
                                          arguments['generatedArgument']
                                                  .toString()
                                                  .startsWith('F')
                                              ? '${statusList![index].quantity}'
                                              : '${mobileList![index].quantity}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ))),
              ]),
            )
          : Scaffold(
              body: Center(
                  child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              )),
            ),
    );
  }

  @override
  void onShopInvoiceError(String error) {}

  @override
  void onShopInvoiceSuccess(ShopInvoice shopinvoiceModel) {
    setState(() {
      load = true;
      state = true;
    });
    statusList = shopinvoiceModel.data!.dataList;
  }

  @override
  void onInvoiceError(String error) {}

  @override
  void onInvoiceSuccess(MobileInvoices invoicesModel) {
    setState(() {
      load = true;
      state = true;
    });
    mobileList = invoicesModel.data!.dataList!;
  }

  void getPdfString(String num1) {
    presenterPdf!.getPdfString(num1);
  }

  @override
  void onPdfError(String error) {
    // TODO: implement onPdfError
    print('onpdffailed');
  }

  @override
  void onPdfSuccess(PdfModel pdfModel) {
    // TODO: implement onPdfSuccess

    print('onpdfsuccess ${pdfModel.status} ');

    if (pdfModel.status == true) {
      String? fileData = pdfModel.data!.fileData;
      String? fileName = pdfModel.data!.fileName;

      createPdf(fileName!, fileData!);
    }
  }

  createPdf(String fileName, String invoiceString) async {
    print('invoice string $invoiceString');
    var base64v = invoiceString;
    var bytes = base64Decode(base64v);

    Directory? directory;
    try {
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await Directory('/storage/emulated/0/Download');
        // Put file in global download folder, if for an unknown reason it didn't exist, we fallback
        // ignore: avoid_slow_async_io
        print('directory.......... path ${directory.path}');
        if (!await directory.exists()) {
          print('491 directory exist');

          directory = await getExternalStorageDirectory();

          var output = directory?.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
          print('directry issss ${await directory?.exists()}');
        } else {
          print('506 exisddsd.........');
          var output = directory.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
        }
      }
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    //  var output = await ExtStorage.getExternalStoragePublicDirectory(
    //   ExtStorage.DIRECTORY_DOWNLOADS);
//    getTemporaryDirectory();
  }

  Future<void> _startDownload(String savePath) async {
    print('save path.............${savePath}');

    //
    BotToast.showSimpleNotification(
        onTap: () {},
        backgroundColor: AppColors().toastsuccess,
        title: "Saved in Download");
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      print('savedddddddd ${savePath}');
      final response = await dio.download(
          savePath /*'http://www.africau.edu/images/default/sample.pdf'*/,
          savePath /*'/data/user/0/com.paliwalkisaan.paliwal_kisaan_bazaar/cache/4.pdf'*/,
          onReceiveProgress: _onReceiveProgress);

      print('response check ${response.data}');
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        progress = (received / total * 100).toStringAsFixed(0) + "%";

        print('progress ........................${progress}');
      });
    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails('channel id', 'channel name',
        priority: Priority.high, importance: Importance.max);
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android: android, iOS: iOS);
    final json = jsonEncode(downloadStatus);
    print('json edcoc $downloadStatus');
    final isSuccess = true /*downloadStatus['isSuccess']*/;

    await flutterLocalNotificationsPlugin.show(
        0, // notification id
        isSuccess ? 'Success' : 'Failure',
        isSuccess
            ? 'File has been downloaded successfully!'
            : 'There was an error while downloading the file.',
        platform,
        payload: json);
  }

  Future<void> _onSelectNotification(String? json) async {
    final obj = jsonDecode(json!);
    // print('objfilePath ${obj['filePath']}');
    OpenFile.open(finalPdfPath /*obj['filePath']*/);

/*
    if (obj['isSuccess']) {
      OpenFile.open(obj['filePath']);
    } else {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Error'),
          content: Text('${obj['error']}'),
        ),
      );
    }
*/
  }

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      setState(() {
        getPdfString(orderId);
        // permissionGranted = true;
      });
    }
  }
}
