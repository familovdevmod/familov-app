import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/getrechargeitem.dart';
import 'package:familov/presenter/getRecharge_presenter.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'transactionCheckout.dart';

class MobileNextScreen extends StatefulWidget {
  final String mobile;
  final String code;

  const MobileNextScreen({Key? key, required this.mobile, required this.code})
      : super(key: key);
  @override
  _MobileNextScreenState createState() => _MobileNextScreenState();
}

class _MobileNextScreenState extends State<MobileNextScreen>
    implements GetRechargeItemContract {
  Color col = Color.fromRGBO(0, 219, 167, 1);
  GetRechargeItemPresenter? presenter;

  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  Color col1 = Color.fromRGBO(189, 189, 189, 1);
  var ind = -1;

  _MobileNextScreenState() {
    presenter = new GetRechargeItemPresenter(this);
  }

  bool isRefresh = true;
  int currentPage = 1;
  int length = 0;
  ScrollController? _controller;
  List<DataList>? statusList = [];
  var imageRecharge;
  //List<OperatorDetails>? opertaionList = [];
  var nameRecharge;
  String message = "";
  var data0, data1, data2, data3, price;
  String? maximumSendCurrencyIso;
  bool statusmsg = true;

  _scrollListener() {
    print('scroll check 2');
    if (_controller!.offset >= _controller!.position.maxScrollExtent &&
        !_controller!.position.outOfRange) {
      setState(() {
        isRefresh = false;
        presenter!.getRechargeItem(widget.mobile, widget.code, currentPage);
        message = "reach the bottom";
      });
    }
    // if (_controller!.offset <= _controller!.position.minScrollExtent &&
    //     !_controller!.position.outOfRange) {
    //   setState(() {
    //     print('scroll check 3');
    //     statusList = [];
    //     currentPage = 1;
    //     isRefresh = true;
    //     presenter!.getRechargeItem(widget.mobile, widget.code,
    //         currentPage); //get-recharge-item?number=8840993989&country_code=91&page=1
    //     message = "reach the top";
    //   });
    // }
  }

  String? valueKey;

  tokenCheck() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    if (valueKey != '' && valueKey != null) {
      print('tokenCheckIs :  ${valueKey.toString()}');
    }
  }

  bool load = false;

  @override
  void initState() {
    tokenCheck();
    _controller = ScrollController();
    _controller!.addListener(_scrollListener);
    presenter!.getRechargeItem(widget.mobile, widget.code, currentPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: Scaffold(
            bottomNavigationBar: FlatButton(
                height: 48,
                minWidth: MediaQuery.of(context).size.width,
                shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                color: Color(0xff00DBA7),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Proceed to Recharge',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          letterSpacing:
                              0 /*percentages not used in flutter. defaulting to zero*/,
                          fontWeight: FontWeight.normal,
                          height: 1.5 /*PERCENT not supported*/
                          ),
                    )
                  ],
                ),
                onPressed: () async {
                  print('data1 $data1');
                  print('data2 $data2');
                  print('widget.mobile ${widget.mobile}');
                  print('widget.code ${widget.code}');
                  print('price ${price}');

                  if (data0 != null &&
                      data1 != null &&
                      data2 != null &&
                      widget.mobile != null &&
                      widget.code != null &&
                      price != null) {
                    SharedPreferences preferences =
                        await SharedPreferences.getInstance();
                    var currency = preferences.getString('currency');
                    var currencySymbol =
                        preferences.getString('currencySymbol');
                    var rate = preferences.getString('rate');
                    print('currency $currency');
                    print('rate $rate');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TransactionCheckOut(
                                imageRecharge: data0,
                                skuCode: statusList![0].skuCode,
                                data1: '$data1',
                                data2: '$data2',
                                mobile: '${widget.mobile}',
                                code: '${widget.code}',
                                price: '$price',
                                maximumSendCurrencyIso: maximumSendCurrencyIso,
                                currencySymbol: currencySymbol,
                                rate: rate)));
                  } else {
                    BotToast.showSimpleNotification(
                        backgroundColor: AppColors().toastsuccess,
                        title: 'Please select package');
                  }
                }),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              toolbarHeight: 80,
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text(
                'Mobile Recharge',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xffF93E6C),
                ),
              ),
            ),
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: false,
            body: load == true
                ? Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: Column(
                      children: [
                        Container(
                            alignment: Alignment.center,
                            height: MediaQuery.of(context).size.height / 14,
                            child: Image.network('$imageRecharge')
                            // Image.asset(
                            //   'assets/images/airtel.png',
                            // ),
                            ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          '$nameRecharge',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(96, 32, 189, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 20,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 20,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3.0),
                            child: Text(
                              'Select Plan',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontFamily: 'Gilroy',
                                  fontSize: 16,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.5),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: ListView.builder(
                              controller: _controller,
                              shrinkWrap: true,
                              itemCount: statusList!.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  highlightColor: Colors.transparent,
                                  splashFactory: NoSplash.splashFactory,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Container(
                                      child: Card(
                                        elevation: ind == index ? 6 : 3,
                                        shadowColor: ind == index
                                            ? col.withOpacity(.5)
                                            : Color.fromARGB(
                                                136, 245, 245, 245),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 15),
                                                    child: Text(
                                                      '${statusList![index].maximumSendCurrencyIso}  ${statusList![index].minimumSendValue}',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              79, 79, 79, 1),
                                                          fontFamily: 'Gilroy',
                                                          fontSize: 18,
                                                          letterSpacing: 0,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          height: 1.5),
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/images/rightgrey.png',
                                                      scale: 2,
                                                      color: ind == index
                                                          ? col
                                                          : col1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15, top: 3),
                                              child: Text(
                                                '${statusList![index].displayText!.trim()}',
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        79, 79, 79, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 14,
                                                    letterSpacing: 0,
                                                    fontWeight: FontWeight.w300,
                                                    height: 1.5),
                                              ),
                                            ),
                                            statusList![index]
                                                            .descriptionMarkdown ==
                                                        '' ||
                                                    statusList![index]
                                                            .descriptionMarkdown ==
                                                        ''
                                                ? Container()
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 15, top: 5),
                                                    child: Text(
                                                      '${statusList![index].descriptionMarkdown!.trim()}',
                                                      maxLines: 3,
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontFamily: 'Gilroy',
                                                          fontSize: 12,
                                                          letterSpacing: 0,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          height: 1.5),
                                                    ),
                                                  ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15, top: 5),
                                              child: Text(
                                                statusList![index]
                                                            .validityPeriodIso ==
                                                        null
                                                    ? 'Unlimited validity'
                                                    : 'Valid for ${statusList![index].validityPeriodIso.toString().substring(1, statusList![index].validityPeriodIso.toString().length - 1)} days',
                                                maxLines: 3,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        130, 130, 130, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 12,
                                                    letterSpacing: 0,
                                                    fontWeight: FontWeight.w300,
                                                    height: 1.5),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 8,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    ind = index;
                                    setState(() {
                                      data0 = imageRecharge;
                                      data1 = nameRecharge;
                                      data2 = statusList![index].displayText;
                                      price =
                                          statusList![index].maximumSendValue;
                                      maximumSendCurrencyIso =
                                          statusList![index]
                                              .maximumSendCurrencyIso;
                                      print(
                                          'check data value :  $data1 and $data2');
                                    });
                                  },
                                );
                              }),
                        ),
                      ],
                    ),
                  )
                :

                //  statusmsg
                //     ?
                Center(
                    child: CircularProgressIndicator(
                      color: AppColors().appaccent,
                    ),
                  )
            // :
            //  Center(
            //     child: Text('Enter a valid mobile number'),
            //   )
            ));
  }

  @override
  void onGetRechargeItemError(String error) {
    print(error);
    print('recharge errros === $error');
  }

  @override
  void onGetRechargeItemSuccess(Getrechargeitem recharge) {
    if (recharge.status == false) {
      statusmsg = false;
      Navigator.pop(context);
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "Enter a valid mobile number");
      setState(() {});
    }
    print('to ye dikkat hai ${recharge.errorMsg}');

    print(
        'Getrechargeitem ${recharge.data!.operatorDetails!.validationRegex}'); //opertaionList
    print(
        'Getrechargeitem ${recharge.data!.dataList![0].benefits}'); //opertaionList
    print('refresh completed55 $isRefresh');
    length = recharge.data!.count!;
    print('refresh completed490 $length');
    if (length != 0) {
      if (isRefresh) {
        statusList = [];
        statusList = recharge.data!.dataList!;
      } else {
        statusList!.addAll(recharge.data!.dataList!.toList());
      }
      currentPage++;
      setState(() {
        nameRecharge = recharge.data!.operatorDetails!.name;
        imageRecharge = recharge.data!.operatorDetails!.providerImage;
        load = true;
      });
    }
  }
}
