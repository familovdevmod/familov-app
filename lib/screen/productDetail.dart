import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/deleteCart.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/place.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/presenter/cartcontroller.dart';
import 'package:familov/presenter/deleteCart_presenter.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:familov/screen/CartScreen.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/db_helper.dart';

import 'package:familov/model/product_detail_model.dart';
import 'package:familov/presenter/productDetail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

class ProductDetail extends StatefulWidget {
  // String samePage = '';
  final String shopId;
  final String productId;
  final String specialPrice;
  final String image;
  final String price;
  final String discount;

  final String disPercentage;

  ProductDetail({
    Key? key,
    required this.shopId,
    required this.productId,
    required this.specialPrice,
    required this.image,
    required this.price,
    required this.discount,
    required this.disPercentage,
    required int totalNumber,
  }) : super(key: key);
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail>
    implements
        ProductDetailClass,
        SetCartContract,
        GetCartContract,
        DeleteCartContract {
  SetCartPresenter? presenter;
  ProductDetailPresenter? _presenter;
  DeleteCartPresenter? delpresenter;
  DataList? productDetails;
  // bool isAdded = false;
  GetCartPresenter? presenter1;
  List<String> cartproductid = [];
  int newcount = 1;
  var len;
  List<Data9> dataList2 = [];
  int localcount = 1;
  var localcartproductid;
  List dataList = [];
  int length = 0;
  var totalNumber = 0;
  List<Place> items = [];
  var loads = false;
  var valueKey;
  bool isAdded = false;
  
   CartController cartController = Get.put(CartController());
  _ProductDetailState() {
    presenter = new SetCartPresenter(this);
    _presenter = new ProductDetailPresenter(this);
    presenter1 = new GetCartPresenter(this);
    delpresenter = DeleteCartPresenter(this);
  }
  @override
  void initState() {
    _presenter!.productDetail('${widget.productId}');
    presenter1!.getCart();
    database();

    shared();
    super.initState();
  }

  var checkCurrency;
  var rate;

  shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    checkCurrency = preferences.getString('currency');
    rate = preferences.getString('rate');
    print('currency is $checkCurrency');
    setState(() {});
  }

  double discount = 0;
  double actualPrice = 0;

  var addedd = false;
  var removedd = false;

  double specialPrice = 0;

  @override
  Widget build(BuildContext context) {
    print('Spe      =      ${widget.specialPrice}');
    late var quantityprice = widget.specialPrice == '0.00'
        ? double.parse(widget.price.toString())
        : double.parse(widget.specialPrice.toString()) * totalNumber;
    late var localquantityprice = widget.specialPrice == '0.00'
        ? double.parse(widget.price.toString()) * localcount
        : double.parse(widget.specialPrice.toString()) * localcount;
    late var newquantityprice = widget.specialPrice == '0.00'
        ? double.parse(widget.price.toString()) * newcount
        : double.parse(widget.specialPrice.toString()) * newcount;
    return Scaffold(
        bottomNavigationBar: InkWell(
          onTap: () {
            print('new count $newcount');
            print(
                '${widget.productId.toString()},${widget.shopId.toString()},${totalNumber}');
            presenter!.setCart(
                widget.shopId.toString(),
                widget.productId.toString(),
                newcount.toString(),
                '',
                '',
                '',
                '');
               
            if (valueKey == null) {

              DBHelper.insert('thecart', {
                'id': widget.productId.toString(),
                'quantity': localcount,
              });
                database();
              setState(() {
                BotToast.showSimpleNotification(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CartScreen()));
                    },
                    backgroundColor: Color(0xff00DBA7),
                    title: "Product Added Sucessfully");
              });
            }
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              'Add to Bag',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.w600,
                  fontSize: 18),
            ),
            height: 48,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8),
              ),
              color: Color.fromRGBO(0, 219, 167, 1),
            ),
          ),
        ),

        //     getBottomButtonWithTwoFill(context, '', 'Add to Bag', () {}, () {

        //   print(
        //       '${widget.productId.toString()},${widget.shopId.toString()},${widget.totalNumber}');
        //   presenter!.setCart(
        //       widget.shopId.toString(),
        //       widget.productId.toString(),
        //       widget.totalNumber.toString(),
        //       '',
        //       '',
        //       '',
        //       '');
        //   if (widget.totalNumber > 0) {
        //     DBHelper.insert('thecart', {
        //       'id': widget.productId.toString(),
        //       'quantity': widget.totalNumber.toString(),
        //     });
        //     setState(() {
        //       BotToast.showSimpleNotification(
        //           onTap: () {
        //             Navigator.push(context,
        //                 MaterialPageRoute(builder: (context) => CartScreen()));
        //           },
        //           backgroundColor: Color(0xff00DBA7),
        //           title: "Product Added Sucessfully");
        //     });
        //   }
        // }, 1),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: [
           Container(
                    alignment: Alignment.center,
                    child: Stack(children: <Widget>[
                      Container(
                        height: 28,
                        width: 26,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen(2, '')));
                            print('kane');
                           
                          },
                          child: Icon(
                            Icons.local_grocery_store_outlined,
                            color: Colors.black38,
                          ),
                        ),
                      ),
                      new Positioned(
                        // draw a red marble
                        top: 0.0,
                        right: 0.0,
                        left: 12,
                        child: Obx(() => Visibility(
                            visible:  valueKey==null ?cartController.getcount.value==0?false:  true:cartController.getcountapi.value==0?false
                                    :true,
                            // cartController.getcount == 0
                            //     ? lencart == null || lencart == 0
                            //         ? false
                            //         : true
                            //     : true,
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                  '${valueKey ==null ?cartController.getcount :cartController.getcountapi}',
                                  // '${cartlength==0?lencart:cartlength.toString()}',
                                  style:
                                      TextStyle(color: Colors.white, fontSize: 12),
                                ),
                              
                              decoration: BoxDecoration(
                                  color: AppColors().toastsuccess,
                                  shape: BoxShape.circle),
                              height: 15,
                              width: 10,
                            ),
                          ),
                        ),
                      )
                    ]),
                  ),
                 SizedBox(
              width: 16,
            )
          ],
          title: Text(
            'Product Detail',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        // backgroundColor: Colors.white,

        resizeToAvoidBottomInset: false,
        body: loads == true
            ? SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  // height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Visibility(
                        visible: removedd,
                        child: Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: MediaQuery.of(context).size.width,
                            child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width / 1.1,
                                decoration: BoxDecoration(
                                    color: Color(0xff00DBA7).withOpacity(0.3),
                                    borderRadius: BorderRadius.circular(6)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: Color(0xff00DBA7),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      'Product is removed from cart',
                                      style: TextStyle(
                                          fontFamily: 'Gilroy',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ))),
                      ),
                      Visibility(
                        visible: addedd,
                        child: Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: MediaQuery.of(context).size.width,
                            child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width / 1.1,
                                decoration: BoxDecoration(
                                    color: Color(0xff00DBA7).withOpacity(0.3),
                                    borderRadius: BorderRadius.circular(6)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: Color(0xff00DBA7),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      'Product is added to cart',
                                      style: TextStyle(
                                          fontFamily: 'Gilroy',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ))),
                      ),
                      Center(
                        child: Container(
                          height: MediaQuery.of(context).size.width * .7,
                          width: MediaQuery.of(context).size.width,
                          child: Image.network(
                            '${widget.image}',
                            alignment: Alignment.center,
                            errorBuilder: (context, error, stackTrace) {
                              return Container(
                                margin: EdgeInsets.only(left: 10),
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'assets/images/fami.png',
                                  scale: 1,
                                  alignment: Alignment.center,
                                ),
                              );
                            },
                          ),
                          //  Image.asset(
                          //   'assets/images/cans.png',
                          //   scale: 1,
                          //   alignment: Alignment.center,
                          // ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Text(
                          '${productDetails!.productName}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(79, 79, 79, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Row(
                          children: [
                            widget.specialPrice == '0.00'
                                ? InkWell(
                                    highlightColor: Colors.transparent,
                                    // splashFactory: NoSplash.splashFactory,
                                    onTap: () {
                                      print('here our rate $rate');
                                    },
                                    child: Text(
                                      // '',
                                      '${checkCurrency.toString().substring(0, 2)} ${widget.price}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color.fromRGBO(77, 77, 77, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 24,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  )
                                : Text(
                                    '${checkCurrency.toString().substring(0, 2)} ${widget.specialPrice}',
                                    // '${checkCurrency.toString().substring(0, 2)}${widget.discount.length > 5 ? widget.discount.substring(0, 5) : widget.discount}',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Gilroy',
                                        fontSize: 24,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),

                            SizedBox(
                              width: 10,
                            ),
                            // ignore: unrelated_type_equality_checks
                            widget.specialPrice == '0.00'
                                ? Container(
                                    width: 0,
                                    height: 0,
                                  )
                                :
                                //here

                                Text(
                                    // '1${productDetails!.productPrices.toString().substring(0, 4)}',
                                    '${checkCurrency.toString().substring(0, 2)}${widget.price}',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough,
                                        color: Color.fromRGBO(130, 130, 130, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 20,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                            SizedBox(
                              width: 5,
                            ),
                            // ignore: unrelated_type_equality_checks
                            widget.specialPrice != '0.00'
                                ? Text(
                                    // '',
                                    '${widget.disPercentage.length > 5 ? widget.disPercentage.substring(0, 5) : widget.disPercentage} %',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(249, 62, 108, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 24,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  )
                          ],
                        ),
                      ),
                      Divider(
                          color: Color.fromRGBO(189, 189, 189, 1),
                          thickness: 0.5),
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Row(
                          children: [
                            Text(
                              'Select Quantity',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Color.fromRGBO(77, 77, 77, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            // isAdded == false
                            //     ?
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  highlightColor: Colors.transparent,
                                  splashFactory: NoSplash.splashFactory,
                                  onTap: () {
                                    if (totalNumber > 1) {
                                      totalNumber--;
                                    }
                                    if (newcount > 1) {
                                      newcount--;
                                    }
                                    if (localcount > 1) {
                                      localcount--;
                                    }
                                    // if (newcount == 1) {
                                    //   delpresenter!
                                    //       .deleteCart(widget.productId);
                                    // }
                                    // : isAdded = false;
                                    setState(() {});
                                    // setState(() {
                                    //   removedd = true;
                                    //   Future.delayed(
                                    //       Duration(milliseconds: 1500), () {
                                    //     setState(() {
                                    //       removedd = false;
                                    //     });
                                    //   });
                                    // });
                                  },
                                  child: Container(
                                    width: 24,
                                    height: 24,
                                    child: const Center(
                                        child: Text(
                                      "-",
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    )),
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                  ),
                                ),
                                Container(
                                    alignment: Alignment.center,
                                    width: 20,
                                    height: 24,
                                    child: Text(
                                      localcount == 0
                                          ? '${newcount.toString()}'
                                          : '${localcount.toString()}',
                                      maxLines: 1,
                                    )),
                                InkWell(
                                  highlightColor: Colors.transparent,
                                  splashFactory: NoSplash.splashFactory,
                                  onTap: valueKey != null
                                      ? () {
                                          print('OMG it works');
                                          setState(() {
                                            newcount++;
                                          });
                                        }
                                      : () {
                                          print('OMG it works');
                                          setState(() {
                                            localcount++;
                                          });
                                        },
                                  child: Container(
                                    width: 24,
                                    height: 24,
                                    child: const Center(
                                        child: Text(
                                      "+",
                                      style: TextStyle(color: Colors.white),
                                    )),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: const Color(0xff00DBA7)),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Text(
                              localcount == 0
                                  ? newcount == 0
                                      ? '${checkCurrency.toString().substring(0, 2)} ${quantityprice.toStringAsFixed(2)}'
                                      : '${checkCurrency.toString().substring(0, 2)} ${newquantityprice.toStringAsFixed(2)}'
                                  : '${checkCurrency.toString().substring(0, 2)} ${localquantityprice.toStringAsFixed(2)}',
                            ),
                            // Text(
                            //   '${checkCurrency.toString().substring(0, 2)} ${newquantityprice.toStringAsFixed(2)}',
                            // ),
                            // Text(
                            //   '${checkCurrency.toString().substring(0, 2)} ${quantityprice.toStringAsFixed(2)}',
                            // ),
                            SizedBox(
                              width: 30,
                            )
                          ],
                        ),
                      ),
                      Divider(
                          color: Color.fromRGBO(189, 189, 189, 1),
                          thickness: 0.5),
                      Container(
                        margin: EdgeInsets.only(left: 15, bottom: 20),
                        child: Text(
                          //     decoration: TextDecoration.lineThrough
                          'Description',
                          textAlign: TextAlign.left,

                          style: TextStyle(
                              // decoration: TextDecoration.lineThrough,
                              color: Color.fromRGBO(77, 77, 77, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 14,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                      Container(
                        // margin: EdgeInsets.only(left: 15, bottom: 20),
                        child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Center(
                              child: SingleChildScrollView(
                                child: Html(
                                  data:
                                      '${productDetails!.productDesc.toString()}',
                                  padding: EdgeInsets.all(8.0),
                                  onLinkTap: (url) {
                                    print("Opening $url...");
                                  },
                                ),
                              ),
                            )
                            // ReadMoreText(

                            // '${productDetails!.productDesc.toString()}',
                            //   // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsan Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsan Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsanLorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsanLorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsanLorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsanLorem ipsum dolor sit amet, consectetur adipiscing elit. Et cras vulputate ac et accumsan nunc ac accumsan vulputate ac et accumsan',
                            //   trimLines: 5,

                            //   style: TextStyle(
                            //       fontFamily: 'Gilroy',
                            //       fontSize: 14,
                            //       letterSpacing: 0,
                            //       fontWeight: FontWeight.normal,
                            //       height: 1.5),
                            //   colorClickableText: Colors.pink,
                            //   trimMode: TrimMode.Line,

                            //   trimCollapsedText: '...Read more',
                            //   trimExpandedText: ' Less',
                            // ),

                            ),
                      ),
                    ],
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                    color: Color.fromRGBO(249, 62, 108, 1)),
              ));
  }

  @override
  void onproductDetailError(String error) {}

  @override
  void onproductDetailSuccess(ProductDetailModel detailModels) {
    loads = true;
    print('ckeck 21000');
    productDetails = detailModels.data!.dataList!;
    print('21000refres ${productDetails!.productDesc}');
    setState(() {
      actualPrice = double.parse('${productDetails!.productPrices}');
      specialPrice = double.parse('${widget.specialPrice}');
      print('actual price : $actualPrice and special price is : $specialPrice');

      if (actualPrice > specialPrice && specialPrice != 0) {
        print(
            'actual price11 : $actualPrice and special price is : $specialPrice');
        discount = actualPrice - specialPrice;
        print('discount is :  $discount');
      }
    });
  }

  @override
  void onSetCartError(String error) {
    print('error iss ${error}');
  }

  @override
  void onSetCartSuccess(SetCart setCartModel) {
     presenter1!.getCart();
    if (setCartModel.status == true) {}
    print('Erroe message is : ${setCartModel.status}');
    setState(() {
      if (setCartModel.errorMsg == "Different shop") {}
    });
    BotToast.showSimpleNotification(
        backgroundColor: Color(0xff00DBA7), title: "Product Added Sucessfully");
  }

  @override
  void onGetCartError(String error) {
    // TODO: implement onGetCartError
  }

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    // TODO: implement onGetCartSuccess
    localcount = 0;
    print('orailnoor --item product detail screen $len');
    dataList2 = getCartModel.data!;
    len = dataList2.length;
    cartController.getcountapi.value=len;
    print('orailnoor --item cart screen $len');
    for (var i = 0; i < len; i++) {
      widget.productId == dataList2[i].productId
          ? newcount = int.parse(dataList2[i].quantity.toString())
          : print('object');

      cartproductid =
          List<String>.generate(len, (counter) => "${dataList2[i].productId}");
      print('cart quantity :${dataList2[i].productId}');
    }
    print(cartproductid[0].toString());
    print('blank');
    setState(() {});
  }

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;
    cartController.getcount.value=length;
    print('Lenght of local cart $length');
    items = dataList
        .map(
          (item) => Place(
              id: item['id'],
              quantity: item['quantity'],
              shopId: item['shopId']),
        )
        .toList();
    for (int i = 0; i < length; i++) {
      widget.productId == items[i].id
          ? localcount = int.parse(items[i].quantity)
          : print('');
      print('items ID: ${items[i].id}\n item quantity : ${items[i].quantity}');
    }
    print('here it id $items');
    setState(() {
      // print('shopIdshopId is : ${dataList[0]['shopId'].toString()}');
      // print('id is : ${dataList[0]['id'].toString()}');
      // print('quantity is : ${dataList[0]['quantity'].toString()}');
    });
  }

  @override
  void onDeleteCartError(String error) {
    // TODO: implement onDeleteCartError
  }

  @override
  void onDeleteCartSuccess(DeleteCart deleteCartModel) {
    // TODO: implement onDeleteCartSuccess
    BotToast.showSimpleNotification(
        backgroundColor: Color(0xff00DBA7),
        title: "${deleteCartModel.message}");
  }
}
