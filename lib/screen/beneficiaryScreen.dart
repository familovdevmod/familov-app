import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/beneficiaryModel.dart';
import 'package:familov/model/deleteBeneficiaryModel.dart';
import 'package:familov/presenter/beneficiary_presenter.dart';
import 'package:familov/presenter/deleteBeneficiary_presenter.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Beneficiary extends StatefulWidget {
  const Beneficiary({Key? key}) : super(key: key);
  @override
  _BeneficiaryState createState() => _BeneficiaryState();
}

class _BeneficiaryState extends State<Beneficiary>
    implements GetBeneficiaryContract, DeleteBeneficiaryContract {
  List<Data_list>? statusList = [];

  UserBeneficiaryPresenter? _presenter;
  DeleteBeneficiaryPresenter? presenter;

  _BeneficiaryState() {
    _presenter = new UserBeneficiaryPresenter(this);
    presenter = new DeleteBeneficiaryPresenter(this);
  }

  var load = false;
  @override
  void initState() {
    _presenter!.getBeneficiary();
    print('$statusList-----benestatus');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return load == true
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              toolbarHeight: 80,
              automaticallyImplyLeading: true,
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text(
                'My Beneficiary',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xffF93E6C),
                ),
              ),
              actions: [
                // IconButton(
                //     icon: Icon(
                //       Icons.add,
                //       color: Colors.black54,
                //       size: 24,
                //     ),
                //     onPressed: () {
                //       Navigator.of(context).pushNamed('/BeneficiaryAddScreen');
                //     }),
                Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: Center(
                    child: InkWell(
                      child: Text(
                        '+ Add',
                        style: TextStyle(
                          fontFamily: 'Gilroy',
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff6020BD),
                        ),
                      ),
                      // Text(
                      //   '+ Add',
                      //   style: TextStyle(
                      //       fontFamily: 'Gilroy',
                      //       fontSize: 20,
                      //       fontWeight: FontWeight.w600,
                      //       color: Color(0xff6020BD)),
                      // ),
                      onTap: () {
                        Navigator.of(context).pushNamed(
                          '/BeneficiaryAddScreen',
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
            body: statusList!.isEmpty
                ? Center(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 150,
                          ),
                          Image.asset(
                            'assets/images/Notransaction.png',
                            width: 220,
                            //       height: 400,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'No beneficiary yet',
                            style: TextStyle(
                                color: Color.fromRGBO(96, 32, 189, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 22,
                                letterSpacing: 0,
                                fontWeight: FontWeight.normal,
                                height: 1.5),
                          )
                        ],
                      ),
                    ),
                  )
                : ListView.builder(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: statusList!.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(
                            bottom: 10, left: 16, right: 16, top: 10),
                        child: Card(
                          elevation: 4,
                          shadowColor: Color.fromARGB(153, 245, 245, 245),
                          child: Column(children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 8),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(
                                                bottom: 5, top: 20),
                                            child: SizedBox(
                                                // width: 170,
                                                child: Text(
                                              'Beneficiary name',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ))),
                                        Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: Container(
                                              alignment: Alignment.centerLeft,

                                              // width: 170,
                                              child: Text(
                                                'Main number',
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        79, 79, 79, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 14,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1.5),
                                              ),
                                            )),
                                        Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: SizedBox(
                                                // width: 170,
                                                child: Text(
                                              'Other number',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ))),
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    margin: EdgeInsets.only(right: 8),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      //  mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: 5, top: 20),
                                          child: SizedBox(
                                            child: Text(
                                              '${statusList![index].beneficiaryFullName}',
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: SizedBox(
                                              child: Text(
                                                '${statusList![index].beneficiaryMainPhoneNo}',
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        79, 79, 79, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 14,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1.5),
                                              ),
                                            )),
                                        Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: SizedBox(
                                                child: Text(
                                              '${statusList![index].beneficiaryAltPhoneNo}',
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ))),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8),
                              child: Container(
                                child: Transform.rotate(
                                  angle: 0.000005008956130975318,
                                  child: Divider(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      thickness: 0.4),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 15),
                              child: Row(
                                //   mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Spacer(),
                                  InkWell(
                                    child: Text(
                                      'Edit',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color.fromRGBO(96, 32, 189, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 14,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1),
                                    ),
                                    onTap: () {
                                      Navigator.pushReplacementNamed(
                                          context, '/BeneficiaryUpdateScreen',
                                          arguments: {
                                            'id': statusList![index]
                                                .beneficiaryId,
                                            'name': statusList![index]
                                                .beneficiaryFullName,
                                            'mainNumber': statusList![index]
                                                .beneficiaryMainPhoneNo,
                                            'otherNumber': statusList![index]
                                                .beneficiaryAltPhoneNo,
                                          });

                                      // Navigator.of(context)
                                      //     .pushNamed('/BeneficiaryUpdateScreen', arguments: {
                                      //       'id':  statusList![index].beneficiaryId,
                                      //   'name': statusList![index].beneficiaryFullName,
                                      //   'mainNumber':
                                      //       statusList![index].beneficiaryMainPhoneNo,
                                      //   'otherNumber':
                                      //       statusList![index].beneficiaryAltPhoneNo,
                                      // });
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 16, right: 8),
                                    child: InkWell(
                                      child: Text(
                                        'Remove',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(255, 59, 94, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                      onTap: () {
                                        _showDialog(context, index);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]),
                        ),
                      );
                    },
                  ),
          )
        : Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: Color(0xffDA3C5F),
            )),
          );
  }

  @override
  void onBeneficiaryError(String error) {}

  @override
  void onBeneficiarySuccess(BeneficiaryModel beneficiaryModel) {
    print('success');
    statusList = beneficiaryModel.data!.dataList!;
    print('$statusList-----benestatus');
    setState(() {
      load = true;
    });
  }

  @override
  void onDeleteBeneficiaryError(String error) {}

  @override
  void onDeleteBeneficiarySuccess(DeleteBeneficiary deleteBeneficiaryModel) {
    // Fluttertoast.showToast(
    //   msg: '${deleteBeneficiaryModel.message}',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
    BotToast.showSimpleNotification(
        backgroundColor: AppColors().toasterror,
        title: "${deleteBeneficiaryModel.message}");
    print('success1');
    setState(() {
      _presenter!.getBeneficiary();
      load = true;
    });
  }

  void _showDialog(BuildContext context, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              width: 233.0,
              height: 22.0,
              child: Center(
                child: Text(
                  'Delete Recipient',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.right,
                ),
              )),
          content: new Text("Are you sure you want to delete this Recipient",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      presenter!
                          .deleteBeneficiary(statusList![index].beneficiaryId);
                      setState(() {
                        Navigator.of(context).pop();
                      });
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }
}
