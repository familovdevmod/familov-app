import 'dart:io';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/getBottomButton.dart';
import 'package:familov/model/detailUpdate_model.dart';
import 'package:familov/model/detail_model.dart';
import 'package:familov/presenter/detailUpdate_presenter.dart';
import 'package:familov/presenter/detail_presenter.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:country_code_picker/country_code_picker.dart';

class MyDetailScreen extends StatefulWidget {
  const MyDetailScreen({Key? key}) : super(key: key);
  @override
  _MyDetailScreenState createState() => _MyDetailScreenState();
}

class _MyDetailScreenState extends State<MyDetailScreen>
    implements DetailContract, DetailUpdateScreenContract {
  DetailModel? detailResponsemodel;
  File? image, filepath;
  var _isLoading = false;

  var code1 = '+91';
  var code2 = '+91';
  final ImagePicker _picker = ImagePicker();
  String imagePath = '',
      imagePath1 =
          "https://www.pinclipart.com/picdir/middle/496-4968268_profile-icon-png-white-clipart.png";

  var notifyEmail = '';
  var notifyText = '';

  bool notifyEmails = false;
  bool notifyTexts = false;

  DetailPresenter? _presenter;
  DetailUpdateScreenPresenter? _detailpresenters;

  _MyDetailScreenState() {
    _presenter = new DetailPresenter(this);
    _detailpresenters = new DetailUpdateScreenPresenter(this);
  }

  var _editedProduct = Data_list(
      username: '',
      lastname: '',
      emailAddress: '',
      phoneNumber: '',
      countryId: '',
      cityId: '',
      homeAddress: '',
      postalCode: '',
      phoneCode: '',
      notificationEmail: '',
      notificationText: '');

  var _initValues = {
    'username': '',
    'lastname': '',
    'email_address': '',
    'phone_number': '',
    'country_id': '',
    'city_id': '',
    'home_address': '',
    'postal_code': '',
    'phone_code': '',
    'notification_email': '',
    'notification_text': '',
    'profile_photo': '',
  };

  bool flag = false;

  @override
  void initState() {
    _presenter!.getDetail();
    print(_initValues['username'].toString());
    print('======================================');
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  final GlobalKey<FormState> _formKey = GlobalKey();
  String? profilePath;

  Future<void> _saveForm() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState!.save();
    print('from here');
    print('1   : ${_editedProduct.username}');
    print('2   : ${_editedProduct.lastname}');
    print('3   : ${_editedProduct.cityId}');
    print('4   : ${_editedProduct.homeAddress}');
    print('5   : ${_editedProduct.homeAddress}');
    print('6   : ${_editedProduct.postalCode}');
    print('7   : ${_editedProduct.phoneCode}');
    print('8   : ${_editedProduct.phoneNumber}');
    print('9   : ${notifyEmails}');
    print('10  : ${_editedProduct.notificationText}');

    _detailpresenters!.detailUpdates(
      _editedProduct.username!,
      _editedProduct.lastname!,
      _editedProduct.cityId!,
      _editedProduct.homeAddress!,
      '',
      _editedProduct.postalCode!,
      code1,
      _editedProduct.phoneNumber!,
      notifyEmails == true ? '1' : '0',
      notifyTexts == true ? '1' : '0',
      filepath == null ? '' : filepath!.path.toString(),
    );
    print('pre1 :  $filepath');
  }

  Future<File?> getImage() async {
    // final XFile? imageP = await _picker.pickImage(source: ImageSource.camera);
    // ignore: deprecated_member_use
    var imageP = await ImagePicker().getImage(
      maxHeight: 500,
      maxWidth: 500,
      source: ImageSource.camera,
      imageQuality: 0,
    );
    image = File(imageP!.path);
    setState(() {
      filepath = image;

      if (filepath != null) {
        imagePath = "";
        print(
            'filepath... $filepath'); //filepath... File: '/data/user/0/com.example.familov/cache/image_picker2245256299161767957.png'
      } else {
        imagePath = imagePath1;
        print('filepath>>>.. $filepath');
      }
    });
    return filepath;
  }

  Future<File?> getImagegallary() async {
    // final XFile? imageP = await _picker.pickImage(source: ImageSource.camera);
    // ignore: deprecated_member_use
    var imageP = await ImagePicker().getImage(
      maxHeight: 500,
      maxWidth: 500,
      source: ImageSource.gallery,
      imageQuality: 0,
    );
    image = File(imageP!.path);
    setState(() {
      filepath = image;

      if (filepath != null) {
        imagePath = "";
        print(
            'filepath... $filepath'); //filepath... File: '/data/user/0/com.example.familov/cache/image_picker2245256299161767957.png'
      } else {
        imagePath = imagePath1;
        print('filepath>>>.. $filepath');
      }
    });
    return filepath;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: getBottomButton('Save Changes', () => _saveForm()),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 60,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'My Details',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: _isLoading == false
          ? Center(
              child: CircularProgressIndicator(
              color: Color(0xffDA3C5F),
            ))
          : SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, left: 16, right: 16),
                  child: Container(
                    child: Column(
                      children: [
                        Container(
                            child: new Column(children: [
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(top: 0.0),
                              child: new Stack(fit: StackFit.loose, children: [
                                new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    detailResponsemodel!
                                                .data!.dataList!.profilePhoto !=
                                            null
                                        ? Center(
                                            child: Container(
                                              width: 96.0,
                                              height: 96.0,
                                              decoration: filepath != null
                                                  ? BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      image: DecorationImage(
                                                          image: Image.file(
                                                                  filepath!)
                                                              .image,
                                                          fit: BoxFit.cover))
                                                  // request.files.add(await http.MultipartFile.fromPath('image', filepath));
                                                  : BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Colors.grey,
                                                      image: DecorationImage(
                                                          fit: BoxFit.cover,
                                                          image: NetworkImage(
                                                              '${detailResponsemodel!.data!.dataList!.profilePhoto!}')),
                                                    ),
                                            ),
                                          )
                                        : Center(
                                            child: Container(
                                                width: 96.0,
                                                height: 96.0,
                                                decoration: filepath != null
                                                    ? BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        image: DecorationImage(
                                                            image: Image.file(
                                                                    filepath!)
                                                                .image,
                                                            fit: BoxFit.cover),
                                                      )
                                                    // request.files.add(await http.MultipartFile.fromPath('image', filepath));
                                                    : BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Colors.grey,
                                                        image: DecorationImage(
                                                            fit: BoxFit.fill,
                                                            image: NetworkImage(
                                                                '${imagePath1}')),
                                                      )))
                                  ],
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(top: 70.0, left: 70.0),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                          onTap: () async {
                                            imagePath = '';
                                            showModalBottomSheet(
                                                backgroundColor: Colors.white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topRight: Radius
                                                                .circular(12),
                                                            topLeft:
                                                                Radius.circular(
                                                                    12))),
                                                isScrollControlled: true,
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return Container(
                                                    decoration: BoxDecoration(),
                                                    height: 90,
                                                    child: Column(
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 8.0),
                                                          child: Text(
                                                            'Select Source',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16),
                                                          ),
                                                        ),
                                                        Divider(),
                                                        Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            SizedBox(
                                                              width: 80,
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                getImage();
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              child: Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Padding(
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            8.0),
                                                                    child: Icon(
                                                                      Icons
                                                                          .camera,
                                                                      size: 30,
                                                                      color: AppColors()
                                                                          .toastsuccess,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    'Camera',
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 20,
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                getImagegallary();
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              child: Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Padding(
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            8.0),
                                                                    child: Icon(
                                                                      Icons
                                                                          .collections_outlined,
                                                                      color: AppColors()
                                                                          .toastsuccess,
                                                                      size: 30,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                      'Gallary'),
                                                                ],
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 80,
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                });

                                            //  ImageCache();

                                            // takefoto();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                        0, 219, 167, 1)
                                                    .withOpacity(.6),
                                                shape: BoxShape.circle),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.colorize_outlined,
                                                color: Colors.grey.shade200,
                                                size: 16,
                                              ),
                                            ),
                                          )
                                          // Container(
                                          //   //   final bytes = image. readAsBytesSync(). lengthInBytes;
                                          //   alignment: Alignment.center,
                                          //   width: 30.0,
                                          //   height: 30.0,
                                          //   decoration: BoxDecoration(

                                          //     shape: BoxShape.circle,
                                          //     color:
                                          //         Color.fromRGBO(0, 219, 167, 1),
                                          //   ),
                                          //   child: Padding(
                                          //     padding: const EdgeInsets.all(8.0),
                                          //     child: Icon(
                                          //       Icons.add_a_photo_outlined,
                                          //       color: Colors.white,
                                          //     ),
                                          //   ),
                                          // ),
                                          ),
                                    ],
                                  ),
                                )
                              ])),
                        ])),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          child: Text(
                            'Tab on the fields to make the changes',
                            style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Gilroy',
                                color: Color(0xff828282),
                                fontSize: 14),
                          ),
                        ),
                        Column(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 20,
                              ),
                              child: Text('First Name'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 10,
                              ),
                              child: TextFormField(
                                  onTap: () {
                                    print(detailResponsemodel!
                                        .data!.dataList!.username!);
                                    print(_initValues['username']);
                                  },
                                  initialValue: _initValues['username'] == ''
                                      ? detailResponsemodel!
                                          .data!.dataList!.username!
                                      : _initValues['username'],
                                  decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        borderSide: BorderSide(
                                          color: Color(0xFF7F7F7F),
                                          width: 0.5,
                                        ),
                                      ),
                                      hintText: 'My first name',
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffDA3C5F)),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8))),
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 16,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8)),
                                          borderSide: BorderSide(
                                              color: Color(0xffDA3C5F)))),
                                  // controller: _firstNameController,
                                  keyboardType: TextInputType.text,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Name is empty!';
                                    }
                                    return null;
                                  },
                                  // onChanged: (text) => {print('textData : $text')},
                                  onSaved: (value) {
                                    _editedProduct = Data_list(
                                      username: value,
                                      lastname: _editedProduct.lastname,
                                      emailAddress: _editedProduct.emailAddress,
                                      phoneNumber: _editedProduct.phoneNumber,
                                      countryId: _editedProduct.countryCode,
                                      cityId: _editedProduct.cityId,
                                      homeAddress: _editedProduct.homeAddress,
                                      postalCode: _editedProduct.postalCode,
                                    );
                                  }),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 20,
                              ),
                              child: Text('Last Name'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 10,
                              ),
                              child: TextFormField(
                                initialValue: _initValues['lastname'] == ''
                                    ? detailResponsemodel!
                                        .data!.dataList!.lastname!
                                    : _initValues['lastname'],
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(
                                        color: Color(0xFF7F7F7F),
                                        width: 0.5,
                                      ),
                                    ),
                                    hintText: 'My last name',
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)))),
                                // controller: _lastNameController,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'last Name is empty!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedProduct = Data_list(
                                    username: _editedProduct.username,
                                    lastname: value,
                                    emailAddress: _editedProduct.emailAddress,
                                    phoneNumber: _editedProduct.phoneNumber,
                                    countryId: _editedProduct.countryCode,
                                    cityId: _editedProduct.cityId,
                                    homeAddress: _editedProduct.homeAddress,
                                    postalCode: _editedProduct.postalCode,
                                  );
                                  print('text11 : ${_editedProduct.username}');
                                },
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 25,
                              ),
                              child: Text('Email'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 5,
                              ),
                              child: TextFormField(
                                readOnly: true,
                                initialValue: _initValues['email_address'] == ''
                                    ? detailResponsemodel!
                                        .data!.dataList!.emailAddress!
                                    : _initValues['email_address'],

                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(
                                        color: Color(0xFF7F7F7F),
                                        width: 0.5,
                                      ),
                                    ),
                                    hintText: 'My Email',
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)))),
                                // controller: _emailController,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Email is empty!';
                                  }
                                  if (!value.contains('@')) {
                                    return 'Invalid email!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedProduct = Data_list(
                                    username: _editedProduct.username,
                                    lastname: _editedProduct.lastname,
                                    emailAddress: value,
                                    phoneNumber: _editedProduct.phoneNumber,
                                    countryId: _editedProduct.countryCode,
                                    cityId: _editedProduct.cityId,
                                    homeAddress: _editedProduct.homeAddress,
                                    postalCode: _editedProduct.postalCode,
                                  );
                                },
                              ),
                            ),
                            Container(
                              // height: 110,
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                      top: 25,
                                    ),
                                    child: Text('Mobile Number'),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height: 49,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Color(0xFF7F7F7F),
                                                width: 0.5,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0))),
                                          child: CountryCodePicker(
                                            onChanged: (e) {
                                              code1 = e.dialCode.toString();
                                              print('code1$code1');
                                            },
                                            initialSelection:
                                                _initValues['phone_code'] == ''
                                                    ? detailResponsemodel!.data!
                                                        .dataList!.phoneCode
                                                    : _initValues['phone_code'],
                                            showCountryOnly: false,
                                            showOnlyCountryWhenClosed: false,
                                            favorite: const ['+39', 'FR'],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 18,
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.75,
                                          child: Container(
                                            child: TextFormField(
                                              initialValue: _initValues[
                                                          'phone_number'] ==
                                                      ''
                                                  ? detailResponsemodel!.data!
                                                      .dataList!.phoneNumber
                                                  : _initValues['phone_number'],
                                              decoration: InputDecoration(
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    borderSide: BorderSide(
                                                      color: Color(0xFF7F7F7F),
                                                      width: 0.5,
                                                    ),
                                                  ),
                                                  hintText: 'My mobile number',
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Color(
                                                              0xffDA3C5F)),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  8))),
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  8)),
                                                      borderSide: BorderSide(
                                                          color: Color(
                                                              0xffDA3C5F)))),
                                              //    controller: _mobileController,
                                              keyboardType:
                                                  TextInputType.number,
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return 'Mobile number is empty!';
                                                }
                                                return null;
                                              },
                                              onSaved: (value) {
                                                _editedProduct = Data_list(
                                                  username:
                                                      _editedProduct.username,
                                                  lastname:
                                                      _editedProduct.lastname,
                                                  emailAddress: _editedProduct
                                                      .emailAddress,
                                                  phoneNumber: value,
                                                  countryId: _editedProduct
                                                      .countryCode,
                                                  cityId: _editedProduct.cityId,
                                                  homeAddress: _editedProduct
                                                      .homeAddress,
                                                  postalCode:
                                                      _editedProduct.postalCode,
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 14,
                              ),
                              child: Text('City'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 5,
                              ),
                              child: TextFormField(
                                initialValue: _initValues['city_id'],
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(
                                        color: Color(0xFF7F7F7F),
                                        width: 0.5,
                                      ),
                                    ),
                                    hintText: 'City',
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)))),
                                // controller: _cityController,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'City is empty!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedProduct = Data_list(
                                    username: _editedProduct.username,
                                    lastname: _editedProduct.lastname,
                                    emailAddress: _editedProduct.emailAddress,
                                    phoneNumber: _editedProduct.phoneNumber,
                                    countryId: _editedProduct.countryCode,
                                    cityId: value,
                                    homeAddress: _editedProduct.homeAddress,
                                    postalCode: _editedProduct.postalCode,
                                  );
                                },
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 25,
                              ),
                              child: Text('Address Line 1'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 5,
                              ),
                              child: TextFormField(
                                initialValue: _initValues['home_address'],
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(
                                        color: Color(0xFF7F7F7F),
                                        width: 0.5,
                                      ),
                                    ),
                                    hintText: 'Address Line 1',
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)))),
                                // controller: _addressNameController,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Address is empty!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedProduct = Data_list(
                                    username: _editedProduct.username,
                                    lastname: _editedProduct.lastname,
                                    emailAddress: _editedProduct.emailAddress,
                                    phoneNumber: _editedProduct.phoneNumber,
                                    countryId: _editedProduct.countryCode,
                                    cityId: _editedProduct.cityId,
                                    homeAddress: value,
                                    postalCode: _editedProduct.postalCode,
                                  );
                                },
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(
                                top: 25,
                              ),
                              child: Text('Zip Code'),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 5,
                              ),
                              child: TextFormField(
                                initialValue: _initValues['postal_code'],
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(
                                        color: Color(0xFF7F7F7F),
                                        width: 0.5,
                                      ),
                                    ),
                                    hintText: 'Zip Code',
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        borderSide: BorderSide(
                                            color: Color(0xffDA3C5F)))),
                                //  controller: _zipController,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Zip Code is empty!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedProduct = Data_list(
                                    username: _editedProduct.username,
                                    lastname: _editedProduct.lastname,
                                    emailAddress: _editedProduct.emailAddress,
                                    phoneNumber: _editedProduct.phoneNumber,
                                    countryId: _editedProduct.countryCode,
                                    cityId: _editedProduct.cityId,
                                    homeAddress: _editedProduct.homeAddress,
                                    postalCode: value,
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Your Preferences',
                            style: TextStyle(
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                color: Color(0xff4D4D4D)),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Choose how you want to receive notifications',
                            style: TextStyle(
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                color: Color(0xff828282)),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 24,
                          child: Row(
                            children: <Widget>[
                              Checkbox(
                                value: notifyEmails,
                                onChanged: (notifyEmails) {
                                  setState(() {
                                    this.notifyEmails = notifyEmails!;
                                    print(
                                        'email notify : ${this.notifyEmails}');
                                  });
                                },
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                'News and offers by Email',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.w500,
                                    height: 1.5),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 24,
                          child: Row(
                            children: <Widget>[
                              Checkbox(
                                value: notifyTexts,
                                onChanged: (notifyTexts) {
                                  setState(() {
                                    this.notifyTexts = notifyTexts!;
                                    print('text notify : ${this.notifyTexts}');
                                  });
                                },
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                'News and offers by Text',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.w500,
                                    height: 1.5),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  @override
  void onDetailError(String error) {}

  @override
  void onDetailSuccess(DetailModel detailResponse) {
    print('login res DetailModel ${detailResponse.data!.dataList?.username}');
    detailResponsemodel = detailResponse;
    setState(() {
      flag = true;
      _isLoading = true;
      notifyEmail = detailResponsemodel!.data!.dataList!.notificationEmail!;
      notifyText = detailResponsemodel!.data!.dataList!.notificationText!;

      if (notifyEmail == '1') {
        notifyEmails = true;
      } else {
        notifyEmails = false;
      }
      if (notifyText == '1') {
        notifyTexts = true;
      } else {
        //861488048727957
        notifyTexts = false;
      }
      print('load $_isLoading');
      setState(() {});
      _initValues = {
        'username': detailResponsemodel!.data!.dataList!.username!,
        'lastname': detailResponsemodel!.data!.dataList!.lastname!,
        'email_address': detailResponsemodel!.data!.dataList!.emailAddress!,
        'phone_number': detailResponsemodel!.data!.dataList!.phoneNumber!,
        'country_id': detailResponsemodel!.data!.dataList!.countryId!,
        'city_id': detailResponsemodel!.data!.dataList!.cityId!,
        'home_address': '${detailResponsemodel!.data!.dataList!.homeAddress!}',
        'postal_code': detailResponsemodel!.data!.dataList!.postalCode!,
        'phone_code': detailResponsemodel!.data!.dataList!.phoneCode!,
        //   'profile_photo': detailResponsemodel!.data!.dataList!.profilePhoto,
      };
      setState(() {
        _isLoading = true;
      });
    });
  }

  @override
  void onDetailUpdateError(String error) {
    print('error message------->$error');
    Fluttertoast.showToast(
      msg: '$error ',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.black45,
      fontSize: 18,
    );
  }

  @override
  void onDetailUpdateSuccess(Detailupdatemodel update) {
    print('error message------->${update.message}');
    if (update.message != null || update.message != '') {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${update.message}");
      Navigator.pop(context);
    }

    // update.message == null || update.message == ''
    //     ? Fluttertoast.showToast(
    //         msg: 'Choose another image',
    //         toastLength: Toast.LENGTH_SHORT,
    //         gravity: ToastGravity.BOTTOM,
    //         backgroundColor: Colors.black45,
    //         fontSize: 18,
    //       )
    //     : Navigator.of(context).pop();
  }

  // Future takefoto() async {
  //   File? image;
  //   try {
  //     final image = await ImagePicker().pickImage(source: ImageSource.camera);
  //     if (image == null) return;
  //     final imageTemp = File(image.path);
  //     setState(() => this.image = imageTemp);
  //   } on PlatformException catch (e) {
  //     print('Failed to pick image: $e');
  //   }
  // }
}
