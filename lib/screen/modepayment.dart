import 'package:familov/model/homeModel.dart';
import 'package:familov/model/summmaryy.dart';
import 'package:familov/presenter/home_presenter.dart';
import 'package:familov/presenter/summary_presenter.dart';
import 'package:familov/screen/transactionDetailScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CartScreen.dart';

// ignore: must_be_immutable
class ModePayment extends StatefulWidget {
  String? promo;
  var charges;
  ModePayment({Key? key, this.charges, required this.promo}) : super(key: key);
  @override
  _ModePaymentState createState() => _ModePaymentState();
}

SummaryPresenter? presenter4;
Data12? list1;

class _ModePaymentState extends State<ModePayment>
    implements HomeContract, SummaryContract {
  var load = true;
  var load1 = true;
  var confirm = false;

  var isLoading = 'loading';

  String pickupFromShopStatus = '';

  String homeDeliveryStatus = '';
  String homeaddresssprefs = '';

  pref(mode) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('mode', mode);
  }

  final myController1 = TextEditingController();
  var pickupcharge = '';
  var homecharge = '';
  late HomePresenter _homePresenter;
  _ModePaymentState() {
    presenter4 = new SummaryPresenter(this);
    _homePresenter = HomePresenter(this);
  }

  @override
  void initState() {
    presenter4!.summary('pickup', '');
    presenter4!.summary('home_delivery', '');
    Future.delayed(Duration(milliseconds: 1000), () {});
    print('${widget.charges} delivery charges');
    super.initState();
    print('shopIdddd $shopIdddd');
    _homePresenter.home(shopIdddd, '1');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Mode of Delivery',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 25,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        body: isLoading == "loading"
            ? Center(
                child: CircularProgressIndicator(color: Color(0xffDA3C5F)),
              )
            : isLoading == "failed"
                ? Center(
                    child: Text(
                        'Something went wrong!\nCheck your internet connection'),
                  )
                : Container(
                    child: Column(
                    children: [
                      pickupFromShopStatus != "Activated"
                          ? Container()
                          : load == true
                              ? Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 15, top: 20),
                                  // width: MediaQuery.of(context).size.width,
                                  // height: MediaQuery.of(context).size.height / 6,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(4),
                                        topRight: Radius.circular(4),
                                        bottomLeft: Radius.circular(4),
                                        bottomRight: Radius.circular(4),
                                      ),
                                      border: Border.all(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        width: 0.5,
                                      )),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Center(
                                                child: Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Image.asset(
                                                      'assets/images/store.png',
                                                    )), //pickup
                                              ),
                                              SizedBox(
                                                width: 20,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Pickup in Store  ',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            79, 79, 79, 1),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 18,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                  Text(
                                                    'Charges ${widget.charges.toString()}$pickupcharge',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                                79, 79, 79, 1)
                                                            .withOpacity(.8),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 14,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                ],
                                              ),
                                              Spacer(),
                                              InkWell(
                                                child: Text(
                                                  'Select',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          0, 219, 167, 1),
                                                      fontFamily: 'Gilroy',
                                                      fontSize: 18,
                                                      letterSpacing: 0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      height: 1.5),
                                                ),
                                                onTap: () {
                                                  print(
                                                      '${widget.promo}-----widgetpromo');
                                                  if (load == true) {
                                                    load = false;
                                                  } else {
                                                    load = true;
                                                  }
                                                  setState(() {
                                                    pref('Pickup in Store');
                                                    Navigator.of(context)
                                                        .pop('pickup');
                                                    // Navigator.pushReplacement(
                                                    //     context,
                                                    //     MaterialPageRoute(
                                                    //         builder: (context) =>
                                                    //             TransactionDetailScreen(
                                                    //               address: '',
                                                    //               promo: widget
                                                    //                   .promo,
                                                    //             )));
                                                  });
                                                },
                                              ),
                                              SizedBox(
                                                width: 15,
                                              )
                                            ],
                                          ),
                                        ),
                                      ]))
                              : Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(4),
                                        topRight: Radius.circular(4),
                                        bottomLeft: Radius.circular(4),
                                        bottomRight: Radius.circular(4),
                                      ),
                                      border: Border.all(
                                        width: 0.5,
                                      )),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: [
                                            Center(
                                                child: Image.asset(
                                              'assets/images/store.png',
                                            )),
                                            Text(
                                              'Pickup in Store ',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                            Spacer(),
                                            InkWell(
                                              child: Text(
                                                'Select',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        0, 219, 167, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 18,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1.5),
                                              ),
                                              onTap: () {
                                                print(
                                                    '${widget.promo}-----widgetpromo');
                                                if (load == true) {
                                                  load = false;
                                                } else {
                                                  load = true;
                                                }
                                                setState(() {
                                                  pref('Pickup in Store');
                                                  Navigator.of(context)
                                                      .pop('pickup');
                                                  // Navigator.pushReplacement(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) =>
                                                  //             TransactionDetailScreen(
                                                  //               address: '',
                                                  //               promo: widget
                                                  //                   .promo,
                                                  //             )));
                                                });
                                              },
                                            ),
                                            SizedBox(
                                              width: 15,
                                            )
                                          ],
                                        ),
                                      ])),
                      homeDeliveryStatus != "Activated"
                          ? Container()
                          : load1 == true
                              ? Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 15, top: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(4),
                                      topRight: Radius.circular(4),
                                      bottomLeft: Radius.circular(4),
                                      bottomRight: Radius.circular(4),
                                    ),
                                    border: Border.all(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      width: 0.5,
                                    ),
                                  ),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Row(
                                            children: [
                                              Center(
                                                child: Image.asset(
                                                  'assets/images/carbon_delivery.png',
                                                ),
                                              ),
                                              SizedBox(
                                                width: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Home Delivery',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            79, 79, 79, 1),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 18,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                  Text(
                                                    'Charges ${widget.charges.toString()}$homecharge  ',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                                79, 79, 79, 1)
                                                            .withOpacity(.8),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 14,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                ],
                                              ),
                                              Spacer(),
                                              InkWell(
                                                child: Text(
                                                  'Select',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          0, 219, 167, 1),
                                                      fontFamily: 'Gilroy',
                                                      fontSize: 18,
                                                      letterSpacing: 0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      height: 1.5),
                                                ),
                                                onTap: () {
                                                  saveaddress();
                                                  print(
                                                      '${widget.promo}-----widgetpromo');
                                                  if (load1 == true) {
                                                    load1 = false;
                                                  } else {
                                                    load1 = true;
                                                  } //home_delivery
                                                  setState(() {
                                                    pref('Home Delivery');

                                                    confirm = true;
                                                    // Navigator.pushReplacement(
                                                    //     context,
                                                    //     MaterialPageRoute(
                                                    //         builder: (context) =>
                                                    //             TransactionDetailScreen())
                                                    // );
                                                  });
                                                },
                                              ),
                                              SizedBox(
                                                width: 15,
                                              )
                                            ],
                                          ),
                                        ),
                                      ]))
                              : Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 15, top: 20),
                                  width: MediaQuery.of(context).size.width,
                                  // height: confirm == false
                                  //     ? MediaQuery.of(context).size.height / 6
                                  //     : MediaQuery.of(context).size.height / 3,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(4),
                                      topRight: Radius.circular(4),
                                      bottomLeft: Radius.circular(4),
                                      bottomRight: Radius.circular(4),
                                    ),
                                    border: Border.all(
                                      color: AppColors().appaccent,
                                      width: 0.5,
                                    ),
                                  ),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Row(
                                            children: [
                                              Center(
                                                child: Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Image.asset(
                                                      'assets/images/carbon_delivery.png',
                                                    )),
                                              ),
                                              SizedBox(
                                                width: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Home Delivery',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            79, 79, 79, 1),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 18,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                  Text(
                                                    'Charges ${widget.charges.toString()}$homecharge',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                                79, 79, 79, 1)
                                                            .withOpacity(.8),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 14,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                ],
                                              ),
                                              Spacer(),
                                              SizedBox(
                                                width: 15,
                                              )
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              16, 0, 16, 10),
                                          child: Container(
                                              // margin: EdgeInsets.only(left: 30),
                                              child: Text(
                                            'Address',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    77, 77, 77, 1),
                                                fontFamily: 'Gilroy',
                                                fontSize: 14,
                                                letterSpacing: 0,
                                                fontWeight: FontWeight.w600,
                                                height: 1),
                                          )),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              16, 0, 16, 16),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(4),
                                                topRight: Radius.circular(4),
                                                bottomLeft: Radius.circular(4),
                                                bottomRight: Radius.circular(4),
                                              ),
                                              border: Border.all(
                                                color: Colors.grey,
                                                width: 0.5,
                                              ),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10, top: 10),
                                              child: TextField(
                                                maxLines: 4,
                                                decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  focusedBorder:
                                                      InputBorder.none,
                                                  enabledBorder:
                                                      InputBorder.none,
                                                  errorBorder: InputBorder.none,
                                                  disabledBorder:
                                                      InputBorder.none,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 11,
                                                          top: 5,
                                                          right: 15),
                                                  hintText: "Enter Address",
                                                  hintStyle: TextStyle(
                                                    color: Colors
                                                        .black54, // <-- Change this
                                                    fontSize: null,
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        130, 130, 130, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 16,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1),
                                                controller: myController1,
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          child: Container(
                                            alignment: Alignment.center,
                                            height: 40,
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    0, 219, 167, 1),
                                                borderRadius:
                                                    BorderRadius.circular(6)),
                                            margin: EdgeInsets.only(
                                                left: 10,
                                                right: 10,
                                                bottom: 15,
                                                top: 20),
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: Text(
                                              'Select',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.5),
                                            ),
                                          ),
                                          onTap: () {
                                            saveaddress();
                                            if (load1 == true) {
                                              load1 = false;
                                            } else {
                                              load1 = true;
                                            }
                                            setState(() {
                                              pref('Home Delivery');
                                              if (myController1.text != '') {
                                                homeaddresssprefs =
                                                    myController1.text;
                                                print(
                                                    'control -${myController1.text}');
                                                Navigator.of(context)
                                                    .pop('home_delivery');
                                                // Navigator.pushReplacement(
                                                //     context,
                                                //     MaterialPageRoute(
                                                //         builder: (context) =>
                                                //             TransactionDetailScreen(
                                                //               address:
                                                //                   myController1
                                                //                       .text,
                                                //               promo: widget
                                                //                   .promo,
                                                //             )));
                                              }
                                            });
                                          },
                                        ),
                                      ]))
                    ],
                  )),
      ),
    );
  }

  @override
  void onHomeError(String error) {
    // TODO: implement onHomeError
    setState(() {
      isLoading = "failed";
    });
  }

  @override
  void onHomeSuccess(HomeModel homeModel) {
    setState(() {
      isLoading = "false";
    });
    // TODO: implement onHomeSuccess
    print("homeeemeememe ${homeModel.message}");
    print("homeeemeememe ${homeModel.data!.shopDetails!.pickupFromShopStatus}");
    print("homeeemeememe ${homeModel.data!.shopDetails!.homeDeliveryStatus}");

    pickupFromShopStatus = homeModel.data!.shopDetails!.pickupFromShopStatus!;
    homeDeliveryStatus = homeModel.data!.shopDetails!.homeDeliveryStatus!;
  }

  void saveaddress() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('homeaddress', homeaddresssprefs);
    print('$homeaddresssprefs---here we go baby');
  }

  @override
  void onSummaryError(String error) {
    // TODO: implement onSummaryError
  }

  @override
  void onSummarySuccess(Summmaryy summaryModel) {
    list1 = summaryModel.data;

    list1!.deliveryMode == 'pickup'
        ? pickupcharge = list1!.deliveryCharge
        : print('00');
    list1!.deliveryMode == 'home_delivery'
        ? homecharge = list1!.deliveryCharge
        : print('00');
    print('modeeeeeeee $pickupcharge');
    print('modeeeeeeee $homecharge');
    setState(() {});

    // TODO: implement onSummarySuccess
  }
}
