import 'dart:convert';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/model/reorder_model.dart';
import 'package:familov/presenter/clearCart+presenter.dart';
import 'package:familov/presenter/reorder_Presenter.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:familov/model/getUserTransaction.dart';
import 'package:familov/presenter/getUserTransaction_presenter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AllScreen extends StatefulWidget {
  final String data;

  const AllScreen({Key? key, required this.data}) : super(key: key);

  @override
  _AllScreenState createState() => _AllScreenState();
}

class _AllScreenState extends State<AllScreen>
    implements
        GetUserTransactionContract,
        ReorderPresenterContract,
        ClearCartContract {
  var dateIs;
  var a;
  String message = "";
  int currentPage = 1;
  bool isRefresh = false;
  int offset = 0;
  int limit = 4;
  var isR = false;
  var check = "All";
  bool load = true;

  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  ScrollController? _controller;

  late int totalPages;
  List<Data_list> passengers = [];
  UserTransactionPresenter? _presenter;
  ClearCartPresenter? presenterclear;
  ReorderPresenter? _reorderpresenter;
  List<Data_list>? statusList = [];
  var flag = true;
  int length = 0;
  var indexnumber;

  //var _isLoading = false;
  bool isRefreshValue = true;

  _AllScreenState() {
    _presenter = new UserTransactionPresenter(this);
    _reorderpresenter = new ReorderPresenter(this);
    presenterclear = new ClearCartPresenter(this);
  }

  // Future<bool> getPassengerData({bool isRefreshValue = false}) async {
  //   print('refresh $isRefreshValue');
  //   if (isRefreshValue) {
  //     currentPage = 1;
  //   } else {
  //     if (currentPage > totalPages) {
  //       refreshController.loadNoData();
  //       return false;
  //     }
  //   }
  //   isRefresh = isRefreshValue;
  //   _presenter!.getUserTransaction(widget.data, currentPage);
  //   return true;
  // }

  _scrollListener() {
    // print('scroll check 1');
    if (_controller!.offset >= _controller!.position.maxScrollExtent &&
        !_controller!.position.outOfRange) {
      setState(() {
        print('scroll check 2');

        isRefresh = false;
        _presenter!.getUserTransaction(widget.data, currentPage, '');

        message = "reach the bottom";
      });
    }
    // if (_controller!.offset <= _controller!.position.minScrollExtent &&
    //     !_controller!.position.outOfRange) {
    //   setState(() {
    //     print('scroll check 3');
    //     statusList = [];
    //     currentPage = 1;
    //     isRefresh = true;
    //     _presenter!.getUserTransaction(widget.data, currentPage, '');
    //     message = "reach the top";
    //   });
    // }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller!.addListener(_scrollListener);
    _presenter!.getUserTransaction(widget.data, currentPage, '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: load
          ? Container(
              alignment: Alignment.center,
              color: Colors.white,
              child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              ))
          : statusList?.length == 0
              ? Center(
                  child: Container(
                    // child: LoadingIndicator()
                    child: Image.asset(
                      'assets/images/Notransaction.png',
                      width: 200,
                      height: 400,
                    ),
                  ),
                )
              : ListView.builder(
                  controller: _controller,
                  itemCount: statusList?.length,
                  itemBuilder: (context, index) {
                    dateIs = (statusList![index].dateTime)!;
                    var testCase = DateTime.parse('$dateIs');
                    String time =
                        DateFormat('MMMM dd, yyyy | hh:mm a').format(testCase);
                    //  print(time);
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10, left: 16, right: 16, top: 0),
                          child: Card(
                            elevation: 4,
                            shadowColor: Color.fromARGB(153, 245, 245, 245),
                            child: Column(children: <Widget>[
                              Row(children: [
                                Container(
                                  margin: EdgeInsets.only(left: 15, top: 10),
                                  child: Text(
                                    '${statusList![index].generateCode}',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                                ),
                                Spacer(),
                                check == ""
                                    ? Container()
                                    : Container(
                                        margin:
                                            EdgeInsets.only(right: 5, top: 15),
                                        width: 90,
                                        height: 20,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            topRight: Radius.circular(8),
                                            bottomLeft: Radius.circular(8),
                                            bottomRight: Radius.circular(8),
                                          ),
                                          color: statusList![index]
                                                      .orderStatus ==
                                                  'Wait'
                                              ? Color.fromRGBO(242, 201, 76, 1)
                                              : statusList![index]
                                                          .orderStatus ==
                                                      'Delivered'
                                                  ? Color(0xff00DBA7)
                                                  : statusList![index]
                                                              .orderStatus ==
                                                          'Cancel'
                                                      ? Colors.red
                                                      : Color.fromRGBO(
                                                          242, 201, 76, 1),
                                        ),
                                        child: Text(
                                          '${statusList![index].orderStatus}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  255, 255, 255, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 12,
                                              fontWeight: FontWeight.normal,
                                              height: 1.4),
                                        )),
                              ]),
                              Container(
                                alignment: Alignment.topLeft,
                                margin: EdgeInsets.only(left: 15, top: 5),
                                child: Text(
                                  '$time',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 12,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 15, top: 0),
                                    child: Text(
                                      'Payment : ${statusList![index].paymentMethod}',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color.fromRGBO(79, 79, 79, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 13,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    margin: EdgeInsets.only(
                                      right: 5,
                                    ),
                                    child: Text(
                                      '${statusList![index].paymentCurrCode}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color.fromRGBO(0, 219, 167, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 18,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      right: 15,
                                    ),
                                    child: Text(
                                      '${statusList![index].grandTotal}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color.fromRGBO(0, 219, 167, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 18,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  ),
                                ],
                              ),
                              // SizedBox(
                              //   height: 5,
                              // ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 10),
                                child: Transform.rotate(
                                  angle: 0.000005008956130975318,
                                  child: Divider(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      thickness: 0.5),
                                ),
                              ),
                              Container(
                                height: 15,
                                width: MediaQuery.of(context).size.width,
                                // color: Colors.red,
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(
                                  top: 4,
                                  bottom: 4,
                                ),
                                child: Row(
                                  children: [
                                    InkWell(
                                      child: Container(
                                        margin: EdgeInsets.only(left: 15),
                                        child: Text(
                                          'Invoice',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(77, 77, 77, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 12,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1),
                                        ),
                                      ),
                                      onLongPress: () {
                                        print(
                                            '${statusList![index].deliveryOption}-amount');
                                        print(
                                            '${statusList![index].promoCode}-amount');
                                        print(
                                            '${statusList![index].discount}-amount');
                                      },
                                      onTap: () {
                                        print(
                                            '${statusList![index].grandTotal},${statusList![index].deliveryCharges},${statusList![index].generateCode}----,${statusList![index].receiverPhone},${statusList![index].subTotal},${statusList![index].serviceTax},${statusList![index].dateTime},${statusList![index].greetingMsg},${statusList![index].deliveryOption},${statusList![index].anotherPhoneNumber},here${statusList![index].homeDeliveryAddress}here${statusList![index].receiverPhone},${statusList![index].paymentCurr}--here i am${statusList![index].orderId}');
                                        print(
                                            '${statusList![index].deliveryOption}-amount');
                                        print(
                                            'GenerateCode${statusList![index].generateCode}');
                                        var code = statusList![index]
                                            .generateCode
                                            .toString();
                                        print('Code is  :  $code');
                                        code.startsWith('F')
                                            ? Navigator.of(context).pushNamed(
                                                '/ShopInvoiceScreen',
                                                arguments: {
                                                  'greeting_msg':
                                                      statusList![index]
                                                          .greetingMsg,
                                                  'currency_check':
                                                      statusList![index]
                                                          .paymentCurrCode,
                                                  'grand_total':
                                                      statusList![index]
                                                          .grandTotal,
                                                  'delivery_price':
                                                      statusList![index]
                                                          .deliveryCharges,
                                                  'generatedArgument':
                                                      statusList![index]
                                                          .generateCode,
                                                  'mobileArgument':
                                                      statusList![index]
                                                          .receiverPhone,
                                                  'sub_totalArgument':
                                                      statusList![index]
                                                          .subTotal,
                                                  'service_taxArgument':
                                                      statusList![index]
                                                          .serviceTax,
                                                  'urlData':
                                                      '?order_id=${statusList![index].orderId}',
                                                  'greeting_msg':
                                                      statusList![index]
                                                          .greetingMsg,
                                                  'date_time':
                                                      statusList![index]
                                                          .dateTime,
                                                  'delivery_option':
                                                      statusList![index]
                                                          .deliveryOption,
                                                  'receiver_name':
                                                      statusList![index]
                                                          .receiverName,
                                                  'receiver_phone':
                                                      statusList![index]
                                                          .receiverPhone,
                                                  'another_phone_number':
                                                      statusList![index]
                                                          .anotherPhoneNumber,
                                                  'payment_curr_code':
                                                      statusList![index]
                                                          .paymentCurrCode,
                                                  'home_delivery_address':
                                                      statusList![index]
                                                          .homeDeliveryAddress,
                                                  'promo_discount_amount':
                                                      statusList![index]
                                                          .promoDiscountAmount,
                                                  'promo_code':
                                                      statusList![index]
                                                          .promoCode,
                                                },
                                              )
                                            : Navigator.of(context).pushNamed(
                                                '/MobileInvoiceScreen',
                                                arguments: {
                                                  'greeting_msg':
                                                      statusList![index]
                                                          .greetingMsg,
                                                  'currency_check':
                                                      statusList![index]
                                                          .paymentCurrCode,
                                                  'grand_total':
                                                      statusList![index]
                                                          .grandTotal,
                                                  'delivery_price':
                                                      statusList![index]
                                                          .deliveryCharges,
                                                  'generatedArgument':
                                                      statusList![index]
                                                          .generateCode,
                                                  'mobileArgument':
                                                      statusList![index]
                                                          .receiverPhone,
                                                  'sub_totalArgument':
                                                      statusList![index]
                                                          .subTotal,
                                                  'service_taxArgument':
                                                      statusList![index]
                                                          .serviceTax,
                                                  'urlData':
                                                      '?order_id=${statusList![index].orderId}',
                                                  'greeting_msg':
                                                      statusList![index]
                                                          .greetingMsg,
                                                  'date_time':
                                                      statusList![index]
                                                          .dateTime,
                                                  'delivery_option':
                                                      statusList![index]
                                                          .deliveryOption,
                                                  'receiver_name':
                                                      statusList![index]
                                                          .receiverName,
                                                  'receiver_phone':
                                                      statusList![index]
                                                          .receiverPhone,
                                                  'another_phone_number':
                                                      statusList![index]
                                                          .anotherPhoneNumber,
                                                  'payment_curr_code':
                                                      statusList![index]
                                                          .paymentCurrCode,
                                                  'home_delivery_address':
                                                      statusList![index]
                                                          .homeDeliveryAddress,
                                                  'promo_discount_amount':
                                                      statusList![index]
                                                          .promoDiscountAmount,
                                                  'promo_code':
                                                      statusList![index]
                                                          .promoCode,
                                                },
                                              );
                                      },
                                    ),
                                    SizedBox(width: 25),
                                    InkWell(
                                      child: Text(
                                        'Delivery Note',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(96, 32, 189, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 12,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                      onTap: () {
                                        Navigator.of(context).pushNamed(
                                            '/DeliveryNoteScreen',
                                            arguments: {
                                              'home_delivery_address':
                                                  statusList![index],
                                              'greeting_msg': statusList![index]
                                                  .greetingMsg,
                                              'another_phone_number':
                                                  statusList![index]
                                                      .anotherPhoneNumber,
                                              'generatedArgument':
                                                  statusList![index]
                                                      .generateCode,
                                              'receiver_name':
                                                  statusList![index]
                                                      .receiverName,
                                              'receiver_phone':
                                                  statusList![index]
                                                      .receiverPhone,
                                              'date_time':
                                                  statusList![index].dateTime,
                                              'delivery_option':
                                                  statusList![index]
                                                      .deliveryOption,
                                              'urlData':
                                                  '?order_id=${statusList![index].orderId}',
                                            });
                                      },
                                    ),
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 15),
                                      child: Container(
                                        child: InkWell(
                                          child: Text(
                                            'Reorder',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    249, 62, 108, 1),
                                                fontFamily: 'Gilroy',
                                                fontSize: 12,
                                                letterSpacing: 0,
                                                fontWeight: FontWeight.normal,
                                                height: 1),
                                          ),
                                          onTap: () {
                                            _showDialog(context,
                                                statusList![index].orderId);
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 7,
                              ),
                            ]),
                          ),
                        ),
                      ],
                    );
                  }),
    );
  }

  @override
  void onTransactionError(String error) {}

  @override
  void onTransactionSuccess(GetuserTransaction transactionModel) {
    print('refresh completed3 $isRefresh');
    // print('refresh completed4 ${jsonEncode(transactionModel.data!.dataList!.length)}');
    length = transactionModel.data!.dataList!.length;
    print('refresh completed490 $length');
    if (length != 0) {
      if (isRefresh) {
        statusList = [];
        statusList = transactionModel.data!.dataList!;
      } else {
        statusList!.addAll(transactionModel.data!.dataList!.toList());
      }
      currentPage++;
    }
    setState(() {
      load = false;
    });
    // print('orader details'+statusList![0].shopId.toString()+statusList[0].);
  }

  void _showDialog(BuildContext context, String? orderId) {
    indexnumber = orderId;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              width: 233.0,
              height: 22.0,
              child: Center(
                child: Text(
                  'Reorder',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.right,
                ),
              )),
          content: new Text("Are you sure you want to Reorder this order?",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      _reorderpresenter!.Orderagain(orderId);
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                        // border: Border.all(
                        //   width: 1.0,
                        // ),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onReoderFail(String error) {
    // TODO: implement onReoderFail
  }

  @override
  void onReoderSuccess(ReorderModel reoder_model) {
    if (reoder_model.status == true) {
      BotToast.showSimpleNotification(
          title: reoder_model.message.toString(),
          backgroundColor: AppColors().toastsuccess);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen(2, '')));
    }
    if (reoder_model.errorMsg.toString() == 'Different Shop') {
      Navigator.pop(context);
      _showDialogshop(context);
    }
    print('reoder message ${reoder_model.message}');
  }

  void _showDialogshop(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AnimatedContainer(
          duration: Duration(milliseconds: 400),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            title: SizedBox(
                width: 233.0,
                height: 22.0,
                child: Center(
                  child: Text(
                    'Shop Change',
                    style: TextStyle(
                        color: AppColors().appaccent,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.right,
                  ),
                )),
            content: SizedBox(
              height: 80,
              child: Column(
                children: [
                  new Text("Are you sure you want to\nchange shop?",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18, color: Color(0xff828282))),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "(If you change the shop you will loose your cart)",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 14, color: Color(0xff828282)),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                          alignment: Alignment(0.02, 0.0),
                          width: 120,
                          height: 32.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              width: 1,
                            ),
                          ),
                          child: Text(
                            'No',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 59, 94, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 18,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w600,
                                height: 1),
                          )),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          presenterclear!.clearCart();
                        });
                      },
                      child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 120,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(0, 219, 167, 1),
                        ),
                        child: Text(
                          'Yes',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  void onClearCartError(String error) {
    // TODO: implement onClearCartError
  }

  @override
  void onClearCartSuccess(CartClear clearCart) {
    if (clearCart.status == true) {
      _reorderpresenter!.Orderagain(indexnumber);
    }
  }
}
// TODO: implement onClearCartSuccess