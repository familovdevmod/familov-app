import 'dart:convert';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/model/reorder_model.dart';
import 'package:familov/presenter/clearCart+presenter.dart';
import 'package:familov/presenter/reorder_Presenter.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:intl/intl.dart';
import 'package:familov/model/getUserTransaction.dart';
import 'package:familov/presenter/getUserTransaction_presenter.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Cancelled extends StatefulWidget {
  final String data;
  const Cancelled({Key? key, required this.data}) : super(key: key);

  @override
  _CancelledState createState() => _CancelledState();
}

class _CancelledState extends State<Cancelled>
    implements
        GetUserTransactionContract,
        ReorderPresenterContract,
        ClearCartContract {
  UserTransactionPresenter? _presenter;
  ClearCartPresenter? presenterclear;
  ReorderPresenter? _reorderpresenter;
  bool flag = false;
  bool load = true;
  var dateIs;
  int currentPage = 1;
  int offset = 0;
  int limit = 4;
  var isR = false;
  late int totalPages;
  var indexnumber;
//  var _isLoading = false;
  var check = "";
  bool isRefreshValue = true;
  List<Data_list> passengers = [];
  List<Data_list>? statusList = [];
  bool isRefresh = true;

  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  @override
  void initState() {
    _controller = ScrollController();
    _controller!.addListener(_scrollListener);
    _presenter!.getUserTransaction(widget.data, currentPage, '');
    super.initState();
  }

  _CancelledState() {
    _presenter = new UserTransactionPresenter(this);
    _reorderpresenter = new ReorderPresenter(this);
    presenterclear = new ClearCartPresenter(this);
  }
  String message = "";
  ScrollController? _controller;

  _scrollListener() {
    // print('scroll check 1');
    if (_controller!.offset >= _controller!.position.maxScrollExtent &&
        !_controller!.position.outOfRange) {
      setState(() {
        print('scroll check 2 $currentPage');
        _presenter!.getUserTransaction(widget.data, currentPage, '');
        isRefresh = true;
        message = "reach the bottom";
      });
    }
    if (_controller!.offset <= _controller!.position.minScrollExtent &&
        !_controller!.position.outOfRange) {
      setState(() {
        print('scroll check 3');
        isRefresh = false;
        currentPage = 1;
        _presenter!.getUserTransaction(widget.data, currentPage, '');

        message = "reach the top";
      });
    }
  }

  // Future<bool> getPassengerData({bool isRefreshValue = false}) async {
  //   print('refresh $isRefreshValue');
  //   if (isRefreshValue) {
  //     currentPage = 1;
  //   } else {
  //     if (currentPage > totalPages) {
  //       refreshController.loadNoData();
  //       return false;
  //     }
  //   }
  //   isRefresh = isRefreshValue;
  //   await _presenter!.getUserTransaction(widget.data, limit,);
  //   return true;
  // }

  // @override
  // void initState() {
  //   _presenter!.getUserTransaction(widget.data, limit, offset);
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return
        // _is   == false
        //     ? Center(
        //         child: CircularProgressIndicator(),
        //       )
        //     :
        Scaffold(
      backgroundColor: Colors.white,
      body:
          // SmartRefresher(
          //   controller: refreshController,
          //   enablePullUp: true,
          //   onRefresh: () async {
          //     print('check123 onrefresh completed :');
          //     offset = 0;
          //     final result = await getPassengerData(isRefreshValue: true);
          //     isR = result;
          //     if (result) {
          //       refreshController.refreshCompleted();
          //     } else {
          //       refreshController.refreshFailed();
          //     }
          //   },
          //   onLoading: () async {
          //     print('check123 onLoading completed:');
          //     final result = await getPassengerData();
          //     print('The Result $result');
          //     if (result) {
          //       refreshController.loadComplete();
          //     } else {
          //       refreshController.loadFailed();
          //     }
          //   }, //TransactionBox(data: "", dataList: statusList),
          //  child:
          load
              ? Container(
                  alignment: Alignment.center,
                  color: Colors.white,
                  child: CircularProgressIndicator(
                    color: Color(0xffDA3C5F),
                  ))
              : statusList?.length == 0
                  ? Center(
                      child: Container(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 150,
                            ),
                            Image.asset(
                              'assets/images/Notransaction.png',
                              width: 220,
                              //       height: 400,
                            ),
                            Text(
                              'No transactions yet',
                              style: TextStyle(
                                  color: Color.fromRGBO(96, 32, 189, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 25,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            )
                          ],
                        ),
                      ),
                    )
                  : ListView.builder(
                      controller: _controller,
                      itemCount: statusList?.length,
                      itemBuilder: (context, index) {
                        dateIs = (statusList![index].dateTime)!;
                        var testCase = DateTime.parse('$dateIs');
                        String time = DateFormat('MMMM dd, yyyy | hh:mm a')
                            .format(testCase);
                        print(time);
                        return Container(
                          // color: Colors.white,
                          margin:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              check == ""
                                  ? Container()
                                  : Container(
                                      width: 100,
                                      height: 16,
                                      decoration: BoxDecoration(
                                        color: statusList![index].orderStatus ==
                                                'Wait'
                                            ? Color.fromRGBO(242, 201, 76, 1)
                                            : statusList![index].orderStatus ==
                                                    'Delivered'
                                                ? Color(0xff00DBA7)
                                                : statusList![index]
                                                            .orderStatus ==
                                                        'Cancel'
                                                    ? Colors.red
                                                    : Color.fromRGBO(
                                                        242, 201, 76, 1),
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8),
                                          bottomLeft: Radius.circular(8),
                                          bottomRight: Radius.circular(8),
                                        ),
                                      ),
                                      child: Text(
                                        '${statusList![index].orderStatus}',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal,
                                            height: 1.4),
                                      )),
                              SizedBox(
                                height: 5,
                              ),
                              Card(
                                elevation: 4,
                                shadowColor: Color.fromARGB(153, 245, 245, 245),
                                child: Column(children: <Widget>[
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 15, top: 0, bottom: 10),
                                        child: Text(
                                          '${statusList![index].generateCode}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(79, 79, 79, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 16,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1.5),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                  Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(top: 5, left: 15),
                                    child: Text(
                                      '$time',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color.fromRGBO(79, 79, 79, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 12,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                          left: 15,
                                        ),
                                        child: Text(
                                          'Payment : ${statusList![index].paymentMethod}',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(79, 79, 79, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 13,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1.5),
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        margin: EdgeInsets.only(
                                          right: 5,
                                        ),
                                        child: Text(
                                          '${statusList![index].paymentCurrCode}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  0, 219, 167, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 18,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1.5),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                          right: 15,
                                        ),
                                        child: Text(
                                          '${statusList![index].grandTotal}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  0, 219, 167, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 18,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1.5),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Transform.rotate(
                                      angle: 0.000005008956130975318,
                                      child: Divider(
                                          color:
                                              Color.fromRGBO(189, 189, 189, 1),
                                          thickness: 0.5),
                                    ),
                                  ),
                                  Container(
                                    height: 15,
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      children: [
                                        InkWell(
                                          child: Container(
                                            margin: EdgeInsets.only(left: 15),
                                            child: Text(
                                              'Invoice',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      77, 77, 77, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 12,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1),
                                            ),
                                          ),
                                          onTap: () {
                                            print(
                                                'GenerateCode${statusList![index].generateCode}');
                                            var code = statusList![index]
                                                .generateCode
                                                .toString();
                                            print('Code  :  $code');
                                            code.startsWith('F')
                                                ? Navigator.of(context)
                                                    .pushNamed(
                                                    '/ShopInvoiceScreen',
                                                    arguments: {
                                                      'grand_total':
                                                          statusList![index]
                                                              .grandTotal,
                                                      'delivery_price':
                                                          statusList![index]
                                                              .deliveryCharges,
                                                      'generatedArgument':
                                                          statusList![index]
                                                              .generateCode,
                                                      'mobileArgument':
                                                          statusList![index]
                                                              .receiverPhone,
                                                      'sub_totalArgument':
                                                          statusList![index]
                                                              .subTotal,
                                                      'service_taxArgument':
                                                          statusList![index]
                                                              .serviceTax,
                                                      'urlData':
                                                          '?order_id=${statusList![index].orderId}',
                                                      'greeting_msg':
                                                          statusList![index]
                                                              .greetingMsg,
                                                      'date_time':
                                                          statusList![index]
                                                              .dateTime,
                                                      'delivery_option':
                                                          statusList![index]
                                                              .deliveryOption,
                                                      'receiver_name':
                                                          statusList![index]
                                                              .receiverName,
                                                      'receiver_phone':
                                                          statusList![index]
                                                              .receiverPhone,
                                                      'another_phone_number':
                                                          statusList![index]
                                                              .anotherPhoneNumber,
                                                      'payment_curr_code':
                                                          statusList![index]
                                                              .paymentCurrCode,
                                                      'home_delivery_address':
                                                          statusList![index]
                                                              .homeDeliveryAddress,
                                                      'promo_discount_amount':
                                                          statusList![index]
                                                              .promoDiscountAmount,
                                                      'promo_code':
                                                          statusList![index]
                                                              .promoCode,
                                                      'greeting_msg':
                                                          statusList![index]
                                                              .greetingMsg,
                                                    },
                                                  )
                                                : Navigator.of(context)
                                                    .pushNamed(
                                                    '/MobileInvoiceScreen',
                                                    arguments: {
                                                      'greeting_msg':
                                                          statusList![index]
                                                              .greetingMsg,
                                                      'currency_check':
                                                          statusList![index]
                                                              .paymentCurrCode,
                                                      'grand_total':
                                                          statusList![index]
                                                              .grandTotal,
                                                      'delivery_price':
                                                          statusList![index]
                                                              .deliveryCharges,
                                                      'generatedArgument':
                                                          statusList![index]
                                                              .generateCode,
                                                      'mobileArgument':
                                                          statusList![index]
                                                              .receiverPhone,
                                                      'sub_totalArgument':
                                                          statusList![index]
                                                              .subTotal,
                                                      'service_taxArgument':
                                                          statusList![index]
                                                              .serviceTax,
                                                      'urlData':
                                                          '?order_id=${statusList![index].orderId}',
                                                      'greeting_msg':
                                                          statusList![index]
                                                              .greetingMsg,
                                                      'date_time':
                                                          statusList![index]
                                                              .dateTime,
                                                      'delivery_option':
                                                          statusList![index]
                                                              .deliveryOption,
                                                      'receiver_name':
                                                          statusList![index]
                                                              .receiverName,
                                                      'receiver_phone':
                                                          statusList![index]
                                                              .receiverPhone,
                                                      'another_phone_number':
                                                          statusList![index]
                                                              .anotherPhoneNumber,
                                                      'payment_curr_code':
                                                          statusList![index]
                                                              .paymentCurrCode,
                                                      'home_delivery_address':
                                                          statusList![index]
                                                              .homeDeliveryAddress,
                                                      'promo_discount_amount':
                                                          statusList![index]
                                                              .promoDiscountAmount,
                                                      'promo_code':
                                                          statusList![index]
                                                              .promoCode,
                                                    },
                                                  );
                                          },
                                        ),
                                        SizedBox(width: 25),
                                        InkWell(
                                          child: Text(
                                            'Delivery Note',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    96, 32, 189, 1),
                                                fontFamily: 'Gilroy',
                                                fontSize: 12,
                                                letterSpacing: 0,
                                                fontWeight: FontWeight.normal,
                                                height: 1),
                                          ),
                                          onTap: () {
                                            Navigator.of(context).pushNamed(
                                                '/DeliveryNoteScreen',
                                                arguments: {
                                                  'currency_check':
                                                      statusList![index]
                                                          .paymentCurrCode,
                                                  'home_delivery_address':
                                                      statusList![index]
                                                          .homeDeliveryAddress,
                                                  'greeting_msg':
                                                      statusList![index]
                                                          .greetingMsg,
                                                  'another_phone_number':
                                                      statusList![index]
                                                          .anotherPhoneNumber,
                                                  'generatedArgument':
                                                      statusList![index]
                                                          .generateCode,
                                                  'receiver_name':
                                                      statusList![index]
                                                          .receiverName,
                                                  'receiver_phone':
                                                      statusList![index]
                                                          .receiverPhone,
                                                  'date_time':
                                                      statusList![index]
                                                          .dateTime,
                                                  'delivery_option':
                                                      statusList![index]
                                                          .deliveryOption,
                                                  'urlData':
                                                      '?order_id=${statusList![index].orderId}',
                                                });
                                          },
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 15),
                                          child: Container(
                                            child: InkWell(
                                              child: Text(
                                                'Reorder',
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        249, 62, 108, 1),
                                                    fontFamily: 'Gilroy',
                                                    fontSize: 12,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1),
                                              ),
                                              onTap: () {
                                                _showDialog(context,
                                                    statusList![index].orderId);
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  )
                                ]),
                              )
                            ],
                          ),
                        );
                      }),
    );
  }

  @override
  void onTransactionError(String error) {}

  @override
  void onTransactionSuccess(GetuserTransaction transactionModel) {
    print('refresh completed3 $isRefresh');
    print('refresh completed4 ${jsonEncode(transactionModel.data!.dataList!)}');
    if (isRefresh) {
      print('refresh completed4786 ${transactionModel.data!.dataList!.length}');
      if (transactionModel.data!.dataList!.length != 0) {
        statusList!.addAll(transactionModel.data!.dataList!.toList());
        print('refresh completed4789 ${jsonEncode(statusList)}');
      }

      print('refresh completed4784 ${jsonEncode(statusList)}');
    } else {
      statusList = [];
      statusList = transactionModel.data!.dataList!;
    }
    // if (isRefresh) {
    //   statusList = transactionModel.data!.dataList!;
    //   print('refresh completed5 ${jsonEncode(statusList)}');
    // } else {
    //   statusList!.addAll(transactionModel.data!.dataList!.toList());
    //   print('refresh completed6 ${jsonEncode(statusList)}');
    // }
    // offset = offset + limit;
    currentPage++;
    // String? cc = transactionModel.data!.totalCount;
    // print('Checks1 $cc');
    // int c = int.parse(cc!);
    // String? pp = transactionModel.data!.perPage;
    // print('Checks2 $pp');
    // int p = int.parse(pp!);
    // double tPage = c / p;
    // totalPages = tPage.ceil();
    // print('Total Pages :  $totalPages');
    setState(() {
      load = false;
    });
  }

  void _showDialog(BuildContext context, String? orderId) {
    indexnumber = orderId;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              width: 233.0,
              height: 22.0,
              child: Center(
                child: Text(
                  'Reorder',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.right,
                ),
              )),
          content: new Text("Are you sure you want to Reorder this order?",
              textAlign: TextAlign.center),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      _reorderpresenter!.Orderagain(orderId);
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                        // border: Border.all(
                        //   width: 1.0,
                        // ),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onReoderFail(String error) {
    // TODO: implement onReoderFail
  }

  @override
  void onReoderSuccess(ReorderModel reoder_model) {
    // TODO: implement onReoderSuccess
    if (reoder_model.status == true) {
      BotToast.showSimpleNotification(
          title: reoder_model.message.toString(),
          backgroundColor: AppColors().toastsuccess);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen(2, '')));
    }
    if (reoder_model.errorMsg.toString() == 'Different Shop') {
      Navigator.pop(context);
      _showDialogshop(context);
    }
    print('reoder message ${reoder_model.message}');
  }

  @override
  void onClearCartError(String error) {
    // TODO: implement onClearCartError
  }

  @override
  void onClearCartSuccess(CartClear clearCart) {
    if (clearCart.status == true) {
      _reorderpresenter!.Orderagain(indexnumber);
    }
  }

  void _showDialogshop(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AnimatedContainer(
          duration: Duration(milliseconds: 400),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            title: SizedBox(
                width: 233.0,
                height: 22.0,
                child: Center(
                  child: Text(
                    'Shop Change',
                    style: TextStyle(
                        color: AppColors().appaccent,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.right,
                  ),
                )),
            content: SizedBox(
              height: 80,
              child: Column(
                children: [
                  new Text("Are you sure you want to\nchange shop?",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18, color: Color(0xff828282))),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "(If you change the shop you will loose your cart)",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 14, color: Color(0xff828282)),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                          alignment: Alignment(0.02, 0.0),
                          width: 120,
                          height: 32.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              width: 1,
                            ),
                          ),
                          child: Text(
                            'No',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 59, 94, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 18,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w600,
                                height: 1),
                          )),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          presenterclear!.clearCart();
                        });
                      },
                      child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 120,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(0, 219, 167, 1),
                        ),
                        child: Text(
                          'Yes',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
