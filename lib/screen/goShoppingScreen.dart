import 'dart:convert';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/cart_clear.dart';
import 'package:familov/model/category_model.dart';
import 'package:familov/model/cityModel.dart';
import 'package:familov/model/country_model.dart';
import 'package:familov/model/shopModel.dart';
import 'package:familov/presenter/category_presenter.dart';
import 'package:familov/presenter/city_presenter.dart';
import 'package:familov/presenter/clearCart+presenter.dart';
import 'package:familov/presenter/country_presenter.dart';
import 'package:familov/presenter/shop_presenter.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GoShopping extends StatefulWidget {
  const GoShopping({Key? key}) : super(key: key);
  @override
  _GoShoppingState createState() => _GoShoppingState();
}

class _GoShoppingState extends State<GoShopping>
    implements
        CityContract,
        CountryContract,
        ShopContract,
        CategoryContract,
        ClearCartContract,
        GetCartContract {
  var id2;
  var id1;
  var id3;
  List<CountryData_list> statusList = [];
  List<Data_list> statusLists = [];
  List<Data_lists> statusListss = [];
  List<Data_lisst> statusCategory = [];

  var shopId;
  var categoryId;

  var country1;
  var city1;
  var cate1;
  var shop1;
  var shopidapi;
  var shopidlocal;

  var cityList = [];
  var countryList = [];
  var shopList = [];
  var categoryList = [];
  var citybar = false;
  var countrybar = true;
  var categorybar = false;
  var shopbar = false;
  var citySelection;
  var categorySelection;
  var countrySelection;
  var shopSelection;
  List dataList = [];
  var length;
  var Tokensave;
  var cartidapi;
  bool prefilled = false;
  var prefilledshopid;
  var prefilledcatid;

  CityPresenter? _presenter;
  CountryPresenter? presenters0;
  ShopPresenter? presenter1;
  CategoryPresenter? presenter2;
  ClearCartPresenter? presenterclear;
  GetCartPresenter? getCartPresenter1;

  _GoShoppingState() {
    presenters0 = new CountryPresenter(this);
    _presenter = new CityPresenter(this);
    presenter1 = new ShopPresenter(this);
    presenter2 = new CategoryPresenter(this);
    presenterclear = new ClearCartPresenter(this);
    getCartPresenter1 = new GetCartPresenter(this);
  }

  @override
  void initState() {
    pref();
    getCartPresenter1!.getCart();
    getPreference();
    presenters0!.getCountry();
    super.initState();
  }

  delCart() async {
    print('yea its deleted');
    await DBHelper.fulldelete();
  }

  var sho;
  pref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    sho = preferences.getString('shop');
    print('sho  is : $sho');
  }

  prefclear() async {
    sho = '';
    SharedPreferences preferences = await SharedPreferences.getInstance();
    sho = preferences.setString('shop', sho);
    print('sho  iss : $sho');
  }

  setPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('myShop', shop1);
    preferences.setString('myCategory', category!);
    preferences.setString('myCity', city!);
    preferences.setString('myCityId', id1!);
    preferences.setString('myCountry', country!);
    preferences.setString('myCountryId', id2!);
    preferences.setString('myShopID', shopId);
    preferences.setString('myCategoryID', categoryId);

    print('seeeeeetttttttt ');
  }

  var myShop, myCategory, myCity, myCountry, myShopID, myCategoryID, myCityID;
  getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    myShop = preferences.getString('myShop');
    myCategory = preferences.getString('myCategory');
    myCity = preferences.getString('myCity');
    myCountry = preferences.getString('myCountry');
    myShopID = preferences.getString('myShopID');
    myCategoryID = preferences.getString('myCategoryID');
    myCityID = preferences.getString('myCityId');
  }

  var code1 = '+91';
  String? city, country, shop, category;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Go Shopping',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(15),
            child: Column(
              children: [
                Text(
                  'Use Familov and stay connected with those you love.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Go shopping, we deliver your loved ones to the country in less than 36 hours',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text('Select Country'),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 10.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(width: 0.5, color: Color(0xFF7F7F7F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      hint: Text("Select Country"),
                      icon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: countrybar == false
                              ? Image.asset(
                                  'assets/images/down_arrow.png',
                                  height: 16,
                                  width: 11,
                                  fit: BoxFit.contain,
                                )
                              : Container(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.grey.shade300,
                                  ),
                                )),
                      value: country,
                      onChanged: (String? newValue) {
                        setState(() {
                          myCountry = null;
                          // newValue = country == null ? myCountry : '';
                          country = newValue;
                          var id;

                          cityList = [];
                          city = null;

                          city1 = null;
                          cate1 = null;
                          shop1 = null;

                          categoryList = [];
                          category = null;

                          shopList = [];
                          shop = null;

                          for (int i = 0; i < statusList.length; i++) {
                            if (statusList[i].countryName == newValue) {
                              id = statusList[i].countryId;
                              country1 = statusList[i].countryName;
                              id2 = id;
                              _presenter!.city(id);
                            }
                          }
                          citybar = true;
                        });
                      },
                      items: countryList.map((user) {
                        return new DropdownMenuItem<String>(
                          value: user,
                          child: new Text(
                            user,
                          ),
                        );
                      }).toList(),
                    )),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text('Select City'),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 10.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(width: 0.5, color: Color(0xFF7F7F7F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      hint: citySelection == 0
                          ? Text(
                              "City not available ",
                              style: TextStyle(color: Colors.red),
                            )
                          : Text("Select City"),
                      icon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: citybar == false
                              ? Image.asset(
                                  'assets/images/down_arrow.png',
                                  height: 16,
                                  width: 11,
                                  fit: BoxFit.contain,
                                )
                              : Container(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.grey.shade300,
                                  ),
                                )),
                      value: city,
                      onChanged: (String? newValue) {
                        setState(() {
                          myCountry = null;
                          city = newValue;

                          categoryList = [];
                          category = null;

                          cate1 = null;
                          shop1 = null;

                          shopList = [];
                          shop = null;

                          for (int i = 0; i < statusLists.length; i++) {
                            if (statusLists[i].cityName == newValue) {
                              city1 = statusLists[i].cityName;
                              id1 = statusLists[i].cityId;
                            }
                          }
                          print('id1 $id1   &&   id2  $id2');

                          presenter2!.category(id1, id2);
                          categorybar = true;
                        });
                      },
                      items: cityList.map((user) {
                        return new DropdownMenuItem<String>(
                          value: user,
                          child: new Text(
                            user,
                          ),
                        );
                      }).toList(),
                    )),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text('Select Category'),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 10.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(width: 0.5, color: Color(0xFF7F7F7F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      isExpanded: true,
                      hint: categorySelection == 0
                          ? Text(
                              "Category not available ",
                              style: TextStyle(color: Colors.red),
                            )
                          : Text("Select Category"),
                      //  Text("Select Category"),
                      icon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: categorybar == false
                              ? Image.asset(
                                  'assets/images/down_arrow.png',
                                  height: 16,
                                  width: 11,
                                  fit: BoxFit.contain,
                                )
                              : Container(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.grey.shade300,
                                  ),
                                )),
                      value: category,
                      onChanged: (String? newValue) {
                        myCountry = null;
                        setState(() {
                          shopList = [];
                          shop = null;
                          category = newValue;

                          shop1 = null;

                          for (int i = 0; i < statusCategory.length; i++) {
                            print('Category check');
                            if (statusCategory[i].mainCategoryName ==
                                newValue) {
                              cate1 = statusCategory[i].mainCategoryName;
                              id3 = statusCategory[i].mainCategoryId;
                              categoryId = id3;
                            }
                          }
                          print('The category id is :$categoryId and $id3');
                          presenter1!.shop(id1, id3);
                          shopbar = true;
                        });
                      },
                      items: categoryList.map((user) {
                        return new DropdownMenuItem<String>(
                          value: user,
                          child: new Text(
                            user,
                          ),
                        );
                      }).toList(),
                    )),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text('Choose a Store'),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 10.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(width: 0.5, color: Color(0xFF7F7F7F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      isExpanded: true,
                      hint: shopSelection == 0
                          ? Text(
                              "Shop not Available",
                              style: TextStyle(color: Colors.red),
                            )
                          : Text("Select Shop"),
                      icon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: shopbar == false
                              ? Image.asset(
                                  'assets/images/down_arrow.png',
                                  height: 16,
                                  width: 11,
                                  fit: BoxFit.contain,
                                )
                              : Container(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.grey.shade300,
                                  ),
                                )),
                      value: shop,
                      onChanged: (String? newValue) {
                        setState(() {
                          myCountry = null;
                          shop = newValue;
                          for (int i = 0; i < statusListss.length; i++) {
                            if (statusListss[i].shopName == newValue) {
                              shop1 = statusListss[i].shopName;
                              shopId = statusListss[i].shopId;
                              print('Shop id is : $shopId');
                            }
                          }
                          citybar = false;
                        });
                      },
                      items: shopList.map((user) {
                        return new DropdownMenuItem<String>(
                          value: user,
                          child: new Text(
                            user,
                            maxLines: 1,
                          ),
                        );
                      }).toList(),
                    )),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                buttonWidget('Next', context, () {
                  // print('cartshopid == $cartidapi  shopid== $sho ');
                  cartidapi == null ? cartidapi = sho : print('object');
                  print('cartshopid == $cartidapi  shopid== $sho ');
                  if (cartidapi == shopId ||
                      cartidapi == null ||
                      cartidapi == '') {
                    if (country1 != null &&
                        city1 != null &&
                        cate1 != null &&
                        shop1 != null) {
                      print(country1 + city1 + cate1 + shop1);

                      setPreference();
                      Navigator.of(context).pushNamed('/StorePage', arguments: {
                        'shopId': shopId,
                        'categoryId': categoryId,
                      });
                    }
                  } else {
                    _showDialog(context);

                    // setState(() {});
                  }
                })
              ],
            ),
          ),
        ));
  }

  @override
  void onCityError(String error) {
    citybar = true;
  }

  @override
  void onCitySuccess(CityModel cityModel) {
    statusLists = cityModel.data!.dataList!;
    var length = cityModel.data!.count;
    for (var i = 0; i < length!; i++) {
      cityList.add((cityModel.data!.dataList![i].cityName.toString()));
    }
    print(statusLists.toString());
    print('here i am');
    print(length);

    citySelection = length;
    print('here i am');
    myCountry == null
        ? print('object')
        : setState(() {
            categoryList = [];
            category = null;

            cate1 = null;
            shop1 = null;

            shopList = [];
            shop = null;

            for (int i = 0; i < statusLists.length; i++) {
              if (statusLists[i].cityName == myCity) {
                city1 = statusLists[i].cityName;
                id1 = statusLists[i].cityId;
              }
            }
            print('id1 $id1   &&   id2  $id2');

            presenter2!.category(id1, id2);
            categorybar = true;
            city = city1;
          });

    setState(() {
      citybar = false;
    });
  }

  @override
  void onCountryError(String error) {
    citybar = false;
  }

  @override
  void onCountrySuccess(CountryModel countries) {
    statusList = countries.data!.dataList!;
    var length = countries.data!.count;
    for (var i = 0; i < length!; i++) {
      countryList.add((countries.data!.dataList![i].countryName.toString()));
    }
    countrySelection = length;
    myCountry == null
        ? print('')
        : setState(() {
            var id;

            cityList = [];
            city = null;

            city1 = null;
            cate1 = null;
            shop1 = null;

            categoryList = [];
            category = null;

            shopList = [];
            shop = null;

            for (int i = 0; i < statusList.length; i++) {
              if (statusList[i].countryName == myCountry) {
                id = statusList[i].countryId;
                country1 = statusList[i].countryName;
                id2 = id;
                _presenter!.city(id);
              }
            }
            citybar = true;
            country = country1;
          });

    setState(() {
      countrybar = false;
    });
  }

  @override
  void onShopError(String error) {
    citybar = true;
  }

  @override
  void onShopSuccess(ShopModel shopModel) {
    statusListss = shopModel.data!.dataList!;
    print('Length is  :  ${shopModel.data!.count}');
    print('Shop is : ${jsonEncode(shopModel.data!.dataList)}');
    var length = shopModel.data!.count;
    print('Length is  :  ${shopModel.data!.count}');
    for (var i = 0; i < length!; i++) {
      shopList.add((shopModel.data!.dataList![i].shopName.toString()));
      print('Shop List is : $shopList');
    }
    shopSelection = length;

    myCountry == null
        ? print('object')
        : setState(() {
            for (int i = 0; i < statusListss.length; i++) {
              if (statusListss[i].shopName == myShop) {
                shop1 = statusListss[i].shopName;
                shopId = statusListss[i].shopId;
                print('Shop id is : $shopId');
              }
            }
            citybar = false;
          });

    setState(() {
      shopbar = false;
      shop = shop1;
    });
  }

  @override
  void onCategoryError(String error) {
    citybar = true;
  }

  @override
  void onCategorySuccess(CategoryModel categoryModel) {
    statusCategory = categoryModel.data!.dataList!;
    var length = categoryModel.data!.count;
    print('Length is  :  ${categoryModel.data!.count}');
    for (var i = 0; i < length!; i++) {
      categoryList
          .add((categoryModel.data!.dataList![i].mainCategoryName.toString()));
    }
    categorySelection = length;
    myCountry == null
        ? print('object')
        : setState(() {
            shopList = [];
            shop = null;

            shop1 = null;

            for (int i = 0; i < statusCategory.length; i++) {
              print('Category check');
              if (statusCategory[i].mainCategoryName == myCategory) {
                cate1 = statusCategory[i].mainCategoryName;
                id3 = statusCategory[i].mainCategoryId;
                categoryId = id3;
              }
            }
            print('The category id is :$categoryId and $id3');
            presenter1!.shop(id1, id3);
            category = cate1;
            shopbar = true;
          });
    setState(() {
      categorybar = false;
    });
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          title: SizedBox(
              child: Center(
            child: Text(
              'Shop Change',
              style: TextStyle(
                  color: Color(0xffF93E6C), fontWeight: FontWeight.w600),
              textAlign: TextAlign.right,
            ),
          )),
          content: Text.rich(
            TextSpan(
              text: 'Are you sure you want to\nchange shop?',
              style: TextStyle(fontSize: 18, color: Colors.black38),
              children: [
                // TextSpan(),

                TextSpan(
                    text: '\n(If you change the shop you will loose your cart)',
                    style: TextStyle(fontSize: 14)),
              ],
            ),
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        alignment: Alignment(0.02, 0.0),
                        width: 110.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Color.fromRGBO(255, 255, 255, 1),
                          border: Border.all(
                            color: Color.fromRGBO(255, 59, 94, 1),
                            width: 1,
                          ),
                        ),
                        child: Text(
                          'No',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 59, 94, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 18,
                              letterSpacing: 0,
                              fontWeight: FontWeight.w600,
                              height: 1),
                        )),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      setPreference();
                      delCart();
                      setState(() {
                        presenterclear!.clearCart();
                        Navigator.of(context)
                            .pushNamed('/StorePage', arguments: {
                          'shopId': shopId,
                          'categoryId': categoryId,
                        });
                        // Navigator.pushReplacementNamed(
                        //   context,
                        //   '/HomeScreen',
                        // );
                      });
                      // } else {
                      //   presenterclear!.clearCart();
                      //   Navigator.pushReplacementNamed(
                      //     context,
                      //     '/HomeScreen',
                      //   );
                      // }
                      setState(() {});
                    },
                    child: Container(
                      alignment: Alignment(0.02, 0.0),
                      width: 110.0,
                      height: 32.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color.fromRGBO(0, 219, 167, 1),
                      ),
                      child: Text(
                        'Yes',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                            letterSpacing: 0,
                            fontWeight: FontWeight.w600,
                            height: 1),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }

  @override
  void onClearCartError(String error) {}

  @override
  void onClearCartSuccess(CartClear clearCart) {
    if (clearCart.status == true) {
      sho = '';
      prefclear();
      BotToast.showSimpleNotification(
          title: '${clearCart.message}',
          backgroundColor: AppColors().toastsuccess);
    }
  }

  @override
  void onGetCartError(String error) {
    print('cart faillll');
    // TODO: implement onGetCartError
  }

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    if (getCartModel.data?.isEmpty ?? true) {
      print('orailnoor --get cart cart screen');
    } else {
      // shopidapi = getCartModel.data![0].shopId;

      cartidapi = getCartModel.data![0].shopId.toString();
      cartidapi == null ? cartidapi = 'data' : print('kali');
      print('object------- $cartidapi');
    }
    if (getCartModel.status == true) {
      shopidlocal = '';
    }
  }

  void cartconflict() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    Tokensave = await preferences.getString('value_key')!;
  }

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;

    setState(() {
      if (cartidapi == null) {
        cartidapi = dataList[0]['shopId'].toString();
      }
      print('datss ${dataList[0]['shopId'].toString()}');
    });
  }
}
