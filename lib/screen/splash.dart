import 'dart:async';

import 'package:familov/screen/homeScreen.dart';
import 'package:familov/screen/infoScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SpashScreen extends StatefulWidget {
  const SpashScreen({Key? key}) : super(key: key);

  @override
  State<SpashScreen> createState() => _SpashScreenState();
}

class _SpashScreenState extends State<SpashScreen> {
  @override
  void initState() {
    skip();
    print('yea it"s here');
    Timer(Duration(milliseconds: 1500), () => skip());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Center(
            child: Container(
          child: Image.asset('assets/images/splash_400.png'),
        )),
      ),
    );
  }

  void skip() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var boolValue = prefs.getBool('valBool');
    print('boolValue :  $boolValue');
    if (boolValue == true) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen(0,'')));
      //  Navigator.of(context).pushNamed('/HomeScreen');
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => InfoScreen()));
      //  Navigator.of(context).pushNamed('/InfoScreen');
    }
  }
}
