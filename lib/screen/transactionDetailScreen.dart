import 'dart:convert';
import 'package:animated_text_kit/animated_text_kit.dart';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/screen/beneficiaryAddScreen.dart';
import 'package:familov/screen/modepayment.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/bottom_two_fill.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/beneficiaryModel.dart';
import 'package:familov/model/checkout.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/place.dart';
import 'package:familov/model/product_detail_model.dart';
import 'package:familov/model/stripecheckout.dart';
import 'package:familov/model/summmaryy.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/presenter/productDetail.dart';
import 'package:familov/presenter/stripeCheckout.dart';
import 'package:familov/presenter/summary_presenter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:familov/model/promo.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:im_stepper/stepper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:dotted_decoration/dotted_decoration.dart' as Shape;
import 'package:fluttertoast/fluttertoast.dart';
import '../presenter/beneficiary_presenter.dart';
import '../presenter/checkout_presenter.dart';

// ignore: must_be_immutable
class TransactionDetailScreen extends StatefulWidget {
  String? detailsClass;
  String? address;
  String? promo;
  TransactionDetailScreen({
    Key? key,
    required this.address,
    required this.promo,
  }) : super(key: key);
  @override
  _TransactionDetailScreenState createState() =>
      _TransactionDetailScreenState();
}

class _TransactionDetailScreenState extends State<TransactionDetailScreen>
    implements
        SummaryContract,
        GetCartContract,
        ProductDetailClass,
        CheckoutContract,
        StripeCheckoutContract,
        GetBeneficiaryContract {
  int activeStep = 0;
  SummaryPresenter? presenter4;

  GetCartPresenter? presenter1;
  ProductDetailPresenter? presenter2;
  CheckoutPresenter? presenter5;
  StripeCheckoutPresenter? presenter6;
  TextEditingController messagecontroller = TextEditingController();
  TextEditingController mainnumber = TextEditingController();
  TextEditingController othernumber = TextEditingController();
  _TransactionDetailScreenState() {
    presenter1 = new GetCartPresenter(this);
    presenter2 = new ProductDetailPresenter(this);
    presenter4 = new SummaryPresenter(this);
    presenter5 = new CheckoutPresenter(this);
    presenter6 = new StripeCheckoutPresenter(this);
    presenters = new UserBeneficiaryPresenter(this);
  }
  FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
  var load = false;
  var checkCurrency;
  DataList? productDetails;
  List ids = [];
  List prices = [];
  var quan;
  double? calculation;
  Color pickcolor = Colors.grey.shade400;

  List imagesIs = [];
  List productIds = [];
  List<Data_list>? benList = [];

  Data1? statusList;
  List<Data9> dataList2 = [];
  List<Data90> statusList12 = [];
  Data66? dataList66;
  var code1;
  var promoremove = 'Remove';
  String? homeaddress;
  String? mode = '';
  List<Place> items = [];
  int? length = 0;
  var stuck = true;
  var up;

  var ph1;
  var ph2;
  bool promoapplied = false;
  Color promocolor = Color(0xffF6F6F6);
  var stripelinkvisible = false;

  UserBeneficiaryPresenter? presenters;

  TextEditingController controller = TextEditingController();

  List dataList = [];

  List<String> countList = <String>[];
  List<String> countList1 = <String>[];
  var checkStripe;
  var itemSelected = '';

  prefs() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    homeaddress = preferences.getString('homeaddress');
    mode = preferences.getString('mode');
    if (mode == 'Pickup in Store') {
      up = 'pickup';
    }
    if (mode == 'Home Delivery') {
      up = 'home_delivery';
    }
    valueKey = preferences.getString('value_key');
    if (valueKey == null) {
      print('Mode is : $mode  and $valueKey');
    }
    if (mode == null) {
      mode = '';
    }
  }

  bool loadB = true;
  var ch1 = 1;
  var idx;
  var ind = 0;
  var code2;

  @override
  void initState() {
    // widget.promo = 'Select Promo Code';
    controller = new TextEditingController(text: widget.promo);
    presenters!.getBeneficiary();
    print('pre6 checkout');
    presenter5!.checkout();
    prefs();
    shared();
    database();
    super.initState();
  }

  bool load1 = false;

  Color col = Colors.black38;

  Data12? list1;
  var promodiscount;

  List<Data9> dataListcheck = [];
  String? currency = '\$ USD';

  var recpId;
  String? promosave = '';
  var valueKey;
  bool loadcheck = false;

  promosaved() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (promosave == 'Enter Promo Code') {
      preferences.setString('promosave', promosave!);
    } else {
      promosave = preferences.getString('promosave');
    }
  }

  shared() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    currency = preferences.getString('currency');

    valueKey = preferences.getString('value_key');
    if (valueKey != null) {
      presenter4!.summary('$up', '$promosave');
      print('tokenCheckIs :  ${valueKey.toString()}');
      presenter1!.getCart();
    }
    if (valueKey == null) {
      loadcheck = true;
    }
    // checkCurrency = preferences.getString('currency');
    var checkCurrencypre =
        preferences.getString('currency').toString().substring(0, 1);
    checkCurrency = checkCurrencypre == 'c'
        ? preferences.getString('currency').toString().substring(0, 2)
        : preferences.getString('currency').toString().substring(0, 1);
    print('currency is $checkCurrency');
    setState(() {});
  }

  var icheck = 0;
  String? val;

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;
    items = dataList
        .map(
          (item) => Place(
              id: item['id'],
              quantity: item['quantity'],
              shopId: item['shopId']),
        )
        .toList();
    setState(() {});
  }

  apicall(int index) {
    ind = index;
    for (int i = 0; i < dataList.length; i++) {
      icheck = i;
      presenter2!.productDetail('${dataList[i]['id']}');
    }
    stuck = false;
  }

  var cases = 0;

  check() {
    // presenter6!.stripeCheckout(
    //   'home_delivery', //up
    //   'Noida 65, UP 208011',
    //   'Stripe',//checkStripe
    //   '3257', //recpId
    //   'Vaibhav Sen',//itemSelected
    //   '+91 7042032501', //ph1
    //   '+91 7042032502', //ph2
    //   'eur',
    //   'WELCOME',
    //   'some message',
    //   '',
    //   'p');
    print('here');
    print('$up');
    print('$homeaddress');
    print('$checkStripe');
    print('$recpId');
    print('$itemSelected');
    print('$ph1');
    print('$ph2');
    print('${currency!.split(' ')[1]}');
    print('${controller.text}');
    print('here');
    print('${controller.text}');
    presenter6!.stripeCheckout(
        '$up',
        '$homeaddress',
        '$checkStripe',
        '$recpId',
        '$itemSelected',
        '$ph1',
        '$ph2',
        '${currency!.split(' ')[1]}',
        '${controller.text.toString()}',
        '${messagecontroller.text.toString()}',
        '',
        '');
    setState(() {});
  }

  var url = '';
  // launchURL() async {
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    prefs();
    print('address : ${widget.address} and currency is $currency');
    return Scaffold(
        bottomNavigationBar: getBottomButtonWithTwoFillcheckout(
          context,
          'Cancel',
          'Continue',
          () {
            print('oh wow its working');
            Navigator.pop(context);
          },
          () {
            mode == '' ? pickcolor = Colors.red : cases++;
            if (cases == 1) {
              activeStep = cases;
              print('case is : $cases  and $activeStep');
            }
            if (cases == 2) {
              if (itemSelected == '') {
                BotToast.showSimpleNotification(
                    backgroundColor: AppColors().toastsuccess,
                    title: "Select beneficiary first");
              } else {
                activeStep = cases;
                print('case is :2 $cases  and $activeStep');
              }
            }
            if (cases == 3) {
              check();
              // launchURL();
            }
            setState(() {});
          },
        ),
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Checkout',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 25,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        body: (loadcheck == true)
            ? SingleChildScrollView(
                child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: NumberStepper(
                              enableStepTapping:
                                  itemSelected == '' ? false : true,
                              lineColor: Color.fromRGBO(0, 219, 167, 1)
                                  .withOpacity(.4),
                              // lineLength: 100,
                              alignment: Alignment.center,

                              lineLength: MediaQuery.of(context).size.width / 4,
                              // alignment: Alignment.topLeft,

                              stepColor: Colors.grey.shade200,
                              activeStepColor: Color.fromRGBO(0, 219, 167, 1),
                              activeStepBorderColor: Colors.grey,
                              direction: Axis.horizontal,
                              stepRadius: 22,
                              numbers: [
                                1,
                                2,
                                3,
                              ],
                              lineDotRadius: 2,
                              enableNextPreviousButtons: false,
                              scrollingDisabled: true,
                              activeStep: activeStep,
                              onStepReached: (index) {
                                setState(() {
                                  activeStep = index;
                                });
                              },
                            ),
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 12,
                              ),
                              Text(
                                'Transaction \nDetails',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: activeStep != 0
                                        ? Color.fromRGBO(130, 130, 130, 1)
                                        : Colors.black,
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5),
                              ),
                              Spacer(),
                              Text(
                                'Beneficiaries \nInformation',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: activeStep != 1
                                        ? Color.fromRGBO(130, 130, 130, 1)
                                        : Colors.black,
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5),
                              ),
                              Spacer(),
                              Text(
                                'Mode of \nPayment',
                                style: TextStyle(
                                    color: activeStep != 2
                                        ? Color.fromRGBO(130, 130, 130, 1)
                                        : Colors.black,
                                    fontFamily: 'Gilroy',
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),

                          Container(
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  selected(),
                                ],
                              ),
                            ),
                          ),
                          // Container(
                          //     margin: EdgeInsets.only(
                          //         left: 0, right: 0, bottom: 15),
                          //     width: MediaQuery.of(context).size.width,
                          //     height: MediaQuery.of(context).size.height / 5,
                          //     decoration: BoxDecoration(
                          //       borderRadius: BorderRadius.only(
                          //         topLeft: Radius.circular(4),
                          //         topRight: Radius.circular(4),
                          //         bottomLeft: Radius.circular(4),
                          //         bottomRight: Radius.circular(4),
                          //       ),
                          //       border: Border.all(
                          //         color: Color.fromRGBO(189, 189, 189, 1),
                          //         width: 0.5,
                          //       ),
                          //     ),
                          //     child: Column(
                          //         crossAxisAlignment: CrossAxisAlignment.start,
                          //         children: <Widget>[
                          //           Row(
                          //             children: [
                          //               Container(
                          //                   width: 100,
                          //                   height: 100,
                          //                   alignment: Alignment.topLeft,
                          //                   child: Image.asset(
                          //                     'assets/images/cans.png',
                          //                   )),
                          //               Text(
                          //                 'Canned tomatoes \nCalypso,70g',
                          //                 textAlign: TextAlign.left,
                          //                 style: TextStyle(
                          //                     color:
                          //                         Color.fromRGBO(79, 79, 79, 1),
                          //                     fontFamily: 'Gilroy',
                          //                     fontSize: 18,
                          //                     letterSpacing: 0,
                          //                     fontWeight: FontWeight.normal,
                          //                     height: 1.5),
                          //               ),
                          //               Spacer(),
                          //               Container(
                          //                 margin: EdgeInsets.only(
                          //                   right: 15,
                          //                 ),
                          //                 child: Text(
                          //                   '${checkCurrency.toString()}  1.29',
                          //                   textAlign: TextAlign.center,
                          //                   style: TextStyle(
                          //                       color: Color.fromRGBO(
                          //                           0, 219, 167, 1),
                          //                       fontFamily: 'Gilroy',
                          //                       fontSize: 18,
                          //                       letterSpacing: 0,
                          //                       fontWeight: FontWeight.normal,
                          //                       height: 1.5),
                          //                 ),
                          //               ),
                          //             ],
                          //           ),
                          //         ])),
                        ])),
              )
            : Center(
                child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              )));
  }

  @override
  void onSummaryError(String error) {}

  @override
  void onSummarySuccess(Summmaryy summaryModel) {
    list1 = summaryModel.data;
    print('refreshs summaryy ${jsonEncode(list1!.promoCode)}');
    // print(summaryModel.data!.promocodeDetails!.isApplied);

    setState(() {
      if (summaryModel.status == true) {
        loadcheck = true;
      }
    });

    promoapplied == true
        ? summaryModel.data!.promocodeDetails!.isApplied == false
            ? promocolor = Colors.pink.shade50
            : promocolor = Colors.green.shade100
        : print('object');
    setState(() {
      promoapplied == true
          ? promocolor == Colors.pink.shade50
              ? BotToast.showSimpleNotification(
                  backgroundColor: Colors.pink.shade50,
                  title: "Enter valid promo code")
              : BotToast.showSimpleNotification(
                  backgroundColor: Colors.green.shade100,
                  title: "Promo code applied")
          : print('object');
      promodiscount =
          summaryModel.data!.promocodeDetails!.promocode!.iAmount.toString();
      print('${promodiscount}----promo discount');
    });
  }

  @override
  void onGetCartError(String error) {}

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    if (getCartModel.status == true) {
      dataList2 = getCartModel.data!;
    }
    print('len ${dataList2.length} ');
    print('refreshs getCart ${jsonEncode(dataList2)}');
    setState(() {});
  }

  @override
  void onproductDetailError(String error) {}

  @override
  void onproductDetailSuccess(ProductDetailModel detailModels) {
    if (detailModels.status == true) {
      productDetails = detailModels.data!.dataList!;
      imagesIs.add(productDetails!.productImage);
      ids.add(productDetails!.productName);
      prices.add(productDetails!.productPrices);
      productIds.add(productDetails!.productId);
      calculation =
          double.parse(prices[ind]) * double.parse(dataList[ind]['quantity']);
      quan = dataList[ind]['quantity'];
      print('calculation is : $calculation');
      print('price  :  ${productDetails!.productId}');
      print('the data ${productDetails!.productName}');
      print('the data of ids ${ids}');
      print('the data of images $imagesIs');
    }
    if (icheck == dataList.length - 1) {
      print('datalist length 695');
      setState(() {});
    } else {
      print('datalist length 699');
    }
  }

  selected() {
    switch (activeStep) {
      case 0:
        cases = 0;
        return Container(
          child: Column(
            children: [
              Container(
                //  height: MediaQuery.of(context).size.height / 3,
                child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: valueKey == null ? length : dataList2.length,
                    itemBuilder: (context, index) {
                      stuck == true
                          ? apicall(index)
                          : CircularProgressIndicator(
                              color: Color(0xffDA3C5F),
                            );
                      return Container(
                          margin:
                              EdgeInsets.only(left: 14, right: 14, bottom: 15),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 5.4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(4),
                              topRight: Radius.circular(4),
                              bottomLeft: Radius.circular(4),
                              bottomRight: Radius.circular(4),
                            ),
                            border: Border.all(
                              color: Color.fromRGBO(189, 189, 189, 1),
                              width: 0.5,
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: [
                                  valueKey == null
                                      ? Container(
                                          width: 100,
                                          // height: 100,
                                          alignment: Alignment.topLeft,
                                          child: imagesIs.length == 0
                                              ? Image.asset(
                                                  'assets/images/fami.png',
                                                  scale: 1,
                                                  alignment: Alignment.center,
                                                  errorBuilder: (context, error,
                                                      stackTrace) {
                                                    return Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Image.asset(
                                                        'assets/images/fami.png',
                                                        scale: 1,
                                                        alignment:
                                                            Alignment.center,
                                                      ),
                                                    );
                                                  },
                                                )
                                              : Image.network(
                                                  '${imagesIs[index]}',
                                                  errorBuilder: (context, error,
                                                      stackTrace) {
                                                    return Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Image.asset(
                                                        'assets/images/fami.png',
                                                        scale: 1,
                                                        alignment:
                                                            Alignment.center,
                                                      ),
                                                    );
                                                  },
                                                ))
                                      : Container(
                                          width: 100,
                                          // height: 100,
                                          alignment: Alignment.center,
                                          child: dataList2.length == 0
                                              ? Image.asset(
                                                  'assets/images/fami.png',
                                                  scale: 1,
                                                  alignment: Alignment.center,
                                                  errorBuilder: (context, error,
                                                      stackTrace) {
                                                    return Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Image.asset(
                                                        'assets/images/fami.png',
                                                        scale: 1,
                                                        alignment:
                                                            Alignment.center,
                                                      ),
                                                    );
                                                  },
                                                )
                                              : Image.network(
                                                  '${dataList2[index].productImage}',
                                                  errorBuilder: (context, error,
                                                      stackTrace) {
                                                    return Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Image.asset(
                                                        'assets/images/fami.png',
                                                        scale: 1,
                                                        alignment:
                                                            Alignment.center,
                                                      ),
                                                    );
                                                  },
                                                )),
                                  Container(
                                    width: MediaQuery.of(context).size.width /
                                        1.98,
                                    child: valueKey == null
                                        ? Text(
                                            ids.length == 0
                                                ? 'Food product'
                                                : '${ids[index]}',
                                            textAlign: TextAlign.left,
                                            maxLines: 2,
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    79, 79, 79, 1),
                                                fontFamily: 'Gilroy',
                                                fontSize: 18,
                                                letterSpacing: 0,
                                                overflow: TextOverflow.fade,
                                                fontWeight: FontWeight.normal,
                                                height: 1.5),
                                          )
                                        : Container(
                                            width: 80,
                                            child: Text(
                                              dataList2.length == 0
                                                  ? 'Food product'
                                                  : '${dataList2[index].productName}',
                                              textAlign: TextAlign.left,
                                              maxLines: 3,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  overflow: TextOverflow.fade,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  alignment: Alignment.center,
                                  width: 90,
                                  height: 28,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(4),
                                      topRight: Radius.circular(4),
                                      bottomLeft: Radius.circular(4),
                                      bottomRight: Radius.circular(4),
                                    ),
                                    border: Border.all(
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      width: 0.5,
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      valueKey == null
                                          ? Text('$quan')
                                          : Container(
                                              // width: 5,
                                              child: Text(
                                                  ' ${dataList2[index].quantity}'),
                                            ),
                                      // SizedBox(
                                      //   width: 6,
                                      // ),
                                      Text(' x '),
                                      // SizedBox(
                                      //   width: 5,
                                      // ),
                                      valueKey != null
                                          ? Container(
                                              width: 50,
                                              child: Text(
                                                  '${dataList2[index].price!}'))
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ));
                    }),
              ),
              Container(
                  width: MediaQuery.of(context).size.width - 48,
                  //color: Colors.grey.shade200,
                  height: MediaQuery.of(context).size.height / 14,
                  decoration: BoxDecoration(color: promocolor),
                  child: Container(
                    child: Center(
                        child: Row(
                      children: [
                        // SizedBox(
                        //   width: 15,
                        // ),
                        Container(
                          width: MediaQuery.of(context).size.width - 120,
                          child: TextField(
                            // onChanged: ((value) {
                            //   setState(() {
                            //     promocolor = Color(0xffF6F6F6);
                            //   });
                            // }),
                            controller: controller,
                            decoration: new InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                              hintText: "Enter promocode",
                              hintStyle: TextStyle(
                                color: Colors.grey.shade400, // <-- Change this
                                fontSize: null,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        InkWell(
                            child: Text(
                              '${promocolor == Colors.green.shade100 ? 'Remove' : 'Apply'}',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: promocolor == Colors.green.shade100
                                      ? Colors.green
                                      : Color.fromRGBO(0, 219, 167, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 15,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1),
                            ),
                            onTap: promocolor != Colors.green.shade100
                                ? () {
                                    controller.text == ''
                                        ? BotToast.showSimpleNotification(
                                            backgroundColor:
                                                AppColors().toastsuccess,
                                            title: "Please enter a promocode")
                                        : setState(() {
                                            promoapplied = true;
                                            presenter4!.summary('$up',
                                                '${controller.text.toString()}');
                                          });
                                  }
                                : () {
                                    promocolor = Color(0xffF6F6F6);

                                    promoapplied = false;
                                    promoapplied == false
                                        ? controller.text = ''
                                        : print('f');
                                    presenter4!.summary('$up', '');

                                    setState(() {});
                                  }),
                        SizedBox(
                          width: 15,
                        )
                      ],
                    )),
                  )),
              SizedBox(
                height: 15,
              ),
              Container(
                height: MediaQuery.of(context).size.height / 16,
                width: MediaQuery.of(context).size.width - 48,
                decoration: BoxDecoration(color: Color(0xffF6F6F6)),
                // margin: EdgeInsets.only(left: 10, right: 5),
                child: Center(
                    child: Row(
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      '${mode == '' ? 'Select Mode of Delivery' : mode}',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: mode == '' ? pickcolor : Colors.black,
                          fontFamily: 'Gilroy',
                          fontSize: 16,
                          letterSpacing: 0,
                          fontWeight: FontWeight.normal,
                          height: 1),
                    ),
                    Spacer(),
                    InkWell(
                      child: Text(
                        '${mode == '' ? 'Select' : 'Change'}',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color.fromRGBO(0, 219, 167, 1),
                            fontFamily: 'Gilroy',
                            fontSize: 16,
                            letterSpacing: 0,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ModePayment(
                                charges: '${checkCurrency} ',
                                promo: controller.text),
                          ),
                        ).then((newval) {
                          setState(() {});
                          presenter4!.summary('${newval.toString()}',
                              '${controller.text.toString()}');
                        });
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) =>
                        //             ModePayment(promo: controller.text)));
                      },
                    ),
                    SizedBox(
                      width: 15,
                    ),
                  ],
                )),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                height: 10,
                decoration: Shape.DottedDecoration(
                  shape: Shape.Shape.line,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                //  color: Colors.white,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 16, top: 25),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Order Details',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            color: AppColors().blackText,
                            fontWeight: FontWeight.w500),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sub Total',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                color: AppColors().darkgrey,
                                fontWeight: FontWeight.w500),
                          ),
                          valueKey == null
                              ? Container(
                                  margin: EdgeInsets.only(top: 18, right: 16),
                                  child: Text(
                                    '${checkCurrency.toString()} 10.00',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(top: 18, right: 16),
                                  child: Text(
                                    '${checkCurrency.toString()}${list1!.subTotal}',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Service Charges',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                color: AppColors().darkgrey,
                                fontWeight: FontWeight.w500),
                          ),
                          valueKey == null
                              ? Container(
                                  margin: EdgeInsets.only(top: 10, right: 16),
                                  child: Text(
                                    '${checkCurrency.toString()} 01.00',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(top: 10, right: 16),
                                  child: Text(
                                    '${checkCurrency.toString()}${list1!.serviceCharge}',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                        ],
                      ),
                      list1!.deliveryCharge == null ||
                              list1!.deliveryCharge.toString() == "0.00"
                          ? Container()
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Delivery Charges',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      color: AppColors().darkgrey,
                                      fontWeight: FontWeight.w500),
                                ),
                                list1!.deliveryCharge == null
                                    ? Container(
                                        margin:
                                            EdgeInsets.only(top: 10, right: 16),
                                        child: Text(
                                          '${checkCurrency.toString()} 00.00',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 16,
                                              color: AppColors().darkgrey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    : Container(
                                        margin:
                                            EdgeInsets.only(top: 10, right: 16),
                                        child: Text(
                                          '${checkCurrency.toString()}${list1!.deliveryCharge}',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 16,
                                              color: AppColors().darkgrey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                              ],
                            ),
                      Visibility(
                        visible:
                            promocolor == Colors.green.shade100 ? true : false,
                        child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 10, right: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Promo code (${controller.text})',
                                style: GoogleFonts.montserrat(
                                    fontSize: 16,
                                    color: Colors.red,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                '-${checkCurrency.toString()}$promodiscount',
                                style: GoogleFonts.montserrat(
                                    fontSize: 16,
                                    color: Colors.red,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ),
                      ),
                      Divider(
                          color: AppColors().dividercolor.withOpacity(1),
                          thickness: 2.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total Price',
                            style: GoogleFonts.montserrat(
                                fontSize: 18,
                                color: AppColors().darkgrey,
                                fontWeight: FontWeight.w500),
                          ),
                          valueKey == null
                              ? Container(
                                  margin: EdgeInsets.only(
                                      top: 10, right: 16, bottom: 10),
                                  child: Text(
                                    '${checkCurrency.toString()} 00.00',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 18,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(
                                      top: 10, right: 16, bottom: 10),
                                  child: Text(
                                    '${checkCurrency.toString()}${list1!.totalAmount}',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 18,
                                        color: AppColors().darkgrey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      case 1:
        cases = 1;
        return Container(
            child: Column(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
              top: 25,
            ),
            child: InkWell(
              onTap: () {
                print('here is value---${val}');
              },
              child: Text(
                'Name of Beneficiary',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          // countList.isNotEmpty
          //     ? Container(
          //         width: MediaQuery.of(context).size.width,
          //         height: 45,
          //         // margin: EdgeInsets.only(right: 10),
          //         decoration: BoxDecoration(
          //           borderRadius: BorderRadius.only(
          //             topLeft: Radius.circular(4),
          //             topRight: Radius.circular(4),
          //             bottomLeft: Radius.circular(4),
          //             bottomRight: Radius.circular(4),
          //           ),
          //           color: Colors.white,
          //           border: Border.all(
          //             color: Color.fromRGBO(189, 189, 189, 1),
          //             width: 0.5,
          //           ),
          //         ),
          //         child: InkWell(
          //             child: Container(
          //               width: MediaQuery.of(context).size.width * .9,
          //               margin: EdgeInsets.only(left: 10),
          //               child: DropdownButtonHideUnderline(
          //                   child: DropdownButton<String>(
          //                 menuMaxHeight: 450,
          //                 onChanged: (String? newValue) {
          //                   itemSelected = newValue!;
          //                   setState(() {
          //                     for (var i = 0; i < countList.length; i++) {
          //                       if (itemSelected == countList[i]) {
          //                         ph1 = benList![i].beneficiaryMainPhoneNo;
          //                         ph2 = benList![i].beneficiaryAltPhoneNo;
          //                         recpId = countList1[i];
          //                         print(
          //                             'phone numbers are : $ph1 and $ph2 and $recpId');
          //                       }
          //                     }
          //                   });
          //                 },
          //                 //    value: '$itemSelected',
          //                 hint: new Text(
          //                   '${itemSelected == '' ? 'Select the Name of Beneficiary' : itemSelected}',
          //                   style: TextStyle(color: Colors.black),
          //                 ),
          //                 items: countList.map((String value) {
          //                   print(
          //                       'v is : ${countList.indexWhere((element) => false)}');
          //                   return new DropdownMenuItem<String>(
          //                     value: value,
          //                     child: new Text(
          //                       value,
          //                       style: TextStyle(
          //                         fontFamily: 'Gilroy',
          //                         fontSize: 10.0,
          //                         color: const Color(0xFF111111),
          //                         fontWeight: FontWeight.w500,
          //                       ),
          //                     ),
          //                   );
          //                 }).toList(),
          //               )),
          //             ),
          //             onTap: () {}),
          //       )
          //     :
          Container(
            width: MediaQuery.of(context).size.width,
            height: 45,
            // margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4),
                topRight: Radius.circular(4),
                bottomLeft: Radius.circular(4),
                bottomRight: Radius.circular(4),
              ),
              color: Colors.white,
              border: Border.all(
                color: Color.fromRGBO(189, 189, 189, 1),
                width: 0.5,
              ),
            ),
            child: InkWell(
                child: Container(
                  width: MediaQuery.of(context).size.width * .9,
                  margin: EdgeInsets.only(left: 10),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                    menuMaxHeight: 450,
                    onChanged: (String? newValue) {
                      itemSelected = newValue!;

                      setState(() {
                        for (var i = 0; i < countList.length; i++) {
                          if (itemSelected == countList[i]) {
                            ph1 = benList![i].beneficiaryMainPhoneNo;
                            ph2 = benList![i].beneficiaryAltPhoneNo;
                            recpId = countList1[i];

                            mainnumber.text =
                                ph1.toString().replaceFirst('+91', '');
                            othernumber.text =
                                ph2.toString().replaceFirst('+91', '');
                            print(
                                'phone numbers are : $ph1 and $ph2 and $recpId');
                          }
                        }
                      });
                    },
                    //    value: '$itemSelected',
                    hint: new Text(
                      '${itemSelected == '' ? 'Select the Name of Beneficiary' : itemSelected}',
                      style: TextStyle(color: Colors.black),
                    ),
                    items: countList.map((String value) {
                      print(
                          'v is : ${countList.indexWhere((element) => false)}');
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(
                          value,
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontSize: 13.0,
                            color: const Color(0xFF111111),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      );
                    }).toList(),
                  )),
                ),
                onTap: countList.isNotEmpty
                    ? () {
                        // countList.clear();
                        // countList1.clear();
                        // print('here is value---${val}');
                        // setState(() {
                        //   print('orailnoor');
                        //   presenters!.getBeneficiary();
                        // });
                      }
                    : () {
                        BotToast.showSimpleNotification(
                            backgroundColor: Color(0xff00DBA7),
                            title: "Add Beneficiary First");
                      }),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 0, left: 0, right: 0),
              child: Text(
                'Main Number',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Color.fromRGBO(77, 77, 77, 1),
                    fontFamily: 'Gilroy',
                    fontSize: 14,
                    letterSpacing:
                        0 /*percentages not used in flutter. defaulting to zero*/,
                    fontWeight: FontWeight.normal,
                    height: 1),
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xFF7F7F7F),
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Container(
                      child: CountryCodePicker(
                        padding: EdgeInsets.all(1),
                        // ignore: avoid_print
                        onChanged: (e) => code1 = e.dialCode.toString(),
                        //print(e.dialCode),
                        initialSelection: 'IN',
                        showCountryOnly: false,
                        showOnlyCountryWhenClosed: false,
                        favorite: const ['+39', 'FR'],
                      ),
                    ),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height / 17.35,
                    width: MediaQuery.of(context).size.width / 1.5,
                    child: Container(
                        child: TextFormField(
                      controller: mainnumber,
                      keyboardType: TextInputType.number,

                      // initialValue: _initValues['phone_number'],
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            borderSide: BorderSide(
                              color: Color(0xFF7F7F7F),
                              width: 0.5,
                            ),
                          ),
                          hintText: 'My mobile number',
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffDA3C5F)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 16,
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              borderSide:
                                  BorderSide(color: Color(0xffDA3C5F)))),

                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Mobile number is empty!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        print('text1 : $value');

                        // postalCode: _editedProduct.postalCode,
                      },
                    )),
                  ),
                ]),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 0, right: 0, left: 0),
              child: Text(
                'Other Number',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Color.fromRGBO(77, 77, 77, 1),
                    fontFamily: 'Gilroy',
                    fontSize: 14,
                    letterSpacing: 0,
                    fontWeight: FontWeight.normal,
                    height: 1),
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // height: MediaQuery.of(context).size.height / 17.35,
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: CountryCodePicker(
                    padding: EdgeInsets.all(1),
                    // ignore: avoid_print
                    onChanged: (e) => code2 = e.dialCode.toString(),
                    //print(e.toLongString()),
                    initialSelection: 'IN',
                    showCountryOnly: false,
                    showOnlyCountryWhenClosed: false,
                    favorite: const ['+39', 'FR'],
                  ),
                ),
                Container(
                  // height: MediaQuery.of(context).size.height / 17.35,
                  width: MediaQuery.of(context).size.width / 1.5,
                  child: Container(
                      child: TextFormField(
                    controller: othernumber,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xFF7F7F7F),
                            width: 0.5,
                          ),
                        ),
                        hintText: 'My mobile number',
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffDA3C5F)),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 16,
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Mobile number is empty!';
                      }
                    },
                    onSaved: (value) {
                      print('text1 : $value');

                      // postalCode: _editedProduct.postalCode,
                    },
                  )),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),

          InkWell(
            onTap: () {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => BeneficiaryAddScreen('yes')));
              // Navigator.pushNamed(context, '/BeneficiaryAddScreen')
              //     .then((value) => 'yes');

              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => BeneficiaryAddScreen('yes')));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BeneficiaryAddScreen('yes'),
                ),
              ).then((val) {
                countList.isNotEmpty
                    ? setState(() {
                        Future.delayed(Duration(milliseconds: 500), () {
                          print('orailnoor');
                          countList.clear();
                          countList1.clear();
                          presenters!.getBeneficiary();
                        });
                      })
                    : setState(() {
                        Future.delayed(Duration(milliseconds: 500), () {
                          print('orailnoor');

                          presenters!.getBeneficiary();
                        });
                      });
              });
            },
            child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(12),
              child: Text(
                "+ Add New Beneficiary",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(0, 219, 167, 1)),
              ),
              decoration: BoxDecoration(
                  border: Border.all(width: .5, color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(5))),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.grey,
            height: 1,
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            "Personalized message",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Do you want to leave a message to your beneficiary?",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 14),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(
                  width: .5,
                  color: Colors.grey,
                ),
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: TextField(
              controller: messagecontroller,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'eg God bless you / Miss you'),
            ),
          ),
          SizedBox(
            height: 30,
          )
        ]));

      case 2:
        cases = 2;
        return load1 == true
            ? Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Easy isnt it? Choose the payment method that suits you, accept the terms of service and pay securely.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(130, 130, 130, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          letterSpacing: 0,
                          fontWeight: FontWeight.normal,
                          height: 1.5),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Text(
                        'Select a payment method',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color.fromRGBO(96, 32, 189, 1),
                            fontFamily: 'Gilroy',
                            fontSize: 20,
                            letterSpacing: 0,
                            fontWeight: FontWeight.w600,
                            height: 1.5),
                      ),
                    ),
                    // SingleChildScrollView(
                    //     scrollDirection: Axis.horizontal,
                    //     child:)

                    Container(
                      margin: EdgeInsets.only(top: 15),
                      height: 220,
                      // width: MediaQuery.of(context).size.width,
                      child: GridView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: statusList12.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  topRight: Radius.circular(8),
                                  bottomLeft: Radius.circular(8),
                                  bottomRight: Radius.circular(8),
                                ),
                                border: Border.all(
                                  color: idx == index
                                      ? Colors.red
                                      : Colors.black45,
                                  width: 1,
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              child: Card(
                                  shape: BeveledRectangleBorder(
                                    borderRadius: BorderRadius.circular(1.0),
                                  ),
                                  child: Image.network(
                                    '${statusList12[index].pmImage}',
                                    errorBuilder: (context, error, stackTrace) {
                                      return Container(
                                        margin: EdgeInsets.only(left: 10),
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          'assets/images/fami.png',
                                          scale: 1,
                                          alignment: Alignment.center,
                                        ),
                                      );
                                    },
                                  )),
                            ),
                            onTap: () {
                              setState(() {
                                stripelinkvisible = true;
                                idx = index;
                                checkStripe = statusList12[idx].pmSlug;

                                // _createDynamicLink(true);
                                // _createDynamicLinkfail(true);
                              });
                            },
                          );
                        },
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 1,
                          childAspectRatio: 1,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    // Padding(
                    //     padding: const EdgeInsets.all(8.0),
                    //     child: Visibility(
                    //         visible: stripelinkvisible,
                    //         child: SizedBox(
                    //           // width: 250.0,
                    //           child: Text(
                    //             'wait...',
                    //             style: TextStyle(
                    //                 color: Color.fromRGBO(96, 32, 189, 1),
                    //                 fontSize: 20.0,
                    //                 fontFamily: 'Gilroy',
                    //                 fontWeight: FontWeight.w600),
                    //           ),
                    //           // DefaultTextStyle(
                    //           //   style: TextStyle(
                    //           //       color: Color.fromRGBO(96, 32, 189, 1),
                    //           //       fontSize: 20.0,
                    //           //       fontFamily: 'Gilroy',
                    //           //       fontWeight: FontWeight.w600),
                    //           //   child: AnimatedTextKit(
                    //           //     repeatForever: true,
                    //           //     animatedTexts: [
                    //           //       ScaleAnimatedText('wait.'),
                    //           //       ScaleAnimatedText('redirecting'),
                    //           //       ScaleAnimatedText('to payment')
                    //           //     ],
                    //           //     onTap: () {
                    //           //       print("Tap Event");
                    //           //     },
                    //           //   ),
                    //           // ),
                    //         )))
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              ));
    }
  }

  @override
  void onCheckoutError(String error) {}

  @override
  void onCheckoutSuccess(Checkout checkoutModel) {
    statusList12 = checkoutModel.data!;
    setState(() {
      load1 = true;
    });
  }

  @override
  void onStripeCheckoutError(String error) {}

  @override
  void onStripeCheckoutSuccess(Stripecheckout stripeCheckout) {
    print('check33');

    if (stripeCheckout.status == true) {
      dataList66 = stripeCheckout.data;
      print('stripeCheckout ${jsonEncode(dataList66)}');
      url = dataList66!.checkoutUrl!;
      // ignore: deprecated_member_use
      launch('${stripeCheckout.data!.checkoutUrl}');

      // launchUrl(Uri.parse('${stripeCheckout.data!.checkoutUrl}'));
      // html.window.open('${stripeCheckout.data!.checkoutUrl}', 'name');

      setState(() {});
    } else {
      Fluttertoast.showToast(
        msg: '${stripeCheckout.errorMsg}',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black45,
        fontSize: 18,
      );
    }
    setState(() {});
  }

  @override
  void onBeneficiaryError(String error) {}

  @override
  void onBeneficiarySuccess(BeneficiaryModel beneficiaryModel) {
    benList = beneficiaryModel.data!.dataList;
    print('${benList}');
    setState(() {
      // countList.removeLast();
      // countList1.removeLast();

      for (var i = 0; i < benList!.length; i++) {
        countList.add('${benList![i].beneficiaryFullName}');
        countList1.add('${benList![i].beneficiaryId}');
        print('${benList![i].beneficiaryAltPhoneNo}---here we go baby');
        print('${benList![i].beneficiaryMainPhoneNo}---here we go baby');
      }
      print('CountList $countList1');

      loadB = false;
    });
  }

  Future<void> _createDynamicLinkfail(bool short) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://familov.page.link',
      // longDynamicLink: Uri.parse(
      //   'https://dealfair.page.link/?link=http://www.dealfair.com/&apn=com.example.dealfair&ibi=com.example.ios&product=123',
      // ),
      link: Uri.parse('https://familov.page.link/?status=fail'),

      androidParameters: const AndroidParameters(
        packageName: 'com.example.familov',
        minimumVersion: 0,
      ),
      iosParameters: const IOSParameters(
        bundleId: 'io.invertase.testing',
        minimumVersion: '0',
      ),
    );

    Uri url;
    if (short) {
      final Uri shortLink = await dynamicLinks.buildLink(parameters);
      url = shortLink;
    } else {
      url = await dynamicLinks.buildLink(parameters);
    }

    print("dynamiccccccc $url");
    // share(url);
  }

  Future<void> _createDynamicLink(bool short) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://familov.page.link',
      // longDynamicLink: Uri.parse(
      //   'https://dealfair.page.link/?link=http://www.dealfair.com/&apn=com.example.dealfair&ibi=com.example.ios&product=123',
      // ),
      link: Uri.parse('https://familov.page.link?status=success'),

      androidParameters: const AndroidParameters(
        packageName: 'com.example.familov',
        minimumVersion: 0,
      ),
      iosParameters: const IOSParameters(
        bundleId: 'io.invertase.testing',
        minimumVersion: '0',
      ),
    );

    Uri url;
    if (short) {
      final Uri shortLink = await dynamicLinks.buildLink(parameters);
      url = shortLink;
    } else {
      url = await dynamicLinks.buildLink(parameters);
    }

    print("dynamiccccccc $url");
    // share(url);
  }
}
