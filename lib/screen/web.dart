import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';
//import 'package:bot_toast/bot_toast.dart';

// ignore: must_be_immutable
class Web extends StatefulWidget {
  final String web;
  final String name;

  const Web({
    Key? key,
    required this.web,
    required this.name,
  }) : super(key: key);
  @override
  State<Web> createState() => _WebState();
}

class _WebState extends State<Web> {
  late WebViewController controller;
  @override
  void initState() {
    // BotToast.showLoading(duration: Duration(seconds: 2));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            '${widget.name}',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(249, 62, 108, 1),
                fontFamily: 'Gilroy',
                fontSize: 20,
                letterSpacing: 0,
                fontWeight: FontWeight.normal,
                height: 1),
          )),
      body: WebView(
        initialUrl: '${widget.web}',
        // javascriptMode: JavascriptMode.,
        onWebViewCreated: (controller) {
          this.controller = controller;
        },
      ),
    );
  }
}
