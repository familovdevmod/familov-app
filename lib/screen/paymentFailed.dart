import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:flutter/material.dart';

class PaymentFail extends StatefulWidget {
  const PaymentFail({Key? key}) : super(key: key);

  @override
  _PaymentFailState createState() => _PaymentFailState();
}

class _PaymentFailState extends State<PaymentFail> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Image.asset(
                    'assets/images/cross.png',
                    height: 150,
                    width: 200,
                  ),
                ),
                Text(
                  'Sorry !!! Sparsh Gupta',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget arcu pellentesque hendrerit auctor ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(130, 130, 130, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 16,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1.5),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Thank You !',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(77, 77, 77, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.w600,
                      height: 1.5),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15, bottom: 35),
                  child: Text(
                    'You left with € 1.29 in your wallet',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color.fromRGBO(77, 77, 77, 1),
                        fontFamily: 'Gilroy',
                        fontSize: 16,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1.5),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child:
                        buttonWidget('Send a Mobile Recharge', context, () {})),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    ),
                    border: Border.all(
                      color: Color.fromRGBO(0, 219, 167, 1),
                      width: 1,
                    ),
                  ),
                  margin: EdgeInsets.only(left: 15, right: 15, bottom: 30),
                  child: FlatButton(
                      height: 48,
                      minWidth: MediaQuery.of(context).size.width,
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.horizontal(
                        left: Radius.circular(10),
                        right: Radius.circular(10),
                      )),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text('Back to Home',
                              style: new TextStyle(
                                fontSize: 18.0,
                                color: Color(0xff00DBA7),
                              )),
                        ],
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeScreen(0,'')));
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
