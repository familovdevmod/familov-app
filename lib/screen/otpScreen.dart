import 'package:bot_toast/bot_toast.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/getBottomButton.dart';
import 'package:familov/screen/forgetPassword2.dart';
import 'package:familov/model/forget.dart';
import 'package:familov/presenter/forget.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Otp extends StatefulWidget {
  const Otp({Key? key}) : super(key: key);

  @override
  State<Otp> createState() => _OtpState();
}

class _OtpState extends State<Otp> implements ForgetPasswordScreenContract {
  final GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController currentotp = new TextEditingController();
  var status;

  ForgetPasswordScreenPresenter? presenter;

  _OtpState() {
    presenter = new ForgetPasswordScreenPresenter(this);
  }
  var email;
  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    email = arguments['email'];
    return Scaffold(
          appBar: AppBar(
         toolbarHeight:30,
        foregroundColor: Color(0xff4D4D4D),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
        //  resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        bottomNavigationBar: getBottomButton('SUBMIT', () {
          presenter!
              .forgetPassword('$email', '${currentotp.text}', '', '', '2');
        }),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Forgot Password',
                  style: TextStyle(
                    letterSpacing: 1,
                    height: 1.5,
                    fontFamily: 'Montserrat',
                    color: Color(0xff38409f),
                    fontSize: 32,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
                Container(
                  // height: MediaQuery.of(context).size.width/2.5,
                  // width: MediaQuery.of(context).size.width/2.5,
                  child: Image.asset(
                    'assets/images/Emailotp.png',
                    scale: 4,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14),
                  child: Text(
                    'Your reset code has been emailed.',
                    style: TextStyle(
                      letterSpacing: 1,
                      height: 1.5,
                      fontFamily: 'Gilroy',
                      color: Color(0xff828282),
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  child: Form(
                    key: formKey,
                    child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 30),
                        child: PinCodeTextField(
                          
                          appContext: context,
                          pastedTextStyle: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          length: 6,
                          blinkWhenObscuring: false,
                          animationType: AnimationType.fade,
                          pinTheme: PinTheme(

                            activeFillColor: AppColors().toastsuccess,
                            selectedFillColor: Colors.grey.shade300,
                            activeColor: Colors.grey.shade300,
                            selectedColor: Colors.grey.shade300,
                            inactiveFillColor: Colors.grey.shade300,
                            inactiveColor: Colors.grey.shade300,
                            shape: PinCodeFieldShape.box,
                            borderWidth: 0.5,
                            borderRadius: BorderRadius.circular(8),
                            fieldHeight: 45,
                            fieldWidth: 43,
                          ),
                          cursorColor: Colors.black38,
                          animationDuration: Duration(milliseconds: 300),
                          enableActiveFill: true,
                          controller: currentotp,
                          keyboardType: TextInputType.number,
                          onCompleted: (v) {
                            print("Completed");
                          },
                          onChanged: (value) {
                            print(value);
                            setState(() {});
                          },
                          beforeTextPaste: (text) {
                            print("Allowing to paste $text");
                            return true;
                          },
                        )),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  @override
  void onForgetPassError(String error) {}

  @override
  void onForgetPassSuccess(Forget forgetModel) {
    status = forgetModel.status;
    status == true
        ? Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ForgetPassword2(
                      email: email,
                      otp: currentotp.text,
                    )))
        : BotToast.showSimpleNotification(
            backgroundColor: AppColors().toasterror,
            title: "Enter a valid OTP");
    //  Fluttertoast.showToast(
    //     msg: 'Enter valid OTP',
    //     toastLength: Toast.LENGTH_SHORT,
    //     gravity: ToastGravity.BOTTOM,
    //     backgroundColor: Colors.black45,
    //     fontSize: 18,
    //   );
  }
}
