import 'dart:convert';
import 'package:familov/goshoptest.dart';
import 'package:familov/model/category_model.dart';
import 'package:familov/presenter/category_presenter.dart';
import 'package:familov/screen/goShoppingScreen.dart';
import 'package:familov/screen/hamburgerScreen.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/screen/internetconnection.dart';

import 'package:familov/screen/paymentfail.dart';
import 'package:familov/screen/productDetail.dart';
import 'package:familov/screen/splash.dart';
import 'package:familov/screen/web.dart';
import 'package:page_transition/page_transition.dart';
import 'package:familov/widget/CarouselWithDotsPage.dart';
import 'package:familov/widget/carousel.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/model/currency_model.dart';
import 'package:familov/model/language.dart';
import 'package:familov/model/languageversion.dart';
import 'package:familov/model/recommendation.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/presenter/langVersion.dart';
import 'package:familov/presenter/language.dart';
import 'package:familov/presenter/recommendation.dart';
import 'package:familov/presenter/setCart_presenter.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../model/place.dart';
import '../presenter/currency_presenter.dart';
import 'mobileRechargeScreen.dart';
import 'dart:io';

//import 'package:familov/model/product_detail_model.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    implements
        RecommendationContract,
        LanguageContract,
        SetCartContract,
        LanguageVersionContract,
        CurrencyContract,
        CategoryContract {
  File? jsonFile;
  Directory? dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  Map<String, String>? fileContent;
  RecommendationPresenter? presenter;
  LanguagePresenter? presenter1;
  SetCartPresenter? presenter2;
  LanguageVersionPresenter? presenter3;
  String? lanVersionData;
  late CurrencyPresenter presenter4;
  late CategoryPresenter _categoryPresenter;
  _HomeState() {
    presenter = new RecommendationPresenter(this);
    presenter1 = new LanguagePresenter(this);
    presenter2 = new SetCartPresenter(this);
    presenter3 = new LanguageVersionPresenter(this);
    presenter4 = new CurrencyPresenter(this);
    _categoryPresenter = new CategoryPresenter(this);
  }

  List<LoveProductData> statusList1 = [];
  List<RecentlyAddedProduct> statusList2 = [];
  List<MostSellingProduct> statusList3 = [];
  DataList? statusList4;
  List<Data_lisst> listCategory = [];
  List<Data_lisst> listCategory2 = [];

  var _items;
  var shopId2;
  var catId2;

  // void writeinFIle(String jsonEncodeValue) {
  //   getApplicationDocumentsDirectory().then((Directory directory) {
  //     dir = directory;
  //     jsonFile = new File(dir!.path + "/" + fileName);
  //     fileExists = jsonFile!.existsSync();
  //     if (fileExists) this.setState(() => fileContent = jsonDecode(jsonFile!.readAsStringSync()));
  //   });
  // }

  var load = false;
  var load11 = false;

  void createFile(String content, Directory dir, String fileName) async {
    print("Creating file!");
    File file = new File(dir.path + "/" + fileName);
    file.createSync();
    fileExists = true;
    file.writeAsStringSync(jsonEncode(content));
    final data = await json.decode(content);
    setState(() {
      _items = data;
      print('items data ${_items['LBL_MENU_01']}');
      print('items data ${_items['LBL_MENU_01']}');
    });
  }

  final List<String> imgList = [
    'http://apptest.familov.com/uploads/country/FAMILOV_1616839983.png',
    'http://apptest.familov.com/assets/front/img/Mobile_Banner.png',
    'http://apptest.familov.com/uploads/country/FAMILOV_1604733859.png',
  ];
  final List<String> textList = [
    'Select the city, the store, then the products to be delivered to your loved one.',
    'Pay securely using one of the many payment methods to choose from.',
    'The beneficiary receives the unique CODE by SMS and is delivered in less than 36 hours.',
  ];

  var load1 = false;
  var load2 = false;
  var load3 = false;
  var lang;
  var sho;

  String? valueKey;

  tokenCheck() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    lang = preferences.getString('languages');
    sho = preferences.getString('shop');

    print('tokenCheckIs :  ${valueKey.toString()}  and lang is  : $lang');
    if (valueKey != '' && valueKey != null) {
      print('tokenCheckIs :  ${valueKey.toString()}  and lang is  : $lang');
      presenter!.recommendation();
    }
    setState(() {
      presenter3!.languageVersion();
      presenter1!.language(lang);
    });
  }

  List dataList = [];
  var length;

  List<Place> items = [];
  var ver;

  sharedP(lanVersionData) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    ver = preferences.getString('lanVersionData');
    lang = preferences.getString('languages');
    print('whatIslanguages $lang');
    if (ver == null) {
      preferences.setString("lanVersionData", lanVersionData);
      presenter1?.language(lang);
    } else {
      presenter1?.language(lang);
    }
  }

  database() async {
    dataList = await DBHelper.getData('thecart');
    print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    length = dataList.length;

    setState(() {
      print('lengthss $length');
      if (length > 0) {
        for (var i = 0; i < length; i++) {
          presenter2!.setCart(sho.toString(), '${dataList[i]['id'].toString()}',
              '${dataList[i]['quantity'].toString()}', '', '', '', 'p');
        }
        print('datss ${dataList[0]['id'].toString()}');
      }
    });
  }

  @override
  void initState() {
    // dynamiclink();
    presenter4.getCurrency();
    database();
    tokenCheck();

    getApplicationDocumentsDirectory().then((Directory directory) {
      dir = directory;
    });
    getPreference();
    super.initState();
  }

  var myShop,
      myCategory,
      myCity,
      myCityId,
      myCountry,
      myCountryId,
      myShopID,
      myCategoryID;
  getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    myShop = preferences.getString('myShop');
    myCategory = preferences.getString('myCategory');
    myCity = preferences.getString('myCity');
    myCityId = preferences.getString('myCityId');
    myCountry = preferences.getString('myCountry');
    myCountryId = preferences.getString('myCountryId');
    myShopID = preferences.getString('myShopID');
    myCategoryID = preferences.getString('myCategoryID');

    print('myCityId $myCityId myCountryId $myCountryId');
    if (myCityId != null &&
        myCityId != '' &&
        myCountryId != null &&
        myCountryId != '') {
      _categoryPresenter.category('', '');
    }
  }

//   database() {
//     return (statusList4 as List).map((language) {
//       print('Inserting $language');
//       DBHelper.insert('language', {
//         'language': language
//       });
//     }).toList();
//   }

//   var len;
//   List<Map<String, dynamic>>? dataListss;
//   // List items;
//   dataPrint() async {
//     dataListss = await DBHelper.getData('language');
//     print('Entt ${jsonEncode(dataListss)}  and  ${dataListss!.length}');
//     // len = dataListss!.length;
//     // items = dataListss!
//     //     .map(
//     //       (item) => DataList(
//     //         id: item['id'],
//     //       ),
//     //     )
//     //     .toList();
//     setState(() {});
//   }

  // Future<File> wCounter() {
  //   setState(() {});
  //   return CounterStorage().writeCounter(5);
  // }

  container() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(right: 10, left: 15, bottom: 10),
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black12,
              width: 1,
            ),
            // color: Color(0xff6020BD),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                bottomRight: Radius.circular(10)),
          ),
          child: Image.asset('assets/images/cans.png'),
        ),
        Text(
          'Food',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Color.fromRGBO(77, 77, 77, 1),
              fontFamily: 'Gilroy',
              fontSize: 12,
              letterSpacing: 0,
              fontWeight: FontWeight.normal,
              height: 1),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        // drawer: Handburger(),
        //  Container(
        //     width: MediaQuery.of(context).size.width,
        //     height: MediaQuery.of(context).size.height,
        //     child: Drawer(
        //       child: Handburger(),
        //     )),
        appBar: AppBar(
          leading: GestureDetector(
            child: Icon(Icons.menu),
            onTap: () {
              Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.leftToRightWithFade,
                  alignment: Alignment.topCenter,
                  child: Handburger(),
                ),
              );
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => Handburger()));
            },
          ),
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: InkWell(
            // onDoubleTap: () {
            //   Navigator.push(context,
            //       MaterialPageRoute(builder: ((context) => NetConnection())));
            // },
            // onLongPress: () {
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //           builder: ((context) => FailScreen('', true))));
            // },
            onTap: () {
              // ignore: deprecated_member_use
              launch('https://familov.page.link/orailnoor');
              // html.window.open('https://familov.page.link/orailnoor', 'name');
            },
            child: Container(
                height: 33,
                child: Image.asset('assets/images/familovlogo.png')),
          ),
          actions: [
            // IconButton(
            //     icon: InkWell(
            //       onLongPress: () {
            //         Navigator.push(context,
            //             MaterialPageRoute(builder: (context) => TestWidget()));
            //       },
            //       child: Icon(
            //         Icons.notifications_none_outlined,
            //         color: Colors.black54,
            //         size: 24,
            //       ),
            //     ),
            //     onPressed: () {}),
          ],
        ),
        body: (load == true)
            ? SingleChildScrollView(
                // physics: NeverScrollableScrollPhysics(),
                child: Center(
                  child: Container(
                      width: MediaQuery.of(context).size.width * .94,
                      // margin: EdgeInset
                      // s.all(16),
                      child: Column(children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.fade,
                                alignment: Alignment.topCenter,
                                child: GoShopping(),
                              ),
                            );

                            // Navigator.of(context)
                            //     .pushNamed('/GoShoppingScreen');
                          },
                          child: Row(
                            children: [
                              Container(
                                child: Image.asset(
                                  'assets/images/map-pin.png',
                                  height: 22,
                                  width: 22,
                                  //scale: 1,
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Wrap(
                                direction: Axis.vertical,
                                children: [
                                  Text(
                                    //Ajouter un emplacement
                                    //'Add Location',
                                    myCountry != null
                                        ? '$myCity, $myCountry'
                                        : '${AppLocalizations.of(context)!.location}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(79, 79, 79, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),
                                  myShop == null
                                      ? Container()
                                      : Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .7,
                                          child: Text(myShop,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1)),
                                        )
                                ],
                              ),
                              Spacer(),
                              Text(
                                // '${_items['CHANGE']}',
                                'Change',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(96, 32, 189, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 12,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 12, bottom: 10),
                          child: Container(
                            child: Transform.rotate(
                              angle: 0.000005008956130975318,
                              child: Divider(
                                  color: Color.fromRGBO(189, 189, 189, 1),
                                  thickness: 0.5),
                            ),
                          ),
                        ),
                        myShopID != '179'
                            ? SizedBox(
                                height: listCategory.isEmpty ? 0 : 120,
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: listCategory.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Column(
                                        children: [
                                          Stack(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  listCategory[index]
                                                              .shouldActive !=
                                                          null
                                                      ? Navigator.of(context)
                                                          .pushNamed(
                                                              '/StorePage',
                                                              arguments: {
                                                              'shopId':
                                                                  myShopID,
                                                              'categoryId':
                                                                  listCategory[
                                                                          index]
                                                                      .mainCategoryId,
                                                            })
                                                      : print('not found');
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      color: listCategory[index]
                                                                  .shouldActive !=
                                                              null
                                                          ? Color(0xffF93E6C)
                                                          : Color(0xffF93E6C)
                                                              .withOpacity(0.2),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50)),
                                                  margin: EdgeInsets.fromLTRB(
                                                      8, 14, 8, 8),
                                                  height: 60,
                                                  width: 60,
                                                  child: Image.network(
                                                    listCategory[index]
                                                        .mainCategoryImage
                                                        .toString(),
                                                    errorBuilder:
                                                        (BuildContext context,
                                                            Object exception,
                                                            StackTrace?
                                                                stackTrace) {
                                                      return ClipRRect(
                                                        child: Icon(
                                                          Icons.hide_image,
                                                          color: Colors.white,
                                                          size: 30,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ),
                                              //   Container(
                                              //       decoration: BoxDecoration(
                                              //           color: listCategory[index]
                                              //                       .shouldActive !=
                                              //                   null
                                              //               ? Colors.red.withOpacity(.50)
                                              //               : Colors.grey.withOpacity(.5),
                                              //           borderRadius:
                                              //               BorderRadius.circular(50)),
                                              //       margin: EdgeInsets.all(8),
                                              //       height: 70,
                                              //       width: 70)
                                            ],
                                          ),
                                          SizedBox(
                                              width: 70,
                                              child: Text(
                                                listCategory[index]
                                                    .mainCategoryName
                                                    .toString(),
                                                textAlign: TextAlign.center,
                                                maxLines: 2,
                                                style: TextStyle(
                                                    color: listCategory[index]
                                                                .shouldActive !=
                                                            null
                                                        ? Colors.black
                                                        : Colors.grey),
                                              ))
                                        ],
                                      );
                                    }),
                              )
                            : Container(),
                        CarouselWithDotsPage(imgList: imgList),
                        Container(
                          // width: 328,
                          height: 225,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8),
                              bottomLeft: Radius.circular(8),
                              bottomRight: Radius.circular(8),
                            ),
                            color: Colors.pink.shade50,
                          ),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 40, bottom: 10),
                                child: Text(
                                  'A better way to send what really matters.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(51, 51, 51, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 16,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  'Faster, simpler, safer',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 12,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/star.png',
                                    ),
                                    Image.asset(
                                      'assets/images/star.png',
                                    ),
                                    Image.asset(
                                      'assets/images/star.png',
                                    ),
                                    Image.asset(
                                      'assets/images/star.png',
                                    ),
                                    Image.asset(
                                      'assets/images/star.png',
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                '4.7 rating on facebook',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromRGBO(79, 79, 79, 1),
                                    fontFamily: 'Gilroy',
                                    fontSize: 12,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                    height: 1),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                // ignore: deprecated_member_use
                                child: FlatButton(
                                    height: 40,
                                    minWidth:
                                        MediaQuery.of(context).size.width / 1.6,
                                    // minWidth: MediaQuery.of(context).size.width,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: BorderRadius.horizontal(
                                            left: Radius.circular(10),
                                            right: Radius.circular(10))),
                                    color: Color(0xff00DBA7),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('Go Shopping',
                                            style: new TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.white)),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Icon(
                                          Icons.arrow_forward,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                    onPressed: () {
                                      // Navigator.of(context)

                                      //     .pushNamed('/GoShoppingScreen');
                                      Navigator.push(
                                        context,
                                        PageTransition(
                                          type: PageTransitionType.fade,
                                          alignment: Alignment.topCenter,
                                          child: GoShopping(),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          ),
                        ),

                        valueKey == null
                            ? SizedBox(
                                height: 20,
                              )
                            : Container(
                                height: 20,
                              ),
                        Container(
                          // width: 328,
                          height: 190,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8),
                              bottomLeft: Radius.circular(8),
                              bottomRight: Radius.circular(8),
                            ),
                            color: Colors.lightGreen.shade50,
                          ),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 40, bottom: 10),
                                child: Text(
                                  'Who are you sending top-up to?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(51, 51, 51, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 16,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: 10, right: 20, left: 20),
                                child: Text(
                                  'Every second, someone, somewhere sends mobile recharge online with Familov',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(79, 79, 79, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 12,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.normal,
                                      height: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: FlatButton(
                                    height: 40,
                                    minWidth:
                                        MediaQuery.of(context).size.width / 1.6,
                                    // minWidth: MediaQuery.of(context).size.width,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: BorderRadius.horizontal(
                                            left: Radius.circular(10),
                                            right: Radius.circular(10))),
                                    color: Color(0xff00DBA7),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('Mobile Recharge',
                                            style: new TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.white)),
                                        valueKey == null
                                            ? SizedBox(
                                                width: 10,
                                              )
                                            : Container(
                                                height: 0,
                                              ),
                                        Icon(
                                          Icons.arrow_forward,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  HomeScreen(1, '')));
                                    }),
                              ),
                            ],
                          ),
                        ),
                        valueKey == null
                            ? Container(
                                width: 328,
                                height: 130,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    topRight: Radius.circular(8),
                                    bottomLeft: Radius.circular(8),
                                    bottomRight: Radius.circular(8),
                                  ),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 40, bottom: 10),
                                      child: Text(
                                        'How it works',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color(0xff6020BD),
                                            fontFamily: 'Gilroy',
                                            fontSize: 20, //6020BD
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.w600,
                                            height: 1),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: 10, right: 20, left: 20),
                                      child: Text(
                                        'Be more efficient than traditional money transfer',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color(0xff4F4F4F),
                                            fontFamily: 'Gilroy',
                                            fontSize: 16,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                height: 0,
                              ),
                        // CarouselWithDotsPage(imgList: imgList),
                        valueKey == null
                            ? CarouselPage(
                                textList: textList,
                              )
                            : Container(
                                height: 0,
                              ),
                        valueKey == null
                            ? SizedBox(
                                height: 10,
                              )
                            : Container(
                                height: 0,
                              ),

                        valueKey == null
                            ? Container(
                                height: 500,
                                child: Stack(
                                  children: [
                                    Positioned(
                                      child: Container(
                                        height: 400,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black12,
                                            width: 1,
                                          ),
                                          color: Colors.deepPurple,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10),
                                              bottomRight: Radius.circular(10),
                                              topLeft: Radius.circular(10),
                                              bottomLeft: Radius.circular(10)),
                                        ),
                                        child: Column(
                                          children: [
                                            valueKey == null
                                                ? SizedBox(
                                                    height: 20,
                                                  )
                                                : Container(
                                                    height: 0,
                                                  ),
                                            Text(
                                              'Join thousands\nof happy families',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 22,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.5),
                                            ),
                                            valueKey == null
                                                ? SizedBox(
                                                    height: 20,
                                                  )
                                                : Container(
                                                    height: 0,
                                                  ),
                                            Text(
                                              'Familov is the new way families around the\n world stay connected.',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 16,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              '* 94% of our users are satisfied',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      255, 193, 7, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.5),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      left: 30,
                                      right: 30,
                                      child: Container(
                                        height: 300,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black12,
                                            width: 1,
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10),
                                              bottomRight: Radius.circular(10),
                                              topLeft: Radius.circular(10),
                                              bottomLeft: Radius.circular(10)),
                                        ),
                                        child: Column(
                                          children: [
                                            CarouselWithDotsPage(
                                                imgList: imgList),
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 15),
                                              child: Text(
                                                'It’s a good idea and it works very well. No more hassles and calls of all kinds. At least like that I am at ease.',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        20, 33, 61, 1),
                                                    fontFamily: 'Noto Sans',
                                                    fontSize: 16,
                                                    letterSpacing: 0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    height: 1.5),
                                              ),
                                            ),
                                            Text(
                                              'Michel Francis',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.deepPurple,
                                                  fontFamily: 'Noto Sans',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.5),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? SizedBox(
                                height: 20,
                              )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? load1 == true
                                ? Row(
                                    children: [
                                      Text(
                                        'Products you love',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(51, 51, 51, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 18,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.w600,
                                            height: 1),
                                      ),
                                    ],
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? load1 == true
                                ? SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 15),
                                      height: 150,
                                      //  width: MediaQuery.of(context).size.width,
                                      child: Row(children: [
                                        ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount: statusList1.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 12),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    InkWell(
                                                      child: Container(
                                                        width: 100,
                                                        height: 100,
                                                        child: Card(
                                                          elevation: 2,
                                                          child: Image.network(
                                                            '${statusList1[index].productImage}',
                                                            errorBuilder:
                                                                (context, error,
                                                                    stackTrace) {
                                                              return Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child:
                                                                    Image.asset(
                                                                  'assets/images/fami.png',
                                                                  scale: 1,
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ProductDetail(
                                                                          productId: statusList1[index]
                                                                              .productId
                                                                              .toString(),
                                                                          specialPrice: statusList1[index]
                                                                              .specialProductPrices
                                                                              .toString(),
                                                                          image: statusList1[index]
                                                                              .productImage
                                                                              .toString(),
                                                                          price: statusList1[index]
                                                                              .productPrices
                                                                              .toString(),
                                                                          discount:
                                                                              '',
                                                                          disPercentage:
                                                                              '',
                                                                          shopId:
                                                                              '',
                                                                          totalNumber:
                                                                              0,
                                                                        )));
                                                      },
                                                    ),
                                                    Container(
                                                      width: 90,
                                                      child: Text(
                                                        '${statusList1[index].productName}',
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    77,
                                                                    77,
                                                                    77,
                                                                    1),
                                                            fontFamily:
                                                                'Gilroy',
                                                            fontSize: 12,
                                                            letterSpacing: 0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            height: 1),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            })
                                      ]),
                                    ),
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? SizedBox(
                                height: 15,
                              )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? load2 == true
                                ? Row(
                                    children: [
                                      Text(
                                        'Products recently Added ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(51, 51, 51, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 18,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.w600,
                                            height: 1),
                                      ),
                                      Spacer(),
                                      InkWell(
                                        child: Text(
                                          'View all',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  96, 32, 189, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 14,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1),
                                        ),
                                        onTap: () {
                                          print(
                                              'ids is : $shopId2 and $catId2');
                                          setState(() {
                                            if (shopId2 != null &&
                                                catId2 != null) {
                                              Navigator.of(context).pushNamed(
                                                  '/StorePage',
                                                  arguments: {
                                                    'shopId':
                                                        shopId2.toString(),
                                                    'categoryId':
                                                        catId2.toString(),
                                                  });
                                            }
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        width: 15,
                                      )
                                    ],
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              ),

                        valueKey != null
                            ? load2 == true
                                ? SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 15),
                                      height: 150,
                                      //   width: MediaQuery.of(context).size.width,
                                      child: Row(children: [
                                        ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount: statusList2.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 12),
                                                child: Column(
                                                  children: [
                                                    InkWell(
                                                      child: Container(
                                                        width: 100,
                                                        height: 100,
                                                        child: Card(
                                                          elevation: 2,
                                                          child: Image.network(
                                                            '${statusList2[index].productImage}',
                                                            errorBuilder:
                                                                (context, error,
                                                                    stackTrace) {
                                                              return Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child:
                                                                    Image.asset(
                                                                  'assets/images/fami.png',
                                                                  scale: 1,
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ProductDetail(
                                                                          productId: statusList2[index]
                                                                              .productId
                                                                              .toString(),
                                                                          specialPrice: statusList2[index]
                                                                              .specialProductPrices
                                                                              .toString(),
                                                                          image: statusList2[index]
                                                                              .productImage
                                                                              .toString(),
                                                                          price: statusList2[index]
                                                                              .productPrices
                                                                              .toString(),
                                                                          discount:
                                                                              '',
                                                                          disPercentage:
                                                                              '',
                                                                          shopId:
                                                                              '',
                                                                          totalNumber:
                                                                              0,
                                                                        )));
                                                      },
                                                    ),
                                                    Container(
                                                      width: 100,
                                                      child: Text(
                                                        '${statusList2[index].productName}',
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    77,
                                                                    77,
                                                                    77,
                                                                    1),
                                                            fontFamily:
                                                                'Gilroy',
                                                            fontSize: 12,
                                                            letterSpacing: 0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            height: 1),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            })
                                      ]),
                                    ),
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? load3 == true
                                ? Row(
                                    children: [
                                      Text(
                                        'Most Selling Products',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(51, 51, 51, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 18,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.w600,
                                            height: 1),
                                      ),
                                      Spacer(),
                                      InkWell(
                                        child: Text(
                                          'View all',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  96, 32, 189, 1),
                                              fontFamily: 'Gilroy',
                                              fontSize: 14,
                                              letterSpacing: 0,
                                              fontWeight: FontWeight.normal,
                                              height: 1),
                                        ),
                                        onTap: () {
                                          print(
                                              'ids is : $shopId2 and $catId2');
                                          setState(() {
                                            if (shopId2 != null &&
                                                catId2 != null) {
                                              Navigator.of(context).pushNamed(
                                                  '/StorePage',
                                                  arguments: {
                                                    'shopId':
                                                        shopId2.toString(),
                                                    'categoryId':
                                                        catId2.toString(),
                                                  });
                                            }
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        width: 15,
                                      )
                                    ],
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              ),
                        valueKey != null
                            ? load3 == true
                                ? SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 15),
                                      height: 150,
                                      // width: MediaQuery.of(context).size.width,
                                      child: Row(children: [
                                        ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount: statusList3.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 12),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    InkWell(
                                                      child: Container(
                                                        width: 100,
                                                        height: 100,
                                                        child: Card(
                                                          elevation: 2,
                                                          child: Image.network(
                                                            '${statusList3[index].productImage}',
                                                            errorBuilder:
                                                                (context, error,
                                                                    stackTrace) {
                                                              return Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child:
                                                                    Image.asset(
                                                                  'assets/images/fami.png',
                                                                  scale: 1,
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ProductDetail(
                                                                          productId: statusList3[index]
                                                                              .productId
                                                                              .toString(),
                                                                          specialPrice: statusList3[index]
                                                                              .specialProductPrices
                                                                              .toString(),
                                                                          image: statusList3[index]
                                                                              .productImage
                                                                              .toString(),
                                                                          price: statusList3[index]
                                                                              .productPrices
                                                                              .toString(),
                                                                          discount:
                                                                              '',
                                                                          disPercentage:
                                                                              '',
                                                                          shopId:
                                                                              '',
                                                                          totalNumber:
                                                                              0,
                                                                        )));
                                                      },
                                                    ),
                                                    Container(
                                                      width: 100,
                                                      child: Text(
                                                        '${statusList3[index].productName}',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    77,
                                                                    77,
                                                                    77,
                                                                    1),
                                                            fontFamily:
                                                                'Gilroy',
                                                            fontSize: 12,
                                                            letterSpacing: 0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            height: 1),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            })
                                      ]),
                                    ),
                                  )
                                : Container(
                                    height: 0,
                                  )
                            : Container(
                                height: 0,
                              )
                      ])),
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                    color: Color.fromRGBO(249, 62, 108, 1)),
              ));
  }

  @override
  void onRecommendationError(String error) {}

  @override
  void onRecommendationSuccess(Recommendation recommendation) {
    print('recommendationss ${recommendation.message}');
    statusList1 = recommendation.data!.loveProductData!;
    if (statusList1.length != 0) {
      load1 = true;
    }
    statusList2 = recommendation.data!.recentlyAddedProduct!;
    if (statusList2.length != 0) {
      load2 = true;
    }
    statusList3 = recommendation.data!.mostSellingProduct!;
    if (statusList3.length != 0) {
      load3 = true;
    }
    shopId2 = recommendation.data!.recentlyAddedProduct![0].shopId;

    catId2 = recommendation.data!.recentlyAddedProduct![0].mainCategoryId;
    print('Data recc  : ${jsonEncode(recommendation.data!.loveProductData!)}');
    print(
        'all length is : Loved is  ${statusList1.length}  and Recent is  ${statusList2.length} and most selling is : ${statusList3.length}');
    setState(() {});
  }

  @override
  void onLanguageError(String error) {}

  @override
  void onLanguageSuccess(Language language) {
    statusList4 = language.data!.dataList;
    print('language printed ${jsonEncode(statusList4)}');
    setState(() {
      print('$dir  and $fileName ');
      createFile(jsonEncode(statusList4), dir!, fileName);
      load11 = true;
    });
  }

  @override
  void onSetCartError(String error) {}

  @override
  void onSetCartSuccess(SetCart setCartModel) {}

  @override
  void onLanguageVersionError(String error) {}

  @override
  void onLanguageVersionSuccess(Languageversion langVersion) {
    print('langVersion... ${langVersion.data}');
    lanVersionData = langVersion.data;
    setState(() {
      sharedP(lanVersionData);
      load = true;
    });
  }

  @override
  void onCurrencyError(String error) {
    // TODO: implement onCurrencyError
  }

  @override
  void onCurrencySuccess(CurrencyModel login) {
    // TODO: implement onCurrencySuccess
  }

  @override
  void onCategoryError(String error) {
    // TODO: implement onCategoryError
  }

  @override
  void onCategorySuccess(CategoryModel categoryModel) {
    // TODO: implement onCategorySuccess
    if (categoryModel.status == true) {
      if (categoryModel.data != null) {
        if (categoryModel.data!.dataList!.isNotEmpty) {
          print('qqqqqqqqqqqqqqqqqqqqqqq ');

          if (listCategory.isEmpty) {
            print("nulllllllllllllllllllllllllll ");
            listCategory = categoryModel.data!.dataList!;

            if (myCityId != null &&
                myCityId != '' &&
                myCountryId != null &&
                myCountryId != '') {
              _categoryPresenter.category(myCountryId, myCityId);
            }
          } else {
            print("elssssssssssssssssssss  ");
            List<Data_lisst>? data = categoryModel.data!.dataList;

            print("data[index].countryId ${data![0].mainCategoryId}");
            for (int index = 0; index < data.length; index++) {
              for (int i = 0; i < listCategory.length; i++) {
                if (data[index].mainCategoryId ==
                    listCategory[i].mainCategoryId) {
                  listCategory[i].shouldActive = 'true';
                  print(
                      "doooooooooooooooonnnnnnnnnnnnnnn ${data[index].mainCategoryId}  ${listCategory[i].mainCategoryId}");
                }
              }
            }
          }

          setState(() {
            listCategory;
          });

          // if(categoryModel.data!.dataList![0].countryId==null){
          //   print('countryIdNull');
          //   setState(() {
          //     listCategory=categoryModel.data!.dataList!;
          //   });
          // }else{
          //   print('countryIdNotNull');
          //   setState(() {
          //     listCategory2=categoryModel.data!.dataList!;
          //   });
          // }

        }
      }
    }

    print('cccccccccccate ${categoryModel.data!.dataList![0].countryId}');
    print('listCategory ${listCategory.length}');
    print('listCategory2 ${listCategory2.length}');
  }

  Future<void> dynamiclink() async {
    print('here our');
    final PendingDynamicLinkData? initialLink =
        await FirebaseDynamicLinks.instance.getInitialLink();
    if (initialLink != null) {
      final Uri deepLink = initialLink.link;
      print('here our deep link');
      Navigator.pushNamed(context, '/HomeScreen');
    }
  }
}

// class CounterStorage {
//   Future<String> get _localPath async {
//     final directory = await getApplicationDocumentsDirectory();
//     return directory.path;
//   }

//   Future<File> get _localFile async {
//     final path = await _localPath;
//     print(path);
//     Fluttertoast.showToast(
//       msg: '$path',
//       toastLength: Toast.LENGTH_SHORT,
//       gravity: ToastGravity.BOTTOM,
//       backgroundColor: Colors.black45,
//       fontSize: 18,
//     );
//     return File('$path/counter.txt');
//   } // /data/user/0/com.example.expe/app_flutter

//   Future<int> readCounter() async {
//     try {
//       final file = await _localFile;
//       // Read the file
//       final contents = await file.readAsString();
//       print('content is : $contents');
//       return int.parse(contents);
//     } catch (e) {
//       print('${e.toString()}');
//       // If encountering an error, return 0     createFile(jsonEncode(login.data), dir, fileName);

//       return 0;
//     }
//   }

//   Future<File> writeCounter(int counter) async {
//     final file = await _localFile;
//     // Write the file
//     return file.writeAsString('$counter');
//   }
// }
