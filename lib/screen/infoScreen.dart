import 'package:familov/screen//homeScreen.dart';
import 'package:familov/screen/changelangscreen.dart';
import 'package:familov/screen/forgetPasswordScreen.dart';
import 'package:familov/screen/languageScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:fluttertoast/fluttertoast.dart';

class InfoScreen extends StatefulWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  State<InfoScreen> createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  @override
  void initState() {
    print('init work');
    // initDynamicLinks(context);
    super.initState();
    skip();
  }

  initDynamicLinks(BuildContext context) async {
    var data = await FirebaseDynamicLinks.instance.getInitialLink();
    var deepLink = data?.link;
    // Fluttertoast.showToast(
    //   msg: '${deepLink}',
    //   toastLength: Toast.LENGTH_SHORT,
    //   gravity: ToastGravity.BOTTOM,
    //   backgroundColor: Colors.black45,
    //   fontSize: 18,
    // );
    print('deepLink url $deepLink');
    if (deepLink != null) {
      if (deepLink.toString().contains('ashu')) {
        print('deeplinking credithistory = ');
        Fluttertoast.showToast(
          msg: 'Link found',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black45,
          fontSize: 18,
        );
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ForgetPassword()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    bool? boolValue = false;
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            // color: Colors.red,
            width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height / 2,
            child: Image.asset(
              'assets/images/Group 10233.png',
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 35,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14),
            child: Text(
              ' ipsum dolor sit amet, consectetur adipiscing elit. Eget vulputate blandit lacus lectus ornare venenatis quam. Purus ornare et congue id rhoncus. Eu, ultricies turpis pulvinar sed. ',
              style: TextStyle(
                letterSpacing: 1,
                height: 1.5,
                fontFamily: 'Gilroy',
                color: Color(0xff828282),
                fontSize: 18.0,
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14),
            child: FlatButton(
              height: MediaQuery.of(context).size.height / 15,
              minWidth: MediaQuery.of(context).size.width,
              shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(10), right: Radius.circular(10))),
              color: Color(0xff00DBA7),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Continue',
                      style: new TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 18.0,
                        color: Colors.white,
                      )),
                ],
              ),
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                boolValue = prefs.getBool('valBool');
                await prefs.setBool('valBool', true);

                print('boolValue :  $boolValue');
                if (boolValue == true) {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HomeScreen(0, '')));
                  //  Navigator.of(context).pushNamed('/HomeScreen');
                } else {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LanguageScreen()));
                  await onboardinfo();
                  //  Navigator.of(context).pushNamed('/InfoScreen');
                }
              },
            ),
          ),
        ],
      ),
    ));
  }

  void skip() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('skip', true);
  }

  onboardinfo() async {
    print("Shared pref called");
    int isViewed = 0;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('onBoard', isViewed);
    print(prefs.getInt('onBoard'));
  }
}
