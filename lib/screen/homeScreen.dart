import 'package:familov/model/getCart.dart';
import 'package:familov/presenter/cartcontroller.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:familov/model/setCart.dart';
import 'package:familov/presenter/cartprovider.dart';
import 'package:familov/presenter/currency_presenter.dart';
import 'package:familov/presenter/getCart_Presenter.dart';

import 'package:familov/presenter/setCart_presenter.dart';
import 'package:familov/screen/CartScreen.dart';
import 'package:familov/screen/ProfileScreen.dart';

import 'package:familov/screen/internetconnection.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/customicons.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'package:familov/screen/home.dart';
import 'package:familov/screen/mobileRechargeScreen.dart';
import 'package:familov/model/currency_model.dart';
import 'package:familov/screen/paymentfail.dart';
import 'package:familov/screen/success.dart';
import 'package:familov/widget/db_helper.dart';
import 'package:familov/widget/sphere_bottom_navigation_bar.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';

class HomeScreen extends StatefulWidget with ChangeNotifier {
  var i;
  var count;
  HomeScreen(this.i, this.count, {Key? key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    implements CurrencyContract, GetCartContract, SetCartContract {
  int _selectedIndex = 0;
  int currentIndex = 0;
  late Widget child;
  var local;
  Color backgroudColor = Colors.white;
  var intenetconnectionduration = 10;
  late File jsonFile;
  late Directory dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  var fileContent;
  var cartdata;
  var cartlength;
  var valueKey;
  var shopId;

  late SetCartPresenter _setCartPresenter;
  CartController cartController = Get.put(CartController());
  FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;

  var items;
  var cartcount;
  late CurrencyPresenter _presenter;
  GetCartPresenter? presenter1;

  _HomeScreenState() {
    _presenter = new CurrencyPresenter(this);
    presenter1 = new GetCartPresenter(this);
    _setCartPresenter = new SetCartPresenter(this);
  }

  @override
  void initState() {
    initDynamicLinks();
    tokenpref();
    Internetconnection();
    print('run here');
    presenter1!.getCart();

    database();
    _selectedIndex = widget.i;

    getApplicationDocumentsDirectory().then((Directory directory) async {
      dir = directory;
      jsonFile = new File(dir.path + "/" + fileName);
      fileExists = jsonFile.existsSync();
      print('${fileExists}--here it');

      if (fileExists) {
        setState(() {
          final data = json.decode(jsonFile.readAsStringSync());
          dynamic _items = data;
          items = jsonDecode(_items);
          print('items data value ${items['MSG_PRODUCT_DETAIL_04']}');
        });
      }
    });
    //  pref();
    _presenter.getCurrency();
    print("calleddd");
    setState(() {});

    super.initState();
  }

  pref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    local = preferences.getString('languages');
    print('local check is $local');
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // pref();
    // final provider = Provider.of<LocaleProvider>(context, listen: false);
    // provider.setLocale(local);
    // final provider = Provider.of<LocaleProvider>(context);
    // var locale = provider.locale ?? Locale('$local');
    // final provider = Provider.of<CartProvider>(context);
    const List<Widget> _widgetOptions = <Widget>[
      Home(),
      MobileScreenRecharge(),
      CartScreen(),
      ProfileScreen(),
    ];

    void _onItemTapped(int index) {
      setState(() {
        // database();
        print('$cartlength');
        _selectedIndex = index;
      });
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            body: Center(
              child: Container(child: _widgetOptions.elementAt(_selectedIndex)),
            ),
            bottomNavigationBar: AnimatedContainer(
              duration: Duration(milliseconds: 200),
              decoration: BoxDecoration(
                  border: Border.all(width: .1, color: Colors.black26)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    highlightColor: Colors.transparent,
                    splashFactory: NoSplash.splashFactory,
                    onTap: () {
                      //
                      Navigator.push(
                          context,
                          PageTransition(
                            duration: Duration(microseconds: 500),
                            type: PageTransitionType.fade,
                            alignment: Alignment.topCenter,
                            child: HomeScreen(0, 'count'),
                          ));
                      //
                      // _onItemTapped(0);
                    },
                    child: Container(
                      decoration: _selectedIndex == 0
                          ? BoxDecoration(
                              color: Colors.pink.withOpacity(.2),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(60),
                                bottomRight: Radius.circular(60),
                              ))
                          : BoxDecoration(color: Colors.white),

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: 28,
                              // width: 26,
                              child: _selectedIndex == 0
                                  ? Image.asset('assets/images/homepink.png')
                                  : Image.asset('assets/images/home.png')
                              //  Icon(
                              //   CustomIcons.home,
                              //   color: _selectedIndex == 0
                              //       ? AppColors().appaccent
                              //       : Color(0xff4D4D4D),
                              // )
                              ),
                          Text(
                            '${AppLocalizations.of(context)!.home}',
                            style: TextStyle(
                                color: _selectedIndex == 0
                                    ? AppColors().appaccent
                                    : Color(0xff808080),
                                fontSize: 10,
                                fontFamily: 'roboto'),
                          )
                        ],
                      ),
                      width: 88,
                      // color: Colors.amber,
                    ),
                  ),
                  InkWell(
                    highlightColor: Colors.transparent,
                    splashFactory: NoSplash.splashFactory,
                    onTap: (() {
                      // _onItemTapped(1);
                      Navigator.push(
                          context,
                          PageTransition(
                            duration: Duration(microseconds: 500),
                            type: PageTransitionType.fade,
                            alignment: Alignment.topCenter,
                            child: HomeScreen(1, 'count'),
                          ));
                    }),
                    child: Container(
                      decoration: _selectedIndex == 1
                          ? BoxDecoration(
                              color: Colors.pink.withOpacity(.2),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(60),
                                bottomRight: Radius.circular(60),
                              ))
                          : BoxDecoration(color: Colors.white),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: 28,
                              // width: 26,
                              child: Icon(
                                CustomIcons.recharge,
                                color: _selectedIndex == 1
                                    ? AppColors().appaccent
                                    : Color(0xff4D4D4D),
                              )),
                          Text(
                            '${AppLocalizations.of(context)!.recharge}',
                            style: TextStyle(
                                color: _selectedIndex == 1
                                    ? AppColors().appaccent
                                    : Color(0xff808080),
                                fontSize: 10,
                                fontFamily: 'roboto'),
                          )
                        ],
                      ),
                      width: 88,
                      // color: Colors.amber,
                    ),
                  ),
                  InkWell(
                    highlightColor: Colors.transparent,
                    splashFactory: NoSplash.splashFactory,
                    onTap: (() {
                      // _onItemTapped(2);
                      Navigator.push(
                          context,
                          PageTransition(
                            duration: Duration(microseconds: 500),
                            type: PageTransitionType.fade,
                            alignment: Alignment.topCenter,
                            child: HomeScreen(2, 'count'),
                          ));
                    }),
                    child: Container(
                      decoration: _selectedIndex == 2
                          ? BoxDecoration(
                              color: Colors.pink.withOpacity(.2),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(60),
                                bottomRight: Radius.circular(60),
                              ))
                          : BoxDecoration(color: Colors.white),

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(children: <Widget>[
                            Container(
                                height: 28,
                                // width: 26,
                                child: _selectedIndex == 2
                                    ? Image.asset(
                                        'assets/images/shoppingpink.png')
                                    : Image.asset(
                                        'assets/images/shopping-cart.png')),
                            new Positioned(
                              // draw a red marble
                              top: 0.0,
                              right: 0.0,
                              left: 12,
                              child: Obx(
                                () => Visibility(
                                  visible: valueKey == null
                                      ? cartController.getcount.value == 0
                                          ? false
                                          : true
                                      : cartController.getcountapi.value == 0
                                          ? false
                                          : true,
                                  // cartlength == 0
                                  //     ? cartcount == null || cartcount == 0
                                  //         ? false
                                  //         : true
                                  //     : true,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      '${valueKey == null ? cartController.getcount.value : cartController.getcountapi.value} ',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    decoration: BoxDecoration(
                                        color: AppColors().toastsuccess,
                                        shape: BoxShape.circle),
                                    height: 15,
                                    width: 10,
                                  ),
                                ),
                              ),
                            )
                          ]),
                          Text(
                            'Cart',
                            style: TextStyle(
                                color: _selectedIndex == 2
                                    ? AppColors().appaccent
                                    : Color(0xff808080),
                                fontSize: 10,
                                fontFamily: 'roboto'),
                          )
                        ],
                      ),
                      width: 88,
                      // color: Colors.amber,
                    ),
                  ),
                  InkWell(
                    highlightColor: Colors.transparent,
                    splashFactory: NoSplash.splashFactory,
                    onTap: (() {
                      // _onItemTapped(3);
                      Navigator.push(
                          context,
                          PageTransition(
                            duration: Duration(microseconds: 500),
                            type: PageTransitionType.fade,
                            alignment: Alignment.topCenter,
                            child: HomeScreen(3, 'count'),
                          ));
                    }),
                    child: Container(
                      decoration: _selectedIndex == 3
                          ? BoxDecoration(
                              color: Colors.pink.withOpacity(.2),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(60),
                                bottomRight: Radius.circular(60),
                              ))
                          : BoxDecoration(color: Colors.white),

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: 28,
                              // width: 26,
                              child: _selectedIndex == 3
                                  ? Image.asset(
                                      'assets/images/userpink.png',
                                    )
                                  : Image.asset(
                                      'assets/images/details.png',
                                    )),
                          Text(
                            'Profile',
                            style: TextStyle(
                                color: _selectedIndex == 3
                                    ? AppColors().appaccent
                                    : Color(0xff808080),
                                fontSize: 10,
                                fontFamily: 'roboto'),
                          )
                        ],
                      ),
                      width: 88,
                      // color: Colors.amber,
                    ),
                  ),
                ],
              ),
              height: 55,
              // color: Colors.red,
            )
            //  SphereBottomNavigationBar(
            //   defaultSelectedItem: widget.i == 0 ? 0 : 2,
            //   sheetBackgroundColor: Colors.white,
            //   sheetRadius: BorderRadius.only(
            //       topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            //   onItemPressed: (index) {
            //     _onItemTapped(index);
            //   },
            //   onItemLongPressed: (index) => setState(() {
            //     backgroudColor = Color(0xFF44D6B2);
            //   }),
            //   navigationItems: [
            //     BuildNavigationItem(
            //         tooltip: 'Home',
            //         itemColor: Colors.blue,
            //         icon: 'homes',
            //         selectedItemColor: Color(0xFFFFB2D9), //
            //         // title: '${items['MSG_PRODUCT_DETAIL_04']}'),
            //         title: '${AppLocalizations.of(context)!.home}'),
            //     BuildNavigationItem(
            //         tooltip: 'Recharge',
            //         itemColor: Color(0xFFB2F4FF),
            //         icon: 'rechargeicon',
            //         selectedItemColor: Color(0xFFFFB2D9), //
            //         // title: '${items['LBL_RECHARGE_01']} '),
            //         title: '${AppLocalizations.of(context)!.recharge}'),

            //     BuildNavigationItem(
            //         tooltip: 'Cart',
            //         itemColor: Color(0xFFCDB2FF),
            //         // icon: cartlength == 0 ? 'carting' : 'apple',
            //         icon: 'carting',
            //         selectedItemColor: Color(0xFFFFB2D9),
            //         title: 'Cart'),
            //     //                title: '${items['MSG_PRODUCT_CART_04']} '),

            //     BuildNavigationItem(
            //         tooltip: 'Profile',
            //         itemColor: Color(0xFFB2F4FF),
            //         icon: 'group1',
            //         selectedItemColor: Color(0xFFFFB2D9),
            //         title: 'Profile'),
            //   ],
            // ),

            ),
      ),
    );
  }

  @override
  void onCurrencyError(String error) {
    // TODO: implement onCurrencyError
  }

  @override
  Future<void> onCurrencySuccess(CurrencyModel currencyModel) async {
    // TODO: implement onCurrencySuccess
    print('Heeeeeeee ${currencyModel.data!.dataList![0].vName}');
    print('Heeeeeeee ${currencyModel.data!.dataList![0].vSymbol}');
    print('Heeeeeeee ${currencyModel.data!.dataList![0].enRate}');
    if (currencyModel.status == true) {
      List<Data_list>? dataList = currencyModel.data!.dataList;

      SharedPreferences preferences = await SharedPreferences.getInstance();
      var checkCurrency = preferences.getString('currency');
      var currencySymbol = preferences.getString('currencySymbol');
      var rate1 = preferences.getString('rate');

      for (int i = 0; i < dataList!.length; i++) {
        if (checkCurrency == null) {
          if (dataList[i].vName == "USD") {
            preferences.setString(
                'currency',
                dataList[i].vSymbol.toString() +
                    ' ' +
                    dataList[i].vName.toString());
            preferences.setString(
                'currencySymbol', dataList[i].vSymbol.toString());
            preferences.setString('rate', dataList[i].enRate.toString());

            print("UpdatedAlreadyyyyy newwww ");

            break;
          }
        } else {
          if (dataList[i].vSymbol.toString() == currencySymbol.toString()) {
            preferences.setString(
                'currency',
                dataList[i].vSymbol.toString() +
                    ' ' +
                    dataList[i].vName.toString());
            preferences.setString(
                'currencySymbol', dataList[i].vSymbol.toString());
            preferences.setString('rate', dataList[i].enRate.toString());
            print("UpdatedAlreadyyyyy ");
            break;
          }
        }
      }
    }
  }

  Future<bool> _onWillPop() async {
    // This dialog will exit your app on saying yes

    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sure?'),
            content: const Text('Do you want to exit an App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              FlatButton(
                onPressed: () => SystemNavigator.pop(),
                child: const Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }

  database() async {
    List<Map> cartdata = await DBHelper.getData('thecart');
    print("${cartdata.length}here it is");
    // print('${jsonEncode(cartdata)}here it is');

    setState(() {
      cartlength = cartdata.length;
      cartController.getcount.value = cartlength;
      if (cartlength != 0) {
        for (int i = 0; i < cartlength; i++) {
          print('cart lenght is :${cartlength}');
          _setCartPresenter.setCart(
              cartdata[i]['shopId'].toString(),
              cartdata[i]['id'].toString(),
              cartdata[i]['quantity'].toString(),
              '',
              '',
              '',
              '');
        }
        ;
      }
    });
  }

  database1() async {
    // var dataList = await DBHelper.getData('thecart');
    // print('Entt ${jsonEncode(dataList)}  and  ${dataList.length}');
    // var length = dataList.length;
    // var items = dataList
    //     .map(
    //       (item) => Place(
    //           id: item['id'],
    //           quantity: item['quantity'],
    //           shopId: item['shopId']),
    //     )
    //     .toList();
    // items.isEmpty
    //     ? Navigator.push(
    //         context, MaterialPageRoute(builder: (context) => HomeScreen(0, '')))
    //     : print('object');
  }

  Future<void> initDynamicLinks() async {
    print('qwerty');

    var linkkk = await dynamicLinks.getInitialLink();
    print("linkkkkkkkkkkk1 $linkkk");
    if (linkkk != null) {
      linkkk.link.queryParameters.entries.first.value;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SuccessScreen(
                  linkkk.link.queryParameters.entries.first.value, false)));
    }

    dynamicLinks.onLink.listen((dynamicLinkData) {
      print('linkkkkkkkkkkk2');

      print("linkkkkkkkkkkk3 ${dynamicLinkData.link.path}");
      print("linkkkkkkkkkkk4 ${dynamicLinkData.link}");
      print(
          "linkkkkkkkkkkk4 ${dynamicLinkData.link.queryParameters['status']}");
      print(
          "linkkkkkkkkkkk4 ${dynamicLinkData.link.queryParameters['gencode']}");

      if (dynamicLinkData.link.queryParameters.entries.first.value ==
          'success') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessScreen(
                    dynamicLinkData.link.queryParameters.entries.first.value,
                    false)));
      }
      if (dynamicLinkData.link.queryParameters.entries.first.value == 'fail') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => FailScreen(
                    dynamicLinkData.link.queryParameters.entries.first.value,
                    false)));
      }
    }).onError((error) {
      print('onLink error');
      print(error.message);
    });
  }

  Internetconnection() async {
    // Simple check to see if we have internet
    print("The statement 'this machine is connected to the Internet' is: ");
    print(await InternetConnectionChecker().hasConnection);
    // returns a bool

    // We can also get an enum value instead of a bool
    print(
        "Current status: ${await InternetConnectionChecker().connectionStatus}");
    // prints either InternetConnectionStatus.connected
    // or InternetConnectionStatus.disconnected

    // This returns the last results from the last call
    // to either hasConnection or connectionStatus
    // print("Last results: ${InternetConnectionChecker().lastTryResults}");

    // actively listen for status updates
    // this will cause InternetConnectionChecker to check periodically
    // with the interval specified in InternetConnectionChecker().checkInterval
    // until listener.cancel() is called
    var listener = InternetConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case InternetConnectionStatus.connected:
          setState(() {
            intenetconnectionduration = 0;
            setState(() {});
          });
          print('Connected');
          break;
        case InternetConnectionStatus.disconnected:
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NetConnection()));
          // BotToast.showSimpleNotification(
          //     duration: Duration(minutes: intenetconnectionduration),
          //     onTap: () {},
          //     backgroundColor: Colors.red,
          //     title: "Oops! We hit a roadblock");
          break;
      }
    });

    // close listener after 30 seconds, so the program doesn't run forever
    await Future.delayed(Duration(seconds: 30), () {
      setState(() {});
    });
    await listener.cancel();
  }

  @override
  void onGetCartError(String error) {
    // TODO: implement onGetCartError
  }

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    // TODO: implement onGetCartSuccess

    if (getCartModel.status == true) {
      var dataList = getCartModel.data!.length;
      print('cart data length $dataList');
      cartController.getcountapi.value = dataList;
      cartcount = dataList;
      shopId = getCartModel.data![0].shopId;
      setState(() {
        print('$shopId shopping idd');
      });
    }

    setState(() {});
  }

  @override
  void onSetCartError(String error) {
    // TODO: implement onSetCartError
  }

  @override
  void onSetCartSuccess(SetCart setCartModel) {
    // delCart();
    // TODO: implement onSetCartSuccess
  }

  delCart() async {
    print(' orailnoor yea its deleted');
    await DBHelper.fulldelete();
  }

  void tokenpref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
  }

  // setPreference() async {
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   shopId==179?print('object'):
  //   preferences.setString('myShopID', shopId);

  //   print('seeeeeetttttttt $shopId ');
  // }
}
