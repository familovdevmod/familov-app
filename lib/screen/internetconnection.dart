import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class NetConnection extends StatefulWidget {
  const NetConnection({Key? key}) : super(key: key);

  @override
  State<NetConnection> createState() => _NetConnectionState();
}

class _NetConnectionState extends State<NetConnection> {
  @override
  void initState() {
    print("Internet connection");
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            width: MediaQuery.of(context).size.width * .90,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'No Internet connection',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff4F4F4F),
                    fontSize: 24,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Image.asset('assets/images/Group 66.png'),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Please check your internet connection',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff828282),
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => HomeScreen(0,'')),
                        (Route<dynamic> route) => false);
                    // Navigator.pop(context,
                    //     MaterialPageRoute(builder: (context) => Home()));
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 30),
                    width: MediaQuery.of(context).size.width - 15,
                    height: 48,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 219, 167, 1),
                        borderRadius: BorderRadius.circular(12)),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Back to Home',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gilroy',
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
