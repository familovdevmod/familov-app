import 'package:flutter/material.dart';
import 'package:dotted_decoration/dotted_decoration.dart';

class FriendScreen extends StatefulWidget {
  const FriendScreen({Key? key}) : super(key: key);
  @override
  _FriendScreenState createState() => _FriendScreenState();
}

class _FriendScreenState extends State<FriendScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 80,
          automaticallyImplyLeading: true,
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Recommend A friend',
            style: TextStyle(
              fontFamily: 'Gilroy',
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Color(0xffF93E6C),
            ),
          ),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              Card(
                elevation: 0.4,
                child: Container(
                    height: MediaQuery.of(context).size.height / 7,
                    child: Row(children: [
                      Spacer(),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Column(
                          children: [
                            Text(
                              '£ 3.72',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(96, 32, 189, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 18,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            Text(
                              'Earnings',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(79, 79, 79, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Column(
                          children: [
                            Text(
                              '£ 0.00 ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(249, 62, 108, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 18,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            SizedBox(
                              width: 45,
                            ),
                            Text(
                              'Spent',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(79, 79, 79, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Column(
                          children: [
                            Text(
                              '£ 3.72',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(0, 219, 167, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 18,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                            Text(
                              'Available',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(79, 79, 79, 1),
                                  fontFamily: 'Gilroy',
                                  fontSize: 14,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5),
                            ),
                          ],
                        ),
                      ),
                      Spacer()
                    ])),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: Text(
                  'Invite your friends and \n earn money.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(96, 32, 189, 1),
                      fontFamily: 'Gilroy',
                      fontSize: 20,
                      letterSpacing: 0,
                      fontWeight: FontWeight.w600,
                      height: 1.5),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Tap to copy the link',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(130, 130, 130, 1),
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                    letterSpacing: 0,
                    fontWeight: FontWeight.normal,
                    height: 1.5),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                  height: MediaQuery.of(context).size.height / 9,
                  decoration: DottedDecoration(
                    shape: Shape.box,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    child: Center(
                      child: Text(
                        'http://devtest1.familov.com/sign-up/sparshg0007@gmail.com',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )),
              SizedBox(
                height: 40,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      child: new Container(
                          child: Divider(
                        color: Colors.black45,
                        height: 3,
                      )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Or",
                      style:
                          TextStyle(fontSize: 16.0, color: Color(0xff828282)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: new Container(
                          child: Divider(
                        color: Colors.black45,
                        height: 3,
                      )),
                    ),
                  ]),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 48,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  height: 48,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    ),
                    border: Border.all(
                      color: Color.fromRGBO(0, 219, 167, 1),
                      width: 1,
                    ),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(top: 12),
                    child: Text(
                      'Share on Facebook',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(0, 219, 167, 1),
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w600,
                          height: 1),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
