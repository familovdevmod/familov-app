import 'package:bot_toast/bot_toast.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:familov/model/forget.dart';
import 'package:familov/presenter/forget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ForgetPassword2 extends StatefulWidget {
  var email;
  var otp;
  ForgetPassword2({Key? key, this.email, this.otp}) : super(key: key);
  @override
  State<ForgetPassword2> createState() => _ForgetPassword2State();
}

class _ForgetPassword2State extends State<ForgetPassword2>
    implements ForgetPasswordScreenContract {
  final _newController = TextEditingController();

  final _confirmController = TextEditingController();
  var status;
  ForgetPasswordScreenPresenter? presenter;
  _ForgetPassword2State() {
    presenter = new ForgetPasswordScreenPresenter(this);
  }
  // var email;
  bool _obscureText = true;
  bool _obscureTextnpwd = true;
  // var otp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
         toolbarHeight:30,
        foregroundColor: Color(0xff4D4D4D),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.only(top: 80, left: 16, right: 16),
          child: Column(children: [
            Center(
              child: Text(
                'Enter New Password',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  color: Color(0xffF93E6C),
                  fontSize: 32.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 14),
              child: Text(
                'Please enter your new Password and Confirm Password',
                style: TextStyle(
                  letterSpacing: 1,
                  height: 1.5,
                  fontFamily: 'Gilroy',
                  color: Color(0xff828282),
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(
                top: 25,
              ),
              child: Text('New Password'),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 5,
              ),
              child: TextFormField(
                decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureTextnpwd = !_obscureTextnpwd;
                          });
                        },
                        child: _obscureTextnpwd
                            ? Image.asset(
                                'assets/images/eyeoff.png',
                                height: 17,
                                width: 17,
                                color: null,
                                scale: 2,
                              )
                            : Image.asset(
                                'assets/images/visibility.png',
                                height: 0,
                                width: 0,
                                color: null,
                                scale: 2,
                              )),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: 'Enter the new Password',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: _newController,
                obscureText: _obscureTextnpwd,
                keyboardType: TextInputType.visiblePassword,
                validator: null,
                onSaved: null,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(
                top: 25,
              ),
              child: Text('Confirm Password'),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 5,
              ),
              child: TextFormField(
                decoration: InputDecoration(
                    // suffixIcon: GestureDetector(
                    //     onTap: () {
                    //       setState(() {
                    //         _obscureTextnpwd = !_obscureTextnpwd;
                    //       });
                    //     },
                    //     child: _obscureTextnpwd
                    //         ? Image.asset(
                    //             'assets/images/eyeoff.png',
                    //             height: 17,
                    //             width: 17,
                    //             color: null,
                    //             scale: 2,
                    //           )
                    //         : Image.asset(
                    //             'assets/images/visibility.png',
                    //             height: 0,
                    //             width: 0,
                    //             color: null,
                    //             scale: 2,
                    //           )),

                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: Color(0xFF7F7F7F),
                        width: 0.5,
                      ),
                    ),
                    hintText: 'Enter the confirm Password',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffDA3C5F)),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(color: Color(0xffDA3C5F)))),
                controller: _confirmController,
                keyboardType: TextInputType.visiblePassword,
                obscureText: _obscureTextnpwd,
                validator: null,
                onSaved: null,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            buttonWidget('Submit', context, () {
              if (_newController.text != _confirmController.text) {
                // Fluttertoast.showToast(
                //   msg: 'Password not match',
                //   toastLength: Toast.LENGTH_SHORT,
                //   gravity: ToastGravity.BOTTOM,
                //   backgroundColor: Colors.black45,
                //   fontSize: 18,
                // );
                BotToast.showSimpleNotification(
                    backgroundColor: AppColors().toasterror,
                    title: "Password not match");
              }

              print('email is - ${widget.email} and otp is - ${widget.otp}');
              if (_newController.text.isNotEmpty &&
                  _confirmController.text.isNotEmpty &&
                  _newController.text == _confirmController.text) {
                presenter!.forgetPassword(
                    '${widget.email}', //test@mail.com
                    '${widget.otp}', //3c2e9986735470724b2369a4b2f9688d
                    //dGVzdEBtYWlsLmNvbQ==
                    '${_newController.text}', //123456789
                    '${_confirmController.text}', //123456789
                    '3');
              } else {
                BotToast.showSimpleNotification(
                    backgroundColor: AppColors().toasterror,
                    title: "Enter valid Password");
                // Fluttertoast.showToast(
                //   msg: 'Enter valid Password',
                //   toastLength: Toast.LENGTH_SHORT,
                //   gravity: ToastGravity.BOTTOM,
                //   backgroundColor: Colors.black45,
                //   fontSize: 18,
                // );
              }
            }),
          ]),
        ));
  }

  @override
  void onForgetPassError(String error) {}

  @override
  void onForgetPassSuccess(Forget forgetModel) {
    status = forgetModel.status;
    if (status == true) {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toastsuccess,
          title: "${forgetModel.message}");
      // Fluttertoast.showToast(
      //   msg: '${forgetModel.message}',
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM,
      //   backgroundColor: Colors.black45,
      //   fontSize: 18,
      // );
      Navigator.of(context).pushReplacementNamed('/loginScreen');
    } else {
      BotToast.showSimpleNotification(
          backgroundColor: AppColors().toasterror,
          title: "${forgetModel.errorMsg}");
      // Fluttertoast.showToast(
      //   msg: '${forgetModel.errorMsg}',
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.BOTTOM,
      //   backgroundColor: Colors.black45,
      //   fontSize: 18,
      // );
    }
    setState(() {});
  }
}
