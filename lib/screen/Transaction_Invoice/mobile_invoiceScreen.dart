import 'dart:convert';
import 'dart:io';
import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:familov/model/pdf_model.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/loadingindicator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:familov/model/mobile_invoice.dart';
import 'package:familov/presenter/mobileInvoice_presenter.dart';
import 'package:flutter/material.dart';
import 'package:dotted_line/dotted_line.dart';

import '../../presenter/pdf_presenter_invoice.dart';

class MobileInvoiceScreen extends StatefulWidget {
//  String name;
  MobileInvoiceScreen({
    Key? key,
    //  required this.name,
  }) : super(key: key);
  @override
  _InvoiceScreenState createState() => _InvoiceScreenState();
}

class _InvoiceScreenState extends State<MobileInvoiceScreen>
    implements GetMobileInvoiceContract, GetPdfDataContactInvoice {
  UserMobileInvoicePresenter? _presenter;

  List<Data_list>? statusList = [];
  PdfPresenterInvoice? invoicepresenter;
  var orderId;
  var output;
  late String finalPdfPath;
  Dio dio = Dio();

  var load = false;
  var state = false;
  String nameis = '';
  _InvoiceScreenState() {
    _presenter = new UserMobileInvoicePresenter(this);
    invoicepresenter = PdfPresenterInvoice(this);
  }
  @override
  void initState() {
    pref();
    super.initState();
  }

  pref() async {
    print('object');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    nameis = preferences.getString('nameis')!;
    print('name is ;  $nameis');
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    var Total = double.parse('${arguments['sub_totalArgument']}') +
        double.parse('${arguments['service_taxArgument']}');

    if (state == false) {
      setState(() {
        print('the Data is : ${arguments['urlData']}');
        _presenter!.getMobileInvoice(arguments['urlData']);
      });
    }

    return load == true
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              toolbarHeight: 60,
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text(
                'Invoice',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xffF93E6C),
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10, bottom: 5, top: 10),
                    width: 120,
                    height: 24,
                    child: Row(children: <Widget>[
                      Container(
                        width: 24,
                        height: 24,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 1),
                        ),
                        child: Image.asset('assets/images/download.png'),
                      ),
                      InkWell(
                        child: Text(
                          'Download Bill',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(77, 77, 77, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 14,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                        onTap: () {
                          _getStoragePermission();
                          orderId = arguments['urlData'];
                          invoicepresenter!.getPdfInvoicedata(orderId);
                        },
                      ),
                    ]),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          right: 16, left: 16, bottom: 32, top: 12),
                      width: MediaQuery.of(context).size.width,
                      // height: MediaQuery.of(context).size.height / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                          bottomLeft: Radius.circular(4),
                          bottomRight: Radius.circular(4),
                        ),
                        color: Colors.white,
                        border: Border.all(
                          color: Color.fromRGBO(189, 189, 189, 1),
                          width: 0.5,
                        ),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 33,
                                    child: Image.asset(
                                      'assets/images/familovlogo.png',
                                    ),
                                  ),
                                  Spacer(),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        'CODE: ${arguments['generatedArgument']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Text(
                                        //date_time
                                        'Delivery: ${arguments['delivery_option']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Text(
                                        '${statusList![0].productName}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Text(
                                        //date_time
                                        'Date: ${arguments['date_time']}',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      // Text(
                                      //   //date_time
                                      //   'Address: ${arguments['home_delivery_address']}',
                                      //   style: TextStyle(
                                      //       fontSize: 12,
                                      //       fontFamily: 'Gilroy',
                                      //       fontWeight: FontWeight.w500),
                                      // ),
                                    ],
                                  ),
                                ],
                              ), //meline
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Column(
                                // mainAxisAlignment: MainAxisAlignment.end,
                                // crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Name         :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '$nameis',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'For              :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${arguments['receiver_name']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Contact     :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 4),
                                        child: Text(
                                          '${arguments['mobileArgument']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Alternate Contact     :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 4),
                                        child: Text(
                                          '${arguments['another_phone_number']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Message    :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${arguments['greeting_msg']}',
                                          maxLines: 3,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'Orders',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: 'Gilroy',
                                        fontWeight: FontWeight.w600),
                                  )),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                // height: MediaQuery.of(context).size.height / 7,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                    bottomLeft: Radius.circular(4),
                                    bottomRight: Radius.circular(4),
                                  ),
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Color.fromRGBO(189, 189, 189, 1),
                                    width: 0.5,
                                  ),
                                ),
                                child: Column(children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(20),
                                        width: 40,
                                        height: 40,
                                        child: Image.network(
                                          '${statusList![0].productImage}',
                                          alignment: Alignment.center,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container(
                                              margin: EdgeInsets.only(left: 10),
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                'assets/images/airtel.png',
                                                scale: 1,
                                                alignment: Alignment.center,
                                              ),
                                            );
                                          },
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.elliptical(40, 40)),
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.5,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: Text(
                                              '${statusList![0].productName}',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 16,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ),
                                          // Container(
                                          //   margin: EdgeInsets.only(
                                          //       bottom: 5, top: 2),
                                          //   child: Text(
                                          //     // '',
                                          //     '${statusList![0].rechargeItemDetails?.productName}',
                                          //     maxLines: 3,
                                          //     textAlign: TextAlign.left,
                                          //     style: TextStyle(
                                          //         color: Color.fromRGBO(
                                          //             79, 79, 79, 1),
                                          //         fontFamily: 'Gilroy',
                                          //         fontSize: 12,
                                          //         letterSpacing: 0,
                                          //         fontWeight: FontWeight.normal,
                                          //         height: 1.5),
                                          //   ),
                                          // ),
                                          // Text(
                                          //   // '',
                                          //   'Mob : +${statusList![0].rechargeItemDetails?.phoneCode} '
                                          //   ' ${statusList![0].rechargeItemDetails?.phoneNum.toString()}',
                                          //   textAlign: TextAlign.left,
                                          //   style: TextStyle(
                                          //       color: Color.fromRGBO(
                                          //           79, 79, 79, 1),
                                          //       fontFamily: 'Gilroy',
                                          //       fontSize: 12,
                                          //       letterSpacing: 0,
                                          //       fontWeight: FontWeight.normal,
                                          //       height: 1.5),
                                          // ),
                                        ],
                                      ),
                                      Spacer(),
                                      Column(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 0, bottom: 10, right: 10),
                                            // width: 45,
                                            // height: 30,
                                            // decoration: BoxDecoration(
                                            //   borderRadius: BorderRadius.only(
                                            //     topLeft: Radius.circular(4),
                                            //     topRight: Radius.circular(4),
                                            //     bottomLeft: Radius.circular(4),
                                            //     bottomRight: Radius.circular(4),
                                            //   ),
                                            //   color: Colors.white,
                                            //   border: Border.all(
                                            //     color: Color.fromRGBO(
                                            //         189, 189, 189, 1),
                                            //     width: 0.5,
                                            //   ),
                                            // ),
                                            child: Text(
                                              'Qty: ${statusList![0].quantity}',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Text(
                                              '${arguments['payment_curr_code']} ${statusList![0].productPrize}',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      0, 219, 167, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 18,
                                                  letterSpacing: 0,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ]),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 30, bottom: 25),
                                child: DottedLine(
                                  lineThickness: 0.1,
                                  dashColor: Colors.black,
                                ),
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Sub Total',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '€ ${arguments['sub_totalArgument']}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Service Charges',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '€ ${arguments['service_taxArgument']}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Recharge Mobile',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '€ 0.00',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Total Price',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(51, 51, 51, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                                  Spacer(),
                                  Text(
                                    '€ ${Total.toStringAsFixed(2)}',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        color: Color.fromRGBO(51, 51, 51, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  )
                                ],
                              )
                            ],
                          )))
                ],
              ),
            ))
        : Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                color: Color(0xffDA3C5F),
              ),
            ),
          );
  }

  @override
  void onInvoiceError(String error) {}

  @override
  void onInvoiceSuccess(MobileInvoices invoicesModel) {
    print('refresh completed433 ${jsonEncode(invoicesModel.data!.dataList!)}');
    statusList = invoicesModel.data!.dataList!;
    print('status checks234 ${jsonEncode(statusList)}');
    print('product name ${jsonEncode(statusList![0].productName)}');
    setState(() {
      load = true;
      state = true;
    });
    print('done hit');
  }

  @override
  void onPdfError(String error) {
    // TODO: implement onPdfError
  }

  @override
  void onPdfSuccess(PdfModel invoicesModel) {
    print('onpdfsuccess ${invoicesModel.status} ');

    if (invoicesModel.status == true) {
      String? fileData = invoicesModel.data!.fileData;
      String? fileName = invoicesModel.data!.fileName;

      createPdf(fileName!, fileData!);
    }
    // TODO: implement onPdfSuccess
  }

  createPdf(String fileName, String invoiceString) async {
    print('invoice string $invoiceString');
    var base64v = invoiceString;
    var bytes = base64Decode(base64v);

    Directory? directory;
    try {
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await Directory('/storage/emulated/0/Download');
        // Put file in global download folder, if for an unknown reason it didn't exist, we fallback
        // ignore: avoid_slow_async_io
        print('directory.......... path ${directory.path}');
        if (!await directory.exists()) {
          print('491 directory exist');

          directory = await getExternalStorageDirectory();

          output = directory?.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
          print('directry issss ${await directory?.exists()}');
        } else {
          print('506 exisddsd.........');
          var output = directory.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
        }
      }
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    //  var output = await ExtStorage.getExternalStoragePublicDirectory(
    //   ExtStorage.DIRECTORY_DOWNLOADS);
//    getTemporaryDirectory();
  }

  Future<void> _startDownload(String savePath) async {
    print('save path.............${savePath}');

    //
    BotToast.showSimpleNotification(
        onTap: () {},
        backgroundColor: AppColors().toastsuccess,
        title: "Saved in Download");
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      print('savedddddddd ${savePath}');
      final response = await dio.download(
          savePath /*'http://www.africau.edu/images/default/sample.pdf'*/,
          savePath /*'/data/user/0/com.paliwalkisaan.paliwal_kisaan_bazaar/cache/4.pdf'*/,
          onReceiveProgress: _onReceiveProgress);

      print('response check ${response.data}');
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        var progress = (received / total * 100).toStringAsFixed(0) + "%";

        print('progress ........................${progress}');
      });
    }
  }

  _showNotification(Map<String, dynamic> result) {}

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      setState(() {
        invoicepresenter!.getPdfInvoicedata(orderId);
        // permissionGranted = true;
      });
    }
  }
}
