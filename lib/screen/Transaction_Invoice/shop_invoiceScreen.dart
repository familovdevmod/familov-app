import 'dart:convert';
import 'dart:io';
import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:familov/model/pdf_model.dart';
import 'package:familov/model/shop_invoiceModel.dart';
import 'package:familov/presenter/pdf_presenter_invoice.dart';
import 'package:familov/presenter/shopInvoice_presenter.dart';
import 'package:familov/widget/app_colors.dart';

import 'package:flutter/material.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShopInvoiceScreen extends StatefulWidget {
  const ShopInvoiceScreen({
    Key? key,
  }) : super(key: key);
  @override
  _InvoiceScreenState createState() => _InvoiceScreenState();
}

class _InvoiceScreenState extends State<ShopInvoiceScreen>
    implements GetShopInvoiceContract, GetPdfDataContactInvoice {
  List<Data_list>? statusList = [];
  List<Data_list>? bothList = [];
  var load = false;
  var state = false;
  var orderId;
  String nameis = '';
  late String finalPdfPath;
  String progress = "";
  var output;
  Dio dio = Dio();
  PdfPresenterInvoice? invoicepresenter;
  UserShopInvoicePresenter? _presenter;
  _InvoiceScreenState() {
    invoicepresenter = PdfPresenterInvoice(this);
    _presenter = new UserShopInvoicePresenter(this);
  }

  pref() async {
    print('object');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    nameis = preferences.getString('nameis')!;
    print('name is ;  $nameis');
  }

  @override
  void initState() {
    pref();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    var Total = double.parse('${arguments['sub_totalArgument']}') +
        double.parse('${arguments['delivery_price']}') +
        double.parse('${arguments['service_taxArgument']}');
    if (state == false) {
      setState(() {
        print('the Data is : ${arguments['urlData']}');

        _presenter!.getShopInvoice(arguments['urlData']);
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 60,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Invoice',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
      ),
      body: load == true
          ? SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10, bottom: 5, top: 10),
                    width: 120,
                    height: 24,
                    child: Row(children: <Widget>[
                      Container(
                        width: 24,
                        height: 24,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 1),
                        ),
                        child: Image.asset('assets/images/download.png'),
                      ),
                      InkWell(
                        onTap: () {
                          orderId = arguments['urlData'];
                          // List<String> var2 = orderId.split("=");
                          //  print('var1 at index 1 is ${var2[1]}');
                          // print('var1 at index 0 is ${var2[0]}');
                          //  String num1 = var2[1];
                          // code1 = var2[0];

                          print('download pdf 68 ${orderId}');
                          _getStoragePermission();

                          invoicepresenter!.getPdfInvoicedata(orderId);
                          // getPdfString(orderId);
                        },
                        child: Text(
                          'Download Bill',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(77, 77, 77, 1),
                              fontFamily: 'Gilroy',
                              fontSize: 14,
                              letterSpacing: 0,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ]),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          right: 16, left: 16, bottom: 32, top: 12),
                      width: MediaQuery.of(context).size.width,
                      // height: statusList!.length == 1
                      //     ? MediaQuery.of(context).size.height *
                      //         statusList!.length /
                      //         1.1
                      //     : MediaQuery.of(context).size.height *
                      //         statusList!.length /
                      //         1.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                          bottomLeft: Radius.circular(4),
                          bottomRight: Radius.circular(4),
                        ),
                        color: Colors.white,
                        border: Border.all(
                          color: Color.fromRGBO(189, 189, 189, 1),
                          width: 0.5,
                        ),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    height: 33,
                                    child: Image.asset(
                                      'assets/images/familovlogo.png',
                                    ),
                                  ),
                                  Spacer(),
                                  Column(
                                    children: [
                                      SizedBox(
                                        width: 185,
                                        child: Text(
                                          'CODE: ${arguments['generatedArgument']}',
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      SizedBox(
                                        width: 185,
                                        child: Text(
                                          'Delivery: ${arguments['delivery_option']}',
                                          textAlign: TextAlign.end,
                                          //  overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ), //Relais® Familov Marché A - Alimentation generale Bafoussam
                                      SizedBox(
                                        height: 4,
                                      ),
                                      SizedBox(
                                        width: 185,
                                        child: Text(
                                          'Relais® Familov Marché A-Alimentation generale Bafoussam',
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),

                                      SizedBox(
                                        height: 4,
                                      ),
                                      SizedBox(
                                        width: 185,
                                        child: Text(
                                          'Date: ${arguments['date_time']}',
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      SizedBox(
                                        width: 185,
                                        child: InkWell(
                                          onTap: () {
                                            print(arguments[
                                                'home_delivery_address']);
                                          },
                                          child: Text(
                                            'Address: ${arguments['home_delivery_address'] == 'null' ? arguments['delivery_option'] : arguments['home_delivery_address']}',
                                            textAlign: TextAlign.end,
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontFamily: 'Gilroy',
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Name                   :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '$nameis',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'For                        :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${arguments['receiver_name']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Contact              :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 4),
                                        child: Text(
                                          '${arguments['mobileArgument']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Autre Contact   :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${arguments['another_phone_number']}',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Message             :',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Gilroy',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${arguments['greeting_msg']}',
                                          maxLines: 3,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Gilroy',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'Orders',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: 'Gilroy',
                                        fontWeight: FontWeight.w600),
                                  )),
                              ListView.builder(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: statusList!.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    width: MediaQuery.of(context).size.width,
                                    height:
                                        MediaQuery.of(context).size.height / 7,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(4),
                                        topRight: Radius.circular(4),
                                        bottomLeft: Radius.circular(4),
                                        bottomRight: Radius.circular(4),
                                      ),
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        width: 0.5,
                                      ),
                                    ),
                                    child: Column(children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.all(20),
                                            width: 40,
                                            height: 40,
                                            child: Image.network(
                                              '${statusList![index].productImage}',
                                              alignment: Alignment.center,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  alignment: Alignment.center,
                                                  child: Image.asset(
                                                    'assets/images/airtel.png',
                                                    scale: 1,
                                                    alignment: Alignment.center,
                                                  ),
                                                );
                                              },
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.elliptical(40, 40)),
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 5),
                                                  child: SizedBox(
                                                    width: 170,
                                                    child: Text(
                                                      '${statusList![index].productName}',
                                                      maxLines: 3,
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              79, 79, 79, 1),
                                                          fontFamily: 'Gilroy',
                                                          fontSize: 18,
                                                          letterSpacing: 0,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          height: 1.5),
                                                    ),
                                                  )),
                                            ],
                                          ),
                                          Spacer(),
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  'Qty: ${statusList![index].quantity}',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          79, 79, 79, 1),
                                                      fontFamily: 'Gilroy',
                                                      fontSize: 18,
                                                      letterSpacing: 0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      height: 1.5),
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    print(statusList!);
                                                  },
                                                  child: Text(
                                                    //${arguments['payment_curr_code']}
                                                    '${arguments['currency_check']}${statusList![index].productPrices}',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            0, 219, 167, 1),
                                                        fontFamily: 'Gilroy',
                                                        fontSize: 18,
                                                        letterSpacing: 0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        height: 1.5),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ]),
                                  );
                                },
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 30, bottom: 25),
                                child: DottedLine(
                                  lineThickness: 0.1,
                                  dashColor: Colors.black,
                                ),
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Sub Total',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '${arguments['currency_check']}${arguments['sub_totalArgument']}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Service Charges',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '${arguments['currency_check']}${arguments['service_taxArgument']}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        '${arguments['delivery_option']}',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                      Spacer(),
                                      Text(
                                        '${arguments['currency_check']}${arguments['delivery_price']}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                            height: 1.5),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  arguments['promo_code'].toString() != ''
                                      ? Row(
                                          children: [
                                            Text(
                                              'Promo code (${arguments['promo_code']})',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                            Spacer(),
                                            Text(
                                              '- ${arguments['currency_check']}${arguments['promo_discount_amount']}',
                                              textAlign: TextAlign.right,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      79, 79, 79, 1),
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.5),
                                            ),
                                          ],
                                        )
                                      : Container()
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 10),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Transform.rotate(
                                    angle: 0.000005008956130975318,
                                    child: Divider(
                                        color: Color.fromRGBO(189, 189, 189, 1),
                                        thickness: 0.5),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Total Price',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(51, 51, 51, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  ),
                                  Spacer(),
                                  Text(
                                    '${arguments['currency_check']}${arguments['grand_total']}',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        color: Color.fromRGBO(51, 51, 51, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5),
                                  )
                                ],
                              )
                            ],
                          )))
                ],
              ),
            )
          : Scaffold(
              body: Center(
                child: CircularProgressIndicator(
                  color: Color(0xffDA3C5F),
                ),
              ),
            ),
    );
  }

  @override
  void onShopInvoiceError(String error) {}

  @override
  void onShopInvoiceSuccess(ShopInvoice shopinvoiceModel) {
    setState(() {
      load = true;
      state = true;
    });
    statusList = shopinvoiceModel.data!.dataList!;
    // bothList = shopinvoiceModel.data ;
    print('done00 ${jsonEncode(statusList!.length)}');
    print('refresh completed4 ${jsonEncode(shopinvoiceModel.data!.dataList!)}');
    print('hit done for shop');
  }

  @override
  void onPdfError(String error) {
    // TODO: implement onPdfError
  }

  @override
  void onPdfSuccess(PdfModel invoicesModel) {
    // TODO: implement onPdfSuccess

    print('onpdfsuccess ${invoicesModel.status} ');

    if (invoicesModel.status == true) {
      String? fileData = invoicesModel.data!.fileData;
      String? fileName = invoicesModel.data!.fileName;

      createPdf(fileName!, fileData!);
    }
  }

  createPdf(String fileName, String invoiceString) async {
    print('invoice string $invoiceString');
    var base64v = invoiceString;
    var bytes = base64Decode(base64v);

    Directory? directory;
    try {
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await Directory('/storage/emulated/0/Download');
        // Put file in global download folder, if for an unknown reason it didn't exist, we fallback
        // ignore: avoid_slow_async_io
        print('directory.......... path ${directory.path}');
        if (!await directory.exists()) {
          print('491 directory exist');

          directory = await getExternalStorageDirectory();

          output = directory?.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
          print('directry issss ${await directory?.exists()}');
        } else {
          print('506 exisddsd.........');
          var output = directory.path;
          print('output == $output');
          // orderId = '3';
          final file = File("$output/$fileName.pdf");
          await file.writeAsBytes(bytes.buffer.asUint8List());

          print("pdf path $output/$fileName.pdf");

          finalPdfPath = '$output/$fileName.pdf';
          _startDownload('$output/$fileName.pdf');
        }
      }
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    //  var output = await ExtStorage.getExternalStoragePublicDirectory(
    //   ExtStorage.DIRECTORY_DOWNLOADS);
//    getTemporaryDirectory();
  }

  Future<void> _startDownload(String savePath) async {
    print('save path.............${savePath}');

    //
    BotToast.showSimpleNotification(
        onTap: () {},
        backgroundColor: AppColors().toastsuccess,
        title: "Saved in Download");
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      print('savedddddddd ${savePath}');
      final response = await dio.download(
          savePath /*'http://www.africau.edu/images/default/sample.pdf'*/,
          savePath /*'/data/user/0/com.paliwalkisaan.paliwal_kisaan_bazaar/cache/4.pdf'*/,
          onReceiveProgress: _onReceiveProgress);

      print('response check ${response.data}');
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        progress = (received / total * 100).toStringAsFixed(0) + "%";

        print('progress ........................${progress}');
      });
    }
  }

  _showNotification(Map<String, dynamic> result) {}

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      setState(() {
        invoicepresenter!.getPdfInvoicedata(orderId);
        // permissionGranted = true;
      });
    }
  }
}
