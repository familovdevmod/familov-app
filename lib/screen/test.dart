// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
// import 'package:flutter/material.dart';

// class MainWidgetState extends State<MainWidget> {
//   @override
//   void initState() {
//     super.initState();
//     this.initDynamicLinks();
//   }
//   initDynamicLinks(BuildContext context) async {
//     await Future.delayed(Duration(seconds: 3));
//     var data = await FirebaseDynamicLinks.instance.getInitialLink();
//     var deepLink = data?.link;
//     final queryParams = deepLink.queryParameters;
//     if (queryParams.length > 0) {
//       var userName = queryParams['userId'];
//     }
//     FirebaseDynamicLinks.instance.onLink(onSuccess: (dynamicLink)
//       async {
//       var deepLink = dynamicLink?.link;
//       debugPrint('DynamicLinks onLink $deepLink');
//     }, onError: (e) async {
//       debugPrint('DynamicLinks onError $e');
//     });
//   }
// }

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';

class StorageClass extends StatefulWidget {
  const StorageClass({Key? key}) : super(key: key);
  @override
  State<StorageClass> createState() => _StorageClassState();
}

class _StorageClassState extends State<StorageClass> {
  @override
  void initState() {
    _getStoragePermission();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Directory dir = Directory('/storage/emulated/0/Download');
    List<FileSystemEntity> listOfAllFolderAndFiles =
        dir.listSync(recursive: true);
    return Scaffold(
        appBar: AppBar(
            title: const Text(
              "Files",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Color.fromARGB(174, 51, 53, 36)),
        body: ListView.builder(
            itemCount: listOfAllFolderAndFiles.length,
            itemBuilder: ((context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Icon(Icons.file_copy),
                  title: Text(listOfAllFolderAndFiles[index]
                      .path
                      .toString()
                      .replaceAll('/storage/emulated/0/', '')),
                ),
              );
            })));
  }

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      setState(() {
        // permissionGranted = true;
      });
    }
  }
}
