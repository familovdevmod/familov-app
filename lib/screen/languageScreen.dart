import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/locale_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key? key}) : super(key: key);
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  var isClicked = false;
  var _value = 1;
  var languageval;
  var value;
  var once = true;

  pref(local) async {
    print('locale $local');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (local != null && local != '') {
      preferences.setString('languages', local.toString());
    } else {
      preferences.setString('languages', 'en');
    }
    setState(() {});
  }

  @override
  void initState() {
    pref('');
    languagepref();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LocaleProvider>(context);
    var locale = provider.locale ?? Locale('en');
    //   var heights = AppBar().preferredSize.height;
    // final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            //  height:
            //       MediaQuery.of(context).size.height - heights - statusBarHeight,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 0, top: 20),
                  child: Text(
                    'Select Language',
                    style: TextStyle(
                        fontFamily: 'Gilroy',
                        color: Color(0xffF93E6C),
                        fontSize: 22,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(
                  height: 90,
                ),
                Container(
                  height: 116,
                  width: 100,
                  child: Image.asset(
                    _value == 1
                        ? 'assets/images/fa_language.png'
                        : 'assets/images/languageflip.png',
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 10, left: 16, right: 16),
                    decoration: BoxDecoration()),
                SizedBox(
                  height: 35,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 35),
                  child: Row(
                    children: [
                      InkWell(
                        highlightColor: Colors.transparent,
                        splashFactory: NoSplash.splashFactory,
                        onTap: () {
                          value = 1;
                          _value = 1;
                          setState(() {});
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height / 18,
                          width: MediaQuery.of(context).size.width / 2.7,
                          child: Container(
                            decoration: BoxDecoration(
                                color: _value == 1
                                    ? Color(0xffFFF3F6)
                                    : Colors.white,
                                border: Border.all(
                                  color: _value == 1
                                      ? Color(0xffF93E6C)
                                      : Color(0xffBDBDBD),
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(
                                  child: Container(
                                    height: 18,
                                    width: 18,
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Container(
                                        height: 9,
                                        width: 9,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: _value == 1
                                                ? Color(0xffF93E6C)
                                                : Colors.transparent),
                                      ),
                                    ),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            width: _value == 1 ? 1 : 2,
                                            color: Colors.black54)),
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  'English',
                                  style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      color: _value == 1
                                          ? Colors.black
                                          : Colors.black38,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      InkWell(
                        highlightColor: Colors.transparent,
                        splashFactory: NoSplash.splashFactory,
                        onTap: () {
                          value = 2;
                          _value = 2;
                          setState(() {});
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height / 18,
                          width: MediaQuery.of(context).size.width / 2.7,
                          child: Container(
                            decoration: BoxDecoration(
                                color: _value == 2
                                    ? Color(0xffFFF3F6)
                                    : Colors.white,
                                border: Border.all(
                                  color: _value == 2
                                      ? Color(0xffF93E6C)
                                      : Color(0xffBDBDBD),
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Center(
                                    // child: Radio(
                                    //   value: 1,
                                    //   activeColor: Color(0xffF93E6C),
                                    //   groupValue: _value,
                                    //   onChanged: (value) {
                                    //     setState(() {
                                    //       _value = value as int?;
                                    //       isClicked = false;
                                    //       locale = Locale('en');
                                    //       final provider =
                                    //           Provider.of<LocaleProvider>(context,
                                    //               listen: false);
                                    //       print('Local v :  $locale');
                                    //       provider.setLocale(locale);
                                    //       setState(() {
                                    //         pref(locale);
                                    //       });
                                    //     });
                                    //   },
                                    // ),
                                    child: Container(
                                      height: 18,
                                      width: 18,
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Container(
                                          height: 9,
                                          width: 9,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: _value == 2
                                                  ? Color(0xffF93E6C)
                                                  : Colors.transparent),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              width: _value == 2 ? 1 : 2,
                                              color: Colors.black54)),
                                    ),

                                    // child: Radio(
                                    //   value: 2,
                                    //   activeColor: Color(0xffF93E6C),
                                    //   groupValue: _value,
                                    //   onChanged: (value) {
                                    //     setState(() {
                                    //       _value = value as int?;
                                    //     });
                                    //     locale = Locale('fr');
                                    //     final provider =
                                    //         Provider.of<LocaleProvider>(context,
                                    //             listen: false);
                                    //     print('Local v :  $locale');
                                    //     provider.setLocale(locale);
                                    //     setState(() {
                                    //       pref(locale);
                                    //     });
                                    //   },
                                    // ),
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  'French',
                                  style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      color: _value == 2
                                          ? Colors.black
                                          : Colors.black38,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 60,
                ),
                // ignore: deprecated_member_use
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: FlatButton(
                    height: MediaQuery.of(context).size.height / 15,
                    minWidth: MediaQuery.of(context).size.width,
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(10),
                            right: Radius.circular(10))),
                    color: Color(0xff00DBA7),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text('Continue',
                            style: new TextStyle(
                                fontSize: 18.0, color: Colors.white)),
                      ],
                    ),
                    onPressed: () {
                      value == 2 ? french(locale) : english(locale);
                      // once == true
                      //     ? Navigator.of(context)
                      //         .pushReplacementNamed('/loginScreen')
                      //     : Container(
                      //         height: 0,
                      //         width: 0,
                      //       );

                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeScreen(0, '')));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void english(locale) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    languageval = preferences.setInt('Languageinteger', 1);
    preferences.setString('mylang', 'English');

    // isClicked = false;
    locale = Locale('en');
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    print('Local v :  $locale');
    provider.setLocale(locale);
    pref(locale);
  }

  void french(locale) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    languageval = preferences.setInt('Languageinteger', 2);
    preferences.setString('mylang', 'French');
    locale = Locale('fr');
    final provider = Provider.of<LocaleProvider>(context, listen: false);
    print('Local v :  $locale');
    provider.setLocale(locale);
    pref(locale);
  }

  void languagepref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _value = preferences.getInt('Languageinteger')!;
  }
}
