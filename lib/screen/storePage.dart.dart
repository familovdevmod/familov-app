import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:familov/model/getCart.dart';
import 'package:familov/model/homeModel.dart';
import 'package:familov/model/place.dart';
import 'package:familov/model/topRated_Model.dart';
import 'package:familov/presenter/cartcontroller.dart';
import 'package:familov/presenter/cartprovider.dart';
import 'package:familov/presenter/getCart_Presenter.dart';
import 'package:familov/presenter/home_presenter.dart';
import 'package:familov/presenter/product_presenter.dart';
import 'package:familov/presenter/topRated_presenter.dart';
import 'package:familov/screen/homeScreen.dart';
import 'package:familov/widget/Item.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:familov/widget/db_helper.dart';

import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:fluttertoast/fluttertoast.dart';

// ignore: must_be_immutable
class Stores extends StatefulWidget {
  var hintText;
  @override
  StorePages createState() => StorePages();
}

class StorePages extends State<Stores>
    with SingleTickerProviderStateMixin
    implements
        TopRatedContract,
        GetProductContract,
        HomeContract,
        GetCartContract {
  var once = true;
  bool load = false;
  bool load1 = false;
  int offset = 0;
  List<Data_lists> statusList = [];
  var banner;
  var searchtext = '';
  // List<Product_Data_list> statusList1 = [];
  List<Place> items = [];
  var shopName;

  List<Data_lists> searchstatusList = [];
  String message = "";
  var close = false;
  var sortBy = 0;
  int? val = 0;
  var limit = 10;
  int currentPage = 0;
  var shopAddress;
  bool hideopen = true;
  List<The_Data_list> shop_categories = [];
  List<Sort_options> sort_options = [];
  List<Data9> dataList2 = [];
  var lencart;
  var cartlength;
  bool responseDone = false;
  String searchingText = '';
  var length;
  var searchbool = true;
  TopRatedPresenter? _presenter;
  UserProductPresenter? _presenters;
  GetCartPresenter? presenter1;
  HomePresenter? presenter;
  String? shopId;
  int currentIndex = 110;
  var len = 0;
  var homevar;
  var allpro = true;
  var selectedCategory;
  bool showbestseller = false;
  FocusNode focusNode = FocusNode();

  AnimationController? animationcontrols;
  CartController cartController = Get.put(CartController());
  setBottomBarIndex(index, String? categoryId) {
    print(
        'the shop id $shopId and category id is $categoryId and $searchingText');
    setState(() {
      currentIndex = index;
      load1 = false;
      statusList = [];
      length = 0;
      productTap = false;
      currentPage = 1;
      print(
          ' and current page is and catergory ID is $categoryId $currentIndex');
      _presenters!.getUserproduct(
        '$shopId',
        '$categoryId',
        '$searchingText',
        '$sortBy',
        '$currentPage',
      );
      // ListRefresh(_width, '$shopId', '$categoryId');
      //call product list api and pass category id
    });
  }

  _scrollListener() {
    if (_controller!.position.pixels == _controller!.position.maxScrollExtent) {
      setState(() {
        print('scroll check 2');
        print(
            'sort value  $sortBy  shopId is  :  $shopId  cat is   : $cat  and searchingText is : $searchingText   and current page is  :  $currentPage');

        isRefresh = false;
        searchcontrol = false;
        showbestseller == false
            ? _presenters!.getUserproduct(
                '$shopId',
                '$selectedCategory',
                '$searchingText',
                '$sortBy',
                '$currentPage',
              )
            : _presenters!.getUserproduct(
                '',
                '',
                '',
                '',
                '',
              );

        print('index $currentPage');
        message = "reach the bottom";
        print(message);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: Duration(milliseconds: 500),
          backgroundColor: Colors.transparent,
          elevation: 0,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              hideopen == true
                  ? SizedBox(
                      width: MediaQuery.of(context).size.width / 6,
                    )
                  : SizedBox(),
              Container(
                  alignment: Alignment.center,
                  height: 15,
                  width: 15,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    color: Color(0xffDA3C5F),
                  )),
            ],
          ),
        ));
      });
    }
    // if (_controller!.position.pixels == _controller!.position.minScrollExtent
    //     // &&
    //     //     !_controller!.position.outOfRange
    //     ) ;
    //     {
    //   setState(() {
    //     print('scroll check 3');
    //     currentPage = 1;
    //     isRefresh = true;
    //     _presenters!.getUserproduct(
    //       '${shopId}',
    //       '${selectedCategory}',
    //       '',
    //       '',
    //       '$currentPage',
    //     );
    //     message = "reach the top";
    //     print(message);
    //   });
    // }
  }

  var SearchedList;
  ScrollController? _controller;
  StorePages() {
    _presenter = new TopRatedPresenter(this);
    _presenters = new UserProductPresenter(this);
    presenter = new HomePresenter(this);
    presenter1 = new GetCartPresenter(this);
  }

  bool indexSelect = true;
  bool productTap = false;

  late File jsonFile;
  late Directory dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  var fileContent;

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 2000), () {
      setState(() {
        if (len != 0) {
          showbestseller = true;
          setState(() {
            statusList = [];
            indexSelect = true;
            currentIndex = -1;
            selectedCategory = 0000;
            setState(() {
              allpro = false;
              productTap = false;
              _presenter!.topRated('$shopId');
            });
          });
        }
      });
    });
    animationcontrols = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
    )..repeat(reverse: true); // au
    print('here our banner${banner.toString()}');
    // allproducts();
    // jsonFile = new File(dir.path + "/" + fileName);
    // fileExists = jsonFile.existsSync();
    // if (fileExists)
    //   this.setState(
    //       () => fileContent = jsonDecode(jsonFile.readAsStringSync()));
    print('data get ');
    _controller = ScrollController();
    _controller!.addListener(_scrollListener);

    database();
    super.initState();

    getPreference();
    // Future.delayed(Duration(milliseconds: 2000), () {
    //   setState(() {
    //     allproducts();
    //   });
    // });
  }

  var myShop, myCategory, myCity, myCountry, valueKey;
  getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    myShop = preferences.getString('myShop');
    myCategory = preferences.getString('myCategory');
    myCity = preferences.getString('myCity');
    myCountry = preferences.getString('myCountry');
  }

  List<String> categoryLists = [];

  _sideAppBar() {
    print('length is  :  ${shop_categories.length}');
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: AnimatedContainer(
          duration: Duration(milliseconds: 0),
          width: _width / 1.12,
          height: MediaQuery.of(context).size.height,
          color: const Color(0xffF9F6FC),
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  len == 0
                      ? Container()
                      : Row(
                          children: [
                            Container(
                              width: indexSelect == true ? 4 : 0,
                              height: 100,
                              decoration: const BoxDecoration(
                                  color: Color(0xff6020BD),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, bottom: 10),
                              child: Column(
                                children: [
                                  CircleAvatar(
                                    backgroundColor: indexSelect == true
                                        ? activeColor
                                        : Colors.white,
                                    child: GestureDetector(
                                      child: Image.asset(
                                        'assets/images/starIcon.png',
                                        // semanticsLabel: 'A shark?!',
                                        // placeholderBuilder: (BuildContext context) => Container(
                                        //     padding: const EdgeInsets.all(30.0),
                                        //     child: const CircularProgressIndicator()),
                                      ),
                                      onTap: () {
                                        showbestseller = true;
                                        statusList = [];
                                        indexSelect = true;
                                        currentIndex = -1;
                                        selectedCategory = 0000;
                                        setState(() {
                                          allpro = false;
                                          productTap = false;
                                          _presenter!.topRated('$shopId');
                                        });
                                        //  print();
                                        //     'shop_categories   ${shop_categories[index].categoryId}');
                                        // setBottomBarIndex(index, shop_categories[index].categoryId);
                                      },
                                    ),
                                  ),
                                  Container(
                                      width: 40,
                                      margin: EdgeInsets.only(top: 5),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Best seller',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(96, 32, 189, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 12,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.w600,
                                            height: 1),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                  Row(
                    children: [
                      Container(
                        width: allpro == true ? 4 : 0,
                        height: 100,
                        decoration: const BoxDecoration(
                            color: Color(0xff6020BD),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10))),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 10),
                        child: Column(
                          children: [
                            CircleAvatar(
                              backgroundColor:
                                  allpro == true ? activeColor : Colors.white,
                              child: GestureDetector(
                                child: Icon(
                                  Icons.snowing,
                                  color: Color(0xff566985),
                                  size: 45,
                                ),
                                onTap: () {
                                  searchcontrol = true;
                                  allpro = true;
                                  showbestseller = false;
                                  print('Here i am$shopId');
                                  setState(() {
                                    productTap = false;
                                    currentIndex = 100;
                                    indexSelect = false;
                                    offset = 0;
                                    load1 = false;
                                    statusList = [];
                                    cat = '';
                                    // length = 0;
                                    // productTap = false;
                                    currentPage = 1;
                                    selectedCategory = '';
                                    _presenters!.getUserproduct(
                                      '$shopId',
                                      '',
                                      '$searchingText',
                                      '$sortBy',
                                      '$currentPage',
                                    );
                                  });
                                },
                              ),
                            ),
                            Container(
                              width: 40,
                              margin: EdgeInsets.only(top: 5),
                              alignment: Alignment.center,
                              child: RichText(
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12),
                                    text: 'All products',
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: shop_categories.length,
                      itemBuilder: (context, index) {
                        return Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      width: currentIndex == index ? 4 : 0,
                                      height: 100,
                                      decoration: const BoxDecoration(
                                          color: Color(0xff6020BD),
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10),
                                              bottomRight:
                                                  Radius.circular(10))),
                                    ),
                                    Padding(
                                        padding:
                                            const EdgeInsets.only(left: 15),
                                        child: GestureDetector(
                                          onTap: () {
                                            searchcontrol = true;
                                            showbestseller = false;
                                            print(
                                                'here category lenght${shop_categories.length}');
                                            // here
                                            offset = 0;
                                            print(
                                                'shop_categories_image  ${shop_categories[index].categoryImage.toString()}');
                                            selectedCategory =
                                                shop_categories[index]
                                                    .categoryId;
                                            indexSelect = false;
                                            cat = shop_categories[index]
                                                .categoryId;
                                            setBottomBarIndex(
                                                index,
                                                shop_categories[index]
                                                    .categoryId);
                                            setState(() {
                                              allpro = false;
                                            });
                                          },
                                          child: Column(
                                            children: [
                                              CircleAvatar(
                                                backgroundColor:
                                                    currentIndex == index
                                                        ? activeColor
                                                        : Colors.white,
                                                child: SvgPicture.network(
                                                  '${shop_categories[index].categoryImage}',
                                                  placeholderBuilder: (BuildContext
                                                          context) =>
                                                      Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(30.0),
                                                          child:
                                                              const CircularProgressIndicator(
                                                            color: Color(
                                                                0xffDA3C5F),
                                                          )),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0),
                                                child: Container(
                                                  child: RichText(
                                                      textAlign:
                                                          TextAlign.center,
                                                      maxLines: 3,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      text: TextSpan(
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 12),
                                                        text:
                                                            '${shop_categories[index].categoryName.toString()}',
                                                      )),
                                                  width: 50,
                                                ),
                                              )
                                            ],
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                            ]);
                      }),
                ],
              ),
            ),
          )),
    );
  }

  _showDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
              ),
              title: SizedBox(
                  width: 233.0,
                  height: 22.0,
                  child: Center(
                    child: Text(
                      'Shop Change',
                      style: TextStyle(
                          color: Colors.red, fontWeight: FontWeight.w600),
                      textAlign: TextAlign.right,
                    ),
                  )),
              content: new Text(
                  "Are you sure you want to change shop?\n (If you change the shop you will loose your cart)",
                  textAlign: TextAlign.center),
              actions: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                                alignment: Alignment(0.02, 0.0),
                                width: 112.0,
                                height: 32.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  border: Border.all(
                                    color: Color.fromRGBO(255, 59, 94, 1),
                                    width: 1,
                                  ),
                                ),
                                child: Text(
                                  'No',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Color.fromRGBO(255, 59, 94, 1),
                                      fontFamily: 'Gilroy',
                                      fontSize: 18,
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.w600,
                                      height: 1),
                                )),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GestureDetector(
                              child: Container(
                                alignment: Alignment(0.02, 0.0),
                                width: 112.0,
                                height: 32.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: Color.fromRGBO(0, 219, 167, 1),
                                ),
                                child: Text('Yes',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Gilroy',
                                        fontSize: 18,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.w600,
                                        height: 1)),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                                // Route route = MaterialPageRoute(
                                //     builder: (context) => GoShopping());
                                // Navigator.pushReplacement(context, route);

                                // Navigator.pushNamedAndRemoveUntil(
                                //     context, 'GoShoppingScreen', (route) => true);
                                // Navigator.pushReplacement(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => GoShopping()),
                                // );
                              })
                        ]))
              ]);
        });
  }

  var sortvisible = false;
  onSorted() {
    load1 = false;
    if (val == 0) {
//Sort by me 1,2,3,5 Id jayegi
      sortvisible = false;
      sortBy = 1;
    }
    if (val == 1) {
      sortvisible = true;
      sortBy = 2;
    }
    if (val == 2) {
      sortvisible = true;
      sortBy = 3;
    }
    if (val == 3) {
      sortvisible = true;
      sortBy = 5;
    }
    currentPage = 1;
    print(
        'sort value  $sortBy  shopId is  :  $shopId  cat is   : $cat  and searchingText is : $searchingText   and current page is  :  $currentPage');
    statusList = [];
    _presenters!.getUserproduct(
      '$shopId',
      '$cat',
      '$searchingText',
      '$sortBy',
      '$currentPage',
    );
  }

  onSearchTextChanged(String text) async {
    statusList = [];
    searchingText = text;
    // searchstatusList.clear();
    // if (text.isEmpty) {
    //   setState(() {});
    //   return;
    // }
    print('text is : ' + searchingText);
    currentPage = 0;

    sortBy = 0;
    _presenters!.getUserproduct(
      '$shopId',
      '',
      '$searchingText',
      '$sortBy',
      '$currentPage',
    );
    // _presenters!.getUserproduct(
    //   '$shopId',
    //   'cat',
    //   '$searchingText',
    //   '$sortBy',
    //   '$currentPage',
    // );
    setState(() {});
  }

  bool isRefresh = true;
  bool searchcontrol = true;

  Color activeColor = const Color(0xffEFE9F8);
  double _width = 90;
  bool isVisibe = true;
  bool _value = false;

  var call = true;
  var call2 = true;
  var call3 = true;
  var cat;

  Icon customIcon = const Icon(Icons.search, color: Colors.black38);

  final controller = TextEditingController();
  String text = '';
  TextEditingController searchingController = TextEditingController();
  // var imgVariable = NetworkImage(trend['imageUrl']);
  Widget customSearchBar = Text(
    'Store Name',
    textAlign: TextAlign.left,
    style: TextStyle(
        color: Color.fromRGBO(249, 62, 108, 1),
        fontFamily: 'Gilroy',
        fontSize: 20,
        fontWeight: FontWeight.normal,
        height: 1),
  );

  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<CartProvider>(context);
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    shopId = arguments['shopId'];
    print('shop id : $shopId');
    String categoryId = arguments['categoryId'];
    print('MainCategory id : $categoryId');

    if (call2 == true) {
      presenter!.home('$shopId', '$categoryId');
    }

    print('searchText is  ${controller.text}');
    if (once == true) {
      once = false;

      _presenters!.getUserproduct(shopId, categoryId, '', sortBy, '1');
    }

    // String imagevar = banner;
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pushNamedAndRemoveUntil(
            '/HomeScreen', (Route<dynamic> route) => false);
        return false;
      },
      child: SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.white,
              leading: GestureDetector(
                //
                onTap: () {
                  print(
                      'the shop id $shopId and category id is $categoryId and $searchingText');
                  // Navigator.of(context).pop();
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/HomeScreen', (Route<dynamic> route) => false);
                },
                child: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
              ),
              automaticallyImplyLeading: true,
              actions: [
                IconButton(
                    onPressed: () {
                      showkeyboard(context);
                      searchtext = '';
                      // onSearchTextChanged(searchtext);
                      setState(() {
                        statemanual();
                        if (customIcon.icon == Icons.search) {
                          print('appbar check 402');
                          customIcon = const Icon(
                            Icons.cancel,
                            color: Colors.black38,
                          );
                          customSearchBar = ListTile(
                            title: TextField(
                              focusNode: focusNode,
                              controller: controller,
                              decoration: InputDecoration(
                                hintText: 'Search for the product',
                                hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontStyle: FontStyle.italic,
                                ),
                                border: InputBorder.none,
                              ),
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              onChanged: onSearchTextChanged,
                              onSubmitted: onSearchTextChanged,
                              // onTap: statemanual(),
                            ),
                          );
                        } else {
                          print('appbar check 425');

                          controller.clear();
                          onSearchTextChanged('');

                          onSearchTextChanged('');
                          customIcon = const Icon(
                            Icons.search,
                            color: Colors.black38,
                          );
                          customSearchBar = Text(
                            'Store Name',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(249, 62, 108, 1),
                                fontFamily: 'Gilroy',
                                fontSize: 20,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          );
                        }
                      });
                    },
                    icon: customIcon),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Container(
                    alignment: Alignment.center,
                    child: Stack(children: <Widget>[
                      Container(
                        height: 28,
                        width: 26,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen(2, '')));
                            print('kane');
                            presenter1!.getCart();
                          },
                          child: Icon(
                            Icons.local_grocery_store_outlined,
                            color: Colors.black38,
                          ),
                        ),
                      ),
                      new Positioned(
                        // draw a red marble
                        top: 0.0,
                        right: 0.0,
                        left: 12,
                        child: Obx(
                          () => Visibility(
                            visible: valueKey == null
                                ? cartController.getcount.value == 0
                                    ? false
                                    : true
                                : cartController.getcountapi.value == 0
                                    ? false
                                    : true,
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                '${valueKey == null ? cartController.getcount : cartController.getcountapi}',
                                // '${cartlength==0?lencart:cartlength.toString()}',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12),
                              ),
                              decoration: BoxDecoration(
                                  color: AppColors().toastsuccess,
                                  shape: BoxShape.circle),
                              height: 15,
                              width: 10,
                            ),
                          ),
                        ),
                      )
                    ]),
                  ),
                ),
              ],
              title: customSearchBar,
              centerTitle: true,
              //   backgroundColor: AppColors().primaryColor,
            ),
            body: load == true
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed('/GoShoppingScreen');
                          },
                          //selectorail
                          child: Container(
                            width: MediaQuery.of(context).size.width * .94,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Image.asset(
                                      'assets/images/map-pin.png',
                                      height: 22,
                                      width: 22,
                                      //scale: 1,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Wrap(
                                    direction: Axis.vertical,
                                    children: [
                                      Text(
                                        //Ajouter un emplacement
                                        //'Add Location',
                                        myCountry != null
                                            ? '$myCity, $myCountry'
                                            : '${AppLocalizations.of(context)!.location}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(79, 79, 79, 1),
                                            fontFamily: 'Gilroy',
                                            fontSize: 16,
                                            letterSpacing: 0,
                                            fontWeight: FontWeight.normal,
                                            height: 1),
                                      ),
                                      myShop == null
                                          ? Container()
                                          : Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .7,
                                              child: Text(myShop,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          79, 79, 79, 1),
                                                      fontFamily: 'Gilroy',
                                                      fontSize: 14,
                                                      letterSpacing: 0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      height: 1)),
                                            )
                                    ],
                                  ),
                                  Spacer(),
                                  Text(
                                    // '${_items['CHANGE']}',
                                    'Change',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color.fromRGBO(96, 32, 189, 1),
                                        fontFamily: 'Gilroy',
                                        fontSize: 12,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        //divider
                        // print(productTap)
                        const SizedBox(
                          height: 5,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Divider(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        // hide sort buttons
                        Padding(
                          padding: const EdgeInsets.only(top: 3, bottom: 6),
                          child: SizedBox(
                              height: 25,
                              width: MediaQuery.of(context).size.width * .94,
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      highlightColor: Colors.transparent,
                                      // splashFactory: NoSplash.splashFactory,
                                      onTap: () {
                                        _width == 0
                                            ? hideopen = true
                                            : hideopen = false;
                                        _width = _width == 0 ? 90 : 0;
                                        _width == 0
                                            ? close = true
                                            : close = false;
                                        setState(() {});
                                      },
                                      child: Container(
                                        // width: 90,
                                        // height: 25,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(_width == 0 ? "Open" : "Hide"),
                                            Icon(
                                              _width == 0
                                                  ? Icons.chevron_right
                                                  : Icons.chevron_left,
                                              color: Color(0xff6020BD),
                                            )
                                          ],
                                        ),
                                        decoration: BoxDecoration(
                                            color: const Color(0xffF9F6FC),
                                            borderRadius: BorderRadius.only(
                                                topRight:
                                                    const Radius.circular(8),
                                                bottomRight: _width == 0
                                                    ? const Radius.circular(8)
                                                    : const Radius.circular(
                                                        0))),
                                      ),
                                    ),
                                    showbestseller == false
                                        ? InkWell(
                                            child: Row(
                                              children: [
                                                Stack(children: <Widget>[
                                                  Icon(Icons.swap_vert,
                                                      color: Color.fromARGB(
                                                          193, 0, 0, 0)),
                                                  new Positioned(
                                                    // draw a red marble
                                                    top: 0.0,
                                                    right: 0.0,
                                                    child: Visibility(
                                                      visible: sortvisible,
                                                      child: new Icon(
                                                          Icons.brightness_1,
                                                          size: 8.0,
                                                          color:
                                                              Colors.redAccent),
                                                    ),
                                                  )
                                                ]),
                                                Text("SORT"),
                                              ],
                                            ),
                                            onLongPress: () {
                                              Fluttertoast.showToast(
                                                msg: 'wors',
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                backgroundColor: Colors.black45,
                                                fontSize: 18,
                                              );
                                            },
                                            onTap: () async {
                                              showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return Container(
                                                      height: 300,
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              // height: 500,
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 16,
                                                                      left: 24,
                                                                      bottom:
                                                                          24),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    'Sort By',
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    style: TextStyle(
                                                                        color: Color.fromRGBO(
                                                                            79,
                                                                            79,
                                                                            79,
                                                                            1),
                                                                        fontFamily:
                                                                            'Gilroy',
                                                                        fontSize:
                                                                            20,
                                                                        letterSpacing:
                                                                            0,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        height:
                                                                            1.5),
                                                                  ),
                                                                  Spacer(),
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      Navigator.pop(
                                                                          context);
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              16),
                                                                      child: Icon(
                                                                          Icons
                                                                              .close),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            ListView.builder(
                                                                physics:
                                                                    BouncingScrollPhysics(),
                                                                scrollDirection:
                                                                    Axis
                                                                        .vertical,
                                                                shrinkWrap:
                                                                    true,
                                                                itemCount: 4,
                                                                itemBuilder:
                                                                    (context,
                                                                        index) {
                                                                  return RadioListTile(
                                                                      title: Text(
                                                                          "${sort_options[index].label}"),
                                                                      value:
                                                                          index,
                                                                      activeColor:
                                                                          Color(
                                                                              0xffF93E6C),
                                                                      groupValue:
                                                                          val,
                                                                      onChanged:
                                                                          (value) {
                                                                        setState(
                                                                            () {
                                                                          val =
                                                                              index;
                                                                          print(
                                                                              'Index is $index and $val');
                                                                          onSorted();
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        });
                                                                      });
                                                                })
                                                          ]),
                                                    );
                                                  });
                                            })
                                        : Container()
                                  ])),
                        ),
                        Expanded(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _sideAppBar(),
                            Container(
                              height: MediaQuery.of(context).size.height -
                                  60 -
                                  35 -
                                  5 -
                                  25 -
                                  MediaQuery.of(context).padding.top,
                              width: MediaQuery.of(context).size.width - _width,
                              child: SingleChildScrollView(
                                controller: _controller,
                                child: InkWell(
                                  onTap: () {
                                    print(
                                        'here our banner${banner.toString()}');
                                  },
                                  child: Center(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: hideopen != true
                                              ? MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .94
                                              : MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  (_width + 8),
                                          child: CachedNetworkImage(
                                            imageUrl: banner.toString(),
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    Stack(
                                              children: [
                                                Container(
                                                  // width: MediaQuery.of(context)
                                                  //         .size
                                                  //         .width *
                                                  //     .96,
                                                  height: 120,
                                                  decoration: BoxDecoration(
                                                    // color: Colors.white,
                                                    image: DecorationImage(
                                                      image: imageProvider,
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                ),
                                                Positioned(
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20, left: 10),
                                                    child: Text(
                                                      '$shopName',
                                                      textAlign: TextAlign.left,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              51, 51, 51, 1),
                                                          fontFamily: 'Gilroy',
                                                          fontSize: 14,
                                                          letterSpacing: 0,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          height: 1.5),
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 40,
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20, left: 10),
                                                    child: Text(
                                                      '$shopAddress',
                                                      textAlign: TextAlign.left,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              51, 51, 51, 1),
                                                          fontFamily: 'Gilroy',
                                                          fontSize: 14,
                                                          letterSpacing: 0,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          height: 1.5),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),

                                            //     Container(
                                            //   height: 200,
                                            //   decoration: BoxDecoration(
                                            //     image: DecorationImage(
                                            //         image: imageProvider,
                                            //         fit: BoxFit.cover,
                                            //         colorFilter: ColorFilter.mode(
                                            //             Colors.red, BlendMode.colorBurn)),
                                            //   ),
                                            // ),
                                            // placeholder: (context, url) =>
                                            //     CircularProgressIndicator(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .96,
                                              // color: Colors.black12,
                                              child: Stack(
                                                children: [
                                                  Container(
                                                    width: hideopen != true
                                                        ? MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            .94
                                                        : MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            (_width + 7),
                                                    height: 120,
                                                    decoration: BoxDecoration(
                                                      // color: Colors.white,
                                                      image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/rectangle.png'),
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          top: 20, left: 10),
                                                      child: Text(
                                                        '$shopName',
                                                        textAlign:
                                                            TextAlign.left,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    51,
                                                                    51,
                                                                    51,
                                                                    1),
                                                            fontFamily:
                                                                'Gilroy',
                                                            fontSize: 14,
                                                            letterSpacing: 0,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            height: 1.5),
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top: 30,
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          top: 20, left: 10),
                                                      child: Text(
                                                        '$shopAddress',
                                                        textAlign:
                                                            TextAlign.left,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    51,
                                                                    51,
                                                                    51,
                                                                    1),
                                                            fontFamily:
                                                                'Gilroy',
                                                            fontSize: 14,
                                                            letterSpacing: 0,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            height: 1.5),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        load1 == true
                                            ? Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .96,
                                                // height: MediaQuery.of(context)
                                                //         .devicePixelRatio *
                                                //     MediaQuery.of(context)
                                                //         .size
                                                //         .height,
                                                child: searchbool
                                                    ? GridView.builder(
                                                        // scrollDirection: Axis.vertical,
                                                        physics:
                                                            ScrollPhysics(),
                                                        shrinkWrap: true,
                                                        itemCount:
                                                            statusList.length,
                                                        gridDelegate:
                                                            SliverGridDelegateWithFixedCrossAxisCount(
                                                          crossAxisCount:
                                                              _width == 0
                                                                  ? 3
                                                                  : 2,
                                                          crossAxisSpacing: 0,
                                                          mainAxisSpacing: 0,
                                                          childAspectRatio:
                                                              1.05 / 2,
                                                        ),
                                                        itemBuilder: (
                                                          context,
                                                          index,
                                                        ) {
                                                          return AnimationConfiguration
                                                              .staggeredList(
                                                            position: index,
                                                            duration:
                                                                const Duration(
                                                                    milliseconds:
                                                                        200),
                                                            child:
                                                                FadeInAnimation(
                                                              child: InkWell(
                                                                onDoubleTap:
                                                                    () {
                                                                  print(
                                                                      '${statusList[index].specialproductprices.toString()}');
                                                                },
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                          .all(4),
                                                                  child: Item(
                                                                      availability:
                                                                          '${statusList[index].productAvailability.toString()}',
                                                                      specialproductprices:
                                                                          '${statusList[index].specialproductprices}',
                                                                      productId:
                                                                          '${statusList[index].productId.toString()}',
                                                                      name:
                                                                          '${statusList[index].productName.toString()}',
                                                                      price:
                                                                          '${statusList[index].productPrices.toString()}',
                                                                      image:
                                                                          '${statusList[index].productImage.toString()}',
                                                                      shopid:
                                                                          '$shopId'),
                                                                ),
                                                              ),
                                                            ),
                                                          );
                                                        },
                                                      )
                                                    : Center(
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 200),
                                                          child: Column(
                                                            children: [
                                                              Icon(
                                                                Icons
                                                                    .search_off_outlined,
                                                                size: 70,
                                                                color: AppColors()
                                                                    .appaccent,
                                                              ),
                                                              SizedBox(
                                                                height: 20,
                                                              ),
                                                              Container(
                                                                  child: Text(
                                                                'Oops',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    color: Colors
                                                                        .black54),
                                                              )),
                                                              SizedBox(
                                                                height: 10,
                                                              ),
                                                              Container(
                                                                  child: Text(
                                                                "We can't find any item matching",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black45),
                                                              )),
                                                              Container(
                                                                  child: Text(
                                                                "your search",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black54),
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      ))
                                            : Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 250),
                                                  child: Container(
                                                      child:
                                                          CircularProgressIndicator(
                                                    color: Color(0xffDA3C5F),
                                                  )),
                                                ),
                                              )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                      ])
                : Center(
                    child: CircularProgressIndicator(
                      color: Color(0xffDA3C5F),
                    ),
                  )),
      ),
    );
  }

  @override
  void onTopRatedShopError(String error) {
    print('Error1');
  }

  @override
  void onTopRatedSuccess(TopProduct topModel) {
    print('Best seller---${topModel.message.toString()}');
    load = true;
    productTap = true;
    call = false;
    length = topModel.data!.count;
    statusList = [];
    statusList = topModel.data!.dataList!;
    print('Shop is : ${jsonEncode(topModel.data!.dataList)}');
    print(length);
    responseDone = true;
    setState(() {
      print('Succes1');

      len = topModel.data!.count!;
      print('$len lengh isss');
      // showbestseller = true;

      if (len != 0) {
        showbestseller = true;
        allpro = false;
        currentIndex = -1;
      }
      print('hitted $len   cat $cat');
      if (len != 0) {
        currentIndex = -1;
      }
      if (len == 0) {
        print('${shopId} shop idddddd');
        _presenters!.getUserproduct(
          '$shopId',
          '$cat',
          '$searchingText',
          '$sortBy',
          '$currentPage',
        );
      }

      if (topModel.message.toString() == 'Data not available') {
        // statusList = [];
        // searchbool = searchcontrol == false ? true : false;
        searchtext = ' ${topModel.message}';
        // var product = statusList = [];
      }
    });
  }

  @override
  void onProductError(String error) {
    print('Error2');
  }

  @override
  void onProductSuccess(TopProduct productModel) {
    load1 = true;
    length = productModel.data!.count;
    //transactionModel.data!.dataList!.length;
    // print('refresh completed490 $length');
    print('orailnoor search $searchingText');
    print('orailnoor search ${productModel.message}');

    if (productModel.message.toString() == 'Data not available') {
      // statusList = [];
      searchbool = searchcontrol == false ? true : false;
      searchtext = 'Search ${productModel.message}';
      // var product = statusList = [];
    }

    if (length != 0) {
      searchbool = true;
      if (isRefresh) {
        statusList = [];
        statusList = productModel.data!.dataList!;
        setState(() {});
        // print('refreshs completed51 ${jsonEncode(statusList)}');
      } else {
        // statusList = [];
        print('lennnnn $len');

        statusList.addAll(productModel.data!.dataList!.toList());

        setState(() {});

        // print('refresh completed6 ${jsonEncode(statusList)}');
      }

      // print('refresh length ${statusList.length}');

      currentPage++;
    }
    //  length = productModel.data!.count;
    //statusList = productModel.data!.dataList!;
    setState(() {
      // print('Succes2');
    });
  }

  @override
  void onHomeError(String error) {
    print('Error3');
  }

  @override
  void onHomeSuccess(HomeModel homeModel) {
    if (homeModel.data!.shopCategories!.count! >= 1) {
      print('categoryid=== ');
      // allproducts();
      // cat = 'homeModel.data!.shopCategories!.dataList![0].categoryId';
      cat = '';
    }
    selectedCategory = cat;

    print('worked when hit${jsonEncode(homeModel.data!)}');
    shopName = homeModel.data!.shopDetails!.shopName;
    banner = homeModel.data!.shopDetails!.shopBanner;
    shop_categories = homeModel.data!.shopCategories!.dataList!;
    // var shop_cat = homeModel.data!.shopCategories!.dataList!;
    // shop_categories.addAll(banner);
    print('HomeList is : $shop_categories');
    sort_options = homeModel.data!.sortOptions!;
    var lengthis = homeModel.data!.shopCategories!.count;
    shopAddress = homeModel.data!.shopDetails!.shopAddress;

    for (int i = 0; i < lengthis!; i++) {
      print('lengthis is==== $i');
      categoryLists
          .add(homeModel.data!.shopCategories!.dataList![i].categoryId!);
    }

    print('List is === $categoryLists');
    setState(() {
      print('Succes3');

      call2 = false;
      if (call == true) {
        print('shopId is== $shopId');
        _presenter!.topRated('$shopId');
        print('length is== $len');
      }
    });
  }

  statemanual() {
    setState(() {
      currentPage = 0;
      _presenters!.getUserproduct(
        '$shopId',
        '$cat',
        '$searchingText',
        '$sortBy',
        '$currentPage',
      );
    });
  }

  @override
  void onGetCartError(String error) {
    // TODO: implement onGetCartError
  }

  @override
  void onGetCartSuccess(GetCart getCartModel) {
    // TODO: implement onGetCartSuccess
    print('orailnoor --item cart screen on storepage$len');
    dataList2 = getCartModel.data!;
    lencart = dataList2.length;
    print('orailnoor --item cart screen $lencart');
    // for (var i = 0; i < lencart; i++) {}
    // ;
    setState(() {});
  }

  database() async {
    List<Map> cartdata = await DBHelper.getData('thecart');
    print("${cartdata.length}here it is");
    // print('${jsonEncode(cartdata)}here it is');

    setState(() {
      cartlength = cartdata.length;
      homevar = cartlength;
      if (cartlength != 0) {
        for (int i = 0; i < cartlength; i++) {
          print('cart lenght is :${cartlength}');
        }
        ;
      }
    });
  }

  showkeyboard(BuildContext context) {
    focusNode.requestFocus();
  }
}
