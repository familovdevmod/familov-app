import 'dart:convert';
import 'package:bot_toast/bot_toast.dart';
import 'package:familov/screen/mobileNextScreen.dart';
import 'package:familov/widget/app_colors.dart';
import 'package:familov/widget/buttonWidget.dart';
import 'package:familov/model/dingcountry.dart';
import 'package:familov/model/getUserTransaction.dart';
import 'package:familov/presenter/dingcountry_presenter.dart';
import 'package:familov/presenter/getUserTransaction_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fluttercontactpicker/fluttercontactpicker.dart';

class MobileScreenRecharge extends StatefulWidget {
  const MobileScreenRecharge({Key? key}) : super(key: key);

  @override
  _MobileScreenRechargeState createState() => _MobileScreenRechargeState();
}

class _MobileScreenRechargeState extends State<MobileScreenRecharge>
    implements DingCountryClass, GetUserTransactionContract {
  late File jsonFile;
  late Directory dir;
  String fileName = "myJSONFile.json";
  bool fileExists = false;
  var fileContent;
  DingCountryPresenter? presenter;
  UserTransactionPresenter? _presenter;
  final List<String> genderList = <String>[('Male'), ('Female')];
  var pickedcontact;
  String? dingCountryImage;

  int selectedIndex = -1;
  var codelist = [];
  DataList? selectedData;

  _MobileScreenRechargeState() {
    presenter = new DingCountryPresenter(this);
    _presenter = new UserTransactionPresenter(this);
  }
  final GlobalKey<FormState> _formKey = GlobalKey();

  Future<void> _saveForm() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState!.save();
  }

  List<String>? splitted;

  int? len;
  var d = false;
  var c, m;
  String? valueKey;
  var codes;
  var iconarrow = 'up';
  var lastrechargestatus;

  tokenCheck() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    valueKey = preferences.getString('value_key');
    if (valueKey != '' && valueKey != null) {
      print('tokenCheckIs :  ${valueKey.toString()}');
    }
    valueKey != null
        ? _presenter!.getUserTransaction('', '1', 'true')
        : Container(
            height: 0,
          );
  }

  String dingtext = 'Select Country';
  String? code;
  TextEditingController numb = new TextEditingController();
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    getApplicationDocumentsDirectory().then((Directory directory) async {
      dir = directory;
      jsonFile = new File(dir.path + "/" + fileName);
      fileExists = jsonFile.existsSync();

      if (fileExists) {
        setState(() {
          final data = json.decode(jsonFile.readAsStringSync());
          dynamic _items = data;
          var dddd = jsonDecode(_items);
          print('items data value ${dddd['LBL_MENU_01']}');
        });
      }
    });
    tokenCheck();

    if (valueKey == null) {
      load = true;
    }
    presenter!.dingCountry();

    super.initState();
  }

  List<DataList> dingList = [];
  List<DataList> completeDingList = [];
  List<Data_list>? statusList = [];

  var click = false;
  int getUserTranscationcount = 0;
  var counts = 0;
  var load = false;

  searchFunctionality(value) {
    dingList = [];
    print('dingsizeeeee ${dingList.length}');
    print('searchCalled ${completeDingList.length}');
    setState(() {
      dingList = [];
    });
    for (int i = 0; i < completeDingList.length; i++) {
      // print(completeDingList[i].countryName.toString().contains(value));
      if (completeDingList[i]
          .countryName
          .toString()
          .toLowerCase()
          .contains(value)) {
        dingList.add(completeDingList[i]);
        print(
            'completeDingList[i].countryName ${completeDingList[i].countryName}');
      }
    }
    print('dingListdingList ${dingList.length}');
    // setState(() {
    //
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        // padding: EdgeInsets.symmetric(horizontal: 20),
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              toolbarHeight: 80,
              automaticallyImplyLeading: false,
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text(
                '${AppLocalizations.of(context)!.mobilerecharge}',
                style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xffF93E6C),
                ),
              ),
            ),
            body: load == true
                ? Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      physics: iconarrow != 'up'
                          ? BouncingScrollPhysics()
                          : NeverScrollableScrollPhysics(),
                      child: Center(
                        child: Container(
                            width: MediaQuery.of(context).size.width * .94,
                            child: Column(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        top:
                                            20), //${AppLocalizations.of(context)!.followus}
                                    child: Text(
                                      //Envoyez instantanément un mobile sécurisé recharges dans le monde entier
                                      '${AppLocalizations.of(context)!.instantly}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color.fromRGBO(96, 32, 189, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 18,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.w600,
                                          height: 1.5),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 30),
                                    child: Text(
                                      '${AppLocalizations.of(context)!.topup} !',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(130, 130, 130, 1),
                                          fontFamily: 'Gilroy',
                                          fontSize: 16,
                                          letterSpacing: 0,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5),
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: Text('Select Country'),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Color(0xFF7F7F7F),
                                          width: 0.5,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0))),
                                    child: InkWell(
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10, right: 10),
                                          child: TextField(
                                            enabled: false,
                                            decoration: new InputDecoration(
                                                suffixIcon: Icon(
                                                  iconarrow != 'up'
                                                      ? Icons.arrow_drop_up
                                                      : Icons.arrow_drop_down,
                                                ),
                                                border: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                disabledBorder:
                                                    InputBorder.none,
                                                // contentPadding: EdgeInsets.only(
                                                //     left: 15,
                                                //     bottom: 11,
                                                //     top: 11,
                                                //     right: 15),
                                                hintText: code != null
                                                    ? " $dingtext ( +$code )"
                                                    : " $dingtext ",
                                                hintStyle: TextStyle(
                                                  color: Colors.black.withOpacity(
                                                      0.6), // <-- Change this
                                                  // fontSize: null,
                                                  // fontWeight: FontWeight.w400,
                                                  // fontStyle: FontStyle.normal,
                                                ),
                                                prefixIcon: dingCountryImage ==
                                                        null
                                                    ? null
                                                    : Container(
                                                        margin: EdgeInsets.only(
                                                            right: 10),
                                                        width: 20,
                                                        height: 20,
                                                        decoration:
                                                            BoxDecoration(
                                                          image: DecorationImage(
                                                              image: NetworkImage(
                                                                  'https://imagerepo.ding.com/flag/$dingCountryImage.png?height=46&mask=circle&compress=none'),
                                                              fit: BoxFit
                                                                  .fitWidth),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .elliptical(
                                                                          20,
                                                                          20)),
                                                        ))),
                                          ),
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          if (click == false) {
                                            iconarrow = 'down';
                                            click = true;
                                          } else {
                                            iconarrow = 'up';
                                            click = false;
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  click == true
                                      ? Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(8),
                                              topRight: Radius.circular(8),
                                              bottomLeft: Radius.circular(8),
                                              bottomRight: Radius.circular(8),
                                            ),
                                            border: Border.all(
                                              color: Colors.black26,
                                              width: 1,
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(15.0),
                                                child: Container(
                                                  height: 40,
                                                  decoration: BoxDecoration(
                                                    // color: Colors.amber,
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(8),
                                                      topRight:
                                                          Radius.circular(8),
                                                      bottomLeft:
                                                          Radius.circular(8),
                                                      bottomRight:
                                                          Radius.circular(8),
                                                    ),
                                                    border: Border.all(
                                                      color: Colors.black26,
                                                      width: 1,
                                                    ),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 10),
                                                    child: TextField(
                                                      onChanged: (value) {
                                                        searchFunctionality(
                                                            value);
                                                      },
                                                      controller:
                                                          searchController,
                                                      decoration:
                                                          new InputDecoration(
                                                        border:
                                                            InputBorder.none,
                                                        // focusedBorder: InputBorder.none,
                                                        enabledBorder:
                                                            InputBorder.none,
                                                        errorBorder:
                                                            InputBorder.none,
                                                        disabledBorder:
                                                            InputBorder.none,
                                                        // focusedBorder: OutlineInputBorder(
                                                        //     borderRadius:
                                                        //         BorderRadius
                                                        //             .all(Radius
                                                        //                 .circular(
                                                        //                     8)),
                                                        //     borderSide:
                                                        //         BorderSide(
                                                        //             color: Colors
                                                        //                 .cyan)),

                                                        hintText: 'Search',
                                                        hintStyle: TextStyle(
                                                          color: Colors
                                                              .black54, // <-- Change this
                                                          fontSize: null,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                  height: 250,
                                                  // decoration: BoxDecoration(
                                                  //   borderRadius: BorderRadius.only(
                                                  //     topLeft: Radius.circular(8),
                                                  //     topRight: Radius.circular(8),
                                                  //     bottomLeft: Radius.circular(8),
                                                  //     bottomRight: Radius.circular(8),
                                                  //   ),
                                                  //   border: Border.all(
                                                  //     color: Colors.black26,
                                                  //     width: 1,
                                                  //   ),
                                                  // ),
                                                  child: ListView.builder(
                                                      scrollDirection:
                                                          Axis.vertical,
                                                      shrinkWrap: true,
                                                      itemCount:
                                                          dingList.length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return InkWell(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 20,
                                                                      left: 20),
                                                              child: Row(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Container(
                                                                        margin: EdgeInsets.only(
                                                                            right:
                                                                                10),
                                                                        width:
                                                                            26,
                                                                        height:
                                                                            26,
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          image: DecorationImage(
                                                                              image: NetworkImage('https://imagerepo.ding.com/flag/${dingList[index].countryIso}.png?height=46&mask=circle&compress=none'),
                                                                              fit: BoxFit.fitWidth),
                                                                          borderRadius: BorderRadius.all(Radius.elliptical(
                                                                              24,
                                                                              24)),
                                                                        )),
                                                                    Container(
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 5),
                                                                      child:
                                                                          SizedBox(
                                                                        width: MediaQuery.of(context).size.width /
                                                                            1.4,
                                                                        child:
                                                                            Text(
                                                                          '${dingList[index].countryName} (+${dingList[index].prefix})',
                                                                          textAlign:
                                                                              TextAlign.left,
                                                                          style: TextStyle(
                                                                              color: Color.fromRGBO(79, 79, 79, 1),
                                                                              fontFamily: 'Gilroy',
                                                                              fontSize: 16,
                                                                              letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                                                              fontWeight: FontWeight.normal,
                                                                              height: 1),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ])),
                                                          onTap: () {
                                                            setState(() {
                                                              dingtext = dingList[
                                                                      index]
                                                                  .countryName!;
                                                              dingCountryImage =
                                                                  dingList[
                                                                          index]
                                                                      .countryIso;
                                                              code = dingList[
                                                                      index]
                                                                  .prefix!;
                                                              selectedIndex =
                                                                  index;
                                                              selectedData =
                                                                  dingList[
                                                                      index];
                                                              print(
                                                                  'dingtext : $dingtext');
                                                              dingList = [];
                                                              dingList.addAll(
                                                                  completeDingList);
                                                              searchController
                                                                  .text = '';
                                                              click = false;
                                                            });
                                                          },
                                                        );
                                                      })),
                                            ],
                                          ),
                                        )
                                      : Container(
                                          height: 0,
                                        ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: Text('Mobile Number'),
                                  ),
                                  Container(
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   margin: d == false
                                        //       ? EdgeInsets.only(
                                        //           top: 10,
                                        //         )
                                        //       : EdgeInsets.only(top: 10, bottom: 20),
                                        //   height: 50,
                                        //   width: 60,
                                        //   decoration: BoxDecoration(
                                        //     borderRadius: BorderRadius.only(
                                        //       topLeft: Radius.circular(8),
                                        //       topRight: Radius.circular(8),
                                        //       bottomLeft: Radius.circular(8),
                                        //       bottomRight: Radius.circular(8),
                                        //     ),
                                        //     border: Border.all(
                                        //       color: Colors.black26,
                                        //       width: 1,
                                        //     ),
                                        //   ),
                                        //   child: Center(
                                        //       child: code != null
                                        //           ? Text(' + $code')
                                        //           : Text(' + 91')),
                                        // ),
                                        // SizedBox(
                                        //   width: 10,
                                        // ),
                                        Container(
                                          // height: 50,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .94,
                                          margin: EdgeInsets.only(
                                            top: 10,
                                          ),
                                          child: TextFormField(
                                              decoration: InputDecoration(
                                                suffixIcon: InkWell(
                                                  onTap: () {
                                                    contactpicker();
                                                  },
                                                  child: Image.asset(
                                                    'assets/images/contact.png',
                                                    scale: 4,
                                                    alignment:
                                                        Alignment.centerLeft,
                                                  ),
                                                ),
                                                enabledBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xFF7F7F7F),
                                                    width: 0.5,
                                                  ),
                                                ),
                                                hintText:
                                                    //  pickedcontact == null
                                                    // ?
                                                    'Mobile Number', // : pickedcontact,
                                                border:
                                                    const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Color(
                                                                0xffDA3C5F)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .all(Radius
                                                                    .circular(
                                                                        8))),
                                                contentPadding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 16,
                                                ),
                                                // focusedBorder: OutlineInputBorder(
                                                //     borderRadius: BorderRadius.all(
                                                //         Radius.circular(8)),
                                                //     borderSide: BorderSide(
                                                //         color: Color(0xffDA3C5F)))
                                              ),
                                              controller: numb,
                                              keyboardType:
                                                  TextInputType.number,
                                              inputFormatters: [
                                                FilteringTextInputFormatter
                                                    .allow(RegExp(r'[0-9]')),
                                              ],
                                              onChanged: (value) {
                                                _saveForm();
                                              },
                                              validator: (value) {
                                                print(
                                                    '${value}--- here it is ');
                                                print(
                                                    'dinggggg ${dingList.length}');
                                                print(
                                                    'dinggggg $selectedIndex');
                                                print(
                                                    'dinggggg ${selectedData!.countryName}');
                                                print(
                                                    'dinggggg ${selectedData!.minimumLength}');
                                                print(
                                                    'dinggggg ${selectedData!.maximumLength}');
                                                // if (value!.length != 10) {
                                                if (value!.length <
                                                        int.parse(selectedData!
                                                            .minimumLength
                                                            .toString()) ||
                                                    value.length >
                                                        int.parse(selectedData!
                                                            .maximumLength
                                                            .toString())) {
                                                  d = true;
                                                  return selectedData!
                                                              .minimumLength
                                                              .toString() ==
                                                          selectedData!
                                                              .maximumLength
                                                              .toString()
                                                      ? 'Mobile Number must be of ${selectedData!.minimumLength.toString()} digit'
                                                      : 'Mobile Number must be ${selectedData!.minimumLength.toString()} - ${selectedData!.maximumLength.toString()} digit';
                                                  // return 'Mobile Number must be of ${dingList[selectedIndex].minimumLength.toString()} digit';
                                                }
                                                if (value.isEmpty) {
                                                  d = true;
                                                  return 'Please enter mobile number';
                                                }
                                                d = false;
                                                return null;
                                              },
                                              onSaved: null),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 60,
                                  ),
                                  buttonWidget('Next', context, () {
                                    if (dingtext == 'Select Country') {
                                      BotToast.showSimpleNotification(
                                          backgroundColor:
                                              AppColors().toastsuccess,
                                          title: 'Please Select Country');
                                      // Fluttertoast.showToast(
                                      //   msg: 'Please Select Country',
                                      //   toastLength: Toast.LENGTH_SHORT,
                                      //   gravity: ToastGravity.BOTTOM,
                                      //   backgroundColor: Colors.black45,
                                      //   fontSize: 18,
                                      // );
                                    }
                                    _saveForm();
                                    if (selectedIndex > -1) {
                                      if (dingtext != 'Select Country' &&
                                          numb.text != '') {
                                        if (numb.text.length >=
                                                int.parse(selectedData!
                                                    .minimumLength
                                                    .toString()) &&
                                            numb.text.length <=
                                                int.parse(selectedData!
                                                    .maximumLength
                                                    .toString())) {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      MobileNextScreen(
                                                        mobile: '${numb.text}',
                                                        code: '$code',
                                                      )));
                                        } else {
                                          BotToast.showSimpleNotification(
                                              backgroundColor:
                                                  AppColors().toastsuccess,
                                              title:
                                                  'Enter a valid mobile number');
                                        }
                                      } else {
                                        BotToast.showSimpleNotification(
                                            backgroundColor:
                                                AppColors().toastsuccess,
                                            title:
                                                'Please enter mobile number');
                                        // Fluttertoast.showToast(
                                        //   msg: 'Write valid ndsdumber',
                                        //   toastLength: Toast.LENGTH_SHORT,
                                        //   gravity: ToastGravity.BOTTOM,
                                        //   backgroundColor: Colors.black45,
                                        //   fontSize: 18,
                                        // );
                                      }
                                    }
                                    // else {
                                    //   Fluttertoast.showToast(
                                    //     msg: 'Select country',
                                    //     toastLength: Toast.LENGTH_SHORT,
                                    //     gravity: ToastGravity.BOTTOM,
                                    //     backgroundColor: Colors.black45,
                                    //     fontSize: 18,
                                    //   );
                                    // }
                                    setState(() {});
                                  }),
                                  SizedBox(
                                    height: 60,
                                  ),
                                  (valueKey != null && len == 0)
                                      ? Container(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            'Last Recharge',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    51, 51, 51, 1),
                                                fontFamily: 'Gilroy',
                                                fontSize: 18,
                                                letterSpacing: 0,
                                                fontWeight: FontWeight.w600,
                                                height: 1),
                                          ),
                                        )
                                      : Container(
                                          height: 0,
                                        ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                      child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          shrinkWrap: true,
                                          itemCount: getUserTranscationcount,
                                          itemBuilder: (context, index) {
                                            return Column(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 15),
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        alignment:
                                                            Alignment.center,
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height /
                                                            17,
                                                        child: Image.asset(
                                                          'assets/images/airtel.png',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 15,
                                                      ),
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            '${statusList![index].receiverPhone}',
                                                            textAlign:
                                                                TextAlign.start,
                                                            style: TextStyle(
                                                                color:
                                                                    Color.fromRGBO(
                                                                        79,
                                                                        79,
                                                                        79,
                                                                        1),
                                                                fontFamily:
                                                                    'Gilroy',
                                                                fontSize: 16,
                                                                letterSpacing:
                                                                    0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                height: 1.5),
                                                          ),
                                                          Text(
                                                            'Recharge of EUR 0.80 done on 04 Aug,21 ',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                                color: Color
                                                                    .fromRGBO(
                                                                        0,
                                                                        219,
                                                                        167,
                                                                        1),
                                                                fontFamily:
                                                                    'Gilroy',
                                                                fontSize: 10,
                                                                letterSpacing:
                                                                    0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                height: 1.5),
                                                          )
                                                        ],
                                                      ),
                                                      Spacer(),
                                                      InkWell(
                                                        child: Text(
                                                          'Repeat',
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                              color:
                                                                  Color.fromRGBO(
                                                                      79,
                                                                      79,
                                                                      79,
                                                                      1),
                                                              fontFamily:
                                                                  'Gilroy',
                                                              fontSize: 14,
                                                              letterSpacing: 0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                              height: 1),
                                                        ),
                                                        onTap: () {
                                                          print(
                                                              '${numb.text}  $code');
                                                          splitted =
                                                              statusList![index]
                                                                  .receiverPhone!
                                                                  .split(' ');
                                                          print(
                                                              '${splitted![1]}  ${splitted![0]}');
                                                          c = int.parse(
                                                              '${splitted![0]}');
                                                          m = int.parse(
                                                              '${splitted![1]}');
                                                          if (splitted![1] !=
                                                                  null &&
                                                              splitted![1] !=
                                                                  null) {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            MobileNextScreen(
                                                                              mobile: '${splitted![1]}',
                                                                              code: '${splitted![0]}',
                                                                            )));
                                                          }
                                                          setState(() {});
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Divider(
                                                    color: Color.fromRGBO(
                                                        189, 189, 189, 1),
                                                    thickness: 0.5)
                                              ],
                                            );
                                          })),
                                  SizedBox(
                                    height: 60,
                                  ),
                                  SizedBox(
                                    height: 30,
                                  )
                                ])),
                      ),
                    ),
                  )
                : Container(
                    child: Center(
                        child: CircularProgressIndicator(
                      color: Color(0xffDA3C5F),
                    )),
                  )));
  }

  @override
  void onDingCountryError(String error) {}

  @override
  void onDingCountrySuccess(Dingcountry dingModel) {
    counts = dingModel.data!.count!;
    dingList = dingModel.data!.dataList!;
    completeDingList = dingModel.data!.dataList!;

    print('dingListSize ${dingList.length}');
    print('completeDingListSize ${completeDingList.length}');
    // print('refresh ding ${jsonEncode(dingList)}');
  }

  @override
  void onTransactionError(String error) {}

  @override
  void onTransactionSuccess(GetuserTransaction transactionModel) {
    if (transactionModel.status == true) {
      statusList = transactionModel.data!.dataList;
      print('Shop data is  : ${jsonEncode(transactionModel.data!.dataList)}');
      getUserTranscationcount = transactionModel.data!.count!;
      len = statusList!.length;
      print('$len----here it is');
      print('length of getT is : $getUserTranscationcount');
    }
    setState(() {
      //   print('len is : $len');
      load = true;
    });
  }

  void contactpicker() async {
    final PhoneContact contact = await FlutterContactPicker.pickPhoneContact();

    var contactNumber = contact.phoneNumber!.number;

    print(contact.phoneNumber!.number.toString());
    pickedcontact = contactNumber.toString().replaceAll("+91", "");

    numb.text = pickedcontact;
    print(contactNumber);
    print('here it is');
    setState(() {});
    // numb = contact.phoneNumber!.number as TextEditingController;
  }
}
