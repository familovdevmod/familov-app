import 'package:familov/screen/Transaction%20tabs/allScreen.dart';
import 'package:familov/screen/Transaction%20tabs/cancelled.dart';
import 'package:familov/screen/Transaction%20tabs/complete.dart';
import 'package:familov/screen/Transaction%20tabs/underProcess.dart';
import 'package:flutter/material.dart';

class TransactionScreen extends StatefulWidget {
  const TransactionScreen({Key? key}) : super(key: key);

  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  bool flag = true;
  @override
  void initState() {
    _tabController = TabController(length: 4, initialIndex: 0, vsync: this)
      ..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  final List<Tab> topTabs = <Tab>[
    Tab(text: 'All'),
    Tab(text: 'Under Process'),
    Tab(text: 'Completed'),
    Tab(text: 'Cancelled'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        toolbarHeight: 60,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'My Transactions',
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Color(0xffF93E6C),
          ),
        ),
        bottom: TabBar(
          controller: _tabController,
          indicator: UnderlineTabIndicator(
              borderSide: BorderSide(width: 1.0, color: Color(0xff6020BD)),
              insets: EdgeInsets.symmetric(horizontal: 16.0)),
          indicatorSize: TabBarIndicatorSize.label,
          indicatorColor: Colors.white,
          labelPadding: EdgeInsets.all(1),
          tabs: topTabs,
          labelStyle: null,
          labelColor: Color(0xff6020BD),
          unselectedLabelColor: Color(0xff828282),
        ),
      ),
      body: Scaffold(
        body: Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
          child: TabBarView(
            controller: _tabController,
            children: [
              // Pag(),
              AllScreen(
                data: '',
              ),
              UnderProcess(
                data: '&app_order_status=pending',
              ),
              Complete(
                data: '&app_order_status=completed',
              ),
              Cancelled(
                data: '&app_order_status=cancelled',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
